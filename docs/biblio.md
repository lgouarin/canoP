# A list of AMR Codes

* [Dona Calhoun's website](http://math.boisestate.edu/~calhoun/www_personal/research/amr_software/index.html)

# Cell-based AMR

## Articles

- Fully threaded tree algorithms for adaptive mesh fluid dynamics simulations, KhoKhlov, 1998
http://numgeom.ams.sunysb.edu/trac/attachment/wiki/ufdm/Khokhlov%20-%201998%20-%20Fully%20threaded%20tree%20algorithm%20.pdf

- Cosmological Hydrodynamics with Adaptive Mesh Refinement, R. Teyssier, 2002
http://arxiv.org/pdf/astro-ph/0111367.pdf
http://adsabs.harvard.edu/abs/2002A%26A...385..337T

## p4est et ForestClaw

### p4est

http://burstedde.ins.uni-bonn.de/research/papers/BursteddeWilcoxGhattas11.pdf
http://www.speedup.ch/workshops/w42_2013/carsten.pdf
http://numerics.kaust.edu.sa/hpc3/presentations/burstedde_hpc3.pdf
http://burstedde.ins.uni-bonn.de/research/papers/IsaacBursteddeGhattas12.pdf
http://burstedde.ins.uni-bonn.de/research/papers/BursteddeStadlerAlisicEtAl13.pdf

### ForestClaw:
http://math.boisestate.edu/~calhoun/ForestClaw/index.html
http://arxiv.org/pdf/1308.1472.pdf

## AMR et table de hachage
---------------------------

Articles de Eugene Yee et al (surtout le 1er)
http://www.sciencedirect.com/science/article/pii/S0021999110004705
http://www.sciencedirect.com/science/article/pii/S0045782508003113
http://www.sciencedirect.com/science/article/pii/S0045793010000150

AMR 2D en OpenCL a Los Alamos
https://github.com/losalamos/CLAMR
https://github.com/losalamos/ExascaleDocs/blob/master/cell_based_amr_on_gpu.pdf

# Livres
-----------

* Adaptive Mesh Refinement - Theory and Applications, Tomasz Plewa, Timur
Linde and V. Gregory Weirs Editors, Springer, Lecture Notes in
Computational Science and Engineering, 2004.
* Space-Filling Curves: An Introduction With Applications in
Scientific Computing, Michael Bader, Springer, 2013.

# Autres liens divers / slides
--------------------------------

https://hpc-forge.cineca.it/files/ScuolaCalcoloParallelo_WebDAV/public/anno-2013/Summer-School/AMR-BOLOGNA.pdf

http://mars.serc.iisc.ernet.in/downloads/runtime-irregular-gpus/theses/hari-thesis.pdf

http://extremecomputingtraining.anl.gov/files/2013/07/fastmath-amr-atpetsc-08-2013.pdf
http://extremecomputingtraining.anl.gov/files/2013/07/fastmath-pumi-atpesc-08-20131.pdf
http://hipacc.ucsc.edu/Lecture%20Slides/2011ISSAC/AlmgrenHIPACC_July2011.pdf

* mpi-amrvac:
https://gitorious.org/amrvac
http://homes.esat.kuleuven.be/~keppens/

* dccrg:
https://gitorious.org/dccrg
http://arxiv.org/abs/1212.3496

* CHOMBO:
https://seesar.lbl.gov/anag/example/

* Une these de master:
http://scholarworks.gsu.edu/cgi/viewcontent.cgi?article=1059&context=cs_theses
