\chapter{Simulation of a Two-Phase Model using \texttt{p4est}}\label{chapter:twophase}

In this chapter we will consider a hyperbolic system of equations that describes
a simple two-phase model. The model has been studied in~\cite{CVV}.

\section{Model Description}

We suppose given two compressible materials $k = \{0, 1\}$ that are equipped with an
equation of state (EOS) in the form of  a \textbf{barotropic pressure law}:
\[
\rho_k \mapsto p_k(\rho_k),
\]
where $\rho_k$ and $p_k$ are the density and the pressure, respectively,
of component $k$. We make the assumption that:
\[
\od[\rho_k]{p_k} >0
\]
and denote by $c_k$ the sound velocity of the fluid $k$ that verifies:
\[
c_k^2 = \od[\rho_k]{p_k}.
\]
The \emph{global density} of the medium is defined by:
\[
\rho = \alpha_1 \rho_1 + \alpha_2 \rho_2,
\]
where $\alpha_k$ is the \emph{volume fraction} of fluid $k$. We supposed both fluids to be
\textbf{immiscible}, which implies that:
\[
\alpha_1 + \alpha_2 = 1.
\]
%
\begin{notation}
We will denote $\alpha = \alpha_1$ and, by definition, $1 - \alpha = \alpha_2$.
\end{notation}

The pressure $p$ of the medium, as well as the volume fraction $\alpha$,
are defined thanks to the hypothesis that for a given value of $\alpha \rho_1$
and $(1 - \alpha)\rho_2$ the following closure relation stands:
\[
p_1(\rho_1) = p_2(\rho_2).
\]
Consequently, $p$ can be expressed as a function of $\rho_1, \rho_2$ and
$\alpha$. The sound velocity $c$ of the two-phase medium is defined by:
\[
c^2 = \frac{\alpha \rho_1}{\rho} \left(\dpd[(\alpha \rho_1)]{p}\right)_{(1 - \alpha)\rho_2} +
      \frac{(1 - \alpha) \rho_2}{\rho} \left(\dpd[((1 - \alpha) \rho_2)]{p}\right)_{\alpha \rho_1}.
\]
The pressure equilibrium hypothesis yields then that:
\[
c^2 = \frac{1}{\rho} \left(\frac{\alpha}{\rho_1 c_1^2} + \frac{1 - \alpha}{\rho_2 c_2^2}\right),
\]
which is usually referred to as the \emph{Wallis formula}.

From here on we will suppose that each component of our system is a
\emph{stiffened gas} material. This implies that the pressure laws are:
\begin{equation}\label{eq:eosstiffened}
p_k(\rho_k) = p_{k, 0} + c_k^2 (\rho_k - \rho_{k, 0}),
\end{equation}
where $p_{k, 0}, \rho_{k, 0}$ and $c_k$ are positive constants that are characteristic of the fluid $k$.
For such materials, we can see that $\alpha$ is given as the root of a
second order polynomial. Indeed, if we denote $m_1 = \alpha \rho_1$ and
$m_2 = (1 - \alpha) \rho_2$, we have:
\[
p_1(\rho_1) = p_2(\rho_2) \equivto
p_1\left(\frac{m_1}{\alpha}\right) = p_2\left(\frac{m_2}{1 - \alpha}\right)
\]
which can be expanded using~\eqref{eq:eosstiffened}, to:
\[
p_{1, 0} + c_1^2 \left(\frac{m_1}{\alpha} - \rho_{1, 0}\right) =
p_{2, 0} + c_2^2 \left(\frac{m_2}{1 - \alpha} - \rho_{2, 0}\right),
\]
giving the following order 2 polynomial in $\alpha$:
\[
q_1 \alpha^2 - (q_1 + q_2) \alpha + q_3 = 0.
\]
Out of the two roots of the polynomial, the one that respects the constraint
$\alpha \in [0, 1]$ is:
\begin{equation}\label{eq:eosalpha}
\alpha = \frac{(q_1 + q_2) - \sqrt{(q_1 + q_2)^2 - 4 q_1 q_3}}{2 q_1},
\end{equation}
where:
\[
\begin{cases}
q_1 = p_{20} - p_{10} - (c_2^2 \rho_{20} - c_1^2 \rho_{10}), \\
q_2 = c_1^2 m_1 + c_2^2 m_2, \\
q_3 = c_1^2 m_1.
\end{cases}
\]
The kinematics of the medium is determined by a velocity field $\vect{u}$ that
is shared by both materials.

The evolution of the two-phase system is governed by the following equations:
\begin{equation}\label{eq:twophase}
\begin{cases}
\tpd[t]{} (\alpha \rho_1) + \nabla \cdot (\alpha \rho_1 \vect{u}) = 0, \\
\tpd[t]{} ((1 - \alpha) \rho_2) + \nabla \cdot ((1 - \alpha) \rho_2 \vect{u}) = 0, \\
\tpd[t]{} (\rho \vect{u}) + \nabla \cdot (\vect{u} \otimes (\rho \vect{u})) + \grad{p} = 0.
\end{cases}
\end{equation}
The system~\eqref{eq:twophase} can also be written in vector form:
\begin{equation}\label{eq:twophasevector}
\tpd[t]{}\vect{W} + \tpd[x]{}\mathcal{F}(\vect{W}) +
                    \tpd[y]{}\mathcal{G}(\vect{W}) +
                    \tpd[z]{}\mathcal{H}(\vect{W}) = 0,
\end{equation}
where $\vect{W}$ is the state vector containing the conservative variables
$(\alpha \rho_1, (1 - \alpha) \rho_2, \rho u, \rho v, \rho w)$ and the fluxes in
each direction are defined by:
\[
\mathcal{F}(\vect{W}) =
\begin{pmatrix}
\alpha \rho_1 u \\
(1 - \alpha) \rho_2 u \\
\rho u^2 + p \\
\rho uv \\
\rho uw
\end{pmatrix},
\mathcal{G}(\vect{W}) =
\begin{pmatrix}
\alpha \rho_1 v \\
(1 - \alpha) \rho_2 v \\
\rho vu \\
\rho v^2 + p \\
\rho vw
\end{pmatrix} \text{ and }
\mathcal{H}(\vect{W}) =
\begin{pmatrix}
\alpha \rho_1 w \\
(1 - \alpha) \rho_2 w \\
\rho wu \\
\rho wv \\
\rho w^2 + p
\end{pmatrix}.
\]

\section{Numerical Scheme}
%
In order to approximate the solutions of \eqref{eq:twophasevector}, we present a Finite Volume
scheme based on a dimensional splitting strategy.

Following the outline presented in Chapter~\ref{chapter:transport}, the
splitting strategy consists in advancing~\eqref{eq:twophasevector}
in time, with a time step of $\Delta t$, by successively approximating the
solutions of~\eqref{eq:twophasesplitx},~\eqref{eq:twophasesplity} and
\eqref{eq:twophasesplitz}.
\begin{numcases}{}
& $\pd[t]{} \vect{W} + \pd[x]{} \mathcal{F}(\vect{W}) = 0$, \label{eq:twophasesplitx}\\
& $\pd[t]{} \vect{W} + \pd[y]{} \mathcal{G}(\vect{W}) = \mathcal{S}(\vect{W})$, \label{eq:twophasesplity}\\
& $\pd[t]{} \vect{W} + \pd[z]{} \mathcal{H}(\vect{W}) = 0$. \label{eq:twophasesplitz}
\end{numcases}

In~\eqref{eq:twophasesplity}, we have added an additional source term that,
in our case will correspond to the \textbf{gravity} in the $y$ direction:
\[
\mathcal{S}(\vect{W}) =
\begin{pmatrix}
 0 \\
 0 \\
 0 \\
 -\rho\,g \\
 0
\end{pmatrix}.
\]
Let us consider a one-dimensional problem set along the $x$ axis and we
integrate \eqref{eq:twophasesplitx} over $[t_n, t_{n + 1}] \times K_i$. This
yields the integral balance relation:
\[
\int_{t_n}^{t_{n + 1}} \int_{K_i} \tpd[t]{} \vect{W} \dx[\vect{x}] \dx[t] +
\int_{t_n}^{t_{n + 1}} \int_{K_i} \tpd{} \mathcal{F}(\vect{W}) \dx[\vect{x}] \dx[t] = 0
.
\]
Once again, the Finite Volume approximation is obtained by proposing a discrete approximate formula
of this integral balance law. We set:
\begin{equation}\label{eq:twophaseupdate}
\vect{W}_i^{n + 1} = \vect{W}_i^n -
\frac{\Delta t}{|K_i|} \sum_{j \in \mathcal{N}_x(i)} |\Gamma_{ij}| n_{ij} \mathcal{F}_{ij}^n,
\end{equation}
where $\vect{W}_i^{n}$ is a approximation of $\vect{W}$ within the cell $K_i$ at instant $t_n$
and $\mathcal{F}_{ij}^n$ is an approximation of the flux $\mathcal{F}$ along the face $\Gamma_{ij}$
at instant $t_n$.

The full scheme for advancing~\eqref{eq:twophasevector} with a source term is:
\begin{numcases}{}
& $\vect{W}_i^{*} = \vect{W}_i^n - {\displaystyle
   \frac{\Delta t}{|K_i|} \sum_{j \in \mathcal{N}_x(i)} |\Gamma_{ij}| n_{ij} \mathcal{F}_{ij}^n}$, \\\label{eq:updatex}
& $\vect{W}_i^{**} = \vect{W}_i^* - {\displaystyle
   \frac{\Delta t}{|K_i|} \sum_{j \in \mathcal{N}_y(i)} |\Gamma_{ij}| n_{ij} \mathcal{G}_{ij}^n}$, \\[1ex]\label{eq:updatey}
& $\vect{W}_i^{***} = \vect{W}_i^{**} + \Delta t \mathcal{S}_i^n$, \\[1.5ex]\label{eq:updatesource}
& $\vect{W}_i^{n + 1} = \vect{W}_i^{***} - {\displaystyle
   \frac{\Delta t}{|K_i|} \sum_{j \in \mathcal{N}_z(i)} |\Gamma_{ij}| n_{ij} \mathcal{H}_{ij}^n}$. \label{eq:updatez}
\end{numcases}

We note that each equation uses the solution from the previous one as an initial
condition. As in the case of the transport equation, this leads to a first
order scheme since both the directional splitting method and the operator
splitting methods we have used are of first order.

The flux $\mathcal{F}_{ij}^n$ between two cells $K_i$ and $K_j$ is given a
function $\Phi(\vect{W}_L, \vect{W}_R)$, where $\vect{W}_L$ and $\vect{W}_R$
are the values on the left and right, respectively, of the interface between
$K_i$ and $K_j$. In our case, the definition of $\Phi$ is given by an
approximate Riemann solver obtained by a Suliciu-type relaxation scheme
(see~\cite{CVV} and~\cite{SULICIU}).

Let $a$ be a positive constant whose choice will be specified later, the
Suliciu relaxation approximate Riemann solver we have used gives $3$ waves,
as seen in Figure~\ref{fig:discontinuities}.

\begin{figure}[!ht]
\begin{center}
\input{figures/suliciu_riemann.tikz}
\end{center}
\caption{The three waves of the approximate Riemann solver.}
\label{fig:discontinuities}
\end{figure}

The states delimited by the contact discontinuities in Figure~\ref{fig:discontinuities}
are given by:
{\small
\begin{equation}\label{eq:riemann}
\vect{W}^{RP}(\frac{x}{t}; \vect{W_L}, \vect{W_R})
\!=\!\!
\left\{
\begin{array}{LL}
\vect{W}_L \!=\!
    \left((\alpha_1 \rho_1), (\alpha_2 \rho_2), (\rho u), (\rho v), (\rho w)\right)_L,
    & \text{if } \frac{x}{t} \leq u \!-\! \frac{a}{\rho_L}, \\[2ex]
\vect{W}_L^* \!=\!
    \left((\alpha_1 \rho_1), (\alpha_2 \rho_2), (\rho u), (\rho v), (\rho w)\right)_L^*,
    & \text{if } u \!-\! \frac{a}{\rho_L} < \frac{x}{t} \leq u^*, \\[2ex]
\vect{W}_R^* \!=\!
    \left((\alpha_1 \rho_1), (\alpha_2 \rho_2), (\rho u), (\rho v), (\rho w)\right)_R^*,
    & \text{if } u^* < \frac{x}{t} \leq u \!+\! \frac{a}{\rho_R}, \\[2ex]
\vect{W}_R \!=\!
    \left((\alpha_1 \rho_1), (\alpha_2 \rho_2), (\rho u), (\rho v), (\rho w)\right)_R,
    & \text{if } \frac{x}{t} \geq u + \frac{a}{\rho_R},
\end{array}
\right.
\end{equation}
}
where the densities of each of the two materials in the
intermediate states $\vect{W}_L^*$ and $\vect{W}_R^*$ are defined as:
\[
\begin{array}{RCL}
(\alpha \rho_1)^*_L & = & \left(\frac{1}{(\alpha \rho_1)_L} + \frac{u^* - u_L}{a}\right)^{-1}, \\
((1 - \alpha) \rho_2)^*_L & = & \left(\frac{1}{((1 - \alpha) \rho_2)_L} + \frac{u^* - u_L}{a}\right)^{-1}, \\
(\alpha \rho_1)^*_R & = & \left(\frac{1}{(\alpha \rho_1)_R} + \frac{u_R - u^*}{a}\right)^{-1}, \\
((1 - \alpha) \rho_2)^*_R & = & \left(\frac{1}{((1 - \alpha) \rho_2)_R} + \frac{u_R - u^*}{a}\right)^{-1}
\end{array}
\]
with the velocity in the two intermediate states:
\[
u^*_L = u_R^* = u^* = \frac{1}{2} \left(u_R + u_L - \frac{p_R - p_L}{a}\right).
\]
The total mass density in the medium in the intermediate states is:
\[
\begin{cases}
\rho_L^* = (\alpha \rho_1)^*_L + ((1 - \alpha) \rho_2)^*_L, \\
\rho_R^* = (\alpha \rho_1)^*_R + ((1 - \alpha) \rho_2)^*_R, \\
\end{cases}
\]
which permits to compute the momentum in the normal direction:
\[
\begin{cases}
(\rho u)_L^* = \rho_L^* u^*, \\
(\rho u)_R^* = \rho_R^* u^*, \\
\end{cases}
\]
The momentum in the two tangential directions is simply:
\[
\begin{cases}
(\rho v)_L^* = \rho_L^* v_L, \\
(\rho v)_R^* = \rho_R^* v_R, \\
\end{cases}\text{ and }\quad
\begin{cases}
(\rho w)_L^* = \rho_L^* w_L, \\
(\rho w)_R^* = \rho_R^* w_R, \\
\end{cases}
\]
because we are considering only the sweep in the $x$ direction which simply
transports the velocities in the tangential $y$ and $z$ directions.

Using the approximate solution to the Riemann problem given by~\eqref{eq:riemann},
we can define our flux as:
\begin{equation}\label{eq:twophaseflux}
\mathcal{F}_{ij}^n = \Phi(\mathbf{W}_L, \mathbf{W}_R)
= \mathcal{F}(\mathbf{W}^{RP}(x/t = 0; \mathbf{W_L}, \mathbf{W_R})).
\end{equation}

Finally, we recall that in order to avoid numerical instabilities, the
parameter $a$ should be chosen in agreement with the
\emph{Whithman's subcharacteristic condition}:
\[
\rho\, c(\alpha \rho_1, (1 - \alpha)\rho_2) < a.
\]
In practice we define $a$ by setting:
\[
a = K \max_i(\rho_i c_i),
\]
where $K > 1$ is a constant.

\subsubsection{Stability and Time Step Choice}

% TODO: maybe add some more details here

The stability of the scheme is assured by the following CFL condition:
\begin{equation}\label{eq:twophasecfl}
\Delta t \leq \frac{1}{2} \min_i \left[\Delta x_i \left(\norm{\vect{u}_i^n} + \frac{a}{\rho_{i}^n}\right)^{-1}\right].
\end{equation}
A simple way to understand the CFL condition is by looking at
Figure~\ref{fig:twophasecfl} and Figure~\ref{fig:discontinuities}. The main
argument for stability is that the waves at the left and right of the cell
$K_i$ (considering a one dimensional problem) do not intersect during the
time step from $t_n$ to $t_{n + 1}$.

\begin{figure}[!ht]
\begin{center}
\input{figures/suliciu_cfl.tikz}
\end{center}
\caption{Waves at the left and right interface of a cell $K_i$.}
\label{fig:twophasecfl}
\end{figure}

This sufficient stability condition provides a strategy for choosing the time step:
\[
\Delta t = \theta \frac{\Delta x}{\displaystyle\max_i
           \left(\norm{\vect{u}_i^n} + \frac{a}{\rho_{i}^n}\right)},
\quad \theta \in [0, 0.5]
\]
where $\Delta x$ is the size of the octant at the finest level $l_{max}$,
given by:
\[
\Delta x = 2^{-l_{max}}.
\]

\subsubsection{Body Forces Term}
In the numerical tests we shall consider we will need to account for gravity
terms $\mathcal{S}$. We shall achieve thanks to a simple two-step operator
splitting as follows:
%
\[
\begin{cases}
\tpd[t]{} \vect{W} + \tpd[x]{} \mathcal{G}(\vect{W}) = 0, \\
\tpd[t]{} \vect{W} = \mathcal{S}(\vect{W}).
\end{cases}
\]
The second step does not involve any partial differential and thus is a simple
ODE that will be solved by a cell-centered explicit approximation.


\subsubsection{Algorithm}

We have seen in the previous paragraphs how to advance~\eqref{eq:twophasevector}
in time and we will now present a method for advancing the whole system, which
includes computing the volume fraction $\alpha$ (using~\eqref{eq:eosalpha}) and
the pressure $p$ (using~\eqref{eq:eosstiffened}).
%
% \begin{figure}[!ht]
% \begin{center}
% \input{figures/suliciu_scheme.tikz}
% \end{center}
% \caption{Steps for advancing the solution in time.}
% \label{fig:algorithm}
% \end{figure}

The algorithm advances as follows:
\begin{enumerate}
    \item[1.1.] Advance $\vect{W}^n_i$ in time by successively applying
    \eqref{eq:updatex},~\eqref{eq:updatey} and~\eqref{eq:updatez} under the
    stability condition~\eqref{eq:twophasecfl} to solve:
    \[
    \begin{cases}
    \pd[t]{} \vect{W} + \pd[x]{} \mathcal{F}(\vect{W}) = 0,\\
    \pd[t]{} \vect{W} + \pd[y]{} \mathcal{G}(\vect{W}) = 0,\\
    \pd[t]{} \vect{W} + \pd[z]{} \mathcal{H}(\vect{W}) = 0.
    \end{cases}
    \]
    \item[1.2.] Use~\eqref{eq:updatesource} if necessary to introduce the source
    term.
    \item[2.1.] Compute the volume fraction $\alpha^{n + 1}_i$ according
    to~\eqref{eq:eosalpha} using the updated values $(\alpha \rho_1)_{i}^{n + 1}$
    and $((1 - \alpha) \rho_2)_{i}^{n + 1}$.
    \item[2.2.] Compute the new pressure $p_i^{n + 1}$ according to~\eqref{eq:eosstiffened}
    and the updated values $\alpha_i^{n + 1}$, $(\alpha \rho_1)_{i}^{n + 1}$
    and $((1 - \alpha) \rho_2)_{i}^{n + 1}$.
\end{enumerate}

\section{Numerical Results}

In this section we will try to present several results for the two-phase model
using the approximate Riemann solver resulted from a
Suliciu-type relaxation scheme. As in the previous chapter, all the tests
have been done in parallel using the \texttt{p4est} library and our own code
that is available at~\cite{CODE}.

For the two-phase model we require several parameters to be set. These are
available in Table~\ref{table:twophase}. We will note in each particular
example when the source term using gravity is used and when it is not.
{%
\begin{table}[!ht]
\begin{center}
\begin{tabular}{|l|l|}\hline
% use packages: color,colortbl
\textbf{Parameter} & \textbf{Value}\\\hline
$\rho_{1,0}$    & $1$       \si{\kilogram\per\cubic\metre}\\\hline
$\rho_{2,0}$    & $1000$    \si{\kilogram\per\cubic\metre}\\\hline
$p_{1,0}$       & $10^5$    \si{\pascal}\\\hline
$p_{2,0}$       & $10^5$    \si{\pascal}\\\hline
$c_{1,0}$       & $340$     \si{\metre\per\second}\\\hline
$c_{2,0}$       & $1500$    \si{\metre\per\second}\\\hline
$g$             & $9.81$    \si{\metre\per\square\second}\\\hline
\end{tabular}
\end{center}
\caption{Table of parameters for the two-phase model.}
\label{table:twophase}
\end{table}
}%

\subsubsection{Refinement Criteria}

For the two-phase model we have chosen a more advanced refining criterion
to take into account more of the variables in the system. In particular,
we looked at two different choices:
\begin{itemize}
    \item Computing the gradient of the pressure and the total mass density
    of the medium.
    \item Or computing the gradient of the velocity in each direction and
    the total mass density of the medium.
\end{itemize}

The gradient estimator for the pressure is:
\[
\xi_i^p = \max_{j \in \mathcal{N}_x(i)} \frac{|p_i^n - p_j^n|}{\max(\abs{p_i^n}, \abs{p_j^n})}
\]
and the gradient estimator for the total mass density is:
\[
\xi_i^r = \max_{j \in \mathcal{N}_x(i)} \frac{|\rho_i^n - \rho_j^n|}{\max(\abs{\rho_i^n}, \abs{\rho_j^n})}
\]
where:
\[
\rho_i^n = (\alpha \rho_1)^n_i + ((1 - \alpha) \rho_2)^n_{i}.
\]
For the velocity vector, we have used the following indicator for a single
component:
\[
\xi_i^u = \max_{j \in \mathcal{N}_x(i)} \frac{|u_i^n - u_j^n|}{\max(\abs{u_i^n}, \abs{u_j^n})}
\]
and then approximated an indicator by taking the maximum of all the components
of the velocity vector $\xi^{\vect{u}}_i = \max(\xi_i^u, \xi_i^v, \xi_i^w)$.

An octant is then refined if $\max{(\xi_i^p, \xi_i^r)} > \epsilon$, where
$\epsilon$ is a user defined threshold. In our case, we have noticed that
the best results were obtained using $\epsilon = 0.005$. Similarly, an octant
gets coarsened if $\max{(\xi_i^p, \xi_i^r)} < \epsilon$.

\subsection{SOD: Shock and Rarefaction Wave}

The first test we will be looking at is the classical \emph{SOD shock tube}.
The test was first introduced by G. A. Sod in~\cite{SOD78} as a test for
the accuracy of several finite difference methods and has since been used
for testing many fluid dynamics schemes.

The modified test that we have used is defined on a domain $\Omega = [0, 2] \times [0, 1]$.
We have then defined two states, on the left and right of $x = 1$, as follows:
\[
\rho_1(0, \vect{x}) =
\begin{cases}
100, & x < 1, \\
1, & x > 1,
\end{cases} \quad \text{ and }\quad
\alpha(0, \vect{x}) =
\begin{cases}
1 - \lambda, & x < 1, \\
\lambda, & x < 1,
\end{cases}
\]
where $\lambda = 10^{-7}$. The velocity on the domain is uniformly null
$\vect{u}(0, \vect{x}) = (0, 0, 0)$. The other variables are computed starting from
$\rho_1$ and $\alpha$.
\begin{itemize}
    \item $p$ is computed from the barotropic pressure law~\eqref{eq:eosstiffened}.
    \item $\rho_2$ is then computed from the constraint of pressure
    equilibrium and the barotropic pressure law~\eqref{eq:eosstiffened}.
    \item The conservative variables that form the vector $\vect{W}$ can
    now be computed using these values.
\end{itemize}

We do not explicitly set the mass densities in both materials, because we
cannot be sure that the given values will automatically satisfy the pressure
equilibrium.

A first test has been done with the first order scheme with a final time
$T = 0.001$ and a Courant number $\theta = 0.5$. We can see in Figure~\ref{fig:sodorder1}
the initial state and an intermediary state for the pressure and the velocity
in the $x$ direction. Since this is a 1D problem, we only need to
look in one direction, namely $x$, because the solution is constant in all the
others.

\begin{figure}[!ht]
\centering
\begin{subfigure}{.5\textwidth}
  \centering
  \includegraphics[width=0.7\linewidth]{img/two-phase/sod/sod_pressure_order2_initial}
  \captionof{figure}{$p$ at $t = 0$.}
  \label{fig:sodorder1_p0}
\end{subfigure}%
\begin{subfigure}{.5\textwidth}
  \centering
  \includegraphics[width=0.7\linewidth]{img/two-phase/sod/sod_velocity_order2_initial}
  \captionof{figure}{$u$ at $t = 0$.}
  \label{fig:sodorder1_p1}
\end{subfigure}

\begin{subfigure}{.5\textwidth}
  \centering
  \includegraphics[width=0.7\linewidth]{img/two-phase/sod/sod_pressure_order1}
  \captionof{figure}{$p$ at $t = 0.0006$.}
  \label{fig:sodorder1_ux0}
\end{subfigure}%
\begin{subfigure}{.5\textwidth}
  \centering
  \includegraphics[width=0.7\linewidth]{img/two-phase/sod/sod_velocity_order1}
  \captionof{figure}{$u$ at $t = 0.0006$.}
  \label{fig:sodorder1_ux1}
\end{subfigure}%
\caption{Simulation results from the first order scheme.}
\label{fig:sodorder1}
\end{figure}

We can see in Figure~\ref{fig:sodorder1_ux1} that the profile of the velocity
matches that in~\cite{CVV} and, furthermore, the shock on top and the rarefaction
wave at the bottom have developed as one would expect.

The second test we have performed is using a second order space discretization
and the \texttt{minmod} limiter to limit oscillations. The simulation has a
final time $T = 0.001$ and a Courant number $\theta = 0.25$. Again, we will
only look at the result in the $x$ direction.

\begin{figure}[!ht]
\centering
\begin{subfigure}{.5\textwidth}
  \centering
  \includegraphics[width=0.8\linewidth]{img/two-phase/sod/sod_pressure_order2}
  \captionof{figure}{$p$ at $t = 0.0006$.}
  \label{fig:sodorder2_p0}
\end{subfigure}%
\begin{subfigure}{.5\textwidth}
  \centering
  \includegraphics[width=0.7\linewidth]{img/two-phase/sod/sod_velocity_order2}
  \captionof{figure}{$u$ at $t = 0.0006$.}
  \label{fig:sodorder2_p1}
\end{subfigure}
\caption{Simulation results from the second order scheme.}
\label{fig:sodorder2}
\end{figure}

We can see in Figure~\ref{fig:sodorder2} that the profile is the same, but
the solution has improved. The shock on the right side is better represented
with a steeper slope than in the more diffusive first order simulation. As for
the refinement, we can see in Figure~\ref{fig:sodrefined0} that the mesh was
only refined near $x = 0$ where there is a discontinuity in the mass density.
Later, we can see that the refinement follows the shock and the
rarefaction wave produced by the velocity in the $x$ direction very closely
(see Figure~\ref{fig:sodrefined1}).

\begin{figure}[!ht]
\centering
\begin{subfigure}{.5\textwidth}
  \centering
  \includegraphics[width=0.9\linewidth]{img/two-phase/sod/sod_refinement_initial}
  \captionof{figure}{Refinement at $t = 0$.}
  \label{fig:sodrefined0}
\end{subfigure}%
\begin{subfigure}{.5\textwidth}
  \centering
  \includegraphics[width=0.9\linewidth]{img/two-phase/sod/sod_refinement}
  \captionof{figure}{Refinement at $t = 0.0006$.}
  \label{fig:sodrefined1}
\end{subfigure}
\caption{Mesh refinement during the simulation of the second order scheme.}
\label{fig:sodrefinement}
\end{figure}

\subsection{Two Rarefaction Waves}

The second test we will be looking at is also a classical test in fluid
dynamics. It involves creating two rarefaction waves in the middle of our
domain that can be compared to exact solutions.

The initial condition for this test is described by $\rho_1(0, \vect{x}) = 1.0$
and $\alpha(0, \vect{x}) = 0.1$, for all $\vect{x} \in \Omega$.
The velocity field has opposite signs on each half of the domain:
\[
\vect{u}(0, \vect{x}) =
\begin{cases}
-2, & x < 1, \\
2, & x > 1.
\end{cases}
\]
As in the previous case, these variables will allow us to compute the
mass density in the second fluid, as well as the pressure of the medium
and all the conservative variables at time $t = 0$.

The solution was simulated on the same domain $\Omega = [0, 2] \times [0, 1]$
with a final time $T = 0.05$ and a Courant number $\theta = 0.5$ for the
first order simulation and $\theta = 0.25$ for the second order simulation.
The second order scheme used the \texttt{minmod} limiter as before.

\begin{figure}[!ht]
\centering
\begin{subfigure}{.5\textwidth}
  \centering
  \includegraphics[width=0.7\linewidth]{img/two-phase/sod2/sod2_pressure_order1}
  \captionof{figure}{$p$ at $t = 0.05$.}
  \label{fig:sod2order1_p}
\end{subfigure}%
\begin{subfigure}{.5\textwidth}
  \centering
  \includegraphics[width=0.7\linewidth]{img/two-phase/sod2/sod2_rho_order1}
  \captionof{figure}{$\rho$ at $t = 0.05$.}
  \label{fig:sod2order1_rho}
\end{subfigure}

\begin{subfigure}{.5\textwidth}
  \centering
  \includegraphics[width=0.7\linewidth]{img/two-phase/sod2/sod2_pressure_order2}
  \captionof{figure}{$p$ at $t = 0.05$.}
  \label{fig:sod2order2_p}
\end{subfigure}%
\begin{subfigure}{.5\textwidth}
  \centering
  \includegraphics[width=0.7\linewidth]{img/two-phase/sod2/sod2_rho_order2}
  \captionof{figure}{$\rho$ at $t = 0.05$.}
  \label{fig:sod2order2_rho}
\end{subfigure}%
\caption{Simulations with a (a)(b) First order scheme (c)(d) Second order scheme.}
\label{fig:sod2}
\end{figure}

We can see in Figure~\ref{fig:sod2order1_p} and Figure~\ref{fig:sod2order1_rho}
the results for the simulation at final time $T = 0.05$ for the pressure and
the total mass density. The two rarefaction waves are symmetric around $x = 1$
and they have evolved correctly for both the first order and the second order
simulations.

In the case of the second order simulations, we can see at the top of each
rarefaction wave (e.g. Figure~\ref{fig:sod2order2_rho}) there is a change in
slope. We suspect that this is happening because the scheme is second order
in space and not in time. The simple second order space discretization and the
use of limiters seems to have a stiffening effect on the result.

In Figure~\ref{fig:sod2refinement}, we can also see that, in the case of
the simulation done with the second order scheme, the mesh is correctly
refined in the areas where the rarefaction waves are developing, thus correctly
capturing the phenomenon.

\begin{figure}[!ht]
\centering
\includegraphics[width=0.7\linewidth]{img/two-phase/sod2/sod2_refinement}
\caption{Refinement at the end of the simulation, at $t = 0.05$.}
\label{fig:sod2refinement}
\end{figure}

\subsection{Dam Break}

The third test we will be looking at is a sightly more complicated fluid
dynamics example, namely a dam break. A dam break simulation has \textbf{gravity}
as a driving force because the initial condition is at rest.

The dam break test case is defined by the following mass density for the
gas:
\[
\rho_1(0, \vect{x}) = \rho_{1, 0} \left(1 + g \frac{0.5 - y}{c_1^2}\right)
\]
and the volume fraction given by:
\[
\alpha(0, \vect{x}) =
\begin{cases}
1, & (x, y, z) \in [0, 0.3] \times [0, 0.5] \times [0, 0.3], \\
0, & \text{otherwise}.
\end{cases}
\]
The initial velocity is $\vect{u} = (0, 0, 0)$. This test has only been performed
using the first order scheme with a final time $T = 2$ and a Courant number
$\theta = 1.0$. The result of the simulation can be seen in Figure~\ref{fig:dambreak}.

This simulation was performed in parallel on $256$ CPUs with a minimum allowed
refinement level of $7$ and a maximum level of $11$. It is the most massive of
the tests we have performed in this paper, taking slightly over 3 hours.

\begin{figure}[!ht]
\centering
\begin{subfigure}{.5\textwidth}
  \centering
  \includegraphics[width=0.7\linewidth]{img/two-phase/dambreak/dambreak_00}
  \captionof{figure}{$\rho$ at $t = 0.0$.}
\end{subfigure}%
\begin{subfigure}{.5\textwidth}
  \centering
  \includegraphics[width=0.7\linewidth]{img/two-phase/dambreak/dambreak_09}
  \captionof{figure}{$\rho$ at $t = 0.9$.}
\end{subfigure}

\begin{subfigure}{.5\textwidth}
  \centering
  \includegraphics[width=0.7\linewidth]{img/two-phase/dambreak/dambreak_15}
  \captionof{figure}{$\rho$ at $t = 1.5$.}
\end{subfigure}%
\begin{subfigure}{.5\textwidth}
  \centering
  \includegraphics[width=0.7\linewidth]{img/two-phase/dambreak/dambreak_20}
  \captionof{figure}{$\rho$ at $t = 2.0$.}
\end{subfigure}%
\caption{First order simulation of the dam break at different times. Plotting
the total mass density of the medium.}
\label{fig:dambreak}
\end{figure}

We can see in the images from Figure~\ref{fig:dambreak} that the simulation
is very diffusive due to the first order approximation. This dissipation also
leads to \emph{numerical viscosity} that makes the fluids act in non-physical
ways. Nonetheless, the simulation has evolved in predictable ways and can be
easily improved by using the second order scheme.

Figure~\ref{fig:dambreak_refinement} contains examples of the mesh refinement
during the simulation. We can see that it correctly follows the interfaces between
the two fluids even as the liquid collapses on itself.

\begin{figure}[!ht]
\centering
\begin{subfigure}{.5\textwidth}
  \centering
  \includegraphics[width=0.7\linewidth]{img/two-phase/dambreak/dambreak_refined_middle}
  \captionof{figure}{Refinement at $t = 1.5$.}
\end{subfigure}%
\begin{subfigure}{.5\textwidth}
  \centering
  \includegraphics[width=0.7\linewidth]{img/two-phase/dambreak/dambreak_refined_end}
  \captionof{figure}{Refinement at $t = 2.0$.}
\end{subfigure}%
\caption{The refinement in two stages of the simulation.}
\label{fig:dambreak_refinement}
\end{figure}
