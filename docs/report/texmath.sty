\ProvidesPackage{texmath}

% tests if the document is compiled with xetex
\usepackage{ifxetex}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                       Includes
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% ftp://ftp.ams.org/pub/tex/doc/amsmath/amsldoc.pdf
\usepackage{amsmath}
% http://milde.users.sourceforge.net/LUCR/Math/mathpackages/amssymb-symbols.pdf
\usepackage{amssymb}
% http://texdoc.net/texmf-dist/doc/fonts/stmaryrd/stmaryrd.pdf
\usepackage{stmaryrd}
% http://distrib-coffee.ipsl.jussieu.fr/pub/mirrors/ctan/macros/latex/required/tools/array.pdf
\usepackage{array}
% http://ctan.math.utah.edu/ctan/tex-archive/macros/latex/contrib/l3packages/xfrac.pdf
\usepackage{xfrac}
% ftp://sunsite.icm.edu.pl/pub/CTAN/macros/latex/contrib/l3packages/xparse.pdf
\usepackage{xparse}
\usepackage{ifthen}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                  Package Options
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% more space in between array rows
% \renewcommand\arraystretch{1.5}

% define some array column types that use displaystyle
\newcolumntype{R}{>{\displaystyle}r}
\newcolumntype{C}{>{\displaystyle}c}
\newcolumntype{L}{>{\displaystyle}l}

% swap phi and varphi
\let\oldphi\phi
\let\phi\varphi
\let\varphi\oldphi

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                       Commands
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% operators
\DeclareMathOperator{\supp}{Supp}
\DeclareMathOperator{\mes}{mes}
\DeclareMathOperator{\sdim}{dim}
\DeclareMathOperator{\sign}{sign}
\DeclareMathOperator{\argmin}{\mathrm{arg}\,\mathrm{min}}
\DeclareMathOperator{\argmax}{\mathrm{arg}\,\mathrm{max}}

% norms
\NewDocumentCommand \abs { m } { \left\lvert\, #1\, \right\rvert }
\NewDocumentCommand \norm { m } { \left\lVert #1 \right\rVert }
\NewDocumentCommand \psc { m } { \left( #1 \right) }
\NewDocumentCommand \ceil { m } { \left\lceil #1 \right\rceil}
\NewDocumentCommand \floor { m } { \left\lfloor #1 \right\rfloor}

%%%%%%%%%%%%%%%%%%%% Derivatives %%%%%%%%%%%%%%%%%%%%%%%%%%
% the differential operator
\NewDocumentCommand \dx { O{x} } {\,\mathrm{d} #1}

% ordinary derivatives
\NewDocumentCommand \odsign { m }
{
\ifthenelse{\equal{#1}{1}}%
    {'}%
    {%
    \ifthenelse{\equal{#1}{2}}%
        {''}%
        {(#1)}%
    }%
}

% ordinary derivative in text display style
% usage:
%   \tod[t]{y}      to get      y'(t)
%   \tod[t][n]{y}   to get      y^{(n)}(t)
\NewDocumentCommand \tod { O{x} o m }
{
\IfNoValueTF {#2}
    { #3'(#1) }
    { {#3}^{\odsign{#2}}(#1) }
}

% ordinary derivative in display style
% usage:
%   \tod[t]{y}      to get      dy / dx
%   \tod[t][n]{y}   to get      d^n y / d x^n
\NewDocumentCommand \dod { O{x} o m }
{
\IfNoValueTF {#2}
    { \dfrac{\mathrm{d} {#3}}{\mathrm{d} {#1}} }
    { \dfrac{\mathrm{d}^{#2} {#3}}{\mathrm{d} {#1}^{#2}} }
}

% general ordinary derivative
% usage: like dod and tod
\NewDocumentCommand \od { O{x} o m } {%
\ifinner%
    {\tod[#1][#2]{#3}}
\else%
    {\dod[#1][#2]{#3}}
\fi}

% partial derivatives
\NewDocumentCommand \tpd { s O{x} O{} m }
{
\IfBooleanTF #1
    { \partial^{#2} #4 }
    { \partial_{#2}^{#3} #4 }
}

\NewDocumentCommand \dpd { O{x} O{} m } { \dfrac{\partial^{#2} {#3}}{\partial {#1}^{#2}} }

\NewDocumentCommand \pd { O{x} O{} m }
{
\ifinner
    \tpd[#1][#2]{#3}
\else
    \dpd[#1][#2]{#3}
\fi
}

\NewDocumentCommand \grad { m } { \nabla #1 }
\NewDocumentCommand \curl { m } { \nabla \times \mathbf{#1} }

%%%%%%%%%%%%%%%%%%%%%% Probabilities %%%%%%%%%%%%%%%%%%%%%%%
\NewDocumentCommand \E { m } { \mathbb{E}\left[ #1 \right] }
\NewDocumentCommand \V { m } { \mathbb{V}\left[ #1 \right] }
\NewDocumentCommand \Pb { m } { \mathbb{P}\left( #1 \right) }

%%%%%%%%%%%%%%%%%%%%%%     Misc      %%%%%%%%%%%%%%%%%%%%%%%
\NewDocumentCommand \equivto {} {\Longleftrightarrow}
\NewDocumentCommand \bord {} {\partial}
\NewDocumentCommand \set { m m } { \left\{\, {#1} \mid {#2}\,\right\} }
\NewDocumentCommand \ind { m } { {\ \mbox{l\hspace{-0.55em}1}}_{#1} }
\NewDocumentCommand \vect { m } { \mathbf{#1} }
\NewDocumentCommand \field { m } { \mathbb{#1} }
\NewDocumentCommand \cls { s O{k} O{\Omega} }
{
\IfBooleanTF #1
    { \mathcal{C}^{#2} }
    { \mathcal{C}^{#2}(#3) }
}

\NewDocumentCommand \clsc { s O{k} O{\Omega} }
{
\IfBooleanTF #1
    { \mathcal{C}^{#2}_0 }
    { \mathcal{C}^{#2}_0(#3) }
}

\NewDocumentCommand \rstr { m m }
{{
\left.\kern-\nulldelimiterspace #1 \vphantom{\big|} \right|_{#2}
}}

\NewDocumentCommand \fullfct { m m m m m }
{
\begin{array}{CCRCL}
{#1} & \colon & {#2} & \longrightarrow & {#3} \\
     &        & {#4} & \longmapsto     & {#5}
\end{array}
}

% redefining some things
\let\divsymb=\div
\RenewDocumentCommand \div { m } { \mathrm{div}\left(#1\right) }

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                   Specialized Commands
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% common norms
\NewDocumentCommand \normL { s O{2} D<>{\Omega} m }
{
\IfBooleanTF #1
    { \norm{#4}_{L^{#2}} }
    { \norm{#4}_{L^{#2}(#3)} }
}

\NewDocumentCommand \norml { O{2} m } { \norm{#2}_{#1} }

% common fields
\NewDocumentCommand \N  {}       { \field{N} }
\NewDocumentCommand \Nd { O{d} } { \field{N}^{#1} }
\NewDocumentCommand \Z  {}       { \field{Z} }
\NewDocumentCommand \Q  {}       { \field{Q} }
\NewDocumentCommand \R  {}       { \field{R} }
\NewDocumentCommand \Rd { O{d} } { \field{R}^{#1} }

\ifxetex
    \RenewDocumentCommand \C  {}       { \field{C} }
\else
    \NewDocumentCommand \C  {}       { \field{C} }
\fi
