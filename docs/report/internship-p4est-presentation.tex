\documentclass[xcolor=x11names,compress]{beamer}

%% General document %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\usepackage[T1]{fontenc}
\usepackage[utf8]{inputenc}
\usepackage[french]{babel}
\usepackage{texmath}
\usepackage{graphicx}
\usepackage{verbatimbox}

\usepackage{tikz}
\usetikzlibrary{patterns}   % for hatching
\usetikzlibrary{positioning}
\usetikzlibrary{decorations.pathreplacing}
\usetikzlibrary{decorations.pathmorphing}

%% Beamer Layout %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\useoutertheme[subsection=false,shadow]{miniframes}
\useinnertheme{default}
\usefonttheme{serif}
\usepackage{palatino}

\setbeamerfont{title like}{shape=\scshape}
\setbeamerfont{frametitle}{shape=\scshape}

\setbeamercolor*{lower separation line head}{bg=DeepSkyBlue4}
\setbeamercolor*{normal text}{fg=black,bg=white}
\setbeamercolor*{alerted text}{fg=red}
\setbeamercolor*{example text}{fg=black}
\setbeamercolor*{structure}{fg=black}

\setbeamercolor*{palette tertiary}{fg=black,bg=black!10}
\setbeamercolor*{palette quaternary}{fg=black,bg=black!10}
\setbeamertemplate{navigation symbols}{}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\renewcommand{\emph}[1]{\textcolor{DeepSkyBlue4}{#1}}
\setbeamertemplate{caption}{\insertcaption}

\AtBeginSection{
\begin{frame}
\vfill
\centering
Partie~\insertsectionnumber

\usebeamerfont{section title}\insertsection
\vfill
\end{frame}
}
\AtBeginSubsection{
\begin{frame}
\vfill
\centering
Partie~\insertsectionnumber

\usebeamerfont{section title}\insertsection: \insertsubsection
\vfill
\end{frame}
}
\setbeamertemplate{footline}[frame number]
\let\oldfootnotesize\footnotesize
\renewcommand*{\footnotesize}{\oldfootnotesize\tiny}

\begin{document}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}
\title{\LARGE \textbf{Raffinement de maillage adaptatif avec p4est}\vspace{20pt}}
% \subtitle{\Large Algorithmes de volumes finis avec \texttt{p4est}}
\author{
    \vspace{-30pt}\\
    \begin{tabular}{rl}
    \emph{Étudiant}: & Alexandru \textsc{Fikl} \\[2ex]
    \emph{Encadrants}: & Pierre \textsc{Kestener} \\
    & Samuel \textsc{Kokh}
    \end{tabular}
}
\date{
    \includegraphics[scale=0.15]{img/logo_cea} \hfill
    \includegraphics[scale=0.2]{img/logo_galilee} \hfill
    \includegraphics[scale=0.15]{img/logo_mdls}
}
\titlepage
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\begin{frame}{Introduction}
\vfill
\emph{Maison de la Simulation}
\begin{itemize}\setlength{\itemsep}{10pt}
    \item Laboratoire commun entre CEA, CNRS, INRIA et les Universités
    Paris-Sud et Versailles-Saint-Quentin.
    \item \emph{HPC}: recherche, support, formation.
\end{itemize}
\vfill
\emph{Stage}
\begin{itemize}\setlength{\itemsep}{10pt}
    \item \emph{Évaluation} et \emph{documentation} de la bibliothèque
    \texttt{p4est}.
    \item Étude des méthodes AMR (\emph{Adaptive Mesh Refinement}).
    \item Implémentation d'un nouveau code pour \emph{tester} \texttt{p4est}
    (EDP hyperboliques, Volumes Finis).
\end{itemize}
\vfill
\end{frame}

\section{AMR}
\begin{frame}{Motivation}
\begin{picture}(320,250)
\put(-30,60){\includegraphics[width=1.2\linewidth]{img/galaxy.png}}
\put(100,50){\begin{minipage}[t]{0.9\linewidth}
{\small F. Bournaud (CEA/CCRT) sur Titane avec RAMSES.}
\end{minipage}}
\end{picture}
\end{frame}

\begin{frame}{Méthodes adaptatives}
\begin{itemize}
    \item Raffiner une partie du maillage par la superposition d'un
    maillage plus fin (\emph{block-based AMR}).
    \begin{center}
\begin{tikzpicture}[scale=0.5]
\draw (0, 0) grid (10, 5);
\draw [step=0.5] (3, 2) grid (10, 5);
\draw [step=0.5] (2, 0) grid (7, 2);
\draw [step=0.25] (5, 2) grid (8, 5);
\draw [step=0.25] (4, 0) grid (6, 2);
\end{tikzpicture}
    \end{center}
    \item Raffiner le maillage cellule par cellule (\emph{cell-based AMR}).
    \begin{center}
\begin{tikzpicture}[scale=0.8]
\draw [step=2] (0, 0) grid (4, 4);
\draw (0, 0) grid (2, 2);
\draw (2, 2) grid (4, 4);
\draw [step=0.5] (0, 0) grid (1, 1);
\draw [step=0.5] (1, 1) grid (2, 2);
\draw [step=0.5] (2, 2) grid (3, 3);
\draw [step=0.5] (3, 3) grid (4, 4);
\end{tikzpicture}
    \end{center}
\end{itemize}
\end{frame}

\begin{frame}{Block-based AMR}
\begin{figure}[ht!]
\begin{center}
\scalebox{0.8}{\input{figures/amr_block.tikz}}
\end{center}
\end{figure}
\begin{itemize}\setlength{\itemsep}{10pt}
    \item Superposition des maillages de plus en plus fins\footnotemark[1].
    \item Chaque patch peut être traité comme une \emph{grille uniforme}.
    \item Transposition ``directe'' des algorithmes sur maillage fixe.
\end{itemize}
\footnotetext[1]{M. J. Berger, J. Oliger, \emph{Adaptive Mesh Refinement for Hyperbolic Partial Differential Equations}}
\end{frame}

\begin{frame}{Cell-based AMR}
\begin{figure}
\begin{center}
\begin{tikzpicture}[scale=0.7]
\draw [fill=green!40] (0, 0) rectangle (4, 4);
\draw [fill=blue!40,step=2] (6, 0) grid (10, 4) rectangle (6, 0);
\draw [fill=red!40,step=2] (12, 0) grid (16, 4) rectangle (12, 0);
\draw (12, 0) grid (14, 2);
\draw (14, 2) grid (16, 4);

\draw [->, ultra thick, line join=round, decorate, decoration={
    zigzag,
    segment length=10,
    amplitude=1.9,post=lineto,
    post length=6pt
}]  (4.1,2) -- (5.9,2);
\draw [->, ultra thick, line join=round, decorate, decoration={
    zigzag,
    segment length=10,
    amplitude=1.9,post=lineto,
    post length=6pt
}]  (10.1,2) -- (11.9,2);
\end{tikzpicture}
\end{center}
\end{figure}
\begin{itemize}\setlength{\itemsep}{10pt}
    \item Rectangle divisé en $2$ dans chaque direction.
    \item \emph{Chaque cellule est divisé} en 4 (resp. 8) en 2D (resp. 3D).
    \item Représentation hiérarchique: \emph{arbres}.
\end{itemize}
\end{frame}

\begin{frame}{Cell-based AMR: Arbres}
\vspace{-20px}
\begin{tikzpicture}[overlay]
\node [yshift=-2cm] at (0.32\paperwidth, 0) {$\equivto$};
\end{tikzpicture}
\begin{figure}[ht!]
\begin{center}
\scalebox{.8}{\input{figures/amr_cell_mesh.tikz}}\hfill
\scalebox{.7}{\input{figures/amr_cell_tree.tikz}}
\end{center}
\end{figure}
\begin{itemize}\setlength{\itemsep}{10pt}
    \item Représentation par des \emph{quadtrees} (2D) ou \emph{octrees} (3D).
    \item Algorithmes \emph{logarithmiques}.
    \item Stockage: \emph{complet}\footnotemark[2] ou juste les \emph{feuilles},
    \emph{linéaire}\footnotemark[3] ou \emph{hiérarchique}.
\end{itemize}
\footnotetext[2]{A. M. Khokhlov, \emph{Fully Threaded Tree Algorithms for
Adaptive Refinement Fluid Dynamics Simulations}}
\footnotetext[3]{H. Ji, F.-S. Lien, E. Yee, \emph{A New Adaptive Mesh Refinement
Data Structure with an Application to Detonation}}
\end{frame}

\begin{frame}{Frameworks}
\vfill
\emph{Block-based AMR}
\begin{itemize}\setlength{\itemsep}{10pt}
    \item \textbf{Chombo}: C++, \emph{~300,000} lignes de code.
    \item \textbf{PARAMESH}: Fortran, \emph{~150,000} lignes de code.
    \item \textbf{AMRClaw}: Fortan, \emph{~50,000} lignes de code sans ClawPack.
\end{itemize}
\vfill
\emph{Cell-based AMR}
\begin{itemize}\setlength{\itemsep}{10pt}
    \item \textbf{RAMSES}: Fortran, \emph{~120,000} lignes de code.
    \item \textbf{Peano}: C++, \emph{~80,000} lignes de code.
    \item \textbf{Gerris}: C, \emph{~30,000} lignes de code.
\end{itemize}
\vfill
\end{frame}

\section{p4est}
\begin{frame}{p4est}
\begin{itemize}\setlength{\itemsep}{10pt}
    \item Nouveau framework (2011) \emph{cell-based AMR}\footnotemark[4].
    \item Massivement \emph{parallèle} (tests sur plus de 220,000 cœurs).
    \item \emph{Open-source}: \url{http://p4est.github.io/}.
    \item \textit{Juste} ~40,000 ligne de \texttt{C}.
    \item \emph{Qualité du code}: gestion de version, coding guidelines strictes,
    documentation.
    \item Gère \emph{uniquement} le maillage, \emph{découplé} des méthodes numériques.
\end{itemize}
\footnotetext[4]{C. Burstedde, L. C. Wilcox, O. Ghattas, \emph{\texttt{p4est}:
Scalable Algorithms for Parallel Adaptive Mesh Refinement}, 2011}
\end{frame}

\begin{frame}{Représentation du domaine}
\begin{figure}
\includegraphics[width=\linewidth]{img/p4est-algorithms/p4est_sphere}
\end{figure}

\begin{itemize}
    \item \emph{Niveau grossier}: ensemble de $K$ arbres de référence, chacun
    soumis à une transformation:
\[
\varphi_k: [0, 2^b]^d \to \R^d, \quad \forall k \in \llbracket 0, K \rrbracket,
\]
    \item \emph{Niveau fin}: chaque arbre est raffiné et déraffiné indépendamment.
\end{itemize}
\end{frame}

\begin{frame}{Space-filling curve}
\begin{columns}[c]
\column{0.5\textwidth}
\begin{center}
\scalebox{.6}{\input{figures/p4est_zorder_tree.tikz}}
\end{center}
\column{0.5\textwidth}
\begin{center}
\scalebox{.7}{\input{figures/p4est_zorder_mesh.tikz}}
\end{center}
\end{columns}

\begin{itemize}
    \item \texttt{p4est} utilise une méthode de \emph{stockage linéaire} de l'arbre.
    \item Les \emph{space-filling curves} sont principalement
    utilisées pour \emph{l'équilibrage de charge}.
    \item Plusieurs possibilités: la courbe d'\emph{Hilbert}, la courbe de \emph{Peano},
    la \emph{z-courbe}, etc.
\end{itemize}
\end{frame}

\begin{frame}{Répartition parallèle}
\begin{figure}[!ht]
\begin{center}
\scalebox{0.8}{\input{figures/p4est_global_array.tikz}}
\end{center}
\end{figure}

\begin{itemize}\setlength{\itemsep}{10pt}
    \item Chaque processus gère \emph{un morceau du tableau globale} de cellules.
    \item Les morceaux sont \emph{contigus} du point de vue de la z-curve.
\end{itemize}
\end{frame}

\begin{frame}{Fonctionnalités \texttt{p4est}}
\begin{enumerate}\setlength{\itemsep}{10pt}
    \item \emph{Création} d'une nouvelle forêt: \texttt{p4est\_new}.
    \item \emph{Itération} sur les sommets, arêtes, faces et cellules: \texttt{p4est\_iterate}.
    \item \emph{Adaptation}: \texttt{p4est\_refine} et \texttt{p4est\_coarsen}.
    \item \emph{Contrainte}: \texttt{p4est\_balance}.
    \item \emph{IO} avec le format VTK: \texttt{p4est\_vtk\_write\_file}.
    \item \emph{Cellules fantôme}: \texttt{p4est\_ghost\_new}.
    \item \emph{Équilibrage de charge}: \texttt{p4est\_partition}.
\end{enumerate}
\end{frame}

\begin{frame}{Interface avec p4est}
\begin{picture}(320,250)
\put(-40,30){\includegraphics[width=1.3\linewidth]{img/calltree}}
\end{picture}
\end{frame}

\begin{frame}{Développement du code}
\begin{itemize}\setlength{\itemsep}{10pt}
    \item \emph{Nouvelle application} qui utilise la bibliothèque \texttt{p4est} pour
    gérer le maillage.
    \item Environ \emph{7000} lignes de code.
    \item \emph{Contraintes de qualité} d’ingénierie logicielle: gestion de
    version (git), CMake, Doxygen, valgrind, etc.
    \item Tests parallèles sur des \emph{supercalculateurs}: Poincaré et Igloo (> 1000 cœurs).
    \item Tests avec plus de \emph{2-3 millions de cellules} sur \emph{300-400 cœurs}.
\end{itemize}
\end{frame}

\section{Équation de transport}
\begin{frame}{Équation de transport}
\vfill
\[
(T)
\begin{cases}
\pd[t]{c} + \vect{u} \cdot \grad{c} = 0, & \quad \vect{x} \in \Omega \subset \R^d, t \in [0, T], \\
c(0, \vect{x}) = c_0(\vect{x}),
\end{cases}
\]
\vfill
\[
\begin{array}{RCRCL}
\text{\emph{quantité transportée}} & \colon & (t, \vect{x}) \in [0, T] \times \Omega &
                                        \longmapsto & c(t, \vect{x}), \\[2ex]
\text{\emph{champs vitesse}}       & \colon & (t, \vect{x}) \in [0, T] \times \Omega &
                                        \longmapsto & \vect{u}(t, \vect{x}) = (u, v, w), \\[2ex]
\text{\emph{condition initiale}}   & \colon & \vect{x} \in \Omega                    &
                                        \longmapsto & c_0(\vect{x}).
\end{array}
\]
\vfill
\end{frame}

\begin{frame}{Notations}
\begin{figure}[!ht]
\begin{center}
\input{figures/scheme_cell_advanced.tikz}
\end{center}
\end{figure}
\begin{itemize}\setlength{\itemsep}{10pt}
    \item $|K_i| = \Delta x_i^{d}$ est l'aire de la cellule.
    \item Une face est donnée par $\Gamma_{ij} = \overline{K}_i
    \cap \overline{K}_j$.
\end{itemize}
\end{frame}

\begin{frame}{Splitting directionnel}
Approximation successive de chaque équation dans le système:
\[
\begin{cases}
\pd[t]{} c + u \pd[x]{} c = 0, \\
\pd[t]{} c + v \pd[y]{} c = 0, \\
\pd[t]{} c + w \pd[z]{} c = 0.
\end{cases}
\]
Approximation de l'équation en $x$ par une méthode de \emph{Volumes Finis}:
\[
c^{n + 1}_i = c^n_i + \frac{\Delta t}{|K_i|}
\sum_{j \in \mathcal{N}_x(i)} |\Gamma_{ij}|\ u_{ij}^n\ n_{ij} (c^n_i - c^n_{ij})
\]
avec un flux \emph{upwind}:
\[
c_{ij}^n =
\begin{cases}
c_i^n & \quad \text{if } u_{ij}^n\,n_{ij} \geq 0, \\
c_j^n & \quad \text{if } u_{ij}^n\,n_{ij} < 0.
\end{cases}
\]
\end{frame}

\begin{frame}{Stabilité}
\emph{Condition CFL} pour l'équation de transport discrétisée avec le schéma
upwind:
\[
\Delta t \max_{s \in \{x, y, z\}} \max_{i}
\left(
-\frac{1}{|K_i|}\sum_{\substack{j \in \mathcal{N}_s(i) \\ u_{ij}^n\,n_{ij} < 0}} |\Gamma_{ij}| u_{ij}^n\,n_{ij}
\right) \leq 1.
\]
\emph{Choix de $\Delta t$}:
\[
\Delta t = \theta \min_{s \in \{x, y, z\}} \min_i
\left(
-\frac{|K_i|}{\sum_{\substack{j \in \mathcal{N}_s(i) \\ u_{ij}^n\,n_{ij} < 0}} |\Gamma_{ij}| u_{ij}^n\,n_{ij}}
\right),
\]
où $\theta \in [0, 1]$.
\end{frame}

\begin{frame}{Choix du pas en temps}
Plusieurs possibilités pour avancer en temps:
\begin{itemize}\setlength{\itemsep}{10pt}
    \item Limité pas \emph{le plus petit pas en espace}.
\[
\Delta t = \theta \frac{\Delta x}{{\displaystyle \max_{s \in \{x, y, z\}} \max_i}
\left(\sum_{\substack{j \in \mathcal{N}_s(i) \\ u_{ij}^n\,n_{ij} < 0}} |\Gamma_{ij}| u_{ij}^n\,n_{ij}\right)
}.
\]
    \item Pas différent \emph{pour chaque niveau} de raffinement: \textbf{subcycling}.
    \[
    \Delta t_l = 2^{l_{max} - l} \Delta t.
    \]
\end{itemize}
\end{frame}

\section{Modèle diphasique}
\begin{frame}{CEMRACS}
\begin{itemize}\setlength{\itemsep}{10pt}
    \item Centre d'Été Mathématique de Recherche Avancée en Calcul Scientifique.
    \item À \emph{CIRM}, Marseille.
    \item Durée: \emph{2 / 5 semaines} en août.
    \item Équipe \emph{Maison de la Simulation} et \emph{École Centrale Paris}.
    \item Maison de la Simulation: Alexandru \textsc{Fikl}, Pierre
    \textsc{Kestener}, Samuel \textsc{Kokh}.
    \item École Centrale Paris: Florence \textsc{Drui}, Adam \textsc{Larat},
    Vincent \textsc{Le Chenadec}, Marc \textsc{Massot}.
\end{itemize}

\end{frame}

\begin{frame}{Description}
\begin{itemize}
    \item Deux fluides \emph{compressibles} et \emph{non miscibles} $k = \{0, 1\}$\footnotemark[5].
    \item Loi de \emph{pression barotrope}:
    \[
    p_k(\rho_k) = p_{k, 0} + c_k^2 (\rho_k - \rho_{k, 0}).
    \]
    \item \emph{Densité totale} du milieux:
    \[
    \rho = \alpha \rho_1 + (1 - \alpha) \rho_2.
    \]
    où $\alpha$ est la \emph{fraction volumique}.
    \item Équilibre mécanique:
    \[
    p_1(\rho_1) = p_2(\rho_2).
    \]
    \item Champs vitesse $\vect{u}$ dans les deux fluides.
\end{itemize}
\footnotetext[5]{G. Chanteperdrix, P. Villedieu, J. P. Vila, \emph{A Compressible
Model for Separated Two-Phase Flows Computations}}
\end{frame}

\begin{frame}{Équations}
Le système \emph{hyperbolique} est:
\[
(CVV)
\begin{cases}
\tpd[t]{}\vect{W} + \nabla \cdot \mathbf{F}(\vect{W}) = 0, \\
\vect{W}(0, \vect{x}) = \vect{W}_0(\vect{x}),
\end{cases}
\]
avec $\vect{W} = (\alpha \rho_1, (1 - \alpha) \rho_2, \rho u, \rho v, \rho w)$
et $\mathbf{F} = (\mathcal{F}, \mathcal{G}, \mathcal{H})$:
\vfill
{\small
\[
\mathcal{F}(\vect{W}) =
\begin{pmatrix}
\alpha \rho_1 u \\
(1 - \alpha) \rho_2 u \\
\rho u^2 + p \\
\rho uv \\
\rho uw
\end{pmatrix},
\mathcal{G}(\vect{W}) =
\begin{pmatrix}
\alpha \rho_1 v \\
(1 - \alpha) \rho_2 v \\
\rho vu \\
\rho v^2 + p \\
\rho vw
\end{pmatrix} \text{ et }
\mathcal{H}(\vect{W}) =
\begin{pmatrix}
\alpha \rho_1 w \\
(1 - \alpha) \rho_2 w \\
\rho wu \\
\rho wv \\
\rho w^2 + p
\end{pmatrix}.
\]
}
\vfill
\end{frame}

\begin{frame}{Splitting directionnel}
On applique la même stratégie de splitting directionnel.
\[
\begin{cases}
\pd[t]{} \vect{W} + \pd[x]{} \mathcal{F}(\vect{W}) = 0, \\
\pd[t]{} \vect{W} + \pd[y]{} \mathcal{G}(\vect{W}) = 0,\\
\pd[t]{} \vect{W} + \pd[z]{} \mathcal{H}(\vect{W}) = 0.
\end{cases}
\]
En $x$, on approxime par \emph{Volumes Finis}:
\[
\vect{W}_i^{*} = \vect{W}_i^n - {
   \frac{\Delta t}{|K_i|} \sum_{j \in \mathcal{N}_x(i)} |\Gamma_{ij}| n_{ij} \mathcal{F}_{ij}^{n}}
\]
Le \emph{flux} est $\mathcal{F}_{ij}^n = \mathcal{F}(\vect{W}^{Suliciu}(\sfrac{x}{t} = 0;
\vect{W}_L, \vect{W}_R))$, où $\vect{W}^{Suliciu}$ est donnée par un solveur
de Riemann approché associé à une approximation par \emph{relaxation de Suliciu}.
\end{frame}

\section{Résultats}
\subsection{p4est}

\begin{frame}{Condition initiale}
\vfill
\begin{columns}
\column{0.5\textwidth}
\centering
\includegraphics[width=0.9\linewidth]{img/p4est-algorithms/initial_uniform}
\column{0.5\textwidth}
\centering
\includegraphics[width=0.9\linewidth]{img/p4est-algorithms/initial_refined}
\end{columns}
\vfill
Niveau: $[4, 7]$.

Cellules: en 2D \emph{694} vs \emph{16,384}.
\end{frame}

\begin{frame}{Raffinement}
\vfill
\begin{columns}
\column{0.5\textwidth}
\centering
\includegraphics[width=0.9\linewidth]{img/p4est-algorithms/adapt_middle}
\column{0.5\textwidth}
\centering
\includegraphics[width=0.9\linewidth]{img/p4est-algorithms/adapt_middle3d}
\end{columns}
\vfill
Niveau $[4, 7]$.

Cellules: en 2D \emph{694} vs \emph{16,384}, en 3D \emph{12,692} vs \emph{2,097,152}.
\end{frame}

\begin{frame}{Équilibrage de charge}
\vfill
\begin{columns}
\column{0.5\textwidth}
\centering
\includegraphics[width=0.9\linewidth]{img/p4est-algorithms/partition_middle}
\column{0.5\textwidth}
\centering
\includegraphics[width=0.9\linewidth]{img/p4est-algorithms/partition_middle3d}
\end{columns}
\vfill
\end{frame}

\subsection{Équation de transport}
\begin{frame}{Le disque de Zalesak}
\vfill
\begin{columns}
\column{0.5\textwidth}
\centering
\includegraphics[width=0.9\linewidth]{img/transport/zalesak/zalesak_initial_order1}
\column{0.5\textwidth}
\centering
\includegraphics[width=0.9\linewidth]{img/transport/zalesak/zalesak_final_order1}
\end{columns}
\vfill
$\vect{u} = (-(y - 0.5), x - 0.5, 0)$, \emph{périodique}, $T = 6.283$, $\textrm{CFL} = 1.0$.
\end{frame}

\begin{frame}{Le disque de Zalesak}
\vfill
\begin{columns}
\column{0.5\textwidth}
\centering
\includegraphics[width=0.9\linewidth]{img/transport/zalesak/zalesak_final_order2}
\column{0.5\textwidth}
\centering
\includegraphics[width=0.9\linewidth]{img/transport/zalesak/zalesak_refinement}
\end{columns}
\vfill
Ordre 2 en espace: $\textrm{CFL} = 0.5$ (limiteur \emph{minmod}).
\end{frame}


\begin{frame}{Déformation en 2D}
\vfill
\begin{columns}
\column{0.5\textwidth}
\centering
\includegraphics[width=0.9\linewidth]{img/transport/vortex/vortex_middle_refinement_order1}
\column{0.5\textwidth}
\centering
\includegraphics[width=0.9\linewidth]{img/transport/vortex/vortex_middle_refinement_order2}
\end{columns}
\vfill
\[
\vect{u} = (2\sin^2(\pi x) \sin(2\pi y), -\sin(2\pi x) \sin^2(\pi y)).
\]
Ordre 1: \emph{périodique}, $T = 3$, $t = 1.5$ et $\textrm{CFL} = 0.5$.

Ordre 2 en espace: $\textrm{CFL} = 0.5$ (limiteur \emph{minmod}).
\end{frame}

\subsection{Modèle diphasique}
\begin{frame}{Tube à choc de Sod: Vitesse}
\vfill
\begin{columns}
\column{0.5\textwidth}
\centering
\includegraphics[width=0.9\linewidth]{img/two-phase/sod/sod_velocity_order1}
\column{0.5\textwidth}
\centering
\includegraphics[width=0.9\linewidth]{img/two-phase/sod/sod_velocity_order2}
\end{columns}
\vfill
Ordre 1: \emph{périodique en y}, $T = 0.001$, $t = 0.0006$ et $\textrm{CFL} = 0.5$.

Ordre 2 en espace: $\textrm{CFL} = 0.25$ (limiteur \emph{minmod}).
\end{frame}

\begin{frame}{Tube à choc de Sod: Raffinement}
\centering
\includegraphics[width=0.9\linewidth]{img/two-phase/sod/sod_refinement}
\end{frame}

\begin{frame}{Dam Break}
\vfill
\begin{columns}
\column{0.5\textwidth}
\centering
\includegraphics[width=0.9\linewidth]{img/two-phase/dambreak/dambreak_00}

$t = 0$.
\column{0.5\textwidth}
\centering
\includegraphics[width=0.9\linewidth]{img/two-phase/dambreak/dambreak_09}

$t = 0.9$.
\end{columns}
\vfill
Ordre 1: conditions aux limites de type \emph{mur}, $T = 2$, $\textrm{CFL} = 1.0$.
\end{frame}

\begin{frame}{Dam Break II}
\vfill
\begin{columns}
\column{0.5\textwidth}
\centering
\includegraphics[width=0.9\linewidth]{img/two-phase/dambreak/dambreak_15}

$t = 1.5$.
\column{0.5\textwidth}
\centering
\includegraphics[width=0.9\linewidth]{img/two-phase/dambreak/dambreak_20}

$t = 2$.
\end{columns}
\vfill
Ordre 1: conditions aux limites de type \emph{mur}, $T = 2$, $\textrm{CFL} = 1.0$.
\end{frame}

\begin{frame}{Dam Break: Raffinement}
\vfill
\begin{columns}
\column{0.5\textwidth}
\centering
\includegraphics[width=0.9\linewidth]{img/two-phase/dambreak/dambreak_refined_middle}

$t = 1.5$.
\column{0.5\textwidth}
\centering
\includegraphics[width=0.9\linewidth]{img/two-phase/dambreak/dambreak_refined_end}

$t = 2$.
\end{columns}
\vfill
\end{frame}

\begin{frame}{Conclusions}
\vfill
\begin{itemize}\setlength{\itemsep}{10pt}
    \item Collaboration pour le projet \emph{CEMRACS}!
    \item Intégration d'une nouvelle fonctionnalité dans \texttt{p4est}.
    \item Travail de \emph{documentation}: exemples, tests, méthodes, etc.
\end{itemize}
\vfill
\emph{Perspectives}
\begin{itemize}\setlength{\itemsep}{10pt}
    \item Implémenter un code \emph{plus optimisé} (moins modulaire).
    \item \emph{Étude} de scalabilité et de performance.
    \item Application à l'\emph{astrophysique} et à la simulation des \emph{sprays}.
\end{itemize}
\vfill
\end{frame}

\begin{frame}
\begin{center}
    {\Large Je vous remercie pour votre attention!}

    \vfill
    \includegraphics[width=0.6\paperwidth]{img/questions}
    \vfill
\end{center}
\end{frame}
\end{document}



