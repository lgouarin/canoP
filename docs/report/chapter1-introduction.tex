\chapter*{Acknowledgments}

I would like to express my gratitude to my two supervisors at \emph{Maison de la Simulation},
M. Pierre \textsc{Kestener} and M. Samuel \textsc{Kokh}, for all their guidance
and invaluable insight during this project. The long afternoons we have spend
brainstorming and debating different features or design choices from \texttt{p4est}
and AMR in general, have been both fun and incredibly productive.

I would also like to thank all the staff at \emph{Maison de la Simulation} for
making me feel welcome at all times. Their different fields of study and willingness
to talk at length about them have provided a very enriching environment, full
of ideas that one is unlikely to get acquainted with in a more academic setting.

Last, but not least, I would like to thank all the other people that were kind
enough to proofread this work and help me sort out many little kinks.

\chapter{Introduction}\label{chapter:introduction}

Solving partial differential equations has always been one of the most important
facets of scientific computing, be it in theoretical mathematics, astrophysics,
chemistry, etc. Many characteristics and properties as well as a good deal of
intuition was developed around partial differential equations and the ways to
solve them. Even though on a theoretical level knowledge has steadily advanced,
the problems we face have also become larger and larger in scope.

A new challenge in tackling these large problems is utilizing the growing computational
power available to scientists. There have been many attempts at improving
well known numerical methods and transforming them in new ways to better fit
the world of \emph{High Performance Computing}, as well as designed novel
techniques.

In this paper we will study a small segment of these efforts, namely the
interest in adapting the mesh used for some numerical simulations. We will be
looking at the \texttt{p4est} library~\cite{BWG11} that tries to offer a complete framework
for dealing with the mesh in highly parallel environments using scalable
algorithms for creating, adapting and load-balancing the resulting mesh.

\texttt{p4est} provides an implementation of \emph{Adaptive Mesh Refinement} (AMR),
often referred to as \textbf{cell-based AMR} (in contrast to block-based AMR),
that makes use of groups of quadtrees (in 2D) and octrees (in 3D) (conveniently
called \emph{forests}) to allow describing complex geometries and capturing
phenomena developing at different scales (e.g. in the case of turbulence).

\subsubsection{Internship}

The research into \texttt{p4est} was done in the form of an internship, that
doubles as a Masters project, in the third (and final) year at the engineering
school \emph{Institut Sup' Galilée} that is part of \emph{Université Paris XIII}.
The institution that sponsored the internship was \emph{Maison de la Simulation}.

The main goals of this work, and internship, is to \emph{document} and \emph{evaluate} the
\texttt{p4est} library. Even though the algorithms used in the \texttt{p4est}
library have been explained in several articles (~\cite{BWG11},~\cite{IBG2012}
and~\cite{IBWG2014}), they are highly technical and not straightforward to
understand. We have complemented these articles with extensive examples that will,
hopefully, allow for a broader understanding of the methods used by \texttt{p4est}.

To evaluate the \texttt{p4est} library we have developed a new code~\cite{CODE}
that makes use of it, as a mesh-handling backend, to solve hyperbolic equations
using different schemes involving the \emph{Finite Volume Method}, such as
directional splitting and higher order MUSCL schemes. The code was created with
a special focus on flexibility and readability to serve as further documentation
for \texttt{p4est} and its capabilities.

\subsubsection{Maison de la Simulation}

Maison de la Simulation is a joint research facility between \textbf{CEA}
(the \emph{Centre d'énergie atomique}), \textbf{CNRS} (the \emph{Centre national de la
recherche scientifique}), \textbf{INRIA} (\emph{Institut national de recherche
en informatique et en automatique}) with the involvement of two universities:
\textbf{Université Paris-Sud} and \textbf{Université de Versailles}. The laboratory
was created in 2012 in the DigiteoLabs building in Saclay.

The main focus of the laboratory is promoting \textbf{High Performance Computing}
in France and worldwide as part of a bigger effort to fully understand
and make use of emerging technologies and advancing computing power.

In addition to being a research center, Maison de la Simulation is also an
\emph{expertise center} open to the scientific community. As part of this goal,
there is an ongoing effort to educate both existing engineers and students
to efficiently use the computing infrastructure available to them. Given this
direction, Maison de la Simulation has a small number of permanent
staff with most of the personnel consisting of temporary staff from various
research institutions and PhD students.

As a hub for HPC in France, Maison de la Simulation has worked on
various projects that involved either optimizing existing code (e.g. for
\emph{Gysela5D}), fully developing new applications (e.g. \emph{Ramses-GPU})
or developing novel techniques (e.g. \emph{new Godunov-type schemes for
magnetohydrodynamics}).

\subsubsection{Structure}

The paper is structured in $6$ chapters, including the current one, that will
try to offer a broad view on the world of AMR and the \texttt{p4est} library.

\hyperref[chapter:amr]{Chapter 2} deals specifically with the historical context
surrounding the developments of several Adaptive Mesh Refinement Methods. We
will be looking at cell-based AMR and block-based AMR in detail and will try to
integrate them into a wider research field for multiscale numerical simulations.

\hyperref[chapter:p4est]{Chapter 3} tries to offer some insights into the
\texttt{p4est} library by explaining the different methods it uses to store
the mesh and the lengths to which it goes to provide novel parallel algorithms
for AMR. We will also be looking at some preliminary tests for the functionality
offered by \texttt{p4est}.

\hyperref[chapter:transport]{Chapter 4} will look at the first set of equations
and numerical schemes that we have tested using the \texttt{p4est} library.
We will be looking at the \emph{transport equation} and the challenges posed
by implementing a simple Finite Volume scheme in the context of AMR.

\hyperref[chapter:twophase]{Chapter 5} presents a more complex two-phase model.
This model will provide a more comprehensive test for \texttt{p4est} that will
hopefully prove that it is well equipped to handle the different problems that
arise from complex phenomena.

Finally, in \hyperref[chapter:conclusions]{Chapter 6} we will present the
conclusions of our tests and opportunities for future work.
