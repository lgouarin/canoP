# Introduction

This is a small description of how the code is organized and works at the
current time (25.05.2014). Features-wise, we can do: upwind scheme with
or without subcycling in 2d or 3d. Also, IO is done in parallel using HDF5.

# General

The main structure of the program is `solver_t` which is define in the file
`solver.h`. This struct holds all the information about the current state
of the solver. This includes:

* the current time and time step
* the current min and max levels
* the callbacks for refining, coarsening, replacing quadrants as needed
by p4est.
* all the p4est structures: p4est_t, p4est_mesh_t, p4est_ghost_t and the
ghost data per quadrant.
* misc options.

There are a couple of functions that we have to operate on the structure like
the classical new and destroy.

The solver controls the time loop described in the `main.c` file. The time
loop is:

```
#!c
while (!solver_finished(solver)) {
    solver_next_iteration(solver);

    if (solver_should_write(solver)) {
        solver_save_solution(solver);
    }

    if (solver_should_adapt(solver)) {
        solver_adapt(solver);
    }
}
```

The `solver_finished` function defines a stopping criteria for the scheme.
This can be something like `t > tmax` or based on the number of iterations.

Next we have the `solver_next_iteration` function. This is the main function
of the time loop that computes the current time step using the cfl condition
and advances the solution in time with that time step. Advancing in time
is done using a single global time step or per-level subcycling.

The procedure that does subcycling is described in the `subcycling.txt` file,
so we will look here at the single time step case. The algorithm in this case
is very simple:

```
#!c
update_ghosts();

for (i = 0; i < num_local_quadrants; ++i) {
    cnew[i] = cold[i];
}

for (i = 0; i < num_local_quadrants; ++i) {
    for (j = 0; < num_neighbors(i); ++j) {
        flux = compute_flux(u, cold[i], cold[j]);
        cnew[i] += dt * u * (cold[i] - flux) / dt;
    }
}

for (i = 0; i < num_local_quadrants; ++i) {
    cold[i] = cnew[i];
}
```

The code is not written exactly as this, but this is essentially what it does.
In the code we use the p4est provided iterator function `p4est_iterate` and
iterate over quadrants using the two callbacks:

* `swap_values_iter_volume_fn`: to swap new and old values
* `update_cell_iter_volume_fn`: to compute the fluxes and update the cell
values.

The user also has to give a criteria for writing the solution to a file and
adapting the mesh to the current state of the solution.

The refining and coarsening functions are very similar. We will only look
at refining here.

```
#!c
update_ghosts();

for (k = 0; k < num_local_faces; ++k) {
    i = left_cell(k);
    j = right_cell(k);

    indicator = compute_refining_indicator(i, j);

    if (indicator > epsilon) {
        mark_for_refinement(i);
        mark_for_refinement(j);
    }
}

for (i = 0; i < num_local_quadrants; ++i) {
    refine(i);
}

p4est_balance();
```

At the end of every refining and coarsening step we have to do a balancing
step because all the routines depend on the fact that the mesh has a 2:1
balance for every face.

# File by File

## configreader.h

This is small configuration file reader that uses standard Lua files
for writing the configuration options. The reader can read ints, doubles and
strings from the file. If boolean values are needed, one can just use an
integer value of 0 for false and 1 for true.

In the case of strings, it makes a copy of the string given by the lua
interpreter since that is not guaranteed to have the desired lifetime. All
string values are stored in a linked list and freed on destruction of the
configuration reader, so the user of the API doesn't have to worry about
that at all.

The only exception is, of course, that one can't use the strings after the
reader has been destroyed.

## io_writer.h

This file defines a writer for HDF5 and associated XDMF files. It is very
tied in with the p4est structures because it requires the point data and
connectivity data defined there.

The API is very simple. The file defines 5 functions:

* `io_writer_open`: given the basename of a file, it opens the HDF5 and XMF
files for writing.
* `io_writer_close`: it closes the previously opened files.
* `io_write_header`: to write all the usual information like the coordinates,
the connectivity information and, optionally, the tree ID, rank or level
of each quadrant.
* `io_write_attribute`: to write scalar or vector, cell-centered or
node-centered data for the mesh.
* `io_write_footer`: a simple function that just closes all the XDMF tags
that were opened in the header.

The `io_write_attribute` function is not necessary to write a valid XDMF
file, but the header and footer are always necessary. Besides these general
functions, it also defines a `io_write_mesh` function that writes only
mesh information with a `scale` of 0.9 to help visualize the separate
quadrants. This function only takes as arguments the p4est struct and the
filename.

The envisioned use is on the lines of:

* create a new `io_writer_t` using `io_writer_new`. If you intend to write
multiple files that represent a time series, it is recommended to use the
extra argument to write a main XMF file.
* open a file
* write the data
* close the file
* if necessary, open other files and write more data
* destroy the `io_writer_t`.

## userdata.h

This file defines the user defined data structure that is added to each
quadrant. At the moment this is rather specific to a 2D transport equation,
but it can contain anything.

## quadrant_ext.h

We also defined some extra functions on quadrants and face sides (collections
of quadrants) like computing the length of a given quadrant in the real
mesh, compute the center of a quadrant, get the face velocity, etc.

For face sides, we can set the refine or coarsen flag on a whole side,
this means on all the 1, 2 or 4 (3D only) quadrants that are there, get the
level of a side, get the quadrant in a side based on a local id, etc.

## level_mesh.h

Besides the `p4est_mesh_t` struct, we have defined another mesh structure
that contains a list of quadrants for each level to use when subcycling. The
mesh is simply constructed by iterating over all the quadrants and constructing
linked lists for each level.

It also provides a `level_mesh_iterate` function that allows the user to
iterate over the quadrants on a specific level.

## adapting.h

This file contains the 3 functions needed when calling `p4est_refine` and
`p4est_coarsen`. That is: a function that returns 0 or 1 whether the quadrant
should be refined (coarsened) or not and a function that replaces either:

* a refined quadrant with its 4 (8) new children
* a family of 4 (8) quadrants that have to be coarsened with the new quadrant.

## iterators.h

This file contains all the different iterator callbacks used in the program.
As a rule, they are named `name_iter_volume_fn` where volume can be replaced
by `face`, `edge` or `corner`. We only have face and volume iterators.

* `print_iter_volume_fn`: Prints the coordinates and level of a quadrant.
* `swap_values_iter_volume_fn`: Swaps the old and new values in the user data
of a quadrant.
* `update_cell_iter_face_fn`: Computes the fluxes and updates the values on
the two sides of a face.
* `update_cell_iter_volume_fn`: Computes fluxes and updates the value in a cell.
* `update_cell_subcycle_iter_volume_fn`: Same as the previous function, just
that it updates the cell and some of its neighbors in such a way
that the flux is not added twice to the same cell.
* `velocity_max_iter_volume_fn`: Compute the maximum of the sum of velocities
on edges on which the neighbor is the downwind cell. This is used to compute
the CFL condition.
* `norm_iter_volume_fn`: Compute the L2 norm over all the mesh. Per quadrant
it adds `h * c^2`.
* `refine_iter_face_fn`: Compute a refinement indicator between the two faces
and mark them for refinement if necessary.
* `refine_spread_iter_face_fn`: Mark a both sides for refinement if one of them
is already marked. This helps to add another layer of refined cells.
* `refine_iter_volume_fn`: Compute an indicator between the current cell and
its neighbors and mark it for refinement if necessary.
* `coarsen_iter_face_fn`: Same as `refine_iter_face_fn`.

Some of these functions are generic, but most of them are very dependent on
the user data.

## indicators.h

This file contains functions for computing refinement criteria between
two quadrants, a face side and a quadrant an two sides of a face. For the
moment, there is only a gradient based indicator.

# Notes

## p4est_nodes_new

The `p4est_nodes_new` function takes as arguments a `p4est_t` struct and a
ghost layer `p4est_ghost_t`. The ghost layer can be `NULL` if we only look
at local nodes. The results with a ghost layer and without one are quite
different. Without a ghost layer:

* global_owned_indeps is not defined per process
* offset_owned_indeps is not defined either

while with a ghost layer, indep_nodes does not contain hanging nodes on
faces or edges (in 3D).

This probably needs some more investigation to see how it works and why.
