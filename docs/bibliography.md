# List of AMR Codes

A nice list of patch-based and cell based AMR libraries and applications can be found on Donna Calhoun's 
[website](http://math.boisestate.edu/~calhoun/www_personal/research/amr_software/index.html).

# Cell-based AMR

## Articles

Some general articles:

* [Fully Threaded Tree Algorithms for Adaptive Mesh Fluid Dynamics Simulations](http://cds.cern.ch/record/318999/files/9701194.pdf), A. M. Khokhlov, 1998.

* [Cosmological Hydrodynamics with Adaptive Mesh Refinement](http://arxiv.org/pdf/astro-ph/0111367.pdf), R. Teyssier, 2002.

## p4est and ForestClaw

### p4est

The main articles on [p4est](http://p4est.github.io/) are:

* [p4est: Scalable Algorithms for Paralle Adaptive Mesh Refinement on Forests of Octrees](http://burstedde.ins.uni-bonn.de/research/papers/BursteddeWilcoxGhattas11.pdf), Carsten Burstedde, Lucas C. Wilcox, Omar Ghattas, 2011.

* [Low-Cost Parallel Algorithms for 2:1 Octree Balance](http://burstedde.ins.uni-bonn.de/research/papers/IsaacBursteddeGhattas12.pdf), Tobin Isaac, Carsten Burstedde, Omar Ghattas, 2012. 

* [Large-scale Adaptive Mantle Convection Simulation](http://burstedde.ins.uni-bonn.de/research/papers/BursteddeStadlerAlisicEtAl13.pdf), Carsten Burstedde, Georg Stadler et al., 2013.

* [Recursive Algorithms for Distributed Forests of Octrees](http://arxiv.org/pdf/1406.0089v1.pdf), Tobin Isaac, Carsten Burstedde, Lucas C. Wilcox, Omar Ghattas, 2014.

Some presentations:

* [Forest-of-octrees AMR: Algorithms and Interfaces](http://numerics.kaust.edu.sa/hpc3/presentations/burstedde_hpc3.pdf), Carsten Burstedde, 2012.

* [Parallel Adaptive Mesh Refinement Using p4est](http://www.speedup.ch/workshops/w42_2013/carsten.pdf), Carsten Burstedde, 2013.

### ForestClaw:

[ForestClaw](http://math.boisestate.edu/~calhoun/ForestClaw/index.html) is a new library that uses [ClawPack](http://depts.washington.edu/clawpack/) and
p4est to solve hyperbolic PDEs. The main article is:

* [ForestClaw: Hybrid forest-of-octrees AMR for hyperbolic conservation laws](http://arxiv.org/pdf/1308.1472.pdf), Carsten Burstedde, Donna Calhoun, 2013.

## AMR Using Hashmaps

Another set of algorithms for doing cell-based AMR are presented in the following articles. The general numbering of the cells and the cell-based
operations are very similar to those of p4est. Additional application is given in for the case of cut-cells.

* [A New Adaptive Mesh Refinement Data Structure with an Application to Detonation](http://www.sciencedirect.com/science/article/pii/S0021999110004705), Hua Jia, Fue-Sang Lienb, Eugene Yee, 2010.

* [A Robust and Efficient Hybrid Cut-cell / Ghost-cell Method with Adaptive Mesh Refinement](http://www.sciencedirect.com/science/article/pii/S0045782508003113), Hua Jia, Fue-Sang Lienb, Eugene Yee, 2008.

* [Numerical Simulation of Detonation Using an Adaptive Cartesian Cut-cell Method Combined with a Cell-merging Technique](http://www.sciencedirect.com/science/article/pii/S0045793010000150), Hua Jia, Fue-Sang Lienb, Eugene Yee, 2010.

## AMR on GPU

[CLAMR](https://github.com/losalamos/CLAMR) is another library that does AMR with OpenCL. The main article is:

* [Cell-based Adaptive Mesh Refinement Implemented with General Purpose GPUs](https://github.com/losalamos/ExascaleDocs/blob/master/cell_based_amr_on_gpu.pdf), David Nicholaeff, Neal Davis, Dennis Trujillo, Robert Robey, 2011.

# Books

* Adaptive Mesh Refinement - Theory and Applications, Tomasz Plewa, Timur 
Linde and V. Gregory Weirs Editors, Springer, Lecture Notes in Computational Science and Engineering, 2004.

* Space-Filling Curves: An Introduction With Applications in Scientific Computing, Michael Bader, Springer, 2013.

# Misc

Some presentations:

* [Parallel Algorithms: Adaptive Mesh Refinement](https://hpc-forge.cineca.it/files/ScuolaCalcoloParallelo_WebDAV/public/anno-2013/Summer-School/AMR-BOLOGNA.pdf).

* [AMR Technologies](http://extremecomputingtraining.anl.gov/files/2013/07/fastmath-amr-atpetsc-08-2013.pdf), Anshu Dubey, Mark Adams, Ann Almgren, Brian Van Straalen, 2013.

* [Infrastructure for Parallel Adaptive Unstructured Mesh Simulations](http://extremecomputingtraining.anl.gov/files/2013/07/fastmath-pumi-atpesc-08-20131.pdf), 2013.

* [Introduction to Block-Structured Adaptive Mesh Refinement](http://hipacc.ucsc.edu/Lecture%20Slides/2011ISSAC/AlmgrenHIPACC_July2011.pdf), Ann S. Almgren, 2011.

A couple of Master's Theses:

* [Efficient Execution of AMR Computations on GPU Systems](http://mars.serc.iisc.ernet.in/downloads/runtime-irregular-gpus/theses/hari-thesis.pdf), Hari K. Raghavan, 2012.

* [An Adaptive Mesh MPI Framework](http://scholarworks.gsu.edu/cgi/viewcontent.cgi?article=1059&context=cs_theses), Karunamuni Charuka Silva, 2009.

Some other libraries:

* [AMRVAC](https://gitorious.org/amrvac) is a special relativistic MHD code mainly by [Rony Keppens](http://homes.esat.kuleuven.be/~keppens/).

* [DCCRG](https://gitorious.org/dccrg) is an easy to use grid for FVM/FEM simulations. The main article is:
[Parallel grid library for rapid and flexible simulation development](http://arxiv.org/pdf/1212.3496v1.pdf), I. Honkonen, S. von Alfthan et al., 2012.

* [CHOMBO](https://commons.lbl.gov/display/chombo/Chombo+-+Software+for+Adaptive+Solutions+of+Partial+Differential+Equations) 
is a very extensive AMR framework. An example for conservation laws can be found [here](https://seesar.lbl.gov/anag/example/).
