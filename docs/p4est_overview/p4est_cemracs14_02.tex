\documentclass[xcolor=svgnames,9pt]{beamer}
\usepackage{texformat}

\usepackage{caption}
\usepackage{minted}
\usepackage{tikz}

% suppress navigation bar
\beamertemplatenavigationsymbolsempty

\mode<presentation>
{
  \usetheme{bunsen}
  \setbeamertemplate{items}[square]
}

\newenvironment{itemize*}%
  {\begin{itemize}%
    \setlength{\itemsep}{15pt}%
    \setlength{\parskip}{0pt}}%
  {\end{itemize}}

% set fonts
\setbeamerfont{frametitle}{size=\LARGE,series=\bfseries}

\definecolor{uipoppy}{RGB}{225, 64, 5}
\definecolor{uipaleblue}{RGB}{96,123,139}
\definecolor{uiblack}{RGB}{0, 0, 0}

% caption styling
\DeclareCaptionFont{uiblack}{\color{uiblack}}
\DeclareCaptionFont{uipoppy}{\color{uipoppy}}
\captionsetup{labelfont={uipoppy},textfont=uiblack}

\renewcommand{\emph}[1]{\textcolor{MidnightBlue}{\textbf{#1}}}
\newcommand{\floor}[1]{\left\lfloor #1 \right\rfloor}
\newcommand{\ceil}[1]{\left\lceil #1 \right\rceil}
\newcommand*{\rfrac}[2]{{}^{#1}\!/_{#2}}

% add a vertical line between multicol columns
\setlength{\columnseprule}{0.4pt}

% title slide definition
\title{Adaptive Mesh Refinement with p4est}
\author{Alexandru Fikl}
\date{}

%--------------------------------------------------------------------
%                           Introduction
%--------------------------------------------------------------------

\begin{document}

\section{Introduction}
\setbeamertemplate{background}
{\includegraphics[width=\paperwidth,height=\paperheight]{img/frontpage_bg}}
\setbeamertemplate{footline}[default]

\begin{frame}
\vspace{2cm}
\begin{columns}
\begin{column}{0.4\textwidth}
  \titlepage
  \vspace{10cm}
\end{column}
\begin{column}{0.6\textwidth}
\end{column}
\end{columns}
\end{frame}

\setbeamertemplate{background}
 {\includegraphics[width=\paperwidth, height=\paperheight]{img/slide_bg}}
\setbeamertemplate{footline}[bunsentheme]

\section{AMR}
\begin{frame}[fragile]{The Quadrant (p4est.h)}
\begin{columns}
\column{0.5\textwidth}
\vspace{5px}
\begin{minted}[mathescape,frame=lines]{c}
typedef struct p4est_quadrant
{
  p4est_qcoord_t x, y, z;
  int8_t level, pad8;
  int16_t pad16;
  union p4est_quadrant_data
  {
    void *user_data;
    /* ... */
    struct
    {
      p4est_topidx_t which_tree;
      p4est_locidx_t local_num;
    }
    piggy3;
  }
  p;
}
p4est_quadrant_t;
\end{minted}

\column{0.5\textwidth}
\begin{itemize}
    \itemsep1em
    \item \mintinline{c}|p4est_topidx_t| is an \mintinline{c}|int|.
    \item \mintinline{c}|p4est_locidx_t| is an \mintinline{c}|int|.
    \item \mintinline{c}|p4est_gloidx_t| is a \mintinline{c}|long int|.
    \item \mintinline{c}|p4est_qcoord_t| is an \mintinline{c}|int|.
\end{itemize}
\end{columns}
\end{frame}

\begin{frame}[fragile]{The Tree (p4est.h)}
\begin{minted}[mathescape,frame=lines]{c}
typedef struct p4est_tree
{
  sc_array_t quadrants;
  p4est_quadrant_t first_desc, last_desc;
  p4est_locidx_t quadrants_offset;
  p4est_locidx_t quadrants_per_level[P4EST_MAXLEVEL + 1];
  int8_t maxlevel;
}
p4est_tree_t;
\end{minted}

\begin{itemize}
    \itemsep1em
    \item \mintinline{c}|P4EST_MAXLEVEL| is a constant equal to $30$.
    \item The \mintinline{c}|quadrants| array contains \mintinline{c}|p4est_quadrant_t|.
    \item The \mintinline{c}|quadrants_offset| is local.
\end{itemize}
\end{frame}

\begin{frame}[fragile]{The Forest (p4est.h)}
\begin{columns}
\column{0.6\textwidth}
\begin{minted}[mathescape,frame=lines]{c}
typedef struct p4est
{
  /* ... */
  void *user_pointer;

  p4est_topidx_t first_local_tree;
  p4est_topidx_t last_local_tree;

  p4est_locidx_t local_num_quadrants;
  p4est_gloidx_t global_num_quadrants;
  p4est_gloidx_t *global_first_quadrant;

  /* ... */
  p4est_connectivity_t *connectivity;
  sc_array_t *trees;

  /* ... */
}
p4est_t;
\end{minted}

\column{0.4\textwidth}
\begin{itemize}
    \itemsep1em
    \item The \mintinline{c}|user_pointer| is not touched by p4est.
    \item \mintinline{c}|global_first_quadrant| is an array of size \mintinline{c}|mpisize|.
    \item \mintinline{c}|trees| contains only the local trees.
\end{itemize}
\end{columns}
\end{frame}

\begin{frame}[fragile]{Walking in the Forest (p4est\_bits.h)}
\begin{minted}[mathescape,frame=lines]{c}
void p4est_quadrant_parent (const p4est_quadrant_t * q,
                            p4est_quadrant_t * r);

void p4est_quadrant_sibling (const p4est_quadrant_t * q,
                             p4est_quadrant_t * r, int sibling_id);

void p4est_quadrant_childrenv (const p4est_quadrant_t * q,
                               p4est_quadrant_t c[]);

void p4est_quadrant_face_neighbor (const p4est_quadrant_t * q, int face,
                                   p4est_quadrant_t * r);

uint64_t p4est_quadrant_linear_id (const p4est_quadrant_t * quadrant,
                                   int level);

void p4est_quadrant_set_morton (p4est_quadrant_t * quadrant, int level,
                                uint64_t id);
\end{minted}
\end{frame}

\begin{frame}[fragile]{Connectivity (p4est\_connectivity.h)}
\begin{columns}
\column{0.5\textwidth}
\begin{minted}[mathescape,frame=lines]{c}
typedef struct p4est_connectivity
{
  p4est_topidx_t num_vertices;
  p4est_topidx_t num_trees;
  p4est_topidx_t num_corners;

  double *vertices;
  p4est_topidx_t *tree_to_vertex;
  p4est_topidx_t *tree_to_tree;
  int8_t *tree_to_face;

  /* corner connectivity */
}
p4est_connectivity_t;
\end{minted}

\column{0.5\textwidth}
\begin{itemize}
    \item \mintinline{c}{vertices} contains a list of physical points.
    \item \mintinline{c}{tree_to_vertex} list of indexes into the
    \mintinline{c}{vertices} array that define the root of each tree.
    \item \mintinline{c}{tree_to_tree}: face neighbors for each tree.
    \item \mintinline{c}{tree_to_face}: for each \emph{(tree, face)}, it
    contains the neighboring tree's face + orientation.
\end{itemize}
\end{columns}
\end{frame}

\begin{frame}[fragile]{Connectivity (p4est\_mesh.h)}
\begin{columns}
\column{0.5\textwidth}
\begin{minted}[mathescape,frame=lines]{c}
typedef struct
{
  p4est_locidx_t local_num_quadrants;
  p4est_locidx_t ghost_num_quadrants;

  p4est_topidx_t *quad_to_tree;
  int *ghost_to_proc;

  p4est_locidx_t *quad_to_quad;
  int8_t *quad_to_face;
  sc_array_t *quad_to_half;
  sc_array_t *quad_level;

  /* ... */
}
p4est_mesh_t;
\end{minted}

\column{0.5\textwidth}
\begin{itemize}
    \item \mintinline{c}{quad_to_tree} gives the tree index for each quadrant
    (indexed by the local quadrant index).
    \item \mintinline{c}{quad_to_quad} gives the face neighbor of a quadrant,
    indexed by \mintinline{c}{P4EST_FACES * quadid + face}.
    \item \mintinline{c}{quad_to_face} The neighbor's face ID:
    \begin{itemize}
        \item $v = \{ 0, \dots, 7 \}$: same size neighbor. Encoded as $4 r + f$.
        \item $v = \{ 8, \dots, 24 \}$: double size neighbor. Encoded as
        $8 + 8 h + 4 r + f$.
        \item $v = \{-8, \dots, -1\}$: half size neighbor. $8 + v$ is the same
        as for a neighbor of the same size.
    \end{itemize}

    \item \mintinline{c}{quad_to_half} gives the 2 (or 4) indexes of half-size
    neighbors.
\end{itemize}
\end{columns}
\end{frame}

\begin{frame}[fragile]{Haunted Forest (p4est\_ghost.h)}
\begin{columns}
\column{0.5\textwidth}
\small
\begin{minted}[mathescape,frame=lines]{c}
typedef struct
{
  /* ... */
  sc_array_t ghosts;

  /* ... */
  sc_array_t mirrors;

  /* ... */
}
p4est_ghost_t;

void p4est_ghost_exchange_data (p4est_t * p4est,
                                p4est_ghost_t * ghost,
                                void *ghost_data);

void p4est_ghost_exchange_custom (p4est_t * p4est,
                                  p4est_ghost_t * ghost,
                                  size_t data_size,
                                  void **mirror_data,
                                  void *ghost_data);
\end{minted}

\column{0.5\textwidth}
\vspace{-80px}
\begin{itemize}
    \item \mintinline{c}{ghosts} contains the ghost quadrants (no user\_data).
    Each quadrant has the \mintinline{c}{piggy3} set.
    \item \mintinline{c}{mirrors} contains local quadrants that are ghosts for
    another processor (no user\_data). Each quadrant has the
    \mintinline{c}{piggy3} set.
\end{itemize}
\end{columns}
\end{frame}

\begin{frame}[fragile]{Adapting the Forest (p4est\_extended.h)}
The main functions are:
\begin{minted}[mathescape,frame=lines]{c}
void p4est_refine_ext (p4est_t * p4est, int refine_recursive, int maxlevel,
                       p4est_refine_t refine_fn, p4est_init_t init_fn,
                       p4est_replace_t replace_fn);

void p4est_coarsen_ext (p4est_t * p4est, int coarsen_recursive,
                        int callback_orphans, p4est_coarsen_t coarsen_fn,
                        p4est_init_t init_fn, p4est_replace_t replace_fn);

void p4est_balance_ext (p4est_t * p4est, p4est_connect_type_t btype,
                        p4est_init_t init_fn, p4est_replace_t replace_fn);

p4est_gloidx_t p4est_partition_ext (p4est_t * p4est,
                                    int partition_for_coarsening,
                                    p4est_weight_t weight_fn);
\end{minted}
\end{frame}

\begin{frame}[fragile]{Iterating over the Forest (p4est\_iterate.h)}
An iteration is done using:

\begin{minted}[mathescape,frame=lines]{c}
void p4est_iterate (p4est_t * p4est,
                    p4est_ghost_t * ghost_layer,
                    void *user_data,
                    p4est_iter_volume_t iter_volume,
                    p4est_iter_face_t iter_face,
                    /* p8est_iter_edge_t iter_edge, */
                    p4est_iter_corner_t iter_corner);
\end{minted}

\vfill

\textbf{NOTE}: If the only the \mintinline{c}{iter_volume} callback is
given, an $\mathcal{O}(N_p)$ loop over the leaves is made. Otherwise it does more
complicated loop over all the elements, in
$\mathcal{O}((l_{max} + N_p) \log{N_p})$.

If the mesh is uniform, the second version becomes linear as well.
\end{frame}

\begin{frame}[fragile]{Quadrant Iterator (p4est\_iterate.h)}
\begin{minted}[mathescape,frame=lines]{c}
typedef void (*p4est_iter_volume_t) (p4est_iter_volume_info_t * info,
                                     void *user_data);

typedef struct p4est_iter_volume_info
{
  p4est_t *p4est;
  p4est_ghost_t *ghost_layer;
  p4est_quadrant_t *quad;
  p4est_locidx_t quadid;
  p4est_topidx_t treeid;
}
p4est_iter_volume_info_t;
\end{minted}

\begin{itemize}
    \item \mintinline{c}{quadid} is the index inside the given tree.
\end{itemize}
\end{frame}

\begin{frame}[fragile]{Face Iterator (p4est\_iterate.h)}
\begin{minted}[mathescape,frame=lines]{c}
typedef struct p4est_iter_face_info
{
  p4est_t *p4est;
  p4est_ghost_t *ghost_layer;
  int8_t orientation;
  int8_t tree_boundary;
  sc_array_t sides;
}
p4est_iter_face_info_t;
\end{minted}

\begin{itemize}
    \item \mintinline{c}{orientation} of one face with respect to the other
    (0 or 1 in 2D).
    \item \mintinline{c}{sides} can contain 1 or 2 sides.
    \item For Dirichlet / Neumann boundary conditions there is only one side.
    It is up to the user to provide the value on the other side.
    \item For periodic boundary conditions, it automatically gives the
    quadrant on the other side.
\end{itemize}

\end{frame}

\begin{frame}[fragile]{Face Iterator (p4est\_iterate.h)}
\begin{columns}
\column{0.5\textwidth}
\footnotesize
\vspace{10px}
\begin{minted}[mathescape,frame=lines]{c}
typedef struct p4est_iter_face_side
{
  p4est_topidx_t treeid;
  int8_t face;
  int8_t is_hanging;
  union p4est_iter_face_side_data
  {
    struct
    {
      int8_t is_ghost;
      p4est_quadrant_t *quad;
      p4est_locidx_t quadid;
    }
    full;
    struct
    {
      int8_t is_ghost[2]; /* 4 in 3D */
      p4est_quadrant_t *quad[2];
      p4est_locidx_t quadid[2];
    }
    hanging;
  }
  is;
}
p4est_iter_face_side_t;
\end{minted}

\column{0.5\textwidth}
\begin{itemize}
    \item \mintinline{c}{is_hanging} is a boolean to indicate 1 or 2 quadrants
    on this side.
    \item \mintinline{c}{quadid} is the index inside the tree or ghost array.
\end{itemize}
\end{columns}
\end{frame}

\begin{frame}[fragile]{Face Neighbor Iterator (p4est\_mesh.h)}
\small
\begin{minted}[mathescape,frame=lines]{c}
void p4est_mesh_face_neighbor_init2 (p4est_mesh_face_neighbor_t * mfn,
                                     p4est_t * p4est,
                                     p4est_ghost_t * ghost,
                                     p4est_mesh_t * mesh,
                                     p4est_topidx_t which_tree,
                                     p4est_locidx_t quadrant_id);

p4est_quadrant_t *p4est_mesh_face_neighbor_next (p4est_mesh_face_neighbor_t * mfn,
                                                 p4est_topidx_t * ntree,
                                                 p4est_locidx_t * nquad,
                                                 int *nface, int *nrank);

void *p4est_mesh_face_neighbor_data (p4est_mesh_face_neighbor_t * mfn,
                                     void *ghost_data);
\end{minted}
\end{frame}

\begin{frame}{The End}
\begin{center}\LARGE
For more: \url{http://p4est.github.io/api/}
\end{center}
\end{frame}
\end{document}
