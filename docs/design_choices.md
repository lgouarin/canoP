# Patches

A patch is defined by the ``patch_size`` and the ``patch_ghosts`` members of the
solver struct. To get the total number of cells in the patch, we have to do:

    patch_cells = (patch_size + patch_ghosts)^dim

A patch is initialized in the init_fn function in main.c and is freed in
the ``ohmu_replace_fn`` function in ``ohmu_adapt.c``.

To exchange ghost data using patches, we have to manually construct the
mirrors array (defined in ``p4est_ghosts.h``) and call
``p4est_ghost_exchange_custom`` with a data size of:

    patch_data_size = patch_cells * data_size

where data_size is defined by each plugin once we have a complete definion of
``ohmu_plugin_data_t``.

ohmu_plugin_data_t is the type of one cell in the patch. In here, the plugin
can put any sort of information it requires (like the cell value and the
cell velocity in the case of a simple transport scheme). As the size of this
type is not known before loading the plugin, we cannot use it before.

The ghost data aray in this case will be an array of size

    patch_cells * ghosts.elem_count

containing all the patches in all the ghost cells. To access the patch of
a particular ghost cell, given its index i, we have:

    ghost_data[patch_cells * i]


At the end of the time loop, all patches have to be freed!

# Ghosts

One important aspect of ghost cells in p4est is how to treat the relationship
between the actual p4est_quadrant_t structure and the ghost_data that
is exchanged between processes.

Pre-patches, the treatment was: exchanging the ohmu_qdata_t structure
using p4est_ghost_exchange. This function exchanges the data inside the
user_data pointer of the p4est_quadrant_t struct. Once this data was available,
it was directly inserted into the p4est_quadrant_t user_data pointer and
all further treatment of the quadrant was done as if it was a local quadrant.

This is not an idea situation because p4est already stores information in
the p union of the p4est_quadrant_t struct. More exactly, it stores the
treeid and the local_num in the piggy3 member (see p4est_ghost.h for
more information on this). While not very useful during manipulation, it
is not very clean to just erase this information.

For this reason we have decided not to do it this way anymore. A different
way would be to just leave the ghost_data separate from the quadrants and
access it as needed. This raises the question of how to tell if a quadrant
is a ghost quadrant or not. One way to do this (stolen from the way
p4est_mesh_t operates) is to make sure that the quadrant id (given by
the various iterator structures) is of the form:
    quadid = quadid     if quadid < local_num_quads
             ghostid    if quadid >= local_num_quads

That is to say: if the quadid is smaller that the local number of quadrants,
we know it indexes inside the quadrant list of its specific tree, if, on
the other hands, the quadid is larger, we know that it indexes inside the
ghost list and the index inside this list is:
    ghostid = quadid - local_num_quads.

This way we can correctly get any ghost_data if necessary.

Another option would be to use the pad8 member of the p4est_quadrant_t struct.
This member is used to optimize the padding of the structure and has no
real meaning for p4est or us. Even so, re-purposing it as a flag is already
done in p4est_nodes (it is used as a counter for the number of processes that
contain a node). Since we do not use the p4est_nodes, we can safely use this
flag without fear that it gets overwritten or that we are overwriting some
potentially useful information.

Using the pad8 member as a flag, and the quadid from an iterator struct,
we can safely find the corresponding ghost_data when needed.

Which one to use? Any important differences? Probably not..

## pad8 Ghosts

TODO

# Patch Refinement

In a patch with up to date ghost boundaries, we can compute a refinement
indicator based on some indicators between all the patch cells. To get a
global indicator for the patch, we can:
* take a max over all cells and their neighbors
* take a mean

