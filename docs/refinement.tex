\documentclass[DIV14,11pt,parskip=half*]{scrartcl}
\usepackage[utf8]{inputenc}

\usepackage{amsmath}
\usepackage{fancyhdr}
\usepackage{hyperref}
\usepackage{tikz}

% pretty links
\hypersetup{
    colorlinks=true,
    urlcolor=blue,
    citecolor=black,
    linkcolor=black
}

% Custom headers and footers
\pagestyle{fancyplain}
\fancyhead{}
\fancyfoot[L]{}
\fancyfoot[C]{}
\fancyfoot[R]{\thepage}
\renewcommand{\headrulewidth}{0pt}
\renewcommand{\footrulewidth}{0pt}
\setlength{\headheight}{13.6pt}

%-------------------------------------------------------------------------------
%	TITLE SECTION
%-------------------------------------------------------------------------------

\newcommand{\horrule}[1]{\rule{\linewidth}{#1}}

\title{
\normalfont \normalsize
\horrule{0.5pt} \\[0.4cm]
\huge \textbf{Refinement Criteria Round-up} \\
\horrule{2pt} \\[0.5cm]
}
\author{}
\date{}

%-------------------------------------------------------------------------------
%   SHORTCUTS
%-------------------------------------------------------------------------------

\newcommand{\pd}[2]{\dfrac{\partial #1}{\partial #2}}
\renewcommand{\div}[1]{\nabla \cdot (#1)}
\newcommand{\grad}[1]{\nabla #1}

\begin{document}

\maketitle % Print the title

\section{A.M. Khokhlov - Fully Threaded Tree Algorithms}

This article contains the most exhaustive list of refinement criteria that we
have found. It is focused on the Euler equations:
\begin{equation}\label{eq:euler}
\begin{aligned}
\pd{\rho}{t} + \div{\rho \mathbf{U}} & = 0, \\
\pd{\rho \mathbf{U}}{r} + \div{\rho \mathbf{U} \times \mathbf{U}} + \grad{P} & = \rho \mathbf{g},\\
\pd{E}{t} + \div{(E + P)\mathbf{U}} & = \rho \mathbf{U} \cdot \mathbf{g}.
\end{aligned}
\end{equation}

The article proposes three refinement criteria. The first one is a \textbf{shock indicator}
that is defined as the maximum between:
\[\xi_i^s = \max \xi_{i, j}^s\]
where:
\[
\xi_{i, j}^s =
\begin{cases}
1, & \text{if }\dfrac{|P_j - P_i|}{\min(P_j, P_i)} > \epsilon_s, \delta \cdot (U_{j, k} - U_{i, k}) > 0, \\
0, & \text{otherwise}.
\end{cases}
\]
where the index $j$ denotes a neighbour of the cell $i$, $k$ denotes
the face velocity for each cell and
\[
\delta =
\begin{cases}
1, & \text{if $j$ is even}, \\
-1, & \text{if $j$ is odd}.
\end{cases}
\]
denotes the normal orientation for each face.

Second, we have a \textbf{discontinuity indicator}, that is also defined as the
maximum taken over all the neighbours of:
\[
\xi_{i, j}^d =
\begin{cases}
1, & \text{if }\dfrac{|P_j - P_i|}{\min(P_j, P_i)} < \epsilon_s;
               \dfrac{|\rho_j - \rho_i|}{\min(\rho_j, \rho_i)} > \epsilon_d, \\
0, & \text{otherwise}.
\end{cases}
\]

The article sets both $\epsilon_s = \epsilon_d = 0.2$, but this is a user
defined parameter and could be chosen to best fit the scheme at hand.

Finally, we have a \textbf{gradient indicator} defined as:
\[
\xi_{i, j}^g = \frac{||q_j| - |q_i||}{\max(|q_j|, |q_i|)},
\]
where $q$ can be any of the problem variables. In this case it can be
the mass density, the energy, the pressure, the velocity, the vorticity, etc.

The final indicator for a cell $i$ is taken as the maximum of
\[
\xi_i = \max(\xi_i^s, \xi_i^d, \xi_i^g, \dots) \in [0, 1].
\]

With such an indicator calculated, we can decide whether to refine or coarsen
a cell using the following steps:
\begin{itemize}
    \item Choose $\xi_r$ and $\xi_c$ as refinement and coarsening thresholds
    for all $\xi_i$. These are also user defined parameters.
    \item A leaf is refined if $\xi_i > \xi_r$.
    \item A leaf is coarsened if $\xi_i < \xi_c$.
\end{itemize}

Furthermore, the article proposes a smoothing procedure for the refinement
indicator. This allows for a higher quality mesh refinement. The proposed
solution is to consider the indicator $\xi$ as part of a reaction-diffusion
equation:
\[
\pd{\xi}{t} = K \nabla^2 \xi + Q.
\]
where $K = 2^{-2l}L^2$ ($l$ is the current level and $L$ is the size of a cell
at the highest refinement level) and $Q = 1$ if $\xi > \xi_r$, otherwise it is $0$.

Using this equation, we can advance the time step 2-3 times to smooth out and
propagate the refinement criteria outwards.

\section{R. Teyssier - RAMSES}

RAMSES is an N-body and hydrodymanics code used in a cosmological context.
Besides the usual Euler equations, this code also involves a collisionless
N-body solver described by the equations:
\[
\begin{cases}
\pd{\mathbf{x}_p}{t} & = \mathbf{v}_p, \\
\pd{\mathbf{v}_p}{t} & = -\nabla_x \phi, \\
       \Delta_x \phi & = 4 \pi G \rho.
\end{cases}
\]
where $\mathbf{x}_p$ and $\mathbf{v}_p$ are the position and velocity of a
particle $p$. We also have the potential $\phi$ defined in relation to the
mass density $\rho$.

We won't go into the resolution methods of these equations and so focus only
on the refinement strategy. The refinement is heavily influenced by the N-body
system of equations. The first constraint it imposes on the problem is that
we want a constant number of particles in each cell of the mesh. This is done
by refining a cell if the dark matter density exceeds a level-dependent threshold:
\[
\epsilon_c = M_c \times (\Delta x^l)^{-\text{dim}}
\]
where $M_c$ is the maximum mass (or number of particles) per cell. For the
gas dynamics component, we can use a similar criteria:
\[
\epsilon_b = M_b \times (\Delta x^l)^{-\text{dim}}
\]
where $M_b$ is a per-cell baryonic mass, defined as:
\[
M_b = M_c \frac{\Omega_b}{\Omega_m - \Omega_b}.
\]
where $\Omega_m$ is the matter density and $\Omega_b$ is the baryonic density
(?? not defined).

We also have a gradient-based indicator that is computed over all neighbouring
cells. The cell is refined if this gradient exceeds a fraction of the
central cell variable:
\[
\grad{q_i} \geq C_q \frac{q_i}{\Delta x^l},
\]
where $C_q$ is user defined and $q$ is a relevant variable.

In the code, there are some more gradient-based criteria. These can be
found in \\
\href{https://bitbucket.org/rteyssie/ramses/src/db0cc618481cebb2cfb614acbed1f3c2708d8f8c/trunk/ramses/hydro/godunov\_utils.f90?at=master}{hydro/godunov\_utils.f90:hydro\_refine}. For the mass density $\rho$, we have:
\[
    \xi_g = 2 \max\left(\left| \frac{\rho_+ - \rho}{\rho_+ + \rho}\right|,
                        \left| \frac{\rho - \rho_-}{\rho + \rho_-}\right|
                  \right)
\]
where $\rho_+$ is the value in the right neighbor, $\rho_-$ is the value
in the left neighbor and $\rho$ is the value in the current cell. We also
have an equivalent definition for the pressure $P$.

For the velocity field $V = (u, v, w)$, we have:
\[
    \xi_g = 2 \max\left(\left| \frac{u_+ - u}{c_+ + c + |u_+| + |u|}\right|,
                        \left| \frac{u - u_-}{c + c_- + u + u_-}\right|
                  \right)
\]
where $c$ is the speed of sound, defined by:
\[
    c^2 = \frac{\gamma P}{\rho}.
\]

The cell is refined if any of these values are greater then some threshold,
set individually for each of them.

Finally we have a strictly spacial refinement criterion that does not allow
refinement outside a central sphere where all the action happens.

\section{AMRClaw}

AMRClaw uses a simple criteria based on the values of the two neighbors. For
$q$, some physical value, we have:
\[
    \xi = |q_+ - q_-|.
\]

This is then checked against a user defined threshold.

The code can be found in \href{https://github.com/clawpack/amrclaw/blob/master/src/3d/flag2refine.f}{src/3d/flag2refine.f}.

\section{CLAMR}

The code can be found in \href{https://github.com/losalamos/CLAMR/blob/master/state.cpp}{state.cpp:calc\_refine\_potential}.

CLAMR is an AMR code that solves the shallow water equations. As such, it
defines some gradient-based criteria for the height $H$, and the velocity
$(U, V)$. These are defined as:
\[
    \xi = \max\left(\left|\frac{q_+ - q}{q}\right|, \left|\frac{q - q_-}{q}\right|\right).
\]

These indicators are computed in the $x$ and $y$ directions and the cell is
refined if any of them surpass a thershold. In the case in which one of the
neighbors is refined, we take the average of the two cells that share a
face with the current cell.

\section{Berger, Colella - Local Adaptive Mesh Refinement for Shock Hydronamics}

This paper describes a patch-based AMR method, one of the first papers on the
subject. It also deals with the Euler equations~\ref{eq:euler}.

The article proposes computing the error between the current patch and
a virtual patch that is coarsened by one extra level (each cell is divided by
two in every direction). The error is computed using the formula:
\[
    \frac{Q^2u(x, t) - Q_{2h}u(x, t)}{2^{q + 1} - 2} = \tau + O(h^{q + 2}).
\]

Where $Q$ is the integration method used that is accurate to level $q$ in
both time and space and $\tau$ is some leading term in the approximation.

More exactly, this procedure involves projecting the current grid at a given
level onto a virtual grid coarsened by a factor of two in each direction. The
solution on both grids is the advanced in time: the original grid for two
time steps and the coarsened grid for one time step, twice as large. At
coarse cells where the error given by the above formula is bigger that some
threshold, we refine all the four cells in the refined grid.

This method of doing refinement is much more costly in terms of performance
because we would need to compute a new solution over the whole mesh at
each refining step.

\section{G. Chiavassa - Multi-resolution-based adaptive schemes for HCLs}

AMR: Theory and Applications, p. 137.

NOTE: to read some more, very heavy notation.

\section{T. J. Barth - A posteriori Error Estimation for FV and FE methods}

AMR: Theory and Applications, p. 183

NOTE: mostly FE oriented, but had some operators defined to compute errors.

\section{L. Debreu - A General Adaptive Multi-Resolution Approach to Ocean Modelling}

AMR: Theory and Applications, p. 303

Another patch based approach to AMR. This article proposes a somewhat specific
set of refining criteria. Since the flows in the ocean are considered to be
essentially horizontal, the proposed criteria are all 2D.

Let $V = (u, v)$ be a 2D velocity field. The first criteria is a simple $L^2$
norm of the velocity:
\[
    \xi_1(V) = ||V||_2 = \sqrt{u^2 + v^2}.
\]

Then we have a refining criteria regarding the curl of the velocity:
\[
    \xi_2(V) = ||\nabla \times V|| = \left|\pd{v}{x} - \pd{u}{y}\right|
\]

Finally, we have:
\[
    \xi_3(V) = \max(0, \pd{u}{x}\pd{v}{y} - \pd{v}{x}\pd{u}{y})
\]

$V$ is taken as a vertical average of the velocity field in the flow.

NOTE: these things aren't normalized or anything..

\section{Performance of Vector / Parallel Oriented Hydrodynamic Code}

AMR: Theory and Applications, p. 389.

This article uses the FTT data structure with similar refining criteria. One
new criterion is introduced, also based on the gradient:
\[
    G(q) = \frac{\max(|q_j|, |q_i|)}{\min(|q_j|,|q_i|)} - 1.
\]

Where $q$ is any of the physical variables in the equations. The article
proposes the use of the mass density $\rho$ and the pressure $P$ and refine
a cell if:
\[
    \max(G(\rho), G(P)) > 0.1.
\]

It proposes also a non-local refinement criteria that is computed by
summing up the surrounding neighbors with a simple point-spread function.
\[
    \xi_i = \sum \omega \xi_j
\]
where $\omega$ is a weight function.

For the thresholds, they propose:
\begin{itemize}
    \item $\xi_r = 2.9$
    \item $\xi_c = 2.1$
\end{itemize}

\begin{tikzpicture}
    \coordinate (c0) at (0, 0);
    \coordinate (c1) at (1, 0);
    \coordinate (c2) at (2, 0);

    \foreach \i in {0, 1}{
        \foreach \j in {0, 1}{
            \draw (c0) rectangle (\i * 0.5, \j * 0.5);
        }
    }
\end{tikzpicture}
\end{document}
