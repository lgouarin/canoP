settings = {
   solver_name = "ramses",

   -- the max time of the simulation which will run from [0, tmax]
   tmax = 1.3,

   -- the Courant number used to compute the time step
   cfl = 0.5,

   -- unsplit scheme only for Ramses
   dim_splitting = 0,

   -- scheme space order (1 or 2 = MUSCL)
   space_order = 2,

   -- time order (1 or 2 = Hancock)
   time_order = 2,

   -- type of flux limiter
   -- "minmod", "mc", ...
   limiter = "minmod",

   -- the name of the scheme
   name = "blast-cavity-spherical-explosion",

   -- minimum number of starting quadrants per process
   min_quadrants = 16,

   -- initial refinement is uniform (0, 1)
   uniform_fill = 1,

   -- gather different statistics. see statistics.h.
   statistics = 0,

   -- the minimum level of refinement of the mesh per process
   min_refine = 3,

   -- the maximum level of refinement of the mesh per process
   max_refine = 5,

   -- the threshold for refining
   epsilon_refine = 0.005,

   -- the threshold for coarsening
   epsilon_coarsen = 0.01,

   -- the type of the connectivity. see p4est_connectivity.h and
   -- connectivity.h
   -- 'unit', 'periodic', 'two', ...
   connectivity = 'tetris',

   -- initial condition. options are:
   initial_condition = "blast",

   -- boundary conditions
   -- "reflective", "dirichlet", "neuman", ...
   boundary="reflective",

   -- use an indicator. see indicators.h. options are: rho_gradient,
   -- khokhlov.
   indicator = "khokhlov",

   -- number of output files (-1 means, every time step)
   save_count = -1,

   -- set to 1 to write mpirank, level, treeid, etc. for each quadrant.
   mesh_info = 1,

   -- select which variables to write: rho, rho_gas, rho_liquid, velocity,
   -- pressure, alpha, a.
   write_variables = "rho, rhoV, E_tot",

   -- write floats (= 1) or doubles (= 0) to the h5 files
   single_precision = 0
}

blast = {

   -- disk radius in real units)
   radius = 0.25,
   
   -- center of disk to explode (in real units)
   x_c = 0.5,
   y_c = -0.5,
   z_c = 0.5,
   
   -- density of fluid : inside and outside
   d_in = 1.0,
   d_out = 1.0,

   -- velocity
   velocity_x = 1.5,
   velocity_y = 0.0,
   velocity_z = 0.0,

   -- pressure
   p_in = 10.0,
   p_out = 0.1

}
