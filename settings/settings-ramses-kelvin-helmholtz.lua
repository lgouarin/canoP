settings = {
   solver_name = "ramses",

   -- if you want to perform a restart run
   -- uncomment the following to lines, the restart filename
   -- must come from a previous run ;)
   -- you probably need to change parameter "name" below, so that the restart
   -- run new file do not override the old ones
   
   -- restart_run_enabled = 1,
   -- restart_run_filename = "kh_level7_7_nointerpol_00051_restart.p4est",
   
   
   -- the max time of the simulation which will run from [0, tmax]
   tmax = 2.0,
   
   -- the Courant number used to compute the time step
   cfl = 0.8,
   
   -- unsplit scheme only for Ramses
   dim_splitting = 0,
   
   -- scheme space order (1 or 2 = MUSCL)
   space_order = 2,
   
   -- time order (1 or 2 = Hancock)
   time_order = 2,
   
   -- type of flux limiter (not used for ramses for now)
   -- "minmod", "mc", ...
   limiter = "minmod",
   
   -- type of Riemann solver (for Ramses only)
   riemann_solver = "hllc",
   
   -- the name of the scheme
   name = "kh_level7_7_nointerpol",
   
   -- minimum number of starting quadrants per process
   min_quadrants = 16,
   
   -- initial refinement is uniform (0, 1)
   uniform_fill = 1,
   
   -- gather different statistics. see statistics.h.
   statistics = 0,
   
   -- the minimum level of refinement of the mesh per process
   min_refine = 7,
   
   -- the maximum level of refinement of the mesh per process
   max_refine = 7,
   
   -- the threshold for refining
   epsilon_refine = 0.005,
   
   -- the threshold for coarsening
   epsilon_coarsen = 0.01,
   
   -- the type of the connectivity. see p4est_connectivity.h and
   -- connectivity.h
   -- 'unit', 'periodic', 'two', ...
   -- connectivity = 'unit',
   connectivity = 'periodic',
   
   -- initial condition. options are:
   initial_condition = "kelvin_helmholtz",
   
   -- boundary conditions
   -- "reflective", "dirichlet", "neuman", ...
   boundary="neuman",
   
   -- use an indicator. see indicators.h. options are: rho_gradient,
   -- khokhlov.
   indicator = "khokhlov",
   
   -- number of output files (-1 means, every time step)
   save_count = 50,
   
   -- set to 1 to write mpirank, level, treeid, etc. for each quadrant.
   mesh_info = 1,
   
   -- select which variables to write: rho, rho_gas, rho_liquid, velocity,
   -- pressure, alpha, a.
   write_variables = "rho, rhoV, E_tot",
   
   -- write floats (= 1) or doubles (= 0) to the h5 files
   single_precision = 0
}

--
-- see http://www.astro.princeton.edu/~jstone/Athena/tests/kh/kh.html
--
kelvin_helmholtz = {
   
   -- amplitude of interface initial perturbation
   amplitude = 0.01,
   
   -- perturbation type (0 to deactivate, 1 to activate)
   perturbation_sine = 0,
   perturbation_sine_robertson = 1,
   perturbation_rand = 0,
   
   -- single mode perturbation a la Robertson
   mode = 2,
   w0 = 0.1,
   delta = 0.02,

   -- random seed (only used when perturbation_type is random)
   -- each MPI process get initialized with srand(seed*(mpiRank+1))
   rand_seed = 131,

   -- density of the fluids
   d_in = 2.0,
   d_out = 1.0,

   -- half thickness of the two domain. 
   -- inner_size must be smaller than outer_size.
   inner_size = 0.25,
   outer_size = 0.25,

   -- pressure
   pressure = 2.5

}
