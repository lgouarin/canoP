settings = {
   solver_name = "mfplasma",

   -- the max time of the simulation which will run from [0, tmax]
   tmax = 0.5,
   
   -- the CFL condition
   CFL = 0.9,

   -- the name / prefix of output files
   name = "discontinuity",

   -- minimum number of starting quadrants per process
   min_quadrants = 16,

   -- initial refinement is uniform (0, 1)
   uniform_fill = 1,

   -- gather different statistics. see statistics.h.
   statistics = 0,

   -- the minimum level of refinement of the mesh per process
   min_refine = 3,

   -- the maximum level of refinement of the mesh per process
   max_refine = 5,

   -- the threshold for refining
   epsilon_refine = 0.01,

   -- the threshold for coarsening
   epsilon_coarsen = 0.05,

   -- the type of the connectivity. see p4est_connectivity.h and
   -- connectivity.h
   -- 'unit', 'periodic', 'two', ...
   --connectivity = 'unit',
   connectivity = 'unit',

   -- initial condition. options are:
   -- 'discontinuity', 'vertical discontinuity', 'diagonal discontinuity', 'square discontinuity'
   initial_condition = "discontinuity",

   -- boundary conditions
   -- "reflective", "dirichlet", "neuman", ...
   -- boundary="neuman",
   -- boundary="dirichlet",
   boundary="reflective",

   -- use an indicator. see indicators.h. options are: rho_gradient,
   -- khokhlov.
   indicator = "density_gradient",

   -- number of output files (-1 means, every time step)
   save_count = -1,

   -- set to 1 to write mpirank, level, treeid, etc. for each quadrant.
   mesh_info = 1,

   -- select which variables to write: rho, rho_gas, rho_liquid, velocity,
   -- pressure, alpha, a.
   write_variables = "ne,ni,rhoVe,rhoVi",

   -- write floats (= 1) or doubles (= 0) to the h5 files
   single_precision = 0,

   -- gamma - useless now?
   gamma0 = 1.4,
   
   -- speed of sound electrons and ions
   c_e = 1.0,
   c_i = 1.0,

   -- Charge
   q_e = -1,
   q_i = 1,

   -- Kappa
   kappa = 1.0,

   -- Epsilon
   epsilon = 1.0,

   -- Static electric field
   static_elec_field_enabled = 1,
   static_Ex = 0.0,
   static_Ey = 100.0,
   static_Ez = 0.0,

   -- Static magnetic field
   static_mag_field_enabled = 0,
   static_Bx = 0.0,
   static_By = 0.0,
   static_Bz = 10.0,

   -- Ionization
   ionization_enabled = 0,
   nu_iz = 1.0,

   -- source term - only one should be enabled
   source_term_center_enabled = 0,
   source_term_face_enabled = 0,

   -- Low Mach correction
   low_Mach_correction_enabled_e = 0,
   low_Mach_correction_enabled_i = 0,
   
   -- Mach cutoff
   M_co = 0.01;
}

discontinuity = {
   x = 0.5,
   ne_L = 5,
   ne_R= 1,
   ni_L = 1,
   ni_R = 0.1
}

Vdiscontinuity = {
   y = 0.5,
   ne_L = 5,
   ne_R= 1,
   ni_L = 1,
   ni_R = 0.1
}

squareDiscontinuity = {
   x_c = 0.5, -- x coordinate of the center of the square
   y_c = 0.5, -- x coordinate of the center of the square
   a = 0.4, -- length of the sides
   ne_c = 5, -- density in the center of the square
   ne_ext= 1, -- density in oustside of the square
   ni_c = 1,
   ni_ext = 0.1
}
