-------------------------------------------------------------
--  http://nbviewer.jupyter.org/github/numerical-mooc/numerical-mooc/blob/master/lessons/02_spacetime/02_04_1DBurgers.ipynb
-------------------------------------------------------------
--

settings = {
   solver_name = "burgers",

   -- the max time of the simulation which will run from [0, tmax]
   tmax = 1,

   -- the name / prefix of output files
   name = "ramp",

   -- minimum number of starting quadrants per process
   min_quadrants = 16,

   -- initial refinement is uniform (0, 1)
   uniform_fill = 1,

   -- gather different statistics. see statistics.h.
   statistics = 0,

   -- the minimum level of refinement of the mesh per process
   min_refine = 3,
   -- the maximum level of refinement of the mesh per process
   max_refine = 5,

   -- the threshold for refining
   epsilon_refine = 0.01,

   -- the threshold for coarsening
   epsilon_coarsen = 0.05,

   -- the type of the connectivity. see p4est_connectivity.h and
   -- connectivity.h
   -- 'unit', 'periodic', 'two', ...
   --connectivity = 'unit',
   connectivity = 'periodic',

   -- initial condition. options are:
   initial_condition = "ramp",

   -- boundary conditions
   -- "reflective", "dirichlet", "neuman", ...
   -- boundary="neuman",
   boundary="dirichlet",

   -- use an indicator. see indicators.h. options are: rho_gradient,
   -- khokhlov.
   indicator = "U_gradient",

   -- number of output files (-1 means, every time step)
   save_count = -1,

   -- set to 1 to write mpirank, level, treeid, etc. for each quadrant.
   mesh_info = 1,

   -- select which variables to write: rho, rho_gas, rho_liquid, velocity,
   -- pressure, alpha, a.
   write_variables = "U,V",

   -- write floats (= 1) or doubles (= 0) to the h5 files
   single_precision = 0,

   -- gamma
   gamma0 = 1.4
}

ramp = {
   nu = 0.07,
   sigma = 0.01
}
