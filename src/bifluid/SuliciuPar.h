#ifndef SULICIU_PAR_H_
#define SULICIU_PAR_H_

#include "userdata_bifluid.h"

/**
 * A simple to hold global parameter for the Suliciu numerical scheme.
 *
 * We use constexpr here as these are know at compile time.
 * But just in case we might want to init those 
 * variables at runtime, we will have to use static const variables, instead.
 */
namespace canop {
namespace bifluid {
namespace constants {
  
/* Constant slightly bigger than 1 that is multiplied with the a constant */
constexpr double theta = 1.001;

/* The reference pressure in Pascal */
constexpr double p0[2] = {1e+5, 1e+5 };

/* The reference sound velocity in km / s */
constexpr double c[2] = { 3.400, 15.000 };

/* The reference mass density in kg / m^3 */
constexpr double rho0[2] = { 1e+0, 1e+3 };

/* 
 * Gravitational acceleration !!! should be made a member of solver class so that
 * it can be changed at runtime 
 */
constexpr double gravity = 0.00;

} // namespace constants

/**
 * \brief Given rho1, rho2 and the velocity field, find alpha.
 *
 * The value alpha denotes the volume ratio of each fluid.
 *
 * Under the hypothesis of equal pressures in the two fluids, as well as
 * the hypothesis that our fluids are barotropic, we have that:
 *      p_1 (rho_1) = p_2 (rho_2)
 * which is then equal to:
 *  p_10 + c_1^2 (\tilde{rho}_1 / alpha - rho_10) =
 *  p_20 + c_2^2 (\tilde{rho}_2 / (1 - alpha) - rho_20).
 *
 * This gives the following equation on alpha:
 *      q_0 alpha^2 + (q + q_0) alpha + rho_1 c_1^2 = 0.
 *
 * The values \tilde{rho}_1 and \tilde{rho}_2 are the conservative variables
 * given by the scheme, in effect they are equal to rho_1 * alpha and
 * rho_2 * (1 - alpha).
 *
 * Give the second degree polynomial in alpha, we can solve it and set the
 * next value of alpha to the solution.
 */
double suliciu_thermo_alpha (double arho1, double arho2);

/**
 * \brief Compute the pressure.
 *
 * The pressure for each fluid is given by:
 *    p_k = p_0k + c_k^2 (rho_k - rho_0k)
 * where rho_1 = rho_1 * alpha and rho_2 = rho_2 * (1 - alpha).
 */
double suliciu_thermo_pressure (double arho1, double arho2,
				double alpha);

/**
 * \brief Compute the Wallis sound velocity.
 *
 * The Wallis sound velocity is given simply by the formula:
 *    c^2 = (alpha / gamma_1 + (1 - alpha) / gamma_2)^-1
 * where:
 *    gamma_1 = Y * c_1^2 / alpha
 *    gamma_2 = (1 - Y) * c_2^2 / (1 - alpha)
 * and:
 *    Y = alpha * rho_1 / (alpha * rho_1 + (1 - alpha) * rho_2)
 */
double suliciu_thermo_wallis_sound_velocity (double Y, double alpha);

/**
 * \brief Compute the constant a.
 *
 * The constant a is simply given by the formula:
 *      a = rho * c_wallis
 * where
 *      rho = alpha * rho_1 + (1 - alpha) * rho_2
 * and c_wallis is the Wallis sound velocity computed in
 * thermo_wallis_sound_velocity.
 *
 * The value of a is multiplied by a constant theta that is slightly bigger
 * than 1, to make sure that a respects a subcharacteristic condition.
 */
double suliciu_thermo_a (double arho1, double rho2,
			 double alpha);

// ====================================================================
// ====================================================================
/**
 * Data structure used in the computation of local state (newton.cpp).
 */
struct qdata_sod2_t
{
  
  double Y;
  double pressure;
  double velocity[3];
  double rho;
  double alpha;
  double rho_gas;
  double rho_liquid;
  double cSon;
  
}; // struct qdata_sod2_t

// ====================================================================
// ====================================================================
/**
 *
 */
struct suliciu_variables_t
{
  
  using qdata_t               = Qdata;
  using qdata_variables_t     = QdataVar;
  using qdata_reconstructed_t = QdataRecons;
  
  double rho;      /* alpha * rho1 + (1 - alpha) * rho2 */
  double Y;        /* alpha * rho1 / rho */
  double pressure;
  double a;        /* rho * c_wallis */

  /*
   * non-conservative velocities ordered by direction:
   *  0: normal direction, 1: tangent, 2: tangent
   */
  double velocity[P4EST_DIM];

  void init(qdata_t * data, int face, int vi[3], double dx, double dt, int hancock);
  
}; // struct suliciu_variables_t

/**
 * Riemann solver for Suliciu bifluid numerical scheme.
 * \param[in] wl left  suliciu state
 * \param[in] wr right suliciu state
 * \paral[out] flux 
 */
void
suliciu_riemann_solver (const suliciu_variables_t &wl,
			const suliciu_variables_t &wr,
			QdataVar &flux);

/**
 * Update both sides of a face.
 */
void
suliciu_update_face (Qdata *data[2],
		     int face[2], double dt,
                     double dx, double scale_gradient,
		     double scale_flux,
                     int direction, int hancock);

} // namespace canop

} // namespace bifluid

#endif // SULICIU_PAR_H_
