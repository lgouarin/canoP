#ifndef USERDATA_BIFLUID_H
#define USERDATA_BIFLUID_H

#ifdef USE_3D
#include <p4est_to_p8est.h>
#endif

#ifndef P4_TO_P8
#include <p4est.h>
#else
#include <p8est.h>
#endif

#ifndef NUM_FLUIDS
#define NUM_FLUIDS 2
#endif


namespace canop {

namespace bifluid {

/**
 * number of fields in qdata
 * rho1 + rho2 + rho + velocity + alpha + pressure + a 
 */
constexpr int QDATA_NUM_VARIABLES = (NUM_FLUIDS) + 1 + P4EST_DIM + 3;

/**
 * \brief Vector of conserved variables.
 *
 *  We have
 *      arho[0] = alpha * rho1
 *      arho[1] = (1 - alpha) * rho2
 *      velocity = (u * rho, v * rho, w * rho)
 *  with
 *      rho = alpha * rho1 + (1 - alpha) * rho2
 *  and the extra variables
 *      pressure
 *      alpha
 *      a
 *  that do not have update formulas but get recomputed from the new
 *  values of the other variables.
 */
struct QdataVar
{
  
  double              arho[NUM_FLUIDS];
  double              momentum[P4EST_DIM];
  
  double              pressure;
  double              alpha;
  double              a;
  
}; // struct QdataVar

inline void QDATA_INFOF(const QdataVar& w) {
  printf("rho1 %g | rho2 %g | u [%g, %g] | alpha %g | p %g | a %g\n",
	 w.arho[0], w.arho[1], w.momentum[0], w.momentum[1],
	 w.alpha, w.pressure, w.a);
}

struct QdataRecons
{
  
  double              arho[NUM_FLUIDS];
  double              velocity[P4EST_DIM];
  
}; // struct QdataRecons

struct Qdata
{
  
  QdataVar   w;        /* variable at the current time */
  QdataVar   wnext;    /* variable at tn + dt */

  QdataRecons delta[P4EST_DIM];       /* reconstructed gradients */
  
}; // struct Qdata

struct Qdata_sod2
{
  double	  Y;
  double	  pressure;
  double	  velocity[3];
  double	  rho;
  double	  alpha;
  double	  rho_gas;
  double	  rho_liquid;
  double	  cSon;
}; // Qdata_sod2

} // namespace bifluid

} // namespace canop

#endif /* USERDATA_BIFLUID_H */
