#ifndef BOUNDARY_CONDITIONS_FACTORY_BIFLUID_H_
#define BOUNDARY_CONDITIONS_FACTORY_BIFLUID_H_

#include "BoundaryConditionsFactory.h"
#include "bifluid/userdata_bifluid.h"

namespace canop {

namespace bifluid {

/**
 *
 */
class BoundaryConditionsFactoryBifluid : public BoundaryConditionsFactory<Qdata>
{

  using qdata_t = Qdata;
  
public:
  BoundaryConditionsFactoryBifluid();
  virtual ~BoundaryConditionsFactoryBifluid() {};
  
}; // class BoundaryConditionsFactoryBifluid

} // namespace bifluid

} // namespace canop

#endif // BOUNDARY_CONDITIONS_FACTORY_BIFLUID_H_
