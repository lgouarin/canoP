#ifndef INDICATOR_FACTORY_BIFLUID_H_
#define INDICATOR_FACTORY_BIFLUID_H_

#include "IndicatorFactory.h"

#include <bifluid/userdata_bifluid.h>

namespace canop {

namespace bifluid {
/**
 * Concrete implement of an indicator factory for the Bifluid scheme.
 *
 * Valid indicator names are "khokhlov" and "rho_gradient".
 * use method registerIndicator to add another one.
 */
class IndicatorFactoryBifluid : public IndicatorFactory<Qdata>
{

public:
  IndicatorFactoryBifluid();
  virtual ~IndicatorFactoryBifluid() {};
  
}; // class IndicatorFactoryBifluid

} // namespace bifluid

} // namespace canop

#endif // INDICATOR_FACTORY_BIFLUID_H_
