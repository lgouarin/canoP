#include "BoundaryConditionsFactoryBifluid.h"

#include "SolverBifluid.h"
#include "UserDataManagerBifluid.h"
#include "quadrant_utils.h"

// remember that qdata_t is here a concrete type defined in userdata_bifluid.h

#include "bifluid/userdata_bifluid.h"
#include "ConfigReader.h"

namespace canop {

namespace bifluid {

using qdata_t               = Qdata;
using qdata_reconstructed_t = QdataRecons;

// =======================================================
// ======BOUNDARY CONDITIONS CALLBACKS====================
// =======================================================


// =======================================================
// =======================================================
/*
 * TODO: maybe just make the boundary functions return a qdata_variables_t
 * that they have initialized and then we do the copying around.
 *
 * TODO: for order 2, how does delta have to be initialized? does it?
 */
void
bc_bifluid_dirichlet (p4est_t * p4est,
		      p4est_topidx_t which_tree,
		      p4est_quadrant_t * q,
		      int face,
		      qdata_t * ghost_qdata)
{

  UNUSED (p4est);
  UNUSED (which_tree);
  UNUSED (q);
  UNUSED (face);
  UNUSED (ghost_qdata);

} // bc_bifluid_dirichlet

// =======================================================
// =======================================================
void
bc_bifluid_neuman (p4est_t * p4est,
		   p4est_topidx_t which_tree,
		   p4est_quadrant_t * q,
		   int face,
		   qdata_t * ghost_qdata)
{
  UNUSED (p4est);
  UNUSED (which_tree);
  UNUSED (face);

  // retrieve our Solver, UserDataManager and finaly our quadrant data
  SolverBifluid *solver = static_cast<SolverBifluid*>(solver_from_p4est (p4est));

  UserDataManagerBifluid
    *userdata_mgr = static_cast<UserDataManagerBifluid *> (solver->get_userdata_mgr());

  qdata_t *qdata = userdata_mgr->quadrant_get_qdata (q);

  /* 
   * copy the entire qdata is necessary for bifluid scheme
   * so that we have the slope and MUSCL-Hancock extrapoled states
   * and can perform the trace operation
   */
  userdata_mgr->qdata_copy(ghost_qdata, qdata);

} // bc_bifluid_neuman

// =======================================================
// =======================================================
void
bc_bifluid_reflective (p4est_t * p4est,
		       p4est_topidx_t which_tree,
		       p4est_quadrant_t * q,
		       int face,
		       qdata_t * ghost_qdata)
{
  UNUSED (p4est);
  UNUSED (which_tree);

  /* get face normal direction */
  int                 direction = face / 2;

  /* get current cell data */
  // retrieve our Solver, UserDataManager and finaly our quadrant data
  SolverBifluid *solver = static_cast<SolverBifluid*>(solver_from_p4est (p4est));
  UserDataManagerBifluid
    *userdata_mgr = static_cast<UserDataManagerBifluid *> (solver->get_userdata_mgr());
  qdata_t *qdata = userdata_mgr->quadrant_get_qdata (q);

  /* copy current into the ghost outside-boundary cell */
  userdata_mgr->qdata_copy(ghost_qdata, qdata);

  /* reverse the normal velocity */
  ghost_qdata->w.momentum[direction] = -ghost_qdata->w.momentum[direction];
  ghost_qdata->wnext.momentum[direction] = -ghost_qdata->wnext.momentum[direction];

} // bc_bifluid_reflexive

// =======================================================
// =======================================================
void
bc_bifluid_open_tube (p4est_t * p4est,
		      p4est_topidx_t which_tree,
		      p4est_quadrant_t * q,
		      int face,
		      qdata_t * ghost_qdata)
{
  UNUSED (p4est);
  UNUSED (which_tree);

  if (face == 0 || face == 1) {
    bc_bifluid_neuman (p4est, which_tree, q, face, ghost_qdata);
  }
  else {
    bc_bifluid_reflective (p4est, which_tree, q, face, ghost_qdata);
  }
} // bc_bifluid_open_tube

// =======================================================
// =======================================================
void
bc_bifluid_open_box (p4est_t * p4est,
		     p4est_topidx_t which_tree,
		     p4est_quadrant_t * q,
		     int face,
		     qdata_t * ghost_qdata)
{
  UNUSED (p4est);
  UNUSED (which_tree);

  if (face == 3) {
    bc_bifluid_neuman (p4est, which_tree, q, face, ghost_qdata);
  }
  else {
    bc_bifluid_reflective (p4est, which_tree, q, face, ghost_qdata);
  }
} // bc_bifluid_open_box

// =================================================================
// ======CLASS BoundaryConditionsFactoryBifluid IMPL ======
// =================================================================
BoundaryConditionsFactoryBifluid::BoundaryConditionsFactoryBifluid() :
  BoundaryConditionsFactory<Qdata>()
{

  // register the above callback's
  registerBoundaryConditions("dirichlet" , bc_bifluid_dirichlet);
  registerBoundaryConditions("neuman"    , bc_bifluid_neuman);
  registerBoundaryConditions("reflective", bc_bifluid_reflective);
  registerBoundaryConditions("open_tube" , bc_bifluid_open_tube);
  registerBoundaryConditions("open_box"  , bc_bifluid_open_box);


} // BoundaryConditionsFactoryBifluid::BoundaryConditionsFactoryBifluid

} // namespace bifluid

} // namespace canop
