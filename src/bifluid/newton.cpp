#include "newton.h"
#include "QuadrantNeighborUtils.h"
#include "userdata_bifluid.h"

//#include "scheme_bifluid.h"

namespace canop {

namespace bifluid {

static double
RhoGas (double p)
{
  using namespace constants;
  return rho0[0] + 1/SC_SQR(c[0])*(p-p0[0]);
}

static double
RhoLiquid (double p)
{
  using namespace constants;
  return rho0[1] + 1/SC_SQR(c[1])*(p-p0[1]);
}

static double
Alpha(double Y, double p)
{
  double rho_0 = RhoGas(p);
  double rho_1 = RhoLiquid(p);
  return Y * rho_1 / (Y * rho_1 + (1-Y) * rho_0);
}

static double
RhoMix(double Y, double p)
{
  return Alpha(Y,p) * RhoGas(p) + (1-Alpha(Y,p)) * RhoLiquid(p);
}

static double
CWallis (double Y, double p)
{
  using namespace constants;
  double rho_0 = RhoGas(p);
  double rho_1 = RhoLiquid(p);
  double alpha = Alpha(Y,p);
  double rho   = RhoMix(Y,p);
  
  return 1.0 / sqrt(rho * (alpha/(rho_0 * SC_SQR(c[0])) + (1-alpha)/(rho_1 * SC_SQR(c[1]))));
}

static double
PMin ()
{
  using namespace constants;
  return fmax(p0[0] - SC_SQR(c[0])*rho0[0] , p0[1] - SC_SQR(c[1])*rho0[1])+0.01;
}
  

 /*
static double
integrate_Riemann_invariant(double p, double Y)
{
  // function that computes the integral of -1/(rho * c) between p0 and p
  
  double rho_gasLoc, rho_liquidLoc, rhoLoc,	alphaLoc, cLoc;
  double pLoc = p0[0];
  double dp = (p-pLoc)/1.0e6;
  
  double integ = 0.0;
  
  while (fabs(pLoc-p)>fabs(dp))
  {
	rho_gasLoc    = rho0[0] + 1/SC_SQR(c[0])*((pLoc + dp/2) - p0[0]);
	rho_liquidLoc = rho0[1] + 1/SC_SQR(c[1])*((pLoc + dp/2) - p0[1]);
	alphaLoc      = Y*rho_liquidLoc / ((1-Y)*rho_gasLoc + Y*rho_liquidLoc);
	rhoLoc		  = alphaLoc * rho_gasLoc / Y;
	cLoc		  = suliciu_thermo_wallis_sound_velocity (Y, alphaLoc);
	
	integ -= 1/(rhoLoc * cLoc) * dp;

	pLoc += dp;
  }
  
  return integ;
}
*/



static double
simpson_integrate( double Y, double p )
{
  using namespace constants;
  double rhoLocA, rhoLocB, rhoLocC;
  double cLocA, cLocB, cLocC;
  double fA, fB, fC;
  double pLoc = p0[0];
  double dp = (p-pLoc)/1.0e6;
  
  double integ = 0.0;
  
  while (fabs(pLoc-p)>fabs(dp)) {
    
      rhoLocA = RhoMix(Y,pLoc);
      cLocA   = CWallis(Y,pLoc);
      fA      = -1/(rhoLocA*cLocA);
      
      rhoLocB = RhoMix(Y,pLoc+dp/2);
      cLocB   = CWallis(Y,pLoc+dp/2);
      fB      = -1/(rhoLocB*cLocB);
      
      rhoLocC = RhoMix(Y,pLoc+dp);
      cLocC   = CWallis(Y,pLoc+dp);
      fC      = -1/(rhoLocC*cLocC);
      
      integ += dp/6*(fA + 4*fB + fC);
      
      pLoc += dp;
      
  }
  
  return integ;
  
} // simpson_integrate


double
PressureStar(double Y, double p, double uL, double uR)
{
  using namespace constants;
  double pS0 = PMin();
  double pS1 = 11/10*p0[0];
  double pS2 = 0.5*(pS0+pS1);
  
  double f0, f1, f2;
  
  while (fabs(pS0-pS1)>1e-7){
	
	pS2 = 0.5*(pS0+pS1);
	
	f0 = uL - uR + 2 * simpson_integrate(Y,pS0) - 2* simpson_integrate(Y,p);
	f1 = uL - uR + 2 * simpson_integrate(Y,pS1) - 2* simpson_integrate(Y,p);
	f2 = uL - uR + 2 * simpson_integrate(Y,pS2) - 2* simpson_integrate(Y,p);
	
	SC_CHECK_ABORTF(f0*f1<0,"Problem with dichotomia method ! pMin = %g ; f0 = %g ; f1 = %g",PMin(),f0,f1);
	
	if (f0*f2<0){
	  pS1 = pS2;}
	else {
	  pS0 = pS2;}
	
	CANOP_GLOBAL_INFOF("f0 is here : %18.14e       pS0 - pS1 = %18.14e \n",f0, pS0-pS1);
  }
  return pS0;
  
}

double
CSonStar (double Y, double pStar)
{
  return CWallis(Y,pStar);
}

double
VelocityStar (double Y, double p, double pStar, double uL, double uR)
{
  UNUSED(uR);
  double lStar = simpson_integrate(Y,pStar);
  double l0 = simpson_integrate(Y,p);
  
  return uL - l0 + lStar;
}

static double
PressureRaref(double Y, double p, double xi, double v0, double sign)
{
  using namespace constants;
  double pS0 = PMin();
  double pS1 = 11/10*p0[0];
  double pS2 = 0.5*(pS0+pS1);
  
  double f0, f1, f2;
  
  while (fabs(pS0-pS1)>1e-7) {
    
    pS2 = 0.5*(pS0+pS1);
    
    f0 = xi - v0 - sign*CWallis(Y,pS0) + sign*simpson_integrate(Y,pS0) - sign*simpson_integrate(Y,p);
    f1 = xi - v0 - sign*CWallis(Y,pS1) + sign*simpson_integrate(Y,pS1) - sign*simpson_integrate(Y,p);
    f2 = xi - v0 - sign*CWallis(Y,pS2) + sign*simpson_integrate(Y,pS2) - sign*simpson_integrate(Y,p);
    
    SC_CHECK_ABORTF(f0*f1<0,"Problem with dichotomia method ! f0 = %g ; f1 = %g",f0,f1);
    
    if (f0*f2<0){
      pS1 = pS2;}
    else {
      pS0 = pS2;}
    
    CANOP_GLOBAL_INFOF("f0 = %18.14e  ;  pS0 - pS1 = %18.14e \n",f0, pS0-pS1);
    
  }
  
  CANOP_GLOBAL_INFOF("Pressure computation : pLoc = %18.14e  \n",pS0);
  
  return pS0; 
  
}

static double
VelocityRaref(double Y, double p, double pLoc, double v0, double sign)
{
  return v0 + sign*simpson_integrate(Y,p) - sign*simpson_integrate(Y,pLoc);
}

void
StateLoc(double Y, double p, 
	 double xi, double pStar, double uStar, double cStar, 
	 double uL, double uR, qdata_sod2_t *local_state)
{
  double C1 = uL - CWallis(Y,p);
  double C2 = uL - cStar;
  double C3 = uStar;
  double C4 = uR + cStar;
  double C5 = uR + CWallis(Y,p);
  
  CANOP_GLOBAL_INFOF("C1 = %18.14e \n C2 = %18.14e \n C3 = %18.14e \n C4 = %18.14e \n C5 = %18.14e \n",
				  C1, C2, C3, C4, C5);
  
  double pressure = p;
  double velocity = 0;
  
  if (xi < C1) {
    pressure = p;
    velocity = uL;
  } else if (xi < C2) {
    pressure = PressureRaref(Y,p,xi,uL,-1);
    velocity = VelocityRaref(Y,p,pressure,uL,-1);
  } else if (xi < C3) {
    pressure = pStar;
    velocity = uStar;
  } else if (xi < C4) {
    pressure = pStar;
    velocity = uStar;
  } else if (xi < C5) {
    pressure = PressureRaref(Y,p,xi,uR,1);
    velocity = VelocityRaref(Y,p,pressure,uR,1);
  } else {
    pressure = p;
    velocity = uR;
  }
  
  local_state->pressure    = pressure;
  local_state->velocity[0] = velocity;
  local_state->alpha       = Alpha(Y,pressure);
  local_state->rho_gas     = RhoGas(pressure);
  local_state->rho_liquid  = RhoLiquid(pressure);
  local_state->rho         = RhoMix(Y,pressure);
  
} // StateLoc

/*
static double
deriv_sound_pressure(double rho, double Y, double cWallis, double p)
{
  // computes the derivative of sound velocity along pressure
  double derivee = 1.0;
  
  double rho_gas, rho_liquid;
  double H, K;
  double dH, dK;
  double drho;
  
  rho_gas    = rho0[0] + 1/SC_SQR(c[0])*(p - p0[0]);
  rho_liquid = rho0[1] + 1/SC_SQR(c[1])*(p - p0[1]);
  
  K    = Y/rho_gas + (1-Y)/rho_liquid; 
  dK   = - (Y/SC_SQR(c[0]*rho_gas) + (1-Y)/SC_SQR(c[1]*rho_liquid));
  
  drho = -rho/K * dK;
  
  H    = rho * (Y/SC_SQR(c[0]*rho_gas) + (1-Y)/SC_SQR(c[1]*rho_liquid));
  dH   = (Y/SC_SQR(rho_gas*c[0]) + (1-Y)/SC_SQR(rho_liquid*c[1])) * drho -
		 2 * rho * (Y/(SC_SQR(SC_SQR(c[0]) * rho_gas) * rho_gas) + 
					(1-Y)/(SC_SQR(SC_SQR(c[1]) * rho_liquid) * rho_liquid));
  
  derivee = -cWallis / (2*H) * dH;

  return derivee;
}
*/

/*
double 
solve_sod2_pressureStar(qdata_sod2_t l_state, qdata_sod2_t r_state)
{
  // solve intermediate state for pressure according to its left and right values using a newton-raphson algorithm
  
  double integ_l, integ_lStar;
  double integ_r, integ_rStar;
  double integ_l0, integ_r0;
  double integ_l1, integ_r1;
  
  qdata_sod2_t state_l0;
  qdata_sod2_t state_r0;
  
  double pStar0 = 0.75*l_state.pressure;
  double pStar1 = 1.5 *r_state.pressure;
  
  double fonction, deriv;
  double fonction0, fonction1;
  
  integ_l = integrate_Riemann_invariant(l_state.pressure, l_state.Y);
  integ_r = integrate_Riemann_invariant(r_state.pressure, r_state.Y);
  
  double u_l = l_state.velocity[0];
  double u_r = r_state.velocity[0];

  // Y is invariant
  state_l0.Y = l_state.Y;
  state_r0.Y = r_state.Y;

  integ_l0 = integrate_Riemann_invariant(pStar0, state_l0.Y);
  integ_r0 = integrate_Riemann_invariant(pStar0, state_r0.Y);

  fonction = u_l - u_r - integ_l - integ_r + integ_l0 + integ_r0;
  
  fonction0 = u_l - u_r - integ_l - integ_r + integ_l0 + integ_r0;
  
  while (fabs(fonction0)>1.0e-6)
  {
	integ_l0 = integrate_Riemann_invariant(pStar0, state_l0.Y);
	integ_r0 = integrate_Riemann_invariant(pStar0, state_r0.Y);
	integ_l1 = integrate_Riemann_invariant(pStar1, l_state.Y);
	integ_r1 = integrate_Riemann_invariant(pStar1, r_state.Y);
*/	
	/*state_l0.rho_gas    = rho0[0] + 1/SC_SQR(c[0])*(pStar0 - p0[0]);
	state_r0.rho_gas    = state_l0.rho_gas;
	state_l0.rho_liquid = rho0[1] + 1/SC_SQR(c[1])*(pStar0 - p0[1]);
	state_r0.rho_liquid = state_l0.rho_liquid;
	
	state_l0.alpha    = state_l0.Y*state_l0.rho_liquid / 
					  ( (1-state_l0.Y) * state_l0.rho_gas + 
					   state_l0.Y * state_l0.rho_liquid );
	state_r0.alpha    = state_r0.Y * state_r0.rho_liquid / 
					  ( (1-state_r0.Y) * state_r0.rho_gas + 
					   state_r0.Y * state_r0.rho_liquid );
	
	state_l0.rho  =  state_l0.alpha * state_l0.rho_gas / state_l0.Y;
	state_r0.rho  =  state_r0.alpha * state_r0.rho_gas / state_r0.Y;
	
	state_l0.cSon = suliciu_thermo_wallis_sound_velocity (state_l0.Y, state_l0.alpha);
	state_r0.cSon = suliciu_thermo_wallis_sound_velocity (state_r0.Y, state_r0.alpha);
	*/
	
	//fonction = u_l - u_r - integ_l - integ_r + integ_l0 + integ_r0;
/*	
	fonction0 = u_l - u_r - integ_l - integ_r + integ_l0 + integ_r0;
	fonction1 = u_l - u_r - integ_l - integ_r + integ_l1 + integ_r1;
	
	//deriv    = - 1/(state_l0.rho * state_l0.cSon) - 1/(state_r0.rho * state_r0.cSon);
  
	if (fabs(fonction0)>fabs(fonction1)){
	  pStar0 = (pStar1+pStar0)/2;}
	
	else {
	  pStar1 = (pStar1+pStar0)/2;}
	
	//pStar1 = pStar0;
	//pStar0 -= fonction / deriv;
	
	
	CANOP_GLOBAL_INFOF("  --  Function = %18.14e",fonction0);
	CANOP_GLOBAL_INFOF("  --  pStar0 = %18.14e  \n",pStar0);
	
  } 
  
  return pStar0;
  
}
*/

/*
double
solve_sod2_velocityStar(qdata_sod2_t l_state, double pStar)
{
  // solve intermediate state for velocity
  
  double integ_l, integ_lStar;
  
  integ_l     = integrate_Riemann_invariant(l_state.pressure, l_state.Y);
  integ_lStar = integrate_Riemann_invariant(pStar, l_state.Y);
  
  return l_state.velocity[0] + integ_lStar - integ_l;

}
*/
 
 /*
double
solve_sod2_pressure(double xi, qdata_sod2_t state, double pStar, int sign)
{
	// for a given xi returns the coresponding pressure 
  double pStar0 = state.pressure;
  double pStar1 = pStar;
  
  double integ, integ0;
  
  double fonction, deriv;
  qdata_sod2_t local_state;
  
  local_state.Y = state.Y;
  integ = sign * integrate_Riemann_invariant(state.pressure, state.Y);
  
  double u = state.velocity[0];

  integ0 = sign * integrate_Riemann_invariant(pStar0,state.Y);
  
  fonction = u + integ - integ0 + sign * state.cSon - xi;
  
  
  while (fabs(fonction) > 1e-6)
  {
	integ0 = sign * integrate_Riemann_invariant(pStar0,local_state.Y);
	
	local_state.rho_gas    = rho0[0] + 1/SC_SQR(c[0])*(pStar0 - p0[0]);
	local_state.rho_liquid = rho0[1] + 1/SC_SQR(c[1])*(pStar0 - p0[1]);
	
	local_state.alpha    = local_state.Y * local_state.rho_liquid / 
						  ( (1-local_state.Y) * local_state.rho_gas + 
						   local_state.Y * local_state.rho_liquid );
	
	local_state.rho = local_state.alpha * local_state.rho_gas / local_state.Y;
	
	local_state.cSon = suliciu_thermo_wallis_sound_velocity (local_state.Y, local_state.alpha);
	
	
	fonction = u + integ - integ0 + sign * local_state.cSon - xi;
	deriv    = sign / (local_state.rho * local_state.cSon) - 
			   sign * deriv_sound_pressure(local_state.rho,local_state.Y,local_state.cSon,pStar0);
	
	pStar1 = pStar0;
	pStar0 -= fonction / deriv;
  } 
  
  return pStar0;
  
}
*/


} // namespace canop

} // namespace bifluid
