#include "IndicatorFactoryBifluid.h"
#include "utils.h" // for UNUSED macro
#include "SuliciuPar.h"  // for suliciu_thermo_* routines

#include <cmath>

namespace canop {

namespace bifluid {

// needs a concrete qdata_t implementation
using qdata_t = Qdata;

// =======================================================
// =======================================================
double
indicator_bifluid_khokhlov (qdata_t * cella, qdata_t * cellb,
			    indicator_info_t * info)
{
  UNUSED (info);

  double              rho[2] = {
    cella->w.arho[0] + cella->w.arho[1],
    cellb->w.arho[0] + cellb->w.arho[1]
  };

  double              gradient = 0;
  gradient = indicator_scalar_gradient (rho[0], rho[1]);

  double              p[2] = { 0.0, 0.0 };
  double              tmp = 0;

  p[0] =
    suliciu_thermo_pressure (cella->w.arho[0], cella->w.arho[1],
                             cella->w.alpha);
  p[1] =
    suliciu_thermo_pressure (cellb->w.arho[0], cellb->w.arho[1],
                             cellb->w.alpha);

  tmp = indicator_scalar_gradient (p[0], p[1]);
  gradient = fmax (tmp, gradient);

  return fmax (fmin (gradient, 1.0), 0.0);
  
} // indicator_bifluid_khokhlov

// =======================================================
// =======================================================
double
indicator_bifluid_rho_gradient (qdata_t * cella, qdata_t * cellb,
			       indicator_info_t * info)
{
  UNUSED (info);
  double              rho[2] = {
    cella->w.arho[0] + cella->w.arho[1],
    cellb->w.arho[0] + cellb->w.arho[1]
  };

  return indicator_scalar_gradient (rho[0], rho[1]);
  
} // indicator_bifluid_rho_gradient

// =======================================================
// =======================================================
double
indicator_bifluid_alpha(qdata_t * cella, qdata_t * cellb,
			indicator_info_t * info)
{

  UNUSED (info);

  return indicator_scalar_gradient (cella->w.alpha, cellb->w.alpha);
    
} // indicator_bifluid_alpha

// =======================================================
// =======================================================
double
indicator_bifluid_two_fluids (qdata_t * cella, qdata_t * cellb,
			      indicator_info_t * info)
{
  UNUSED (info);
  
  double              delta_alpha = 0.0;
  double              delta_Y = 0.0;
  double              delta_pressure = 0.0;
  double              delta_velocity = 0.0;

  double              rhoa = 0.0;
  double              rhob = 0.0;
  double              Ya = 0.0;
  double              Yb = 0.0;
  double              pressurea = 0.0;
  double              pressureb = 0.0;
  double              vxa = 0.0;
  double              vxb = 0.0;
  double              vya = 0.0;
  double              vyb = 0.0;
#ifdef P4_TO_P8
  double              vza = 0.0;
  double              vzb = 0.0;
#endif

  double              indic = 0.0;

  delta_alpha = fabs (cella->w.alpha - cellb->w.alpha);

  rhoa = cella->w.arho[0] + cella->w.arho[1];
  rhob = cellb->w.arho[0] + cellb->w.arho[1];
  Ya = cella->w.arho[0] / rhoa;
  Yb = cellb->w.arho[0] / rhob;
  delta_Y = fabs (Ya - Yb);

  pressurea = cella->w.pressure;
  pressureb = cellb->w.pressure;
  delta_pressure = fabs (pressurea - pressureb) / fmax (pressurea, pressureb);

  vxa = cella->w.momentum[0] / rhoa;
  vxb = cellb->w.momentum[0] / rhob;

  vya = cella->w.momentum[1] / rhoa;
  vyb = cellb->w.momentum[1] / rhob;

#ifdef P4_TO_P8
  vza = cella->w.momentum[2] / rhoa;
  vzb = cellb->w.momentum[2] / rhob;
#endif

  delta_velocity = fabs (vxa - vxb);    // / fmax (fabs(vxa), fabs(vxb));
  delta_velocity = fmax (delta_velocity, fabs (vya - vyb));     // / fmax (fabs(vya), fabs(vyb)));
#ifdef P4_TO_P8
  delta_velocity = fmax (delta_velocity, fabs (vza - vzb));     // / fmax (fabs(vza), fabs(vzb)));
#endif

  indic = fmax (delta_alpha, delta_Y);
  indic = fmax (indic, delta_pressure);
  indic = fmax (indic, delta_velocity);

  return indic;

} // indicator_bifluid_two_fluids

// =======================================================
// ======CLASS IndicatorFactoryBifluid IMPL ===============
// =======================================================

IndicatorFactoryBifluid::IndicatorFactoryBifluid() :
  IndicatorFactory()
{

  // register the above callback's
  registerIndicator("khokhlov"    , indicator_bifluid_khokhlov);
  registerIndicator("rho_gradient", indicator_bifluid_rho_gradient);
  registerIndicator("alpha"       , indicator_bifluid_alpha);
  registerIndicator("two_fluids"  , indicator_bifluid_two_fluids);
  
} // IndicatorFactoryBifluid::IndicatorFactoryBifluid

} // namespace canop

} // namespace bifluid
