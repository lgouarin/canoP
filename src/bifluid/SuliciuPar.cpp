#include "SuliciuPar.h"

#include <p4est_base.h>
#include <sc.h>
#include "canoP_base.h"
#include "utils.h" // for ISFUZZYNULL

namespace canop {

namespace bifluid {

// const double SuliciuPar::theta   = 1.001;
// const double SuliciuPar::p0[2]   = { 1e+5, 1e+5 };
// const double SuliciuPar::c[2]    = { 3.400, 15.000 };
// const double SuliciuPar::rho0[2] = { 1e+0, 1e+3 };
// const double SuliciuPar::gravity = 0.00;

// ====================================================================
// ====================================================================
double
suliciu_thermo_alpha (double arho1, double arho2)
{
  using namespace constants;
  
  /*
   * solves:
   *    q0 * alpha^2 - (q + q0) * alpha + rho_1 * c_1^2;
   * for alpha, with q and q0 defined below.
   */
  double              alpha = 0.0;
  double              q = 0.0;
  double              q0 = 0.0;
  double              delta = 0.0;

  /* compute the two constants */
  q0 =
    p0[1] - p0[0] +
    rho0[0] * SC_SQR (c[0]) -
    rho0[1] * SC_SQR (c[1]);
  q =
    SC_SQR (c[0]) * arho1 +
    SC_SQR (c[1]) * arho2;

  /* compute the delta */
  delta = SC_SQR (q0 + q) - 4 * q0 * arho1 * SC_SQR (c[0]);
  SC_CHECK_ABORTF (delta > 0, "delta is negative: %18.14e\n", delta);

  /* find the root in the two cases */
  if (ISFUZZYNULL (q0)) {
    CANOP_ASSERT (!ISFUZZYNULL (q));

    alpha = arho1 * SC_SQR (c[0]) / q;
  }
  else if (ISFUZZYNULL (arho1)) {
    alpha = 0;
  }
  else {
    /*
     * NOTE: the root with "+ sqrt (delta)" does not always satisfy:
     *    0 <= alpha <= 1
     * so we only look at the one with a minus
     */
    alpha = ((q + q0) - sqrt (delta)) / (2 * q0);
  }

  return alpha;
  
} // suliciu_thermo_alpha

// ====================================================================
// ====================================================================
double
suliciu_thermo_pressure (double arho1, double arho2, double alpha)
{
  using namespace constants;

  double              pressure[2] = { 0.0, 0.0 };

  if (ISFUZZYNULL (alpha)) {

    pressure[1] = p0[1] +
      SC_SQR (c[1]) * (arho2 - rho0[1]);
    
  } else if (FUZZYCOMPARE (alpha, 1)) {
    
    pressure[0] = p0[0] +
      SC_SQR (c[0]) * (arho1 - rho0[0]);
    
  } else {
    
    pressure[0] = p0[0] +
      SC_SQR (c[0]) * (arho1 / alpha - rho0[0]);

    pressure[1] = p0[1] +
      SC_SQR (c[1]) * (arho2 / (1 - alpha) - rho0[1]);
    
  }

  // SC_CHECK_ABORTF (FUZZYCOMPARE (pressure[0], pressure[1]),
  //     "No pressure equilibrium: %.8lf <-> %.8lf", pressure[0], pressure[1]);
  return (alpha * pressure[0] + (1 - alpha) * pressure[1]);

} // suliciu_thermo_pressure

// ====================================================================
// ====================================================================
double
suliciu_thermo_wallis_sound_velocity (double Y, double alpha)
{
  using namespace constants;

  double              c_wallis = 0.0;

  if (ISFUZZYNULL (Y)) {        /* no gas, sound velocity is liquid one */
    c_wallis = c[1];
  }
  else if (FUZZYCOMPARE (Y, 1)) {       /* no liquid */
    c_wallis = c[0];
  }
  else {
    c_wallis =
      SC_SQR (alpha) / (Y * SC_SQR (c[0])) +
      SC_SQR (1 - alpha) / ((1 - Y) * SC_SQR (c[1]));
    c_wallis = 1.0 / sqrt (c_wallis);
  }
  return c_wallis;
  
} // suliciu_thermo_wallis_sound_velocity

// ====================================================================
// ====================================================================
double
suliciu_thermo_a (double arho1, double arho2, double alpha)
{
  using namespace constants;

  double              rho = 0.0;
  double              Y = 0.0;
  double              c_wallis = 0.0;

  rho = arho1 + arho2;
  Y = arho1 / rho;
  c_wallis = suliciu_thermo_wallis_sound_velocity (Y, alpha);

  return theta * rho * c_wallis;
  
} // suliciu_thermo_a

// ====================================================================
// ====================================================================
void
suliciu_variables_t::init(qdata_t * data,
			  int face,
			  int vi[3],
			  double dx,
			  double dt,
			  int hancock)
{
  
  qdata_variables_t   w = data->w;
  qdata_reconstructed_t delta = data->delta[face / 2];
  double              n = 2.0 * (face % 2 == 1) - 1.0;

  double              wrho = w.arho[0] + w.arho[1];
  double              arho[2] = { 0, 0 };
  double              alpha = 0.0;
  
  /* hancock variables */
  double			  arho_l[2] = { 0, 0 };
  double			  arho_r[2] = { 0, 0 };
  double			  alpha_l, alpha_r;
  double			  pressure_l, pressure_r;
  double			  velocity_l[3] = {0, 0, 0};
  double			  velocity_r[3] = {0, 0, 0};

  /* reconstruct variables for order 2. */
  arho[0] = w.arho[0] + 0.5 * n * dx * delta.arho[0];
  arho[1] = w.arho[1] + 0.5 * n * dx * delta.arho[1];
  this->velocity[0] =
    w.momentum[vi[0]] / wrho + 0.5 * n * dx * delta.velocity[vi[0]];
  this->velocity[1] =
    w.momentum[vi[1]] / wrho + 0.5 * n * dx * delta.velocity[vi[1]];
#ifdef P4_TO_P8
  this->velocity[2] =
    w.momentum[vi[2]] / wrho + 0.5 * n * dx * delta.velocity[vi[2]];
#endif
  
  /* Hancock recontruction part */
  if (hancock){

    // if dt<0, this is signal for order 1 in space,
    // so no hancock reconstruction needed
    arho_l[0] = w.arho[0] - 0.5 * dx * delta.arho[0];
    arho_r[0] = w.arho[0] + 0.5 * dx * delta.arho[0];
    
    arho_l[1] = w.arho[1] - 0.5 * dx * delta.arho[1];
    arho_r[1] = w.arho[1] + 0.5 * dx * delta.arho[1];
    
    alpha_l   = suliciu_thermo_alpha(arho_l[0],arho_l[1]);
    alpha_r   = suliciu_thermo_alpha(arho_r[0],arho_r[1]);
    
    pressure_l = suliciu_thermo_pressure(arho_l[0],arho_l[1],alpha_l);
    pressure_r = suliciu_thermo_pressure(arho_r[0],arho_r[1],alpha_r);
    
    velocity_l[0] = w.momentum[vi[0]] / wrho - 0.5 * dx * delta.velocity[vi[0]];
    velocity_r[0] = w.momentum[vi[0]] / wrho + 0.5 * dx * delta.velocity[vi[0]];
    
    velocity_l[1] = w.momentum[vi[1]] / wrho - 0.5 * dx * delta.velocity[vi[1]];
    velocity_r[1] = w.momentum[vi[1]] / wrho + 0.5 * dx * delta.velocity[vi[1]];
#ifdef P4_TO_P8	
    velocity_l[2] = w.momentum[vi[2]] / wrho - 0.5 * dx * delta.velocity[vi[2]];
    velocity_r[2] = w.momentum[vi[2]] / wrho + 0.5 * dx * delta.velocity[vi[2]];
#endif
    
    this->velocity[0] = (arho[0]+arho[1])*this->velocity[0] - dt/(2*dx)* // ???? how to reconstruct u with flux of rho*u ?
      ((arho_r[0]+arho_r[1])*velocity_r[0]*velocity_r[0] + pressure_r - 
       ((arho_l[0]+arho_l[1])*velocity_l[0]*velocity_l[0] + pressure_l));
    
    this->velocity[1] = (arho[0]+arho[1])*this->velocity[1] - dt/(2*dx)*
      ((arho_r[0]+arho_r[1])*velocity_r[0]*velocity_r[1] -
       ((arho_l[0]+arho_l[1])*velocity_l[0]*velocity_l[1]));
    
#ifdef P4_TO_P8		
    this->velocity[2] = (arho[0]+arho[1])*this->velocity[2] - dt/(2*dx)*
      ((arho_r[0]+arho_r[1])*velocity_r[0]*velocity_r[2] -
       ((arho_l[0]+arho_l[1])*velocity_l[0]*velocity_l[2]));
#endif	
    
    if ((arho_l[0]<1e-6)&&(arho_r[0]>1e-6)){
      CANOP_GLOBAL_INFOF("Before Hancock : rho0 = %g  ;  rho1 = %g  \n", arho[0], arho[1]);
    }
    
    
    arho[0] = arho[0] - dt/(2*dx)*
      (arho_r[0]*velocity_r[0] - arho_l[0]*velocity_l[0]);
    
    arho[1] = arho[1] - dt/(2*dx)*
      (arho_r[1]*velocity_r[0] - arho_l[1]*velocity_l[0]);
    
    
    // Set a maximum principle on rho_0 and rho_1 !?
    
    /*
      if ((arho[0]<fmin(arho_l[0],arho_r[0]))||(arho[0]>fmax(arho_l[0],arho_r[0]))){
      
      arho[0] = w.arho[0] + 0.5 * n * dx * delta.arho[0];
      delta.arho[0] = (arho[0] - (w.arho[0] - dt/(2*dx)*
      (arho_r[0]*velocity_r[0] - arho_l[0]*velocity_l[0])))/
      (0.5*n*dx);
      
      arho[1] = w.arho[1] + 0.5 * n * dx * delta.arho[1];
      delta.arho[1] = (arho[1] - (w.arho[1] - dt/(2*dx)*
      (arho_r[1]*velocity_r[0] - arho_l[1]*velocity_l[0])))/
      (0.5*n*dx);	  
    */ 
    /*
      arho[0] = w.arho[0] - dt/(2*dx)*
      (arho_r[0]*velocity_r[0] - arho_l[0]*velocity_l[0]);
      
      arho[1] = w.arho[1] - dt/(2*dx)*
      (arho_r[1]*velocity_r[0] - arho_l[1]*velocity_l[0]);
    */
    /*}
      
      if ((arho[1]<fmin(arho_l[1],arho_r[1]))||(arho[1]>fmax(arho_l[1],arho_r[1]))){
      
      arho[0] = w.arho[0] + 0.5 * n * dx * delta.arho[0];
      delta.arho[0] = (arho[0] - (w.arho[0] - dt/(2*dx)*
      (arho_r[0]*velocity_r[0] - arho_l[0]*velocity_l[0])))/
      (0.5*n*dx);
      
      arho[1] = w.arho[1] + 0.5 * n * dx * delta.arho[1];
      delta.arho[1] = (arho[1] - (w.arho[1] - dt/(2*dx)*
      (arho_r[1]*velocity_r[0] - arho_l[1]*velocity_l[0])))/
      (0.5*n*dx);
    */
    /*
      arho[0] = w.arho[0] - dt/(2*dx)*
      (arho_r[0]*velocity_r[0] - arho_l[0]*velocity_l[0]);
      
      arho[1] = w.arho[1] - dt/(2*dx)*
      (arho_r[1]*velocity_r[0] - arho_l[1]*velocity_l[0]);
    */
    
    //}
    
    
    if ( (arho_l[0]<1e-6) && (arho_r[0]>1e-6) ) {
      
      CANOP_GLOBAL_INFOF("After Hancock : rho0 = %g  ;  rho1 = %g  \n", arho[0], arho[1]);
      CANOP_GLOBAL_INFOF("                rho0_l = %g  ;  rho0_r = %g  ; dt/dx = %g \n", arho_l[0], arho_r[0], dt/dx);
      CANOP_GLOBAL_INFOF("                delta = %18.14e  \n \n",delta.arho[0]);
    }
    
    // change momentum variables to velocity variables
    this->velocity[0] /= (arho[0]+arho[1]);
    this->velocity[1] /= (arho[0]+arho[1]);
#ifdef P4_TO_P8
    this->velocity[2] /= (arho[0]+arho[1]);
#endif
  }
  
  /* 
   * compute the extra variables according to the
   * reconstructed ones 
   */
  this->rho = arho[0] + arho[1];
  this->Y = arho[0] / this->rho;
  alpha = suliciu_thermo_alpha (arho[0], arho[1]);
  this->pressure = suliciu_thermo_pressure (arho[0], arho[1], alpha);
  this->a = suliciu_thermo_a (arho[0], arho[1], alpha);
  
  SC_CHECK_ABORTF (!isnan (this->a),
                   "a is nan: alpha %g | p %g | delta.arho1 %g | delta.arho2 %g",
                   alpha, this->pressure, delta.arho[0], delta.arho[1]);
  SC_CHECK_ABORTF (FUZZYLIMITS (alpha, 0, 1),
                   "alpha out of bounds: alpha %g | rho %g | rho1 %g | rho2 %g | p %lf",
                   alpha, this->rho, arho[0], arho[1],
                   this->pressure);

} // suliciu_variables_t::init

// ====================================================================
// ====================================================================
void
suliciu_riemann_solver (const suliciu_variables_t &wl,
			const suliciu_variables_t &wr,
			QdataVar &flux)
{
  suliciu_variables_t star;
 
  double              threshold[3] = { 0, 0, 0 };
  double              amax = SC_MAX (wl.a, wr.a);

  star.velocity[0] =
    0.5 * (wr.velocity[0] + wl.velocity[0]) -
    0.5 * (wr.pressure - wl.pressure) / amax;
  star.pressure =
    0.5 * (wr.pressure + wl.pressure) -
    0.5 * (wr.velocity[0] - wl.velocity[0]) * amax;

  threshold[0] = wl.velocity[0] - amax / wl.rho;
  threshold[1] = star.velocity[0];
  threshold[2] = wr.velocity[0] + amax / wr.rho;

  if (threshold[0] > 0) {       /* interface state = WL */

    star.Y = wl.Y;
    star.rho = wl.rho;
    star.pressure = wl.pressure;
    star.velocity[0] = wl.velocity[0];
    star.velocity[1] = wl.velocity[1];
#ifdef P4_TO_P8
    star.velocity[2] = wl.velocity[2];
#endif
    
  } else if (threshold[1] > 0) {  /* interface state = WStarL */

    star.Y = wl.Y;
    star.rho =
      1.0 / (1.0 / wl.rho + (star.velocity[0] - wl.velocity[0]) / amax);
    // star.pressure = star.pressure;
    // star.velocity[0] = star.velocity[0];
    star.velocity[1] = wl.velocity[1];
#ifdef P4_TO_P8
    star.velocity[2] = wl.velocity[2];
#endif
  } else if (threshold[2] > 0) {  /* interface state = WStarR */

    star.Y = wr.Y;
    star.rho =
      1.0 / (1.0 / wr.rho - (star.velocity[0] - wr.velocity[0]) / amax);
    // star.pressure = star.pressure;
    // star.velocity[0] = star.velocity[0];
    star.velocity[1] = wr.velocity[1];
#ifdef P4_TO_P8
    star.velocity[2] = wr.velocity[2];
#endif
  } else {                        /* interface state = WR */

    star.Y = wr.Y;
    star.rho = wr.rho;
    star.pressure = wr.pressure;
    star.velocity[0] = wr.velocity[0];
    star.velocity[1] = wr.velocity[1];
#ifdef P4_TO_P8
    star.velocity[2] = wr.velocity[2];
#endif
  }

  // compute the fluxes (also)
  flux.arho[0] = star.Y * star.rho * star.velocity[0];
  flux.arho[1] = (1 - star.Y) * star.rho * star.velocity[0];
  flux.momentum[0] = star.rho * SC_SQR (star.velocity[0]) + star.pressure;
  flux.momentum[1] = star.velocity[1] * star.rho * star.velocity[0];
#ifdef P4_TO_P8
  flux.momentum[2] = star.velocity[2] * star.rho * star.velocity[0];
#endif

} // suliciu_riemann_solver

// ====================================================================
// ====================================================================
void
suliciu_update_face (Qdata * data[2],
		     int face[2], double dt,
                     double dx, double scale_gradient,
		     double scale_flux,
                     int direction, int hancock)
{
  /* left and right states */
  suliciu_variables_t wl;
  suliciu_variables_t wr;

  /*
   * find the order for the velocities:
   *    vd[0] will contain the velocity in the normal direction
   *    vd[1-2] will contain the velocities in the tangent directions
   */
  int vd[P4EST_DIM];
  vd[0] = direction;
  vd[1] = (direction + 1) % P4EST_DIM;
#ifdef P4_TO_P8
  vd[2] = (direction + 2) % P4EST_DIM;
#endif

  double n = 2 * (face[0] % 2 == 1) - 1;

  /* initialize the left and right states correctly */
  if (face[0] % 2 == 1) {

    wl.init (data[0], face[0], vd, dx, dt, hancock);
    wr.init (data[1], face[1], vd, scale_gradient * dx, dt, hancock);
    
  } else {

    wl.init (data[1], face[1], vd, scale_gradient * dx, dt, hancock);
    wr.init (data[0], face[0], vd, dx, dt, hancock);
    
  }

  /* compute the flux */
  QdataVar flux;
  suliciu_riemann_solver (wl, wr, flux);

  /* update the cells */
  dx *= scale_flux;
  data[0]->wnext.arho[0] -= n * dt / dx * flux.arho[0];
  data[0]->wnext.arho[1] -= n * dt / dx * flux.arho[1];
  data[0]->wnext.momentum[vd[0]] -= n * dt / dx * flux.momentum[0];
  data[0]->wnext.momentum[vd[1]] -= n * dt / dx * flux.momentum[1];
#ifdef P4_TO_P8
  data[0]->wnext.momentum[vd[2]] -= n * dt / dx * flux.momentum[2];
#endif

  if (!FUZZYLIMITS (data[0]->wnext.arho[0], 0, 10000)) {
    QDATA_INFOF (data[0]->w);
    QDATA_INFOF (data[0]->wnext);
    SC_ABORT ("rho1 is out of bounds.");
  }
  
} // suliciu_update_face

} // namespace canop

} // namespace bifluid
