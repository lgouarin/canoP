#include "SolverBifluid.h"
#include "SolverCoreBifluid.h"
#include "UserDataManagerBifluid.h"
#include "IteratorsBifluid.h"

#include "InitialConditionsFactoryBifluid.h" // for init_alpha_exact_byname

#include "IO_Writer.h"

#include "canoP_base.h"
#include "utils.h"

namespace canop {

namespace bifluid {

// =======================================================
// =======================================================
SolverBifluid::SolverBifluid(ConfigReader * cfg,
			     MPI_Comm mpicomm,
			     bool useP4estMemory) :
  Solver(cfg,mpicomm,sizeof(SolverBifluid::qdata_t),useP4estMemory)
{

  // setup solver core
  m_solver_core = new SolverCoreBifluid(cfg);

  // setup UserData Manager
  m_userdata_mgr = new UserDataManagerBifluid(useP4estMemory);

  // we need to setup the right iterators
  m_pIter = new IteratorsBifluid();

  // solver init (this is were p4est_new is called)
  init();
  
  // only MPI rank 0 prints config (thanks to CANOP_GLOBAL_PRODUCTION macro)
  print_config();

} // SolverBifluid::SolverBifluid

// =======================================================
// =======================================================
SolverBifluid::~SolverBifluid()
{

  finalize();

  delete m_solver_core;

  delete m_userdata_mgr;

  delete m_pIter;
  
} // SolverBifluid::~SolverBifluid

// =======================================================
// =======================================================
void SolverBifluid::read_config()
{
  
  // fisrt run base class method
  Solver::read_config();

  // read bifluid scheme specific parameters
  m_alpha_exact_fn = init_alpha_exact_byname(m_solver_core->get_init_name());

} // SolverBifluid::read_config

// =======================================================
// =======================================================
void SolverBifluid::next_iteration_impl()
{

  p4est_t            *p4est = get_p4est ();
  p4est_ghost_t      *ghost = get_ghost ();
    
  /*
   * reconstruct gradients
   */
  if (m_space_order == 2) {
    CANOP_GLOBAL_INFO ("Reconstructing for CFL computation\n");
    p4est_iterate (p4est,
		   ghost,
		   /* user data */ NULL,
		   m_pIter->iteratorsList[IT_ID::RECONSTRUCT_GRADIENT],
		   NULL,        // face_fn
#ifdef P4_TO_P8
                   NULL,        // edge_fn
#endif
                   NULL         // corner_fn
		   );

    // exchange the data again to get all the reconstructed values
    p4est_ghost_exchange_data (p4est, ghost, get_ghost_data_ptr());
  }

  // compute the cfl condition and time step at the smallest level
  compute_time_step ();
  
  /* Strang */

  if (m_dim_splitting==0) { 

    // unstructured_update(); // TODO
    
  } else if (m_dim_splitting==1) { // simple dimensional splitting
	
    single_directional (0);
    single_directional (1);
#ifdef P4_TO_P8
    single_directional (2);
#endif

  } else if (m_dim_splitting == 2){ // Strang splitting

    m_dt /= 2.0;
    single_directional (0);
    single_directional (1);
#ifdef P4_TO_P8
    single_directional (2);
    single_directional (2);
#endif	
    single_directional (1);
    single_directional (0);
    m_dt *= 2.0;
  }
  
} // SolverBifluid::next_iteration_impl

// =======================================================
// =======================================================
void
SolverBifluid::single_directional (int direction)
{
  
  p4est_t            *p4est = get_p4est();
  p4est_ghost_t      *ghost = get_ghost();

  if (direction) {
    p4est_ghost_exchange_data (p4est, ghost, get_ghost_data_ptr());
  }

  CANOP_GLOBAL_INFOF ("#-----------------# \n direction : %d\n", direction);

  if (m_space_order == 2) {
    CANOP_GLOBAL_INFOF ("Reconstructing direction %d \n #-----------------# \n", direction);
    p4est_iterate (p4est, 
		   ghost, 
		   /* user data */ &direction,
                   m_pIter->iteratorsList[IT_ID::RECONSTRUCT_GRADIENT],
		   NULL,        // face_fn
#ifdef P4_TO_P8
                   NULL,        // edge_fn
#endif
                   NULL         // corner_fn
		   );

    // exchange the data again to get all the reconstructed values
    p4est_ghost_exchange_data (p4est, ghost, get_ghost_data_ptr());
  }

  // update the values in all the cell with the required fluxes
  p4est_iterate (p4est, 
		 ghost, 
		 /* user data */ &direction, 
		 m_pIter->iteratorsList[IT_ID::UPDATE],
		 NULL,          // face_fn
#ifdef P4_TO_P8
                 NULL,          // edge_fn
#endif
                 NULL           // corner_fn
		 );

  // update old = new for all the quads
  p4est_iterate (p4est, 
		 ghost, 
		 /* user data */ NULL, 
		 m_pIter->iteratorsList[IT_ID::END_STEP],
		 NULL,          // face_fn
#ifdef P4_TO_P8
                 NULL,          // edge_fn
#endif
                 NULL           // corner_fn
		 );

} // SolverBifluid::single_directional

// =======================================================
// =======================================================
void
SolverBifluid::write_hdf5(const std::string &filename)
{

  p4est_t            *p4est = get_p4est ();
  p4est_geometry_t   *geom_compute = get_geom_compute ();

  //UserDataManagerBifluid* userdata_mgr = static_cast<UserDataManagerBifluid*>(m_userdata_mgr);
  
  const p4est_locidx_t Ntotal = p4est->local_num_quadrants;
  
  p4est_locidx_t      t = 0;
  int                 k = 0;
  p4est_tree_t       *tree = NULL;
  sc_array_t         *quadrants = NULL;
  p4est_quadrant_t   *quadrant = NULL;

  double             *data = NULL;
  qdata_getter_t      getter = NULL; // callback to get actual data
  qdata_vector_getter_t vgetter = NULL; // callback to get vector data
  qdata_field_type    ftype;
  hid_t               output_type =
    (m_single_precision ? H5T_NATIVE_FLOAT : H5T_NATIVE_DOUBLE);
  int                 write_alpha_exact = 0;
  
  // open the new file and write our stuff
  m_writer->open (filename);
  m_writer->write_header (m_t);
  
  /* write the scalar variables */
  for (int i = 0; i < m_num_write_variables; ++i) {
    ftype = m_userdata_mgr->qdata_type_byname (m_write_variables[i]);

    if (ftype == QDATA_FIELD_SCALAR) {
      getter = m_userdata_mgr->qdata_getter_byname (m_write_variables[i]);
      if (getter != nullptr) {
        m_writer->write_quadrant_attribute (m_write_variables[i],
                        m_userdata_mgr,
                        getter,
                        output_type);
      }
    }

    else if (ftype == QDATA_FIELD_VECTOR) {
      // XXX: backwards-compatibility hack
      std::string vname(m_write_variables[i]);
      if (!vname.compare("velocity"))
        vname = "momentum";

      vgetter = m_userdata_mgr->qdata_vgetter_byname (m_write_variables[i]);
      if (vgetter != nullptr) {
        m_writer->write_quadrant_attribute (vname,
                        m_userdata_mgr,
                        3,
                        vgetter,
                        output_type);
      }
    }

    else if ( !m_write_variables[i].compare("alpha_exact") ) {
      write_alpha_exact = 1;
      continue;
    }
  }

  // compute exact solution for alpha
  if (write_alpha_exact == 1 and m_alpha_exact_fn != nullptr) {
    double v[3];
    k = 0;
    
    // allocate the data for the exact solution in alpha
    data = CANOP_ALLOC (double, Ntotal);
    
    for (t = p4est->first_local_tree; t <= p4est->last_local_tree; ++t) {
      tree = p4est_tree_array_index (p4est->trees, t);
      quadrants = &tree->quadrants;
      
      for (size_t i = 0; i < quadrants->elem_count; ++i) {
        quadrant = p4est_quadrant_array_index (quadrants, i);
	
        quadrant_center_vertex (p4est->connectivity, geom_compute, t, quadrant, v);
        data[k++] = m_alpha_exact_fn (v[0], v[1], v[2], m_t);
      }
    }
    
    m_writer->write_attribute ("alpha_exact", data, 0,
			       IO_CELL_SCALAR, H5T_NATIVE_DOUBLE, output_type);
  }

  // close the file
  m_writer->write_footer ();
  m_writer->close ();

  CANOP_FREE (data);
  
} // SolverBifluid::write_hdf5

} // namespace bifluid

} // namespace canop
