#ifndef BOUNDARY_CONDITIONS_FACTORY_ADVECTION_UPWIND_H_
#define BOUNDARY_CONDITIONS_FACTORY_ADVECTION_UPWIND_H_

#include "BoundaryConditionsFactory.h"
#include "advection_upwind/userdata_advection_upwind.h"

namespace canop {

namespace advection_upwind {

/**
 *
 */
class BoundaryConditionsFactoryAdvectionUpwind : public BoundaryConditionsFactory<Qdata>
{

  using qdata_t = Qdata;
  
public:
  BoundaryConditionsFactoryAdvectionUpwind();
  virtual ~BoundaryConditionsFactoryAdvectionUpwind() {};
  
}; // class BoundaryConditionsFactoryAdvectionUpwind

} // namespace canop

} // namespace advection_upwind

#endif // BOUNDARY_CONDITIONS_FACTORY_ADVECTION_UPWIND_H_
