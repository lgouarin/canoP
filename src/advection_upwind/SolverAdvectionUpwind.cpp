#include "canoP_base.h"
#include "SolverAdvectionUpwind.h"
#include "SolverCoreAdvectionUpwind.h"
#include "UserDataManagerAdvectionUpwind.h"
#include "IteratorsAdvectionUpwind.h"

#include "InitialConditionsFactoryAdvectionUpwind.h" // for init_velocity_field_byname

#include "IO_Writer.h"

#include "utils.h"

namespace canop {

namespace advection_upwind {

// =======================================================
// =======================================================
SolverAdvectionUpwind::SolverAdvectionUpwind(ConfigReader * cfg,
					     MPI_Comm mpicomm,
					     bool useP4estMemory) :
  Solver(cfg,mpicomm,sizeof(SolverAdvectionUpwind::qdata_t),useP4estMemory)
{

  // setup solver core
  m_solver_core = new SolverCoreAdvectionUpwind(cfg);

  // setup UserData Manager
  m_userdata_mgr = new UserDataManagerAdvectionUpwind(useP4estMemory);

  // we need to setup the right iterators
  m_pIter = new IteratorsAdvectionUpwind();

  // solver init (this is were p4est_new is called)
  init();
  
  // only MPI rank 0 prints config (thanks to CANOP_GLOBAL_PRODUCTION macro)
  print_config();
  
} // SolverAdvectionUpwind::SolverAdvectionUpwind

// =======================================================
// =======================================================
SolverAdvectionUpwind::~SolverAdvectionUpwind()
{

  finalize();

  delete m_solver_core;

  delete m_userdata_mgr;
  
  delete m_pIter;
  
} // SolverAdvectionUpwind::~SolverAdvectionUpwind


// =======================================================
// =======================================================
void SolverAdvectionUpwind::read_config()
{

  // first run base class method
  Solver::read_config();
  
  m_velocity_field_fn = init_velocity_field_byname(m_solver_core->get_init_name());

} // SolverAdvectionUpwind::read_config

// =======================================================
// =======================================================
void SolverAdvectionUpwind::next_iteration_impl()
{

  p4est_t            *p4est = get_p4est ();
  p4est_ghost_t      *ghost = get_ghost ();
    
  /*
   * reconstruct gradients
   */
  if (m_space_order == 2) {
    CANOP_GLOBAL_INFO ("Reconstructing for CFL computation\n");
    p4est_iterate (p4est,
		   ghost,
		   /* user data */ NULL,
		   m_pIter->iteratorsList[IT_IDD::RECONSTRUCT_GRADIENT],
		   NULL,        // face_fn
#ifdef P4_TO_P8
                   NULL,        // edge_fn
#endif
                   NULL         // corner_fn
		   );

    // exchange the data again to get all the reconstructed values
    p4est_ghost_exchange_data (p4est, ghost, get_ghost_data_ptr());
  }

  // compute the cfl condition and time step at the smallest level
  compute_time_step ();
  
  /* Strang */
  if (m_dim_splitting==0) { 
  
    unstructured_update();
    
  } else if (m_dim_splitting==1) { // simple dimensional splitting
    
    single_directional(0);
    single_directional(1);
#ifdef P4_TO_P8
    single_directional(2);
#endif

  } else if (m_dim_splitting == 2) { // Strang splitting

    m_dt /= 2.0;
    single_directional(0);
    single_directional(1);
#ifdef P4_TO_P8
    single_directional(2);
    single_directional(2);
#endif	
    single_directional(1);
    single_directional(0);
    m_dt *= 2.0;
  }
  
} // SolverAdvectionUpwind::next_iteration_impl

// =======================================================
// =======================================================
void
SolverAdvectionUpwind::compute_time_step()
{

  if (m_dim_splitting == 0) {
    compute_time_step_upwind_unstructured();
  } else {
    // call base class method
    Solver::compute_time_step();
  }
  
} // SolverAdvectionUpwind::compute_time_step


// =======================================================
// =======================================================
void
SolverAdvectionUpwind::compute_time_step_upwind_unstructured()
{
  p4est_t            *p4est   = get_p4est();
  p4est_geometry_t   *geom    = get_geom_compute();
  p4est_ghost_t      *ghost   = get_ghost();
  MPI_Comm            mpicomm = p4est->mpicomm;
  double              max_v_over_dx = -1;

  UNUSED (geom);

  // initialize the min / max current levels
  m_minlevel = P4EST_QMAXLEVEL;
  m_maxlevel = 0;
  
  // compute the max of the velocities
  // update the min / max current levels
  p4est_iterate (p4est, 
		 ghost, 
		 /* user data */ &max_v_over_dx, 
		 m_pIter->iteratorsList[IT_IDD::CFL_TIME_STEP_UNSTRUCTURED],  /* cell_fn */
		 NULL,                                     /* face_fn */
#ifdef P4_TO_P8
                 NULL,                                     /* edge_fn */
#endif
                 NULL                                      /* corner_fn */
		 ); 

  // get the global maximum and minimum level
  MPI_Allreduce (MPI_IN_PLACE, &(m_minlevel), 1,
                 MPI_INT, MPI_MIN, mpicomm);
  MPI_Allreduce (MPI_IN_PLACE, &(m_maxlevel), 1,
                 MPI_INT, MPI_MAX, mpicomm);

  // update the min dt using the given courant number
  if (max_v_over_dx < 0) {
    m_dt = m_tmax;
  }
  else if (!ISFUZZYNULL (max_v_over_dx)) {
    m_dt = 1.0 / max_v_over_dx * m_cfl;
    m_dt = fmin (m_dt, fabs (m_tmax - m_t));
  }
  else {
    SC_ABORT ("max_v_over_dx is 0\n");
  }

  // take the min over all the processes
  MPI_Allreduce (MPI_IN_PLACE, &(m_dt), 1, MPI_DOUBLE, MPI_MIN,
                 mpicomm);
  
} // SolverAdvectionupwind::compute_time_step_upwind_unstructured

// =======================================================
// =======================================================
void
SolverAdvectionUpwind::single_directional (int direction)
{
  
  p4est_t            *p4est = get_p4est();
  p4est_ghost_t      *ghost = get_ghost();

  if (direction) {
    p4est_ghost_exchange_data (p4est, ghost, get_ghost_data_ptr() );
  }

  CANOP_GLOBAL_INFOF ("#-----------------# \n direction : %d\n", direction);

  if (m_space_order == 2) {
    CANOP_GLOBAL_INFOF ("Reconstructing direction %d \n #-----------------# \n", direction);
    p4est_iterate (p4est, 
		   ghost, 
		   /* user data */ &direction,
                   m_pIter->iteratorsList[IT_IDD::RECONSTRUCT_GRADIENT],
		   NULL,        // face_fn
#ifdef P4_TO_P8
                   NULL,        // edge_fn
#endif
                   NULL         // corner_fn
		   );

    // exchange the data again to get all the reconstructed values
    p4est_ghost_exchange_data (p4est, ghost, get_ghost_data_ptr() );
  }

  // update the values in all the cell with the required fluxes
  p4est_iterate (p4est, 
		 ghost, 
		 /* user data */ &direction, 
		 m_pIter->iteratorsList[IT_IDD::UPDATE],
		 NULL,          // face_fn
#ifdef P4_TO_P8
                 NULL,          // edge_fn
#endif
                 NULL           // corner_fn
		 );

  // update old = new for all the quads
  p4est_iterate (p4est, 
		 ghost, 
		 /* user data */ NULL, 
		 m_pIter->iteratorsList[IT_IDD::END_STEP],
		 NULL,          // face_fn
#ifdef P4_TO_P8
                 NULL,          // edge_fn
#endif
                 NULL           // corner_fn
		 );

} // SolverAdvectionUpwind::single_directional

// =======================================================
// =======================================================
void
SolverAdvectionUpwind::unstructured_update ()
{
  
  p4est_t            *p4est = get_p4est ();
  p4est_ghost_t      *ghost = get_ghost ();

  if (m_space_order == 2) { /* CURRENTLY UNIMPLEMENTED */
    CANOP_GLOBAL_INFO ("Upwind unstructured reconstruction - NOT IMPLEMENTED TODO \n #-----------------# \n");
    
    /*     p4est_iterate (p4est,  */
    /* 		   ghost,  */
    /* 		   /\* user data *\/ NULL, */
    /*                    pIter->iterator_reconstruct_gradients,  */
    /* 		   NULL,        // face_fn */
    /* #ifdef P4_TO_P8 */
    /*                    NULL,        // edge_fn */
    /* #endif */
    /*                    NULL         // corner_fn */
    /* 		   ); */

    // exchange the data again to get all the reconstructed values
    //p4est_ghost_exchange_data (p4est, ghost, get_ghost_data_ptr() );
  }

  // update the values in all the cell with the required fluxes
  p4est_iterate (p4est, 
		 ghost, 
		 /* user data */ NULL, 
		 m_pIter->iteratorsList[IT_IDD::UPDATE_UNSTRUCTURED], 
		 NULL,          // face_fn
#ifdef P4_TO_P8
                 NULL,          // edge_fn
#endif
                 NULL           // corner_fn
		 );

  // update old = new for all the quads
  p4est_iterate (p4est, 
		 ghost, 
		 /* user data */ NULL, 
		 m_pIter->iteratorsList[IT_IDD::END_STEP],
		 NULL,          // face_fn
#ifdef P4_TO_P8
                 NULL,          // edge_fn
#endif
                 NULL           // corner_fn
		 );
  
} // SolverAdvectionUpwind::unstructured_update

} // namespace canop

} // namespace advection_upwind
