#ifndef ITERATORS_ADVECTION_UPWIND_H_
#define ITERATORS_ADVECTION_UPWIND_H_

#include "Iterators.h"

namespace canop {

namespace advection_upwind {

/**
 * Adding new IDs to struct IT_ID.
 *
 * We start using number 32 (assuming the number of ID's in base class is
 * less than 32).
 *
 * TODO: enforce strongly that each new IDs below are initialized with
 * a number not already used in base class.
 */
struct IT_IDD : public IT_ID {

  static constexpr int CFL_TIME_STEP_UNSTRUCTURED = 32;
  static constexpr int UPDATE_UNSTRUCTURED = 33;
  
};

struct upwind_variables_t
{
  double              rho;
  double              velocity;
  double              n;
};

/*
 * NB: if you need to add new iterators:
 * 1. resize vector iterator_list_t
 * 2. create a new enum to ease indexing this vector
 * 3. add new iterator to the vector in fill_iterators_list method.
 */

class IteratorsAdvectionUpwind : public IteratorsBase
{

public:
  IteratorsAdvectionUpwind();
  ~IteratorsAdvectionUpwind();

  void fill_iterators_list();
  
};  // class IteratorsAdvectionUpwind

} // namespace canop

} // namespace advection_upwind

#endif // ITERATORS_ADVECTION_UPWIND_H_
