#ifndef USERDATA_ADVECTION_UPWIND_H_
#define USERDATA_ADVECTION_UPWIND_H_

#ifdef USE_3D
#include <p4est_to_p8est.h>
#endif

#ifndef P4_TO_P8
#include <p4est.h>
#else
#include <p8est.h>
#endif

namespace canop {

namespace advection_upwind {

/**
 * \brief Vector of conserved variables.
 *
 *  We have
 *      arho[0] = alpha * rho1
 *      arho[1] = (1 - alpha) * rho2
 *      velocity = (u * rho, v * rho, w * rho)
 *  with
 *      rho = alpha * rho1 + (1 - alpha) * rho2
 *  and the extra variables
 *      pressure
 *      alpha
 *      a
 *  that do not have update formulas but get recomputed from the new
 *  values of the other variables.
 */
struct QdataVar
{
  double              rho;
  double              velocity[P4EST_DIM];

  inline
  QdataVar () : rho(-1.0) {
    for (int i=0; i<P4EST_DIM; ++i)
      velocity[i] = 0.0;
  }
}; // struct QdataVar

/**
 * Reconstructed variables.
 */
struct QdataRecons
{
  double     rho;

  inline
  QdataRecons () : rho(-1.0) {}
  
}; // struct QdataRecons

struct Qdata
{
  QdataVar   w;        /* variable at the current time */
  QdataVar   wnext;    /* variable at tn + dt */

  QdataRecons delta[P4EST_DIM];       /* reconstructed gradients */
}; // struct Qdata

struct QdataSod2
{
  double	  Y;
  double	  pressure;
  double	  velocity[3];
  double	  rho;
  double	  alpha;
  double	  rho_gas;
  double	  rho_liquid;
  double	  cSon;
}; // struct QdataSod2

} // namespace advection_upwind

} // namespace canop

#endif // USERDATA_ADVECTION_UPWIND_H_
