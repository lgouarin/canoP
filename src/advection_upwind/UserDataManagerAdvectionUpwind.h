#ifndef USERDATA_MANAGER_ADVECTION_UPWIND_H_
#define USERDATA_MANAGER_ADVECTION_UPWIND_H_

#include "UserDataManager.h"
#include "userdata_advection_upwind.h"

namespace canop {

namespace advection_upwind {

using UserDataTypesAdvectionUpwind =
  UserDataTypes<Qdata,
		QdataVar,
		QdataRecons>;

class UserDataManagerAdvectionUpwind : public UserDataManager<UserDataTypesAdvectionUpwind>
{
  
public:
  UserDataManagerAdvectionUpwind(bool useP4estMemory);
  virtual ~UserDataManagerAdvectionUpwind();

  virtual void qdata_set_variables (qdata_t * data,
				    qdatavar_t * w);
  
  virtual void quadrant_set_state_data (p4est_quadrant_t * dst,
					p4est_quadrant_t * src);

  virtual void quadrant_compute_mean(p4est_quadrant_t *qmean,
				     p4est_quadrant_t *quadout[P4EST_CHILDREN]);

  
  virtual void qdata_print_variables (int loglevel, qdata_t * data);

  virtual void quadrant_copy_w_to_wnext (p4est_quadrant_t * q);
  virtual void quadrant_copy_wnext_to_w (p4est_quadrant_t * q);
  
  double qdata_get_rho (p4est_quadrant_t * q);
  double qdata_get_velocity (p4est_quadrant_t * q, int direction);

  void qdata_vget_velocity (p4est_quadrant_t * q, size_t dim, double * val);

  void qdata_reconstructed_zero(qdatarecons_t *q);
  void qdata_reconstructed_copy(qdatarecons_t *q1, 
				const qdatarecons_t *q2);
  void qdata_reconstructed_swap(qdatarecons_t *q1, 
				qdatarecons_t *q2);
  void qdata_reconstructed_change_sign(qdatarecons_t *q);

  void reconstruct_variables (const qdatavar_t &w,
			      qdatarecons_t &r);

  void compute_delta(const qdatarecons_t w,const qdatarecons_t *wn,
		     qdatarecons_t *delta,
		     scalar_limiter_t limiter,
		     double dx, double dxl, double dxr);  
  
  void qdata_reconstructed_check (qdatarecons_t w, const char *plus);
  void qdata_variables_check (qdatavar_t w, const char *plus);
  void qdata_check_variables (qdata_t * data);

  void quadrant_check_data (p4est_quadrant_t * q);

  /**
   * Return a pointer to a member that is a qdata field "getter".
   */
  virtual qdata_getter_t qdata_getter_byname(const std::string &varName);

  /**
   * Return a pointer to a member that is a qdata vector field "getter".
   */
  virtual qdata_vector_getter_t qdata_vgetter_byname(const std::string &varName);

  /**
   * Return the field type for the given variable.
   */
  virtual qdata_field_type qdata_type_byname(const std::string &varName);

}; // UserDataManagerAdvectionUpwind

} // namespace canop

} // namespace advection_upwind

#endif // USERDATA_MANAGER_ADVECTION_UPWIND_H_
