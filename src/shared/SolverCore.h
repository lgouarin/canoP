#ifndef SOLVER_CORE_H_
#define SOLVER_CORE_H_

#ifdef USE_3D
#include <p4est_to_p8est.h>
#endif

#ifndef P4_TO_P8
#include <p4est.h> // for p4est_init_t
#else
#include <p8est.h>
#endif

#include <string>

#include "IndicatorFactory.h"          // needed for type IndicatorCallback
#include "BoundaryConditionsFactory.h" // needed for type BC_Callback 
#include "InitialConditionsFactory.h"  // needed for type IC_Callback 
#include "GeometricIndicatorFactory.h" // needed for type GeometricIndicatorCallback

#include "ConfigReader.h"
//class ConfigReader;

/**
 * Define an empty base class, non-template.
 */
class SolverCoreBase
{

public:
  SolverCoreBase(ConfigReader *cfg);
  virtual ~SolverCoreBase();

  virtual std::string& get_init_name();
  
  virtual void set_init_name(std::string name);

  virtual p4est_init_t get_init_fn();

  virtual p4est_refine_t get_geom_indicator_fn();

  virtual std::string& get_boundary_name();
  
  virtual std::string& get_indicator_name();

  virtual std::string& get_geometric_indicator_name();
  
protected:
  //! name of initial condition.
  std::string         m_init_name;

  //! border condition name
  std::string         m_boundary_name;

  //! refine/coarsen indicator name
  std::string         m_indicator_name;

  //! geometric indicator name
  std::string         m_geom_indicator_name;

}; // class SolverCoreBase


/**
 * Solver core class, which defines interface to actual user data.
 *
 * Please note that the actual init and border conditions callbacks
 * are setup in derived concrete class.
 */
template<typename qdata_t>
class SolverCore : public SolverCoreBase
{

  using IC_Callback = InitialConditionsFactory::IC_Callback;

  using BC_Callback = typename BoundaryConditionsFactory<qdata_t>::BC_Callback;
  
  using IndicatorCallback = typename IndicatorFactory<qdata_t>::IndicatorCallback;

  using GeometricIndicatorCallback = GeometricIndicatorFactory::IndicatorCallback;
  
public:
  SolverCore(ConfigReader *cfg) : SolverCoreBase(cfg) {};
  virtual ~SolverCore() {};

  //! initial condition callback.
  IC_Callback         m_init_fn;
  
  //! border condition callback
  BC_Callback         m_boundary_fn;

  //! refine/coarsen indicator callback
  IndicatorCallback   m_indicator_fn;

  //! geometric indicator callback
  GeometricIndicatorCallback m_geom_indicator_fn;

  virtual p4est_init_t get_init_fn() {  return m_init_fn; };

  virtual p4est_refine_t get_geom_indicator_fn() {  return m_geom_indicator_fn; };
      
}; // class SolverCore
  

#endif // SOLVER_CORE_H_
