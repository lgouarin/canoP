#ifndef SUBCYCLING_H_
#define SUBCYCLING_H_

#include <cstdint>
#include <vector>

struct subcycle_info
{
  std::int8_t level;
  std::int8_t min_level;
  std::int8_t max_level;
  int         substep;

  subcycle_info() :
    level(0), min_level(-1), max_level(-1), substep(0)
  {
  }

  subcycle_info(std::int8_t min_level_, std::int8_t max_level_) :
    level(min_level_), min_level(min_level_), max_level(max_level_), substep(0)
  {
  }

  inline void reset(bool reset_minmax=false)
  {
    substep = 0;
    if (reset_minmax) {
      min_level = -1;
      max_level = -1;
      level = 0;
    } else {
      level = min_level;
    }
  }

};

#endif // SUBCYCLING_H_
