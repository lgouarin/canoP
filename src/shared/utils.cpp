#include "utils.h"

#include <ctime>   // for std::time_t, std::tm, std::localtime
#include <sstream> // string stream
#include <string>  // string
#include <iomanip> // for std::put_time

#include "canoP_base.h" // for CANOP_GLOBAL_ESSENTIALF

// =======================================================
// =======================================================
void format_date(std::string& s, std::time_t t)
{

  /* Format and print the time, "ddd yyyy-mm-dd hh:mm:ss zzz" */
  std::tm tm = *std::localtime(&t);

  std::stringstream ss;

  // old versions of g++ don't have std::put_time,
  // so we provide a slight work arround
#if defined(__GNUC__) && (__GNUC__ < 5)

char foo[64];


 if(0 < std::strftime(foo, sizeof(foo), "%Y-%m-%d %H:%M:%S %Z", &tm))
    ss << foo << "\n";

#else

  ss << std::put_time(&tm, "%Y-%m-%d %H:%M:%S %Z");

#endif
  
  s = ss.str();

} // format_date

// =======================================================
// =======================================================
void print_current_date()
{
  
  /* get current time */
  std::time_t     now = std::time(nullptr); 
  
  std::string tmp;
  format_date(tmp, now);

  const char *cstr = tmp.c_str();
  
  CANOP_GLOBAL_ESSENTIALF ("-- %s\n",cstr);

} // print_current_date

