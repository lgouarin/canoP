#ifndef CANOP_UTILS_H_
#define CANOP_UTILS_H_

#include <ctime> // for time_t
#include <string> // for string

// make the compiler ignore an unused variable
#ifndef UNUSED
#define UNUSED(x) ((void)(x))
#endif

// make the compiler ignore an unused function
#ifdef __GNUC__
#define UNUSED_FUNCTION __attribute__ ((unused))
#else
#define UNUSED_FUNCTION
#endif

#define THRESHOLD 1e-12
#define ISFUZZYNULL(a) (fabs(a) < THRESHOLD)
#define FUZZYCOMPARE(a, b) \
    ((ISFUZZYNULL(a) && ISFUZZYNULL(b)) || \
     (fabs((a) - (b)) * 1000000000000. <= SC_MIN(fabs(a), fabs(b))))
#define FUZZYLIMITS(x, a, b) \
    (((x) > ((a) - THRESHOLD)) && ((x) < ((b) + THRESHOLD)))


void format_date(std::string& s, std::time_t t);
void print_current_date();

#endif // CANOP_UTILS_H_
