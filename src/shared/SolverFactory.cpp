#include "SolverFactory.h"

#include "Solver.h"

#include "advection_upwind/SolverAdvectionUpwind.h"

#include "bifluid/SolverBifluid.h"

#include "ramses/SolverRamses.h"

#include "burgers/SolverBurgers.h"

#include "mhd_kt/SolverMHD_KT.h"

#include "mfplasma/SolverMfplasma.h"

// The main solver creation routine
SolverFactory::SolverFactory()
{

  /*
   * Register some possible Solver/UserDataManager.
   */
  registerSolver("advection_upwind", &canop::advection_upwind::SolverAdvectionUpwind::create);
  
  registerSolver("bifluid"         , &canop::bifluid::SolverBifluid::create);
  
  registerSolver("ramses"          , &canop::ramses::SolverRamses::create);

  registerSolver("burgers"         , &canop::burgers::SolverBurgers::create);
		 
  registerSolver("mhd_kt"          , &canop::mhd_kt::SolverMHD_KT::create);

  registerSolver("mfplasma"        , &canop::mfplasma::SolverMfplasma::create);
		 
} // SolverFactory::SolverFactory
