#ifndef RIEMANN_SOLVER_FACTORY_H_
#define RIEMANN_SOLVER_FACTORY_H_

#include <string>
#include <map>

/**
 * An abstract base class to define a common interface for a concrete riemann solver
 * factory.
 *
 * Template parameters will be specified when deriving the concrete factory.
 * Template parameters are supposed to be structs holding data for 
 * conservative and reconstructed variables in a quadrant.
 *
 * The main purpose is to return an RiemannSolverCallback, i.e. a function pointer
 * that will be passed to a p4est_iterate call.
 *
 * The idea here it to define a map between a name and the actual riemann solver
 * function defined by the function pointer type riemannSolverCallback.
 *
 * Each derived class will have to define and register its own 
 * riemannSolverCallback's.
 *
 * The riemann solver is empty, and supposed to be filled in the concrete derived
 * class.
 *
 */
template <typename QdataRecons_t,
	  typename QdataVar_t,
	  typename Params_t>
class RiemannSolverFactory {

public:
  RiemannSolverFactory() {};
  virtual ~RiemannSolverFactory() {};
  
  /**
   * \brief Define riemannSolverCallback as a function pointer.
   *
   * RiemannSolvers functions compute a local value used to decide if a given cell
   * must be refine or coarsen.
   *
   * params is a struct used to pass contextual parameters 
   * (smallr, smallc, gamma0, etc...)
   *
   * TODO : need to check, if it is better to pass qleft/qright by reference instead
   * of by value.
   */
  using RiemannSolverCallback = void (*) (QdataRecons_t   qleft, 
					  QdataRecons_t   qright, 
					  QdataVar_t     *flux,
					  const Params_t &params,
					  double         *extra);
  
private:
  /**
   * Map to associate a label with a an riemann solver.
   */
  using RiemannSolverCallbackMap = std::map<std::string, RiemannSolverCallback>;
  RiemannSolverCallbackMap m_callbackMap;
  
public:
  /**
   * Routine to insert an riemann solver function into the map.
   * Note that this register function can be used to serve 
   * at least two different purposes:
   * - in the concrete factory: register existing callback's
   * - in some client code, register a callback from a plugin code, at runtime.
   */
  void registerRiemannSolver(const std::string& key, RiemannSolverCallback cb) {
    m_callbackMap[key] = cb;
  };

  /**
   * \brief Retrieve one of the possible riemann solvers by name.
   *
   * Allowed default names are defined in the concrete factory.
   */
  virtual RiemannSolverCallback get (const std::string &name) {
    // find the riemann solver in the register map
    typename RiemannSolverCallbackMap::iterator it = m_callbackMap.find(name);
    
    // if found, just return it
    if ( it != m_callbackMap.end() )
      return it->second;
    
    // if not found, return null pointer
    // it is the responsability of the client code to deal with
    // the possibility to have a nullptr callback (does nothing).
    //
    // Note that the concrete factory may chose a different behavior
    // and return a valid default callback (why not ?).
    return nullptr;
  };
  
}; // class RiemannSolverFactory

#endif // RIEMANN_FACTORY_H_
