#ifndef EQUATION_OF_STATE_H_
#define EQUATION_OF_STATE_H_

/**
 * Equation of state (for Ramses scheme).
 * Compute pressure p and speed of sound c, from density rho and
 * internal energy eint using the "calorically perfect gas" equation
 * of state : \f$ eint=\frac{p}{\rho (\gamma-1)} \f$
 * Recall that \f$ \gamma \f$ is equal to the ratio of specific heats \f$ \left[
 * c_p/c_v \right] \f$.
 *
 * @param[in]  rho  density
 * @param[in]  eint internal energy per mass unit
 * @param[out] p    pressure
 * @param[out] c    speed of sound
 * @param[in]  gamma0
 * @param[in]  smallp small pressure
 */
void eos(double rho,
	 double eint,
	 double * p,
	 double * c,
	 double gamma0,
	 double smallp);

/**
 * Isothermal equation of state (for Ramses scheme).
 * Compute pressure p from density rho and and speed of sound c
 * using the isothermal equation
 * of state : \f$ p = c^2 \rho \f$
 *
 * @param[in]  rho  density
 * @param[out] p    pressure
 * @param[in]  c    speed of sound
 * @param[in]  smallp small pressure
 */
void isothermal_eos(double rho,
	 double * p,
	 double c,
	 double smallp);

#endif // EQUATION_OF_STATE_H_
