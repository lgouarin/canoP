#ifndef IO_WRITER_H
#define IO_WRITER_H

#ifdef USE_3D
#include <p4est_to_p8est.h>
#endif

#ifndef P4_TO_P8
#include <p4est.h>
#include <p4est_ghost.h>
#include <p4est_geometry.h>
#include <p4est_nodes.h>
#include <p4est_lnodes.h>
#else
#include <p8est.h>
#include <p8est_ghost.h>
#include <p8est_geometry.h>
#include <p8est_nodes.h>
#include <p8est_lnodes.h>
#endif

#include "quadrant_utils.h"

#include <hdf5.h>
#include <hdf5_hl.h>

#include <string>

#include "UserDataManager.h"

/**
 * This function pointer type can be used with the UserDataManager.
 */
using simple_data_getter_t = double (*) (p4est_quadrant_t * q);

/**
 * A getter type for a quadrant-based vector
 */
using simple_data_vector_getter_t = void (*) (p4est_quadrant_t * q, size_t dim, double * val);


/**
 * \brief The four types of supported attribute types.
 */
enum io_attribute_type_t
  {
    IO_CELL_SCALAR,           /**< A cell-centered scalar. */
    IO_CELL_VECTOR,           /**< A cell-centered vector. */
    IO_NODE_SCALAR,           /**< A node-centered scalar. */
    IO_NODE_VECTOR            /**< A node-centered vector. */
  };

/**
 *\brief The writer structure contains the current state of the HDF5 writer.
 *
 * TODO: io_xmf_* private member put in a separate class.
 * TODO: io_hdf_* private member put in a separate class.
 */
class IO_Writer
{

public:
  /**
   * \brief Constructor.
   *
   * The writer is has no opened files by default except a main xmf file
   * that contains a collection of all the files that are to be written
   * next as a `Temporal` Collection.
   *
   * \param [in] basename The basename of the main XMF file. If it is not NULL,
   * the full name will be "name_main.xmf".
   * \param [in] mesh_info Flag to write mesh information (mpirank, treeid,
   * level, etc). 0 to not write this information and 1 to write it.
   *
   * \return A fully initialized writer with the given defaults.
   */
  IO_Writer(p4est_t * p4est, 
	    p4est_geometry_t * geom,
	    const std::string &basename,
	    int mesh_info);
  ~IO_Writer();
  
  
  p4est_t            *m_p4est;    // not owned
  p4est_geometry_t   *m_geom;     // not owned
  p4est_nodes_t      *m_nodes;    // nullptr or owned

  std::string         m_basename; // the base name of the two files
  hid_t               m_hdff;     // HDF file descriptor
  FILE               *m_xmff;     // XMF file descriptor
  FILE               *m_main_xmff;

  double              m_scale;            // default 1.0
  int8_t              m_write_tree;       // default 1
  int8_t              m_write_level;      // default 1
  int8_t              m_write_rank;       // default 1

  // store information about nodes for writing node data
  p4est_gloidx_t      m_global_num_nodes;
  p4est_locidx_t      m_local_num_nodes;
  p4est_locidx_t      m_start_nodes;


  /**
   * \brief Open the HDF5 and XMF files for writing.
   *
   * Also includes this file inside the main xmf file, if needed.
   */
  void                open (const std::string &basename);

  /**
   * \brief Close the HDF5 and XMF files.
   *
   * Also closes the main xmf file, if it was opened.
   */
  void                close ();

  /**
   * \brief Free the writer struct.
   *
   * This also closes the main xmf file
   */
  void                destroy ();

  /**
   * \brief Write very basic mesh information.
   *
   * This will write the treeid, level and rank for each quadrant in the forest
   * with a scale of 0.95.
   *
   * \param [in] basename The base name of the file to be written to.
   *
   */
  static int          write_mesh (p4est_t * p4est, 
				  p4est_geometry_t *geom,
				  const std::string &basename);

  /**
   * \brief Write the header for the XMF and HDF5 files.
   *
   * The header includes the node information, connectivity information and
   * the treeid, level or mpirank for each quadrant, if required.
   *
   * In the case of the XMF file, this will defined the topology and geometry
   * of the mesh and point to the relevant fields in the HDF5 file.
   */
  int                 write_header (double time);

  /**
   * \brief Write a node-centered or cell-centered attribute.
   *
   * \param [in] w The writer.
   * \param [in] name The name of the attribute.
   * \param [in] data The data to be written.
   * \param [in] dim In the case of a vector, this is the dimension of each
   * element in the vector field.
   * \param [in] ftype The type of the attribute. See supported types in
   *  the io_attribute_type_t enum.
   * \param [in] dtype The type of the data we are writing. This is given as a
   * native HDF5 type. See the types defined in the H5Tpublic.h header.
   * \param [in] wtype The type of the data written to the file. The
   * conversion between the data type and the written data is handled
   * by HDF5.
   */
  int                 write_attribute (const std::string &name,
				       void *data,
				       size_t dim,
				       io_attribute_type_t ftype,
				       hid_t dtype, hid_t wtype);
  
  /**
   * \brief A getter type for a quadrant.
   */
  using io_scalar_getter_t = double (*) (p4est_quadrant_t * q);

  /**
   * \brief Write a cell-centered scalar attribute.
   *
   * \param [in] name The name of the attribute.
   * \param [in] userdata_mgr UserDataManager object.
   * \param [in] get_fn A function pointer to a UserDataManager member that retrieves a specific quadrant attribute.
   * \param [in] type_id The HDF5 type id of the data to be written. The getter
   * returns a double, but we can cast it into another type when writing to the
   * file.
   */
  int                 write_quadrant_attribute (const std::string& name,
						UserDataManagerBase* userdata_mgr,
						const qdata_getter_t get_fn,
						hid_t type_id);

  /**
   * Same as above but without the need to used a UserDataManager.
   */
  int                 write_quadrant_attribute (const std::string& name,
						const simple_data_getter_t get_fn,
						hid_t type_id);

  /**
   * \brief Write a cell-centered vector attribute.
   *
   * \param [in] name The name of the attribute.
   * \param [in] userdata_mgr UserDataManager object.
   * \param [in] dim The dimension of the vector.
   * \param [in] get_fn A function pointer to a UserDataManager member that retrieves a specific quadrant attribute.
   * \param [in] type_id The HDF5 type id of the data to be written. The getter
   * returns a double, but we can cast it into another type when writing to the
   * file.
   */
  int                 write_quadrant_attribute (const std::string& name,
						UserDataManagerBase* userdata_mgr,
						size_t dim,
						const qdata_vector_getter_t get_fn,
						hid_t type_id);

  /**
   * Same as above but without the need to used a UserDataManager.
   */
  int                 write_quadrant_attribute (const std::string& name,
						size_t dim,
						const simple_data_vector_getter_t get_fn,
						hid_t type_id);

  /**
   * \brief Write a lnode-centered attribute. Only works for degree 1 lnodes.
   *
   * \param [in] name The name of the attribute.
   * \param [in] lnodes The \c p4est_lnodes_t pointer describing the lnodes
   * \param [in] lnode_data The actual data, in lnode order
   * \param [in] type_id The HDF5 type id of the data to be written.
   */
  int                 write_lnode_attribute (const std::string& name,
					     p4est_lnodes_t *lnodes,
					     double *lnode_data,
					     hid_t type_id);

  /**
   * \brief Write a lnode-centered attribute. Only works for degree 1 lnodes.
   *
   * \param [in] name The name of the attribute.
   * \param [in] lnodes The \c p4est_lnodes_t pointer describing the lnodes
   * \param [in] lnode_data The actual data, in lnode order
   * \param [in] type_id The HDF5 type id of the data to be written
   * \param [in] hang_default The value to set for hanging nodes.
   */
  int                 write_lnode_attribute (const std::string& name,
					     p4est_lnodes_t *lnodes,
					     double *lnode_data,
					     hid_t type_id,
                         double hang_default);

  /**
   * \brief Write the XMF footer.
   *
   * Closes all the XML tags.
   */
  int                 write_footer ();

private:
  /*
   * XMDF utilities.
   */

  /**
   * \brief Write the header of the main XMF file.
   */
  void io_xmf_write_main_header();

  /**
   * \brief Write the XMF header information: topology and geometry.
   */
  void io_xmf_write_header (double time);

  /**
   * \brief Write the include for the current file.
   */
  void io_xmf_write_main_include (const std::string &name);

  /**
   * \brief Write information about an attribute.
   *
   * \param [in] fd   file descriptor for xmf file
   * \param [in] basename The basename.
   * \param [in] name The name of the attribute.
   * \param [in] tyep The type.
   * \param [in] dims The dimensions of the attribute. If it is a scalar, dims[1]
   * will be ignored.
   */
  void io_xmf_write_attribute(const std::string &name,
			      const std::string &number_type,
			      io_attribute_type_t type,
			      hsize_t dims[2]);

  /**
   * \brief Close the remaining tags for the main file.
   */
  void io_xmf_write_main_footer();

  /**
   * \brief Close all remaining tags.
   *
   * \param[in] fd xmff file descriptor
   */
  void io_xmf_write_footer();

  /*
   * HDF5 utilities.
   */

  /**
   * \brief Write a given dataset into the HDF5 file.
   *
   * \param [in] fd An open file descriptor to a HDF5 file.
   * \param [in] name The name of the dataset we are writing.
   * \param [in] data The data to write.
   * \param [in] dtype_id The native HDF5 type of the given data.
   * \param [in] wtype_id The native HDF5 type of the written data.
   * \param [in] rank The rank of the dataset. 1 if it is a vector, 2 for a matrix.
   * \param [in] dims The global dimensions of the dataset.
   * \param [in] count The local dimensions of the dataset.
   * \param [in] start The offset of the local data with respect to the global
   * positioning.
   *
   * \sa H5TPublic.h
   */
  void
  io_hdf_writev(hid_t fd, const std::string &name, void *data,
		hid_t dtype_id, hid_t wtype_id, hid_t rank,
		hsize_t dims[], hsize_t count[], hsize_t start[]);

  /**
   * \brief Compute and write the coordinates of all the mesh nodes.
   */
  void io_hdf_write_coordinates(p4est_nodes_t * nodes);

  /**
   * \brief Compute and write the connectivity information for each quadrant.
   */
  void io_hdf_write_connectivity(p4est_nodes_t * nodes);

  /**
   * \brief Compute and write the treeid for each quadrant.
   */
  void io_hdf_write_tree();

  /**
   * \brief Compute and write the level for each quadrant.
   */
  void io_hdf_write_level();

  /**
   * \brief Compute and write the MPI rank for each quadrant.
   *
   * The rank is wrapped with IO_MPIRANK_WRAP.
   */
  void io_hdf_write_rank();

}; // class IO_Writer

#endif /* IO_WRITER_H */
