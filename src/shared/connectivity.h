#ifndef CONNECTIVITY_H
#define CONNECTIVITY_H

#ifdef USE_3D
#include <p4est_to_p8est.h>
#endif

#ifndef P4_TO_P8
#include <p4est_connectivity.h>
#else
#include <p8est_connectivity.h>
#endif

class ConfigReader;

typedef enum {
  CONNECTIVITY_PERIODIC_FALSE = 0,
  CONNECTIVITY_PERIODIC_TRUE = 1
} connectivity_periodic_t;


/**
 * \brief Connectivity with two side-by-side trees along direction x.
 *
 * The trees can be periodic in any direction.
 */
#ifndef P4_TO_P8
p4est_connectivity_t *
p4est_connectivity_new_two (connectivity_periodic_t is_periodic_x,
			    connectivity_periodic_t is_periodic_y);
#else
p4est_connectivity_t *
p4est_connectivity_new_two (connectivity_periodic_t is_periodic_x,
			    connectivity_periodic_t is_periodic_y,
			    connectivity_periodic_t is_periodic_z);
#endif

/**
 * \brief 2D connectivity with two side-by-side trees.
 */
p4est_connectivity_t *p4est_connectivity_new_two_simple (void);

/**
 * \brief 2D connectivity with 10 side-by-side trees.
 *
 * The trees are periodic in the y direction.
 */
p4est_connectivity_t *p4est_connectivity_new_shock_tube (void);

#ifndef P4_TO_P8
/**
 * \brief 2D connectivity with five trees in a tetris-like shape.
 *
 *   _ _ _ _
 *  |_|_|_|_|
 *    |_|
 * 
 * Tree numbering:
 *  0 1 2 3
 *    4
 *
 * Vertex numbering:
 * 0  1  2  3  4
 * 5  6  7  8  9
 *   10 11
 * 
 * Note: the 3D equivalent is possible, just use the abaqus input file "tetris3d.inp"
 *
 */
p4est_connectivity_t *p4est_connectivity_new_tetris (void);

/**
 * \brief 2D connectivity with ring shape (cylindrical geometry).
 *
 * No geometry required.
 *
 * \param[in] num_trees_radial number of trees in the radial direction
 * \param[in] num_trees_orthoradial number of trees in the orthoradial direction
 * \param[in] rMin radius of the inner border
 * \param[in] rMax radius of the outer border
 */
p4est_connectivity_t *
p4est_connectivity_new_ring (int num_trees_radial,
			     int num_trees_orthoradial,
			     double rMin,
			     double rMax);

/**
 * \brief 2D connectivity with shell shape (cylindrical geometry).
 *
 * This connectivity is adapted from p4est builtin (3D) shell connectivity.
 * A geometry is required, namely shell2d.
 *
 * This connectivity (adapted from shell in 3D) is  designed using the
 * following ideas:
 * - let's consider a square (4 faces); for each face we define only 2 trees
 * - these tree will be transform from a logical space to cylindrical physical
 *   space (using a p4est_geometry_t structure).
 *
 * The input logical space spans (alpha,beta) in [-1, 1] x [1, 2].
 * Then geometrical transformation will be:
 * x = R cos(theta)
 * y = R sin(theta)
 * with theta = PI/4*alpha (and use trigonometric formula
 *   cos(theta) = 1 / sqrt(1 + tan(theta)^2)
 * and R defined as rMin^2/rMax x (rMax/rMin)^beta
 * so that R(beta=1) = rMin and R(beta=2) = rMax
 *
 * For each original square face, we have 2 trees; so in total we have 4x2=8
 * trees which are all deduced from the first two by successive PI/2 rotation.
 *
 * In vertex space:
 * first  dimension is angular
 * second dimension is radial (logatithmic scaling)
 * third  dimension is z
 *
 */
p4est_connectivity_t *
p4est_connectivity_new_shell2d ();

/**
 * \brief 2D connectivity with disk shape.
 */
p4est_connectivity_t *
p4est_connectivity_new_disk2d ();

/**
 * \brief 2D connectivity using an icosahedron to map the sphere.
 *
 * Vextex numbering:
 *
 *    A00   A01   A02   A03   A04
 *   /   \ /   \ /   \ /   \ /   \
 * A05---A06---A07---A08---A09---A10
 *   \   / \   / \   / \   / \   / \
 *    A11---A12---A13---A14---A15---A16
 *      \  /  \  /  \  /  \  /  \  /
 *      A17   A18   A19   A20   A21
 *
 * Origin of coordinate in A05.
 *
 * Tree numbering:
 *
 * 0  2  4  6  8
 *  1  3  5  7  9
 */
p4est_connectivity_t *
p4est_connectivity_new_icosahedron (void);


#endif

#ifndef P4_TO_P8
/**
 * \brief 2D connectivity for forward facing step test (small).
 *
 *
 * 12 trees.
 * 21 vertices.
 *  6 corners.
 * _ _ _ _ _ 
 *|_|_|_|_|_|
 *|_|_|_|_|_|
 *|_|_|
 *
 */
p4est_connectivity_t *
p4est_connectivity_new_forward_facing_step_small (void);
#endif /* P4_to_P8 */

#ifndef P4_TO_P8
/**
 * \brief 2D connectivity for forward facing step test.
 *
 *
 * 15 trees along x direction and 4 rows + 3 extra trees (below left).
 * That is 63 trees in total.
 * _ _ _ _ _ _ _ _ _ _
 *|_|_|_|_|_|_|_|_|_|_|
 *|_|_|_|_|_|_|_|_|_|_|
 *|_|_|_|_|_|_|_|_|_|_|
 *|_|_|_|_|_|_|_|_|_|_|
 *|_|_|_|
 *
 */
p4est_connectivity_t *
p4est_connectivity_new_forward_facing_step (void);
#endif /* P4_to_P8 */

#ifndef P4_TO_P8
/**
 * \brief 2D connectivity for backward facing step test.
 *
 *
 * 3 trees
 * _ _ 
 *|_|_|
 *  |_|
 *
 */
p4est_connectivity_t *
p4est_connectivity_new_backward_facing_step (void);
#endif /* P4_to_P8 */

/**
 * \brief Print all the information in the connectivity struct.
 */
void                connectivity_print (p4est_connectivity_t * c);

/**
 * \brief Get a connectivity by name.
 *
 * Allowed p4est values are for 2D:
 * brick23, corner, cubed, disk, moebius, periodic, pillow, rotwrap, star, unit
 * and for 3D:
 * brick235, periodic, rotcubes, rotwrap, shell, sphere, twocubes, twowrap, unit
 *
 * Besides the p4est names found in p4est_connectivity.h, we have:
 *      two         connectivity_new_two
 *      two_simple  connectivity_new_two_simple
 *      shock_tube  connectivity_new_shock_tube
 *
 * This function first looks for p4est provided connectivities, if the name
 * is not found there, it falls back to our own and as a last resort, tries
 * to read the connectivity with the filename "name" using
 * p4est_connectivity_read_inp.
 *
 * \param[in] name A connectivity name.
 * \param[in] cfg A config reader.
 * \return A fully allocated connectivity.
 */
p4est_connectivity_t *connectivity_new_byname (const char *name,
					       ConfigReader * cfg);

#endif /* CONNECTIVITY_H */
