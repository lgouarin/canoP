#include <cmath>

// ====================================================================
// ====================================================================
void eos(double rho,
	 double eint,
	 double * p,
	 double * c,
	 double gamma0,
	 double smallp)
{
  *p = fmax((gamma0 - 1.0) * rho * eint, rho * smallp);
  *c = sqrt(gamma0 * (*p) / rho);
} // eos

void isothermal_eos(double rho,
	 double * p,
	 double c,
	 double smallp)
{
  *p = fmax(rho * c * c, rho * smallp);
} // isothermal_eos
