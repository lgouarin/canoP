#ifndef SOLVER_H_
#define SOLVER_H_

#ifdef USE_3D
#include <p4est_to_p8est.h>
#endif

#ifndef P4_TO_P8
#include <p4est.h>
#include <p4est_extended.h>
#include <p4est_geometry.h>
#include <p4est_ghost.h>
#include <p4est_mesh.h>
#include <p4est_iterate.h>
#else
#include <p8est.h>
#include <p8est_extended.h>
#include <p8est_geometry.h>
#include <p8est_ghost.h>
#include <p8est_mesh.h>
#include <p8est_iterate.h>
#endif

#include <string>
#include <vector>
#include <mpi.h>

class Statistics;
class ConfigReader;
class IO_Writer;
class SolverWrap;
#include "SolverCore.h"
class IteratorsBase;
class UserDataManagerBase;
//#include "gradient_reconstruction.h"

#include "ScalarLimiterFactory.h"
using scalar_limiter_t = ScalarLimiterFactory::scalar_limiter_t;


/**
 * Abstract Base class for solving a canoP problem.
 *
 * Provides a backbone to build the time stepping integration procedure.
 * Collect performance (timing) and monitoring (statistics) information.
 *
 * A reall application should derived from this class, to customize the time 
 * integration routine.
 */
class Solver
{

public:

  /**
   * constructor.
   * \param[in] cfg a ConfigReader object (for parsing input parameter file).
   * \param[in] mpicomm MPI communicatior.
   * \param[in] qdata_t_sizeof size in bytes of qdata_t hold by each p4est leaf.
   * \param[in] useP4estMemory specifies if we use p4est internal memory pool.
   *
   * We need to pass here qdata size, because p4est is initialized here but 
   * the actual qdata_t is a template parameter only known in derived concrete
   * Solver class.
   */
  Solver(ConfigReader * cfg,
	 MPI_Comm       mpicomm,
	 int            qdata_t_sizeof,
	 bool           useP4estMemory);

  //! destructor
  virtual ~Solver();

  /**
   * this is were p4est_new is called, parameter file is read, solver_core is setup.
   */
  void init();

  /**
   * companion routine to init.
   * memory release.
   */
  void finalize();
  
  //! p4est structures wrapper.
  SolverWrap         *m_wrap;

  //! io writer object.
  IO_Writer          *m_writer;

  //! Statistics collecting object.
  Statistics         *m_stats;

  
  // data read from the configuration file
  ConfigReader       *m_cfg;      /* the configuration reader */
  MPI_Comm            m_mpicomm;
  double              m_tini;     /* user defined initial time */
  double              m_tmax;     /* user defined maximum time */
  double              m_cfl;      /* user defined Courant number */
  int                 m_max_iterations; /* user defined maximum iteration count */

  //! iterator structure pointer.
  //! Should be itialized in derived class,
  //! from the solver factory.
  IteratorsBase      *m_pIter = nullptr;

  /**
   * sizeof qdata_t (will be known in derived concrete class
   * but needed here to pass to p4est (when using internal memory management).
   */
  int                 m_qdata_t_sizeof;

  //! Do we use p4est internal memory pool.
  bool                m_use_p4est_memory;

  //! is this a restart run ?
  int                 m_restart_run_enabled;

  //! filename containing data from a previous run.
  std::string         m_restart_run_filename;

  //! solver name (use in output file).
  std::string         m_solver_name;

  //! p4est coarse grained inter-tree connectivity object.
  std::string         m_connectivity_name;

  //! p4est geometry name used for computation.
  std::string         m_geometry_name_compute;

  //! p4est geometry name used for io.
  //! it is different from m_geometry_name_compute
  //! because, e.g. in cylindrical coordinates (r,theta,z)
  //! computation is logically cartesian in these coordiantes,
  //! and we actually use the geometry transformation, to visualize
  //! the solution in cylindrical coordinates.
  std::string         m_geometry_name_io;

  //! minimal number of quadrants per MPI processor.
  int                 m_min_quadrants;

  //! TODO
  int                 m_uniform_fill;

  //! minimum level of refinement
  int                 m_min_refine;

  //! maximum level of refinement
  int                 m_max_refine;

  //! threshold value used in refine indicator callback
  double              m_epsilon_refine;

  //! threshold value used in coarsen indicator callback
  double              m_epsilon_coarsen;

  //! geometric refinement
  int                 m_geometric_refine;

  //! physical refinement
  int                 m_physical_refine;

  //! weight exponent for the load balancing
  int                 m_weight_exponent;

  //! subcycle enabled (TODO)
  int                 m_subcycle;

  int                 m_gather_statistics;

  //! output level statistics at each time step
  int                 m_level_stats;

  int                 m_mesh_info;

  int                 m_single_precision;

  int                 m_max_save_count;

  int                 m_max_restart_count;
  
  //! total number of quadrant update
  p4est_gloidx_t      m_total_num_cell_update;

  //! space order of scheme (with MUSCL reconstruction)
  int                 m_space_order;

  //! time order (hancock prediction or not).
  //! TODO: see if this should be application specific, i.e.
  //! moved to derived class
  int                 m_time_order;

  //! type of dimensional splitting (strang or not)
  //! TODO : moved that to derived class 
  int                 m_dim_splitting;

  //! count number of output files
  int                 m_times_saved;

  //! count number of restart files
  int                 m_times_saved_restart;
  
  //! Number of variables to saved
  int                 m_num_write_variables;

  //! names of variables to save
  std::vector<std::string> m_write_variables;

  //! solver core implementation (interface to actual user data)
  SolverCoreBase     *m_solver_core;

  //! userdata manager is the gatekeeper to access actual quadrant data
  //! Should provide a solution for both situation:
  //! 1. when using internal p4est memory pool
  //! 2. when using external memory storage (e.g. a Kokkos array or hash table).
  UserDataManagerBase *m_userdata_mgr;
  
  //! limiter name.
  std::string          m_limiter_name;
  //! limiter callback.
  scalar_limiter_t     m_limiter_fn;

  // iteration info
  double               m_t;        /* the time at the current iteration */
  double               m_dt;       /* the time step at the current level */
  int                  m_iteration;/* the current iteration */
  int                  m_minlevel; /* the min level over MPI in the forest */
  int                  m_maxlevel; /* the max level over MPI in the forest */
  //int                 m_nOutput;  /* the current output file number */

  // p4est structure interface
  p4est_t*          get_p4est();
  p4est_geometry_t* get_geom_compute();
  p4est_geometry_t* get_geom_io();
  p4est_ghost_t*    get_ghost();
  void*             get_ghost_data_ptr();
  p4est_mesh_t*     get_mesh();

  // other structures
  UserDataManagerBase* get_userdata_mgr() {return m_userdata_mgr; };
  
  /*
   *
   * Computation interface that may be overriden in a derived 
   * concrete implementation.
   *
   */

  //! Compute CFL condition (allowed time step).
  virtual void      compute_time_step();

  //! This is where action takes place. Wrapper arround next_iteration_impl.
  virtual void      next_iteration();

  //! This is the next iteration computation (application specific).
  virtual void      next_iteration_impl(); // scheme specific here

  //! Check if current time is larger than end time.
  virtual int       finished (); 

  //! Provides a "strategy" to decide when to adapt
  virtual int       should_adapt ();

  //! Decides if the current time step is eligible for dump data to file
  virtual int       should_write ();

  //! Decides if the current time step is eligible for create a restart file
  virtual int       should_write_restart ();
  
  //! wrapper arround adapt_cycle
  void              adapt (int initial_refine);

  //! collect and write statistics if enabled
  void              write_statistics ();
  
  //! main routine to dump solution to file
  virtual void      save_solution ();

  //! Write restart file using internal format from p4est
  void              write_restart_file ();

  /**
   * Read and parse the configuration file (lua).
   */
  virtual void      read_config();

  /**
   * Print the configuration.
   */
  virtual void      print_config();

private:
  /**
   * This method asks the UserDataManager to realloc its ghost_data 
   * array member.
   */
  void              ghost_data_new(int nbGhosts);

  //! Perform initial refinement (in Solver's constructor)
  void              initial_refinement();

  //! Perform the actual adapt cycle (designed to be called by adapt)
  p4est_locidx_t    adapt_cycle (int initial_refine);

  //! Compute level statistics
  virtual void      compute_level_statistics();

  //! Write HDF5 file (designed to be called inside save_solution)
  virtual void      write_hdf5 (const std::string &filename);

  //! a simple utility to make a list of variables
  void tokenize_variables();
  
}; // class Solver


// other utilities
Solver *solver_from_p4est (p4est_t * p4est);

/**
 * \brief Return the user data for a given quadrant.
 *
 * This function checks whether the requested quadrant is a ghost or not
 * and returns the relevant user_data pointer. It uses the tree and quadid
 * as given by p4est_mesh_t, i.e. if which_quad > local_num_quadrants,
 * then it is a ghost quadrant.
 */
// void *solver_get_data (Solver         *solver,
// 		       p4est_topidx_t  which_tree,
// 		       p4est_topidx_t  which_quad);
#endif // SOLVER_H_
