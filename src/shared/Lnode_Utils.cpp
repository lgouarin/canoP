
#include "Lnode_Utils.h"

#ifndef P4_TO_P8
#include <p4est.h>
#include <p4est_lnodes.h>
#else
#include <p8est.h>
#include <p8est_lnodes.h>
#endif

#include <functional>

#include "canoP_base.h"
#include "quadrant_utils.h"

static const int zero = 0;
static const int ones = P4EST_CHILDREN - 1;  /**< One bit per dimension. */

const int lnodes_corner_num_hanging[P4EST_CHILDREN] =
#ifndef P4_TO_P8
  { 1, 2, 2, 1 }
#else
  { 1, 2, 2, 4, 2, 4, 4, 1 }
#endif
;

const int *lnodes_corner_to_hanging[P4EST_CHILDREN] = {
  &zero,
#ifdef P4_TO_P8
  p8est_edge_corners[0],
  p8est_edge_corners[4],
  p8est_face_corners[4],
  p8est_edge_corners[8],
#endif
  p4est_face_corners[2],
  p4est_face_corners[0],
  &ones,
};


int
lnodes_decode (p4est_lnodes_code_t face_code,
               int hanging_corner[P4EST_CHILDREN])
{
  if (face_code) {
    const int           c = (int) (face_code & ones);
    int                 i, h;
    int                 work = (int) (face_code >> P4EST_DIM);

    /* These two corners are never hanging by construction. */
    hanging_corner[c] = hanging_corner[c ^ ones] = -1;
    for (i = 0; i < P4EST_DIM; ++i) {
      /* Process face hanging corners. */
      h = c ^ (1 << i);
      hanging_corner[h ^ ones] = (work & 1) ? c : -1;
#ifdef P4_TO_P8
      /* Process edge hanging corners. */
      hanging_corner[h] = (work & P4EST_CHILDREN) ? c : -1;
#endif
      work >>= 1;
    }
    return 1;
  }
  return 0;
} /* lnodes_decode */


void
lnodes_interpolate_hanging (p4est_lnodes_code_t face_code,
                           double inplace[P4EST_CHILDREN])
{
  const int           c = (int) (face_code & ones); // corner id
  int                 i, j;
  int                 ef;
  int                 work = (int) (face_code >> P4EST_DIM); // face/direction id
  double              sum;
  const double        factor = 1. / P4EST_HALF;

  /* Compute face hanging nodes first (this is all there is in 2D). */
  for (i = 0; i < P4EST_DIM; ++i) {
    if (work & 1) {
      ef = p4est_corner_faces[c][i];
      sum = 0.;
      for (j = 0; j < P4EST_HALF; ++j) {
        sum += inplace[p4est_face_corners[ef][j]];
      }
      inplace[c ^ ones ^ (1 << i)] = factor * sum;
    }
    work >>= 1;
  }

#ifdef P4_TO_P8
  /* Compute edge hanging nodes afterwards */
  for (i = 0; i < P4EST_DIM; ++i) {
    if (work & 1) {
      ef = p8est_corner_edges[c][i];
      inplace[c ^ (1 << i)] = .5 * (inplace[p8est_edge_corners[ef][0]] +
                                    inplace[p8est_edge_corners[ef][1]]);
    }
    work >>= 1;
  }
#endif
} /* lnodes_interpolate_hanging */


template<typename data_t, class op_t>
static void
lnodes_share_generic (p4est_t * p4est, p4est_lnodes_t * lnodes,
    op_t op, data_t *v)
{
  const int           nloc = lnodes->num_local_nodes;
  const int           npeers = (int) lnodes->sharers->elem_count;
  int                 iq, jn;
  int                 gl;
  sc_array_t          node_data;
  p4est_lnodes_rank_t *lrank;
  p4est_lnodes_buffer_t *buffer;

  sc_array_init_data (&node_data, v, sizeof (data_t), nloc);
  buffer = p4est_lnodes_share_all (&node_data, lnodes);

  for (iq = 0; iq < npeers; ++iq) {
    sc_array_t         *recv_data =
      (sc_array_t *) sc_array_index_int (buffer->recv_buffers, iq);

    CANOP_ASSERT (recv_data->elem_size == node_data.elem_size);
    lrank = (p4est_lnodes_rank_t *) sc_array_index_int (lnodes->sharers, iq);
    if (lrank->rank != p4est->mpirank) {
      const int           nshared = (int) lrank->shared_nodes.elem_count;
      const data_t       *w = (const data_t *) recv_data->array;

      CANOP_ASSERT ((int) recv_data->elem_count == nshared);

      for (jn = 0; jn < nshared; ++jn) {
        gl = (int)
          *(p4est_locidx_t *) sc_array_index_int (&lrank->shared_nodes, jn);
        CANOP_ASSERT (0 <= gl && gl < nloc);
        v[gl] = op(v[gl], w[jn]);
      }
    }
#ifdef P4EST_ENABLE_DEBUG
    else {
      CANOP_ASSERT (recv_data->elem_count == 0);
      CANOP_ASSERT (lrank->owned_offset == 0);
      CANOP_ASSERT (lrank->owned_count == lnodes->owned_count);
    }
#endif
  }

  p4est_lnodes_buffer_destroy (buffer);
  sc_array_reset (&node_data);
} /* lnodes_share_generic */


void
lnodes_share_sum (p4est_t * p4est, p4est_lnodes_t * lnodes, double *v)
{
  return lnodes_share_generic(p4est, lnodes, std::plus<double>(), v);
} /* lnodes_share_sum */


void
interp_quad_to_lnodes (p4est_t * p4est, p4est_lnodes_t * lnodes,
                       std::function<double(p4est_quadrant_t *)> quad_getter,
                       std::function<double(p4est_t *, p4est_topidx_t, p4est_quadrant_t *)> quad_volume,
                       double *lnodes_data)
{
  /* local nodes number */
  const int           nloc = lnodes->num_local_nodes;

  p4est_topidx_t      tt;
  p4est_tree_t       *tree;
  p4est_quadrant_t   *quad;
  sc_array_t         *tquadrants;
  int                 i, j, k;
  int                 q, Q;
  p4est_locidx_t      lni, lni_h;
  int                 anyhang, hanging_corner[P4EST_CHILDREN];
  int                 c, h;
  int                 ncontrib;
  const int          *contrib_corner;
  double              quad_data, volume, factor;
  double             *lnodes_weights;

  lnodes_weights    = CANOP_ALLOC (double, lnodes->num_local_nodes);

  for (lni = 0; lni < nloc; ++lni) {
    lnodes_data[lni] = 0;
    lnodes_weights[lni] = 0;
  }

  for (tt = p4est->first_local_tree, k = 0;
      tt <= p4est->last_local_tree; ++tt) {
    tree = p4est_tree_array_index (p4est->trees, tt);
    tquadrants = &tree->quadrants;
    Q = (p4est_locidx_t) tquadrants->elem_count;
    for (q = 0; q < Q; ++q, ++k) {
      quad = p4est_quadrant_array_index (tquadrants, q);

      quad_data = quad_getter(quad);
      volume = quad_volume(p4est, tt, quad);
      factor = 0.;

      /* figure out the hanging corners on this element, if any. */
      anyhang = lnodes_decode (lnodes->face_code[k], hanging_corner);

      /* count the number of independent nodes for this quadrant */
      if (anyhang) {
        for (i = 0; i < P4EST_CHILDREN; ++i) {
          if (hanging_corner[i] < 0)
            factor += 1.;
        }
      }
      else {
        factor = (double)P4EST_CHILDREN;
      }

      /* compute the corrective factor to take into account the hanging nodes */
      factor = (double)P4EST_CHILDREN / factor;

      /* fill lnodes attached data */
      for (i = 0; i < P4EST_CHILDREN; ++i) {
        lni = lnodes->element_nodes[P4EST_CHILDREN * k + i];

        if (anyhang && hanging_corner[i] >=0 ) {
          /* update lnodes_weights for corresponding independent nodes */
          c = hanging_corner[i];
          ncontrib = lnodes_corner_num_hanging[i ^ c];
          contrib_corner = lnodes_corner_to_hanging[i ^ c];
          for (j = 0; j < ncontrib; ++j) {
            h = contrib_corner[j] ^ c;  /* Inverse transform of node number. */
            lni_h = lnodes->element_nodes[P4EST_CHILDREN * k + h];
            lnodes_weights[lni_h] += volume / (double)ncontrib;
          }
        }
        else {
          lnodes_weights[lni] += volume;
          lnodes_data   [lni] += volume * factor * quad_data;
        }
      }

    } /* end for q */
  } /* end for tt */

  /* Gather contribution from all MPI ranks */
  lnodes_share_sum (p4est, lnodes, lnodes_data);
  lnodes_share_sum (p4est, lnodes, lnodes_weights);

  /* Apply weights */
  for (lni = 0; lni < nloc; ++lni) {
    lnodes_data[lni] /= lnodes_weights[lni];
  }

  CANOP_FREE(lnodes_weights);
} /* interp_quad_to_lnodes */

void
interp_lnodes_to_quad (p4est_t * p4est, p4est_lnodes_t * lnodes,
                       const double *lnodes_data,
                       std::function<void(p4est_quadrant_t *, double)> quad_setter)
{
  p4est_topidx_t      tt;
  p4est_tree_t       *tree;
  p4est_quadrant_t   *quad;
  sc_array_t         *tquadrants;
  p4est_locidx_t      lni;
  int                 i, k;
  int                 q, Q;

  double              ndata[P4EST_CHILDREN];
  double              qdata;

  for (tt = p4est->first_local_tree, k = 0;
       tt <= p4est->last_local_tree; ++tt) {
    tree = p4est_tree_array_index (p4est->trees, tt);
    tquadrants = &tree->quadrants;
    Q = (p4est_locidx_t) tquadrants->elem_count;
    for (q = 0; q < Q; ++q, ++k) {
      quad = p4est_quadrant_array_index (tquadrants, q);

      /* retrieve local node data, interpolating hanging nodes */
      for (i = 0; i < P4EST_CHILDREN; ++i) {
        lni = lnodes->element_nodes[P4EST_CHILDREN * k + i];
        ndata[i] = lnodes_data[lni];
      }
      lnodes_interpolate_hanging (lnodes->face_code[k], ndata);

      /* interpolate back to quad data */
      qdata = 0;
      for (i = 0; i < P4EST_CHILDREN; ++i) {
        qdata += ndata[i] / (double)P4EST_CHILDREN;
      }

      quad_setter(quad, qdata);
    } /* end for q */
  } /* end for tt */
}

void
gradient_lnodes_to_quad (p4est_t * p4est, p4est_lnodes_t * lnodes,
                         std::function<double(p4est_t *, p4est_topidx_t, p4est_quadrant_t *)> quad_size,
                         const double *lnodes_data,
                         std::function<void(p4est_quadrant_t *, double *)> quad_setter)
{
  p4est_topidx_t      tt;
  p4est_tree_t       *tree;
  p4est_quadrant_t   *quad;
  sc_array_t         *tquadrants;
  p4est_locidx_t      lni;
  int                 i, j, k;
  int                 q, Q;

  double              ndata[P4EST_CHILDREN];
  double              gdata[P4EST_DIM];
  double              sgn, dx;

  for (tt = p4est->first_local_tree, k = 0;
       tt <= p4est->last_local_tree; ++tt) {
    tree = p4est_tree_array_index (p4est->trees, tt);
    tquadrants = &tree->quadrants;
    Q = (p4est_locidx_t) tquadrants->elem_count;
    for (q = 0; q < Q; ++q, ++k) {
      quad = p4est_quadrant_array_index (tquadrants, q);
      dx = quad_size(p4est, tt, quad);

      /* retrieve local node data, interpolating hanging nodes */
      for (i = 0; i < P4EST_CHILDREN; ++i) {
        lni = lnodes->element_nodes[P4EST_CHILDREN * k + i];
        ndata[i] = lnodes_data[lni];
      }
      lnodes_interpolate_hanging (lnodes->face_code[k], ndata);

        /* compute gradient */
        for (i = 0; i < P4EST_DIM; ++i) {
          gdata[i] = 0;
          for (j = 0; j < P4EST_CHILDREN; ++j) {
            /* the gradient is computed as (far face average) - (close face
             * average), (j & (1 << i)) >> i is the coordinate of the current
             * node in direction i, a value of 1 corresponds to the far face,
             * and 0 to the close face.
             */
            if (j & (1 << i))
              sgn = 1;
            else
              sgn = -1;
            gdata[i] += sgn * ndata[j] / (double)P4EST_HALF / dx;
          }
        }

      quad_setter(quad, gdata);
    } /* end for q */
  } /* end for tt */
}

void
lnodes_flag_boundaries (p4est_t *p4est, p4est_mesh_t *mesh, p4est_lnodes_t *lnodes,
                        int8_t *bc)
{
  const p4est_locidx_t nloc = lnodes->num_local_nodes;
  int                 anyhang, hanging_corner[P4EST_CHILDREN];
  int                 iface, isub, inode; /* We use plain int for small loops. */
  p4est_topidx_t      tt;       /* Connectivity variables have this type. */
  p4est_locidx_t      k, q, Q;  /* Process-local counters have this type. */
  p4est_locidx_t      lni;      /* Node index relative to this processor. */
  p4est_tree_t       *tree;     /* Pointer to one octree */
  sc_array_t         *tquadrants;       /* Quadrant array for one tree */
  int8_t              bound_node[P4EST_CHILDREN];

  memset (bc, 0, sizeof (int8_t) * nloc);

  for (tt = p4est->first_local_tree, k = 0;
       tt <= p4est->last_local_tree; ++tt) {
    tree = p4est_tree_array_index (p4est->trees, tt);   /* Current tree */
    tquadrants = &tree->quadrants;
    Q = (p4est_locidx_t) tquadrants->elem_count;
    for (q = 0; q < Q; ++q, ++k) {
      /* This is now a loop over all local elements.
       * Users might aggregate the above code into a more compact iterator. */
      anyhang = lnodes_decode (lnodes->face_code[k], hanging_corner);

      memset (bound_node, 0, sizeof (int8_t) * P4EST_CHILDREN);
      for (iface = 0; iface < P4EST_FACES; ++iface) {
        /* Determine if the face is at the global domain boundary.
         * If the neighbour quad across the face has same id as the current
         * quad, we are indeed crossing the boundary. The bound_node array
         * contains 0 for non-boundary nodes, and a non-zero number for
         * boundary nodes: for each face f, the fth bit is set to 1 iff
         * the face touches the boundary AND the node touches the face. */
        if (mesh->quad_to_quad[P4EST_FACES * k + iface] == k) {
          for (isub = 0; isub < P4EST_HALF; ++isub) {
            inode = p4est_face_corners[iface][isub];
            bound_node[inode] |= (1 << iface);
          }
        }
      }
      for (inode = 0; inode < P4EST_CHILDREN; ++inode) {
        lni = lnodes->element_nodes[P4EST_CHILDREN * k + inode];
        CANOP_ASSERT (lni >= 0 && lni < nloc);
        if (!anyhang || hanging_corner[inode] < 0) {
          bc[lni] |= bound_node[inode];
        }
      }
    }
  }

  lnodes_share_generic (p4est, lnodes, std::bit_or<int8_t>(), bc);
}

void
lnodes_set_boundaries(p4est_t *p4est, p4est_geometry_t *geom, p4est_lnodes_t *lnodes,
                      const int8_t *bc, std::function<double(double[3])> func_bc,
                      double *u)
{
  const p4est_locidx_t nloc = lnodes->num_local_nodes;
  int                 i;
  double              vxyz[3];
  int                 anyhang, hanging_corner[P4EST_CHILDREN];
  p4est_topidx_t      tt;
  p4est_locidx_t      k, q, Q;
  p4est_locidx_t      lni;
  p4est_tree_t       *tree;
  p4est_quadrant_t   *quad;
  sc_array_t         *tquadrants;

  for (tt = p4est->first_local_tree, k = 0;
       tt <= p4est->last_local_tree; ++tt) {
    tree = p4est_tree_array_index (p4est->trees, tt);
    tquadrants = &tree->quadrants;
    Q = (p4est_locidx_t) tquadrants->elem_count;
    for (q = 0; q < Q; ++q, ++k) {
      quad = p4est_quadrant_array_index (tquadrants, q);

      anyhang = lnodes_decode (lnodes->face_code[k], hanging_corner);

      for (i = 0; i < P4EST_CHILDREN; ++i) {
        /* Skip hanging nodes: they do not exist in the arrays */
        if (anyhang && hanging_corner[i] >= 0)
          continue;

        lni = lnodes->element_nodes[P4EST_CHILDREN * k + i];
        CANOP_ASSERT (lni >= 0 && lni < nloc);
        if (bc[lni]) {
          /* Transform per-tree reference coordinates into physical space. */
          quadrant_vertex_coord (p4est->connectivity, geom, tt, quad, i, vxyz);

          /* Use physical space coordinates to evaluate the function */
          u[lni] = func_bc (vxyz);
        }
      }
    }
  }
}

void
lnodes_eval(p4est_t *p4est, p4est_geometry_t *geom, p4est_lnodes_t *lnodes,
            std::function<double(double[3])> func, double *u)
{
  const p4est_locidx_t nloc = lnodes->num_local_nodes;
  int                 i;
  double              vxyz[3];
  int                 anyhang, hanging_corner[P4EST_CHILDREN];
  p4est_topidx_t      tt;
  p4est_locidx_t      k, q, Q;
  p4est_locidx_t      lni;
  p4est_tree_t       *tree;
  p4est_quadrant_t   *quad;
  sc_array_t         *tquadrants;

  for (tt = p4est->first_local_tree, k = 0;
       tt <= p4est->last_local_tree; ++tt) {
    tree = p4est_tree_array_index (p4est->trees, tt);
    tquadrants = &tree->quadrants;
    Q = (p4est_locidx_t) tquadrants->elem_count;
    for (q = 0; q < Q; ++q, ++k) {
      quad = p4est_quadrant_array_index (tquadrants, q);

      anyhang = lnodes_decode (lnodes->face_code[k], hanging_corner);

      for (i = 0; i < P4EST_CHILDREN; ++i) {
        /* Skip hanging nodes: they do not exist in the arrays */
        if (anyhang && hanging_corner[i] >= 0)
          continue;

        lni = lnodes->element_nodes[P4EST_CHILDREN * k + i];
        CANOP_ASSERT (lni >= 0 && lni < nloc);
        /* Transform per-tree reference coordinates into physical space. */
        quadrant_vertex_coord (p4est->connectivity, geom, tt, quad, i, vxyz);

        /* Use physical space coordinates to evaluate the function */
        u[lni] = func (vxyz);
      }
    }
  }
}
