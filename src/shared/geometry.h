#ifndef GEOMETRY_H_
#define GEOMETRY_H_

#ifdef USE_3D
#include <p4est_to_p8est.h>
#endif

#ifndef P4_TO_P8
#include <p4est_connectivity.h>
#include <p4est_geometry.h>
#else
#include <p8est_connectivity.h>
#include <p8est_geometry.h>
#endif

class ConfigReader;

/**
 * cylindrical geometry for computation associated to cylindrical connectivity.
 *
 * \param[in] R0 radius of the inner border
 * \param[in] R1 radius of the outer border
 * \param[in] nbTrees_theta number of tree along theta (orthoradial) 
 * \param[in] nbTrees_z     number of tree along z direction
 *
 */
p4est_geometry_t   *
p4est_geometry_new_cylindrical_compute (p4est_connectivity_t * conn,
					double R0, double R1,
					int nbTrees_theta,
					int nbTrees_z);

/**
 * cylindrical geometry used only for IO associated to cylindrical connectivity.
 *
 * \param[in] R0 radius of the inner border
 * \param[in] R1 radius of the outer border
 * \param[in] nbTrees_theta number of tree along theta (orthoradial) 
 * \param[in] nbTrees_z     number of tree along z direction
 *
 */
p4est_geometry_t   *
p4est_geometry_new_cylindrical_io (p4est_connectivity_t * conn,
				   double R0, double R1,
				   int nbTrees_theta,
				   int nbTrees_z);

#ifndef P4_TO_P8

/**
 * disk2d geometry associated to disk connectivity.
 *
 * \param[in] R radius of the outer border
 *
 */
p4est_geometry_t   *
p4est_geometry_new_disk2d (p4est_connectivity_t * conn, double R0, double R1);

/**
 * shell2d geometry associated to shell2d connectivity.
 *
 * \param[in] R2 radius of the outer border
 * \param[in] R1 radius of the inner border
 *
 * \sa p4est_connectivity_new_shell2d for a full description.
 */
p4est_geometry_t   *
p4est_geometry_new_shell2d (p4est_connectivity_t * conn, double R2, double R1);

/**
 * shell2d for cylindrical geometry associated to shell2d connectivity.
 *
 * \param[in] R2 radius of the outer border
 * \param[in] R1 radius of the inner border
 *
 * \sa p4est_connectivity_new_shell2d for a full description.
 */
p4est_geometry_t   *
p4est_geometry_new_shell2d_cyl (p4est_connectivity_t * conn, double R2, double R1);

/**
 * icosahedron geometry associated to icosahedron connectivity.
 *
 * \param[in] a icosahedron edge length
 *
 * \sa p4est_connectivity_new_icosahedron for a full description.
 */
p4est_geometry_t   *
p4est_geometry_new_icosahedron (p4est_connectivity_t * conn,
				double R);

#endif /* P4_TO_P8 */

/**
 * \brief Create a geometry by name.
 *
 * This is optional. If you don't do anything, i.e. geometry parameter is not set, the
 * default geometry is cartesian.
 *
 * Allowed values for 2D:
 * shell2d
 * icosahedron (map the sphere)
 *
 * Allowed values for 3D:
 * shell
 *
 * \param[in] name A geometry name.
 * \param[in] conn A connectivity already created.
 * \param[in] cfg A config reader.
 * \return A fully allocated connectivity.
 */
p4est_geometry_t *geometry_new_byname (const char *name,
				       p4est_connectivity_t *conn,
				       ConfigReader * cfg);


#endif // GEOMETRY_H_
