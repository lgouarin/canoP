#include "Solver.h"
#include "SolverWrap.h"
#include "SolverCore.h"

#include "canoP_base.h"
#include "IO_Writer.h"
#include "UserDataManager.h"

#include "Statistics.h"
#include "ConfigReader.h"
//#include "QuadrantNeighborUtils.h"
#include "quadrant_utils.h"
#include "Iterators.h"
#include "connectivity.h"
#include "geometry.h"
#include "utils.h"

#ifndef P4_TO_P8
#include <p4est_connectivity.h>
const static std::string P4EST_FOREST_SUFFIX("p4est");
#else
#include <p8est_connectivity.h>
const static std::string P4EST_FOREST_SUFFIX("p8est");
#endif

#include <sstream>
#include <iomanip>
#include <ctime>
#include <mpi.h>
//#define IO_FILE_FORMAT "%s_%05d"
//#define IO_FILE_FORMAT_RESTART "%s_%05d_restart.%s"


// =======================================================
// ==== STATIC UTILS =====================================
// =======================================================

/**
 *
 * This replace callback can be used either when refining or coarsening.
 *
 * When refining, num_outgoing = 1 and num_incoming = P4EST_CHILDREN.
 * num_outgoing == 1 is used to trigger the refining behavior and actually
 * num_incoming is discarded
 *
 * When coarsening, num_outgoing = P4EST_CHILDREN and num_incoming = 1.
 * num_incoming == 1 is used to trigger the coarsening behavior and actually
 * num_outgoing is discarded
 *
 * \param [in] p4est          the forest
 * \param [in] which_tree     the tree in the forest containing \a children
 * \param [in] num_outgoing   the number of quadrants that are being replaced:
 *                            either 1 if a quadrant is being refined, or
 *                            P4EST_CHILDREN if a family of children are being
 *                            coarsened.
 * \param [in] quadout        the outgoing quadrants
 * \param [in] num_incoming   the number of quadrants that are being added:
 *                            either P4EST_CHILDREN if a quadrant is being refined, or
 *                            1 if a family of children are being
 *                            coarsened.
 * \param [in,out] quadin     quadrants whose data are initialized.
 */
static void
quadrant_replace_fn (p4est_t * p4est,
                     p4est_topidx_t which_tree,
                     int num_outgoing,
                     p4est_quadrant_t * quadout[],
                     int num_incoming, 
		     p4est_quadrant_t * quadin[])
{

  UNUSED (p4est);
  UNUSED (which_tree);
  UNUSED (num_incoming);

  // get the solver p4est interface
  Solver *solver = solver_from_p4est (p4est);

  if (num_outgoing == 1) {      // we're refining

    // parent data ar in quadout[0]
    for (int8_t i = 0; i < P4EST_CHILDREN; ++i) {
      solver->m_userdata_mgr->quadrant_set_state_data (quadin[i], quadout[0]);
    }

    /*
     * HERE is a placeholder for other refine policies.
     */
    
    // when refining, we also need to call the solver_wrap stuff to check
    // the flags
    wrap_replace_flags (p4est, which_tree,
                        1,              quadout, 
			P4EST_CHILDREN, quadin);
    
  } else {                      // we're coarsening

    solver->m_userdata_mgr->quadrant_compute_mean(quadin[0], quadout);
    
  }
  
} // quadrant_replace_fn

// =======================================================
// ==== CLASS SOLVER IMPL ================================
// =======================================================

// =======================================================
// =======================================================
void
Solver::read_config()
{

  m_tmax        = m_cfg->config_read_double ("settings.tmax", 1.0);
  m_cfl         = m_cfg->config_read_double ("settings.cfl", 1.0);
  m_solver_name = m_cfg->config_read_string ("settings.name", "solver");
  m_max_iterations = m_cfg->config_read_int ("settings.max_iterations", 0);

  /* restart run : default is no */
  m_restart_run_enabled = m_cfg->config_read_int ("settings.restart_run_enabled", 0);
  m_restart_run_filename = m_cfg->config_read_string ("settings.restart_run_filename", "data.p4est");

  /* Dimensional splitting (should be moved into advection_upwind scheme) */
  //m_order = config_read_int ("settings.order", 1);
  
  /* Strang splitting */
  m_dim_splitting = m_cfg->config_read_int ("settings.dim_splitting", 1);
  m_space_order   = m_cfg->config_read_int ("settings.space_order", 1);
  
  /* Hancock or not */
  m_time_order    = m_cfg->config_read_int ("settings.time_order", 1);

  m_min_quadrants = m_cfg->config_read_int ("settings.min_quadrants", 16);
  m_uniform_fill = m_cfg->config_read_int ("settings.uniform_fill", 1);

  m_max_save_count = m_cfg->config_read_int ("settings.save_count", 100);
  m_max_restart_count = m_cfg->config_read_int ("settings.restart_count", 1);
  m_single_precision =
    m_cfg->config_read_int ("settings.single_precision", 1);
  m_mesh_info = m_cfg->config_read_int ("settings.mesh_info", 0);
  m_gather_statistics = m_cfg->config_read_int ("settings.statistics", 0);
  m_level_stats = m_cfg->config_read_int ("settings.level_statistics", 0);
  m_min_refine = m_cfg->config_read_int ("settings.min_refine", 5);
  m_max_refine = m_cfg->config_read_int ("settings.max_refine", 7);
  m_epsilon_refine =
    m_cfg->config_read_double ("settings.epsilon_refine", 0.1);
  m_epsilon_coarsen =
    m_cfg->config_read_double ("settings.epsilon_coarsen", 0.1);
  m_geometric_refine = m_cfg->config_read_int ("settings.geometric_refine", 0);
  m_physical_refine = m_cfg->config_read_int ("settings.physical_refine", 1);
  m_weight_exponent = m_cfg->config_read_int ("settings.weight_exponent", 0);
  m_connectivity_name =
    m_cfg->config_read_string ("settings.connectivity", "unit");
  m_geometry_name_compute =
    m_cfg->config_read_string ("settings.geometry_compute", "cartesian");
  m_geometry_name_io =
    m_cfg->config_read_string ("settings.geometry_io", "cartesian");
  m_solver_core->
    set_init_name(m_cfg->config_read_std_string ("settings.initial_condition", "cvv_gaussian"));

  m_limiter_name =  m_cfg->config_read_string ("settings.limiter", "minmod");
  m_limiter_fn   = ScalarLimiterFactory().limiter_byname (m_limiter_name);

  tokenize_variables ();

} // Solver::read_config

// =======================================================
// =======================================================
void
Solver::print_config()
{

  CANOP_GLOBAL_PRODUCTION ("SOLVER INFORMATION: \n");
  CANOP_GLOBAL_PRODUCTIONF ("%-24s %s\n", "SOLVER NAME", m_solver_name.c_str());
  CANOP_GLOBAL_PRODUCTIONF ("%-24s %g\n", "TMAX", m_tmax);
  CANOP_GLOBAL_PRODUCTIONF ("%-24s %g\n", "CFL", m_cfl);
  CANOP_GLOBAL_PRODUCTIONF ("%-24s %s\n", "IC", m_solver_core->get_init_name().c_str());
  CANOP_GLOBAL_PRODUCTIONF ("%-24s %s\n", "BC", m_solver_core->get_boundary_name().c_str());

  CANOP_GLOBAL_PRODUCTIONF ("%-24s %s\n", "GEOM_REFINE",
                            (m_geometric_refine ?
                             m_solver_core->get_geometric_indicator_name().c_str() :
                             "OFF"));
  CANOP_GLOBAL_PRODUCTIONF ("%-24s %s\n", "PHYS_REFINE",
                            (m_physical_refine ?
                             m_solver_core->get_indicator_name().c_str() :
                             "OFF"));
  CANOP_GLOBAL_PRODUCTIONF ("%-24s %d\n", "MIN_REFINE", m_min_refine);
  CANOP_GLOBAL_PRODUCTIONF ("%-24s %d\n", "MAX_REFINE", m_max_refine);
  CANOP_GLOBAL_PRODUCTIONF ("%-24s %g\n", "EPS_REFINE",
                            m_epsilon_refine);
  CANOP_GLOBAL_PRODUCTIONF ("%-24s %g\n", "EPS_COARSEN",
                            m_epsilon_coarsen);
  CANOP_GLOBAL_PRODUCTIONF ("%-24s %s\n", "CONNECTIVITY",
                            m_connectivity_name.c_str());
  CANOP_GLOBAL_PRODUCTIONF ("%-24s %s\n", "GEOMETRY_COMPUTE",
                            m_geometry_name_compute.c_str());
  CANOP_GLOBAL_PRODUCTIONF ("%-24s %s\n", "GEOMETRY_IO",
                            m_geometry_name_io.c_str());
  CANOP_GLOBAL_PRODUCTIONF ("%-24s %s\n", "STATISTICS",
                            (m_gather_statistics ? "ON" : "OFF"));

} // Solver::print_config

// =======================================================
// =======================================================
void
Solver::ghost_data_new (int nbGhosts)
{

  // ask UserDataManager to realloc its ghost_data array member
  m_userdata_mgr->ghost_data_allocate(nbGhosts);
  
  // ?? put that in SolverCore ??
  
  // if (m_ghost_data) {
  //   CANOP_FREE (m_ghost_data);
  // }

  // p4est_ghost_t      *ghost = get_ghost ();
  // m_ghost_data = CANOP_ALLOC (qdata_t, ghost->ghosts.elem_count);
  
} // Solver::ghost_data_new

// =======================================================
// =======================================================
void
Solver::initial_refinement ()
{
  CANOP_GLOBAL_INFO ("initial refinement..\n");

  if (m_geometric_refine) {
    p4est_init_t        init_fn = m_solver_core->get_init_fn();
    p4est_refine_t      geom_indicator = m_solver_core->get_geom_indicator_fn();

    int                 changed;

    changed = m_wrap->adapt_geometric (init_fn, geom_indicator);

    if (changed)
      changed = m_wrap->partition (m_weight_exponent);

    if (changed)
      m_wrap->complete ();
  }

  if (m_physical_refine) {
    int                 n = m_max_refine - m_min_refine;

    for (int i = 0; i < n; ++i) {
      printf(" solver initial refine - call %d\n",i);
      adapt (1);
    }
  }

  save_solution ();

} // Solver::initial_refinement

// =======================================================
// =======================================================
Solver::Solver (ConfigReader * cfg, MPI_Comm mpicomm, int qdata_t_sizeof, bool useP4estMemory) :
  m_cfg(cfg),
  m_mpicomm(mpicomm),
  m_qdata_t_sizeof(qdata_t_sizeof),
  m_use_p4est_memory(useP4estMemory)
{

  // max number of variables that can be set is 32 (TODO: make that more robust)
  m_write_variables.resize(32);
  
} // Solver::Solver

// =======================================================
// =======================================================
Solver::~Solver()
{

} // Solver::~Solver

// =======================================================
// =======================================================
void Solver::init()
{
  p4est_t              *p4est = NULL;
  p4est_connectivity_t *connectivity = NULL;
  p4est_geometry_t     *geom_compute = NULL;
  p4est_geometry_t     *geom_io = NULL;

  std::time_t t_start = std::time(nullptr);

  // actually parse the configuration parameter file (lua), and fill m_cfg
  read_config ();

  // allocate a SolverWrap (interface to p4est internal implementation)
  // here default constructor, the actual setup is done later
  // instruct the solver wrap about the configuration (might be needed in init conditions)
  m_wrap = new SolverWrap(m_cfg);

  // setup wrap user pointer
  void * user_pointer_for_SolverWrap = (void *) this;
  m_wrap->setup_user_pointer(user_pointer_for_SolverWrap);
  
  ////////////////////////////////////////////////////
  // Some member are initialized in concrete class: //
  // - m_solver_core                                //
  // - m_userdata_mgr                               //
  // - m_pIter                                      //
  ////////////////////////////////////////////////////
  
  if (m_restart_run_enabled) {

    // this is a restart run, p4est and connectivity are created from the
    // restart file data
    p4est = p4est_load_ext(m_restart_run_filename.c_str(),
			   m_mpicomm,
			   m_qdata_t_sizeof, /* size of data per quadrant */
			   1, /* enable data loading */
			   1, /* enable autopartion */
			   0, /* enable header broadcasting is not implemented */
			   (void *)m_wrap,  /* the user pointer */
			   &connectivity    /* connectivity is initialized from 
					       the restart information */
			   );
    
    // we need to assign geom here because it may be used
    // by init_fn in p4est_new_ext

    // which geometry for computation ? if cartesian, don't do anything
    geom_compute = geometry_new_byname(m_geometry_name_compute.c_str(),
				       connectivity, m_cfg);
    m_wrap->set_geometry_compute (geom_compute);

    // which geometry for io ?
    geom_io = geometry_new_byname(m_geometry_name_io.c_str(),
				  connectivity, m_cfg);
    m_wrap->set_geometry_io (geom_io);
    
  } else {

    // build connectivity needed to build geometry structures
    connectivity = connectivity_new_byname (m_connectivity_name.c_str(), m_cfg);
    SC_CHECK_ABORTF (connectivity != NULL,
		     P4EST_STRING " does not have connectivity \"%s\".",
		     m_connectivity_name.c_str());
    
    // we need to assign geometries here because they may be used
    // by init_fn in p4est_new_ext

    // which geometry for computation ? if cartesian, don't do anything
    geom_compute = geometry_new_byname(m_geometry_name_compute.c_str(),
				       connectivity, m_cfg);
    m_wrap->set_geometry_compute (geom_compute);

    // which geometry for io ?
    geom_io = geometry_new_byname(m_geometry_name_io.c_str(),
				  connectivity, m_cfg);
    m_wrap->set_geometry_io (geom_io);
    
    // user_pointer is solver_wrap for which
    // m_wrap->m_cfg
    // m_wrap->m_geom_compute
    // m_wrap->m_geom_io
    // must be initialized, since they may be used
    // in init conditions
    // this one major difference with master branch
    p4est = p4est_new_ext (m_mpicomm,
			   connectivity,
			   m_min_quadrants,
			   m_min_refine,
			   m_uniform_fill,
			   m_qdata_t_sizeof, //sizeof (qdata_t), 
			   m_solver_core->get_init_fn(), 
			   (void *)m_wrap /* the user pointer, m_wrap need to be allocated */
			   );

  } /// end if(m_restart_run_enabled)
  
  SC_CHECK_ABORT (p4est != NULL, "p4est is NULL");

  // need to finish m_wrap to be initialized
  m_wrap->setup (connectivity,
		 geom_compute,
		 geom_io,
		 p4est);
  
  // open needed file
  m_times_saved = 0;
  m_times_saved_restart = 0;

  m_writer = new IO_Writer (p4est, geom_io, m_solver_name, m_mesh_info);
  m_writer->m_scale = 1;

  // initialize all the data
  // m_tini may be modified by settings upon restart run
  m_tini = m_cfg->config_read_double ("settings.t_current", 0.0);
  m_t = m_tini;
  m_dt = m_tmax;
  m_iteration = 0;
  m_maxlevel = 0;
  m_minlevel = P4EST_QMAXLEVEL;
  m_total_num_cell_update = 0;
  //m_nOutput = 0;

  // initialize statistics struct
  if (m_gather_statistics) {
    m_stats = new Statistics (m_mpicomm,
			      p4est->mpisize, p4est->mpirank,
			      m_gather_statistics);
  }
  else {
    m_stats = nullptr;
  }

  // if this is not a restart run, perform the initial refinement
  if (!m_restart_run_enabled) {
    initial_refinement ();
  }

  if (m_stats != nullptr) {
    m_stats->m_start = t_start;
    m_stats->m_timers["t_total"] = (double) clock ();
  }

} // Solver::init

// =======================================================
// =======================================================
void Solver::finalize()
{

  if (m_stats) {
    delete m_stats;
  }

  delete m_writer;

  delete m_wrap;

} // Solver::finalize

// =======================================================
// =======================================================
p4est_t *
Solver::get_p4est ()
{

  return m_wrap->m_p4est;

} // Solver::get_p4est

// =======================================================
// =======================================================
p4est_geometry_t   *
Solver::get_geom_compute ()
{

  return m_wrap->get_geometry_compute();

} // Solver::get_geom_compute

// =======================================================
// =======================================================
p4est_geometry_t   *
Solver::get_geom_io ()
{
  
  return m_wrap->get_geometry_io();
  
} // Solver::get_geom_io

// =======================================================
// =======================================================
p4est_ghost_t *
Solver::get_ghost ()
{
  
  return m_wrap->get_ghost();
  
} // Solver::get_ghost

// =======================================================
// =======================================================
void *
Solver::get_ghost_data_ptr()
{

  return m_userdata_mgr->get_ghost_data_ptr();
  
} // Solver::get_ghost_data_ptr

// =======================================================
// =======================================================
p4est_mesh_t *
Solver::get_mesh ()
{
  
  return m_wrap->get_mesh();
  
} // Solver::get_mesh

// =======================================================
// =======================================================
void
Solver::compute_time_step ()
{
  p4est_t            *p4est = get_p4est();
  p4est_geometry_t   *geom  = get_geom_compute ();
  p4est_ghost_t      *ghost = get_ghost ();
  MPI_Comm            mpicomm = p4est->mpicomm;
  double              max_v_over_dx = -1;

  UNUSED (geom);

  // initialize the min / max current levels
  m_minlevel = P4EST_QMAXLEVEL;
  m_maxlevel = 0;

  // compute the max of the velocities
  // update the min / max current levels
  p4est_iterate (p4est, 
		 ghost, 
		 /* user data */ &max_v_over_dx, 
		 m_pIter->iteratorsList[IT_ID::CFL_TIME_STEP],  /* cell_fn */
		 NULL,                        /* face_fn */
#ifdef P4_TO_P8
                 NULL,                        /* edge_fn */
#endif
                 NULL                         /* corner_fn */
		 ); 

  // get the global maximum and minimum level
  MPI_Allreduce (MPI_IN_PLACE, &(m_minlevel), 1,
                 MPI_INT, MPI_MIN, mpicomm);
  MPI_Allreduce (MPI_IN_PLACE, &(m_maxlevel), 1,
                 MPI_INT, MPI_MAX, mpicomm);

  // update the min dt using the given courant number
  if (max_v_over_dx < 0) {
    m_dt = m_tmax;
  }
  else if (!ISFUZZYNULL (max_v_over_dx)) {
    m_dt = 1.0 / max_v_over_dx * m_cfl;
    m_dt = fmin (m_dt, fabs (m_tmax - m_t));
  }
  else {
    SC_ABORT ("max_v_over_dx is 0\n");
  }

  // take the min over all the processes
  MPI_Allreduce (MPI_IN_PLACE, &(m_dt), 1, MPI_DOUBLE, MPI_MIN,
                 mpicomm);
  
} // Solver::compute_time_step

// =======================================================
// =======================================================
void
Solver::next_iteration_impl()
{

  // This is application dependent
  
} // Solver::next_iteration_impl

// =======================================================
// =======================================================
// NOTE: an iteration over _only_ the quadrants (cells) is equivalent to a
// for loop over all local trees and all quads in those trees. the only
// overhead comes from the extra 3-4 function calls.
void
Solver::next_iteration ()
{
  p4est_t            *p4est = get_p4est ();
  p4est_ghost_t      *ghost = get_ghost ();
  clock_t             t_start = 0;
  double              time = 0;

  // allocate ghost data memory and fill
  ghost_data_new (ghost->ghosts.elem_count);
  p4est_ghost_exchange_data ( p4est, ghost, m_userdata_mgr->get_ghost_data_ptr() );

  // update the total number of cell visited/update during the run
  m_total_num_cell_update += p4est->global_num_quadrants;

  t_start = (double) clock ();

  // genuine implementation called here
  next_iteration_impl();

  // update statistics
  time = (double) (clock () - t_start) / CLOCKS_PER_SEC;
  if (m_stats != nullptr) {
    m_stats->statistics_timing_scheme (time);
    m_stats->statistics_dt (m_dt);
    m_stats->statistics_level (m_minlevel, m_maxlevel);
  }

  // print out the quadrant distribution
  if (m_level_stats)
    compute_level_statistics();

  // incremenent
  ++m_iteration;
  m_t += m_dt;

  CANOP_GLOBAL_ESSENTIAL  ("#######################################################\n");
  CANOP_GLOBAL_ESSENTIALF ("[%d] t = %-10g\t dt = %.8g\t %s\n",
                           m_iteration, m_t, m_dt,
                           (this->should_write () ? "***" : ""));
  print_current_date();
  CANOP_GLOBAL_ESSENTIAL  ("#######################################################\n");

//   solver_compute_mean_values (solver);
  
} // Solver::next_iteration

// =======================================================
// =======================================================
int
Solver::finished ()
{
  
  if (m_max_iterations > 0 && m_iteration >= m_max_iterations)
    return 1;

  return m_t >= (m_tmax - 1e-14);
  
} // Solver::finished

// =======================================================
// =======================================================
// TODO: better strategy to decide when to adapt ?
int
Solver::should_adapt ()
{

  return m_physical_refine && (m_min_refine != m_max_refine);

} // Solver::should_adapt

// =======================================================
// =======================================================
// TODO: let the user choose how many times to write?
int
Solver::should_write ()
{
  double              interval = (m_tmax - m_tini) / m_max_save_count;

  if (m_max_save_count < 0) {
    return 1;
  }

  if ( ((m_t - m_tini) - (m_times_saved - 1) * interval) > interval ) {
    return 1;
  }

  /* always write the last time step */
  if (ISFUZZYNULL (m_t - m_tmax)) {
    return 1;
  }

  return 0;
} // Solver::should_write

// =======================================================
// =======================================================
int
Solver::should_write_restart ()
{

  if (m_max_restart_count > 0) {

    double              interval = m_tmax / m_max_restart_count;

    // never write restart file at t=t_ini
    if (((m_t-m_tini) - m_times_saved_restart * interval) > interval) {
      return 1;
    }
    
    /* always write the restart at the last time step */
    if (ISFUZZYNULL (m_t - m_tmax)) {
      return 1;
    }
    
  }

  return 0;
  
} // Solver::should_write_restart

// =======================================================
// =======================================================
p4est_locidx_t
Solver::adapt_cycle (int do_initial_refine)
{
  p4est_t            *p4est = get_p4est();
  p4est_ghost_t      *ghost = get_ghost ();
  int                 changed = 0;
  clock_t             t_start = 0;
  double              time = 0;
  double	      stat_time[5] = {0, 0, 0, 0, 0};

  p4est_init_t        init_fn = do_initial_refine ? m_solver_core->get_init_fn() : nullptr;
  p4est_replace_t     replace_on_refine =
    do_initial_refine ? wrap_replace_flags : quadrant_replace_fn;
  p4est_replace_t     replace_on_coarsen =
    do_initial_refine ? nullptr : quadrant_replace_fn;

  ghost_data_new (ghost->ghosts.elem_count);
  p4est_ghost_exchange_data ( p4est, ghost, m_userdata_mgr->get_ghost_data_ptr() );

  // iterate over cells and mark them for refine or coarsen
  t_start = (double) clock ();
  p4est_iterate (p4est,
		 ghost,
		 NULL,
		 m_pIter->iteratorsList[IT_ID::MARK_ADAPT],
		 NULL,
#ifdef P4_TO_P8
                 NULL,
#endif
                 NULL);

  time = (double) (clock () - t_start) / CLOCKS_PER_SEC;
  if (m_stats != nullptr)
    m_stats->statistics_timing_adapt_mark (time);

  // adapt the forest
  t_start = (double) clock ();
  changed = m_wrap->adapt (init_fn,
			   replace_on_refine, replace_on_coarsen,
			   stat_time);

  time = (double) (clock () - t_start) / CLOCKS_PER_SEC;
  if (m_stats != nullptr)
    m_stats->statistics_timing_adapt_balance (time, stat_time);

  if (changed) {
    t_start = (double) clock ();

    // partition the forest
    changed = m_wrap->partition (m_weight_exponent);

    time = (double) (clock () - t_start) / CLOCKS_PER_SEC;
    if (m_stats != nullptr)
      m_stats->statistics_timing_adapt_partition (time);
  }

  // complete the cycle
  if (changed) {
    m_wrap->complete ();
  }

  return p4est->local_num_quadrants;

} // Solver::adapt_cycle

// =======================================================
// =======================================================
void
Solver::adapt (int do_initial_refine)
{
  clock_t             t_start = clock ();
  double              time = 0.0;
  p4est_locidx_t      lnq = 0;

  lnq = adapt_cycle (do_initial_refine);

  // update statistics
  time = (double) (clock () - t_start) / CLOCKS_PER_SEC;
  if (m_stats != nullptr) {
    m_stats->statistics_timing_adapt (time);
    m_stats->statistics_num_quadrants (lnq);
  }
  
} // Solver::adapt

// =======================================================
// =======================================================
void
Solver::compute_level_statistics ()
{
  p4est_t            *p4est = get_p4est ();
  p4est_ghost_t      *ghost = get_ghost ();

  const int nlevels = m_max_refine - m_min_refine + 1;
  int nprocs, rank;

  unsigned long *nquads_loc, *nquads_min, *nquads_max, *nquads_tot;

  nquads_loc = new unsigned long[nlevels];
  std::fill_n(nquads_loc, nlevels, 0);

  p4est_iterate (p4est,
                 ghost,
                 nquads_loc,
                 m_pIter->iteratorsList[IT_ID::LEVEL_STATISTICS],
                 NULL,
#ifdef P4_TO_P8
                 NULL,
#endif
                 NULL);

  MPI_Comm_size(m_mpicomm, &nprocs);
  MPI_Comm_rank(m_mpicomm, &rank);

  if (rank == 0) {
    nquads_min = new unsigned long[nlevels];
    nquads_max = new unsigned long[nlevels];
    nquads_tot = new unsigned long[nlevels];
  }

  MPI_Reduce(nquads_loc, nquads_min, nlevels, MPI_UNSIGNED_LONG, MPI_MIN, 0, m_mpicomm);
  MPI_Reduce(nquads_loc, nquads_max, nlevels, MPI_UNSIGNED_LONG, MPI_MAX, 0, m_mpicomm);
  MPI_Reduce(nquads_loc, nquads_tot, nlevels, MPI_UNSIGNED_LONG, MPI_SUM, 0, m_mpicomm);

  if (rank == 0) {
    CANOP_GLOBAL_ESSENTIAL ("Quadrant distribution summary:\n");
    CANOP_GLOBAL_ESSENTIAL ("Level  Total quads  Min/proc  Max/proc  Avg/proc\n");
    CANOP_GLOBAL_ESSENTIAL ("=====  ===========  ========  ========  ========\n");
    for (int i = 0; i < nlevels; ++i) {
      CANOP_GLOBAL_ESSENTIALF ("%5d  %11lu  %8lu  %8lu  %8lu\n",
          m_min_refine + i, nquads_tot[i], nquads_min[i], nquads_max[i], nquads_tot[i] / nprocs);
    }

    delete[] nquads_min;
    delete[] nquads_max;
    delete[] nquads_tot;
  }
} // Solver::compute_level_statistics

// =======================================================
// =======================================================
void
Solver::write_statistics ()
{

  p4est_t            *p4est = get_p4est ();
  p4est_ghost_t      *ghost = get_ghost ();
  std::string         filename;

  // don't do anything if statistics collecting is disabled
  if (m_gather_statistics == 0 or m_stats == nullptr) {
    return;
  }

  // initialize some variables before the loop
  m_minlevel = P4EST_QMAXLEVEL;
  m_maxlevel = 0;

  if (m_stats != nullptr) {
    m_stats->m_norml1 = 0;
    m_stats->m_norml2 = 0;
  }

  /*
   * This will:
   *  * compute an L^1 norm
   *  * compute an L^2 norm
   *  * update level min / max
   */
  p4est_iterate (p4est,
		 ghost,
		 NULL,
		 m_pIter->iteratorsList[IT_ID::GATHER_STATISTICS],
		 NULL,
#ifdef P4_TO_P8
                 NULL,
#endif
                 NULL);

  if (p4est->mpirank == 0) {
    std::ostringstream ss;
    ss << m_solver_name << "_";
#ifdef P4_TO_P8
    ss << "stats3d.txt";
#else
    ss << "stats2d.txt";
#endif
    filename = ss.str();
  }

  // update statistics
  if (m_stats != nullptr) {
    m_stats->m_end = std::time(nullptr);
    m_stats->m_t = m_t;
    m_stats->m_iterations = m_iteration;
    m_stats->m_timers["t_total"] =
      ((double) clock () - m_stats->m_timers["t_total"]) / CLOCKS_PER_SEC;
    m_stats->statistics_level (m_minlevel, m_maxlevel);
    
    // write statistics
    m_stats->statistics_write (filename);
  }
  
} // Solver::write_statistics

// =======================================================
// =======================================================
void
Solver::save_solution ()
{
  clock_t             t_start = 0;
  double              time = 0;

  std::ostringstream   ss;
  ss << m_solver_name << "_";
  ss << std::setw(5) << std::setfill('0') << m_times_saved;
  std::string          filename = ss.str();
  
  // actually write HDF5 file
  t_start = clock ();
  write_hdf5 (filename);

  // update the statistics
  time = (double) (clock () - t_start) / CLOCKS_PER_SEC;
  if (m_stats != nullptr)
    m_stats->statistics_timing_io (time);

  // increment output file number
  ++m_times_saved;
  
} // Solver::save_solution

// =======================================================
// =======================================================
void
Solver::write_hdf5 (const std::string &filename)
{

  qdata_getter_t      getter = nullptr; // callback to get scalar data
  qdata_vector_getter_t vgetter = nullptr; // callback to get vector data
  qdata_field_type    ftype;
  hid_t               output_type =
    (m_single_precision ? H5T_NATIVE_FLOAT : H5T_NATIVE_DOUBLE);

  // open the new file and write our stuff
  m_writer->open (filename);
  m_writer->write_header (m_t);

  for (int i = 0; i < m_num_write_variables; ++i) {
    ftype = m_userdata_mgr->qdata_type_byname (m_write_variables[i]);

    if (ftype == QDATA_FIELD_SCALAR) {
      getter = m_userdata_mgr->qdata_getter_byname (m_write_variables[i]);
      if (getter != nullptr) {
        m_writer->write_quadrant_attribute (m_write_variables[i],
                        m_userdata_mgr,
                        getter,
                        output_type);
      }
    }

    else if (ftype == QDATA_FIELD_VECTOR) {
      vgetter = m_userdata_mgr->qdata_vgetter_byname (m_write_variables[i]);
      if (vgetter != nullptr) {
        m_writer->write_quadrant_attribute (m_write_variables[i],
                        m_userdata_mgr,
                        3,
                        vgetter,
                        output_type);
      }
    }
  }

  // close the file
  m_writer->write_footer ();
  m_writer->close ();

} // Solver::write_hdf5

// =======================================================
// =======================================================
void
Solver::write_restart_file ()
{
  // create filename
  std::ostringstream ss;
  ss << m_solver_name << "_";

  // write_restart_file should be called after save_solution where
  // m_times_saved is incremented
  ss << std::setfill('0') << std::setw(5) << m_times_saved-1;
  ss << "_" << std::setfill('0') << std::setw(5) << m_times_saved_restart;
  ss << "_restart." << P4EST_FOREST_SUFFIX;
  std::string filename = ss.str();
  
  // save p4est connectivity / data to disk
  int save_data_true = 1;
  int save_partition_false = 0;
  p4est_save_ext(filename.c_str(), 
		 get_p4est(), 
		 save_data_true,
		 save_partition_false);

  // increment output file number
  ++m_times_saved_restart;
  
} // Solver::write_restart_file

// =======================================================
// =======================================================
void
Solver::tokenize_variables ()
{

  const char         *variables =
    this->m_cfg->config_read_string ("settings.write_variables", NULL);
  const char          sep[] = " ,";
  char               *p = NULL;

  this->m_num_write_variables = 0;
  p = strtok ((char *) variables, sep);
  while (p != NULL) {
    this->m_write_variables[this->m_num_write_variables] = p;
    this->m_num_write_variables += 1;
    p = strtok (NULL, sep);
  }

} // Solver::tokenize_variables

// =======================================================
// ==== OTHER UTILS ======================================
// =======================================================
Solver *
solver_from_p4est (p4est_t *p4est)
{
  
  SolverWrap      *wrap = static_cast<SolverWrap *> (p4est->user_pointer);

  // should probably be a dynamics cast here
  return static_cast<Solver *> (wrap->m_user_pointer);
  
} // solver_from_p4est


// kate: indent-width 2; tab-width 2;
// vim: set ts=2 sw=2 sts=2:
