#ifndef CONFIG_READER_H_
#define CONFIG_READER_H_

extern "C" {
#include <lua.h>
#include <lualib.h>
#include <lauxlib.h>
}

#include <sc_containers.h>

#include <string>

/**
 * \brief Get the log verbosity by name.
 *
 * The log verbosity is defined in \a sc.h and are of the form SC_LP_NAME.
 * This function only requires the NAME part.
 *
 * \return The ID of the requested log level.
 */
int
config_log_verbosity (const char *name);

/**
 * \brief A configuration reader that uses Lua files.
 *
 * The configuration file can contain variables that define a configuration
 * option. For example, if we set up the Lua file to contain:
 *
 *  settings = {
 *       name = "Leroy Jenkins",
 *       age = 66
 *  }
 *
 * we can then read those values by calling:
 *  name = cfg->config_read_string("settings.name")
 *  cfg->config_read_int("settings.age", &age)
 */
class ConfigReader
{

public:

  /**
   * \brief Create a new configuration reader.
   *
   * \param [in] filename     The configuration file.
   *
   */
  ConfigReader(const char *filename);

  /**
   * \brief Free the configuration reader.
   *
   * \param [out] cfg     The configuration reader to be freed.
   */
  ~ConfigReader();

  /** The Lua state */
  lua_State          *L;

  /** store all the strings we have allocated so that we can free them later */
  sc_list_t          *strings;

  /**
   * \brief Read an integer from the configuration file.
   *
   * \param [in] key      The name of the field.
   * \param [out] preset The default to use in case the key does not exist.
   *
   */
  int config_read_int (const char *key, int preset);
  
  /**
   * \brief Read a double from the configuration file.
   *
   * \param [in] key      The name of the field.
   * \param [out] preset The default to ue in case the key does not exist.
   *
   */
  double config_read_double (const char *key, double preset);
  
  /**
   * \brief Read a string from the configuration file.
   *
   * \param [in] key      The name of the field.
   *
   * \return The string value of the field, NULL if it was not read.
   */
  const char *config_read_string (const char *key, const char *preset);

  /**
   * Just return a std::string instead of character string.
   */
  std::string config_read_std_string (const char *key, const char *preset);
  
}; // class ConfigReader

#endif // CONFIG_READER_H_
