#include "GeometricIndicatorFactory.h"

#include "Solver.h"
#include "quadrant_utils.h"

#include <cmath>

int
geom_indicator_radial (p4est_t *p4est, p4est_topidx_t which_tree,
                       p4est_quadrant_t *quadrant) {

  Solver             *solver = solver_from_p4est (p4est);

  /* geometry (if NULL, it means cartesian by default) */
  p4est_geometry_t   *geom   = solver->get_geom_compute();

  /* get config reader */
  ConfigReader       *cfg    = solver->m_cfg;

  double  XYZ[3] = { 0, 0, 0 };
  double  dx, dy, dz;

  // prevent from refining too much
  if (quadrant->level >= solver->m_max_refine)
    return 0;

  // get the physical coordinates of the center of the quad
  quadrant_center_vertex (p4est->connectivity, geom, which_tree, quadrant, XYZ);

  // get the size of the quad
  quadrant_get_dx_dy_dz (p4est->connectivity, geom, quadrant, which_tree, &dx, &dy, &dz);

  double x_c = cfg->config_read_double ("settings.radial_refine_center_x", 0.5);
  double y_c = cfg->config_read_double ("settings.radial_refine_center_y", 0.5);
#ifdef USE_3D
  double z_c = cfg->config_read_double ("settings.radial_refine_center_z", 0.5);
#endif

  double eps_refine = cfg->config_read_double("settings.radial_refine_epsilon", 0.5);

  double epsilon = SC_SQR((XYZ[0] - x_c) / dx)
                 + SC_SQR((XYZ[1] - y_c) / dy);
#ifdef USE_3D
  epsilon +=       SC_SQR((XYZ[2] - z_c) / dz);
#endif

  if (epsilon < P4EST_DIM / (4.0 * SC_SQR(eps_refine)))
    return 1;

  return 0;

} // geom_indicator_radial

/* The resolution is maximal in a cylinder parametrized by r_hires and z_hires 
 * (that is, all cells with  r < r_hires and z < z_hires are maximally refined), 
 * and the resolution is minimal for r > r_lowres and z > z_lowres.
 * Between these two landmarks, the level of refinement should 
 * linearly drop from level_max to level_min as r and z increase.
 * 
 * NOTE : We should have r_hires < r_lowres and z_hires < z_lowres
 */

int
geom_indicator_cylinder (p4est_t *p4est, p4est_topidx_t which_tree,
                       p4est_quadrant_t *quadrant) {

  Solver             *solver = solver_from_p4est (p4est);

  /* geometry (if NULL, it means cartesian by default) */
  p4est_geometry_t   *geom   = solver->get_geom_compute();

  /* get config reader */
  ConfigReader       *cfg    = solver->m_cfg;

  double  XYZ[3] = { 0, 0, 0 };
  double  level = quadrant->level;

  // prevent from refining too much
  if (level >= solver->m_max_refine) {
    return 0;
  }
  
  // get the physical coordinates of the center of the quad
  quadrant_center_vertex (p4est->connectivity, geom, which_tree, quadrant, XYZ);

  // read parameters
  double x_c = cfg->config_read_double ("cylinder_refine.center_x", 0.5);
  double y_c = cfg->config_read_double ("cylinder_refine.center_y", 0.5);
#ifdef USE_3D
  double z_c = cfg->config_read_double ("cylinder_refine.center_z", 0.5);
#endif

  double r_hires = cfg->config_read_double("cylinder_refine.r_hires", 0.2);
  double r_lowres = cfg->config_read_double("cylinder_refine.r_lowres", 0.47);
#ifdef USE_3D
  double z_hires = cfg->config_read_double("cylinder_refine.z_hires", 0.2);
  double z_lowres = cfg->config_read_double("cylinder_refine.z_lowres", 0.4);
#endif


  // shift coordinate system
  double x = XYZ[0] - x_c;
  double y = XYZ[1] - y_c;
#ifdef USE_3D
  double z = fabs(XYZ[2] - z_c);
#endif

  // polar coordinates 
  double r2 = SC_SQR(x) + SC_SQR(y);
  double r = sqrt(r2);


  double level_target = solver->m_min_refine;
  double delta_level = solver->m_max_refine - solver->m_min_refine;

#ifdef USE_3D
  if (r < r_hires && z < z_hires)  { 
    // We are in the inner highly resolved cylinder
    level_target = solver->m_max_refine;
  } else if (r > r_lowres || z > z_lowres) { 
    // We are in the external badly resolved part
    level_target = solver->m_min_refine;
  } else {
    // The space is split into evenly separated cylinders
    double r_ratio = (r_lowres - r) / (r_lowres - r_hires);
    int r_level =  solver->m_min_refine + floor(delta_level * r_ratio);
    double z_ratio = (z_lowres - z) / (z_lowres - z_hires);
    int z_level = solver->m_min_refine + floor(delta_level * z_ratio);
    level_target = SC_MIN(r_level, z_level);
  }
#else
  if (r < r_hires)  { 
    // We are in the inner highly resolved cylinder
    level_target = solver->m_max_refine;
  } else if (r > r_lowres) { 
    // We are in the external badly resolved part
    level_target = solver->m_min_refine;
  } else {
    // The space is split into evenly separated cylinders
    double r_ratio = (r_lowres - r) / (r_lowres - r_hires);
    level_target =  solver->m_min_refine + floor(delta_level * r_ratio);
  }
#endif

  return (level < level_target);

} // geom_indicator_radial

GeometricIndicatorFactory::GeometricIndicatorFactory() {
  // Register the indicators here

  registerIndicator("none", nullptr);
  registerIndicator("radial", geom_indicator_radial);
  registerIndicator("cylinder", geom_indicator_cylinder);

} // GeometricIndicatorFactory::GeometricIndicatorFactory
