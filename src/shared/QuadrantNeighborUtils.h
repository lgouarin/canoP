#ifndef QUADRANT_EXT_H_
#define QUADRANT_EXT_H_

#ifdef USE_3D
#include <p4est_to_p8est.h>
#endif

/*
 * FACE_NEGATIVE_OFFSET is used to decode 2:1 balance when accessing neighborhood
 * mesh information, for example in p4est_mesh_t structure, quad_to_face array.
 */
#ifndef P4_TO_P8
#include <p4est.h>
#include <p4est_connectivity.h>
#include <p4est_geometry.h>
#include <p4est_iterate.h>
#include <p4est_mesh.h>
#define FACE_NEGATIVE_OFFSET 8
#else
#include <p8est.h>
#include <p8est_connectivity.h>
#include <p8est_geometry.h>
#include <p8est_iterate.h>
#include <p8est_mesh.h>
#define FACE_NEGATIVE_OFFSET 24
#endif

#include "Solver.h"
#include "UserDataManager.h"

#include "canoP_base.h"

/**
 * \brief Structure that contains information about a face neighbor.
 *
 * Modelled after p4est_iter_face_side_t, but with some additional information.
 */
template <typename qdata_t>
struct quadrant_neighbor_t
{
  p4est_topidx_t      treeid;          /**< the treeid on this side */
  p4est_topidx_t      face;            /**< which quadrant side the face
					    touches */
  int                 is_hanging;      /**< boolean: one full quad (0) or
                                            two smaller quads (1) */
  int                 level;
  int                 tree_boundary;

  union neighbor_data
  {
    struct
    {
      int8_t              is_ghost;    /**< boolean: local (0) or ghost (1) */
      p4est_quadrant_t   *quad;        /**< the actual quadrant */
      p4est_locidx_t      quadid_cum;  /**< cumulative index in tree or ghost array */
      qdata_t            *data;
    }
    full; /**< if \a is_hanging = 0,
               use is.full to access per-quadrant data */
    struct
    {
      int8_t              is_ghost[P4EST_HALF];   /**< boolean: local (0) or ghost (1) */
      p4est_quadrant_t   *quad[P4EST_HALF];       /**< the actual quadrant */
      p4est_locidx_t      quadid_cum[P4EST_HALF]; /**< index in tree or ghost array */
      qdata_t            *data[P4EST_HALF];
    }
    hanging; /**< if \a is_hanging = 1,
                  use is.hanging to access per-quadrant data */
  }
  is;
  
}; // quadrant_neighbor_t

// ====================================================================
// ====================================================================
class QuadrantNeighborUtilsBase
{

public:
  /* List of faces in each direction (x, y, z) */
  static const int direction_faces[3][2];

};

// ====================================================================
// ====================================================================
/**
 * This class is meant to have only static member function, used to compute
 * gradients.
 *
 */
template<typename someUserDataTypes>
class QuadrantNeighborUtils: public QuadrantNeighborUtilsBase
{

  using qdata_t       = typename someUserDataTypes::qdata_t;
  using qdatavar_t    = typename someUserDataTypes::qdatavar_t;
  using qdatarecons_t = typename someUserDataTypes::qdatarecons_t;

public:
  QuadrantNeighborUtils();
  virtual ~QuadrantNeighborUtils();
  
  // ====================================================================
  // ====================================================================
  /**
   * \brief Get the data from a neighbor.
   *
   * This function works with the quadid given by p4est_mesh_t::quad_to_quad
   * and sets the corresponding fields in the quadrant_neighbor_t structure.
   * The data that is filled in by this function is:
   *    treeid
   *    level
   *    is_ghost
   *    quad
   *    quadid
   *    data
   *
   * \param [in] p4est The forest.
   * \param [in] ghost The ghost layer.
   * \param [in] ghost_data Ghost data corresponding to each ghost quadrant.
   * \param [in] quadid_cum The cumulative id of the quadrant in the local list or
   *                    the id in the ghost array.
   * \param [in,out] n The quadrant neighbor struct to be filled.
   * \param [in] i If the neighbor is hanging, this is the index of the quadrant
   *               corresponding to the quadid in the hanging array (0..P4EST_HALF).
   */
  static void
  quadrant_neighbor_data (p4est_t                      * p4est,
			  p4est_ghost_t                * ghost,
			  qdata_t                      * ghost_data,
			  p4est_locidx_t                 quadid_cum,
			  quadrant_neighbor_t<qdata_t> * n,
			  int                   i)
  {
    p4est_locidx_t      lnq = p4est->local_num_quadrants;
    p4est_quadrant_t   *quad = NULL;
    qdata_t            *data = NULL;
    p4est_locidx_t      treeid = -1;
    int                 is_ghost = 0;
    
    Solver              *solver = solver_from_p4est(p4est);
    UserDataManager<someUserDataTypes>
      *userdata_mgr = static_cast<UserDataManager<someUserDataTypes> *> (solver->get_userdata_mgr());
    
    //solver_wrap_t        *pp = (solver_wrap_t *)  p4est->user_pointer;
    //p4est_geometry_t     *geom = pp->geom_compute;
    //p4est_connectivity_t *conn = p4est->connectivity;
    
    /*
     * Retrieve quadrant attached data, as well as the tree Id.
     */
    if (quadid_cum < lnq) {        /* local quadrant */
      
      quad = p4est_mesh_quadrant_cumulative (p4est, quadid_cum, &treeid, NULL);
      data = userdata_mgr->quadrant_get_qdata (quad);
      
    } else {                       /* ghost quadrant */
      
      is_ghost = 1;
      quadid_cum = quadid_cum - lnq;
      
      quad = p4est_quadrant_array_index (&ghost->ghosts, quadid_cum);
      treeid = quad->p.piggy3.which_tree;
      data = &(ghost_data[quadid_cum]);
      
    }
    
    /*
     * We can now fill a valid quadrant_neighbor_t structure.
     */
    n->treeid = treeid;
    n->level = quad->level;
    if (n->is_hanging) {
      n->is.hanging.is_ghost[i] = is_ghost;
      n->is.hanging.quad[i] = quad;
      n->is.hanging.quadid_cum[i] = quadid_cum;
      n->is.hanging.data[i] = data;
    }
    else {
      n->is.full.is_ghost = is_ghost;
      n->is.full.quad = quad;
      n->is.full.quadid_cum = quadid_cum;
      n->is.full.data = data;
    }
    
  } // quadrant_neighbor_data
  
  // ====================================================================
  // ====================================================================
  /**
   * \brief Get the data on a face side.
   *
   * This fills in the quadrant_neighbor_t structure with the user data
   * that is there. If the neighbour is outside the domain, we call the
   * boundary_init function from Solver.
   *
   * \param [in] solver The solver.
   * \param [in] quadid The non-cumulative id of the quadrant we want to search the neighbor
   * for.
   * \param [in] treeid The treeid of the quadrant.
   * \param [in] face The face across which to get the neighbor (0..P4EST_FACES).
   * \param [out] n The neighbor.
   */
  static void
  quadrant_get_face_neighbor (Solver * solver,
			      p4est_locidx_t quadid,
			      p4est_topidx_t treeid,
			      int face,
			      quadrant_neighbor_t<qdata_t> * n)
  {
    p4est_t            *p4est = solver->get_p4est();
    p4est_mesh_t       *mesh  = solver->get_mesh();
    p4est_ghost_t      *ghost = solver->get_ghost();
    p4est_tree_t       *tree = p4est_tree_array_index (p4est->trees, treeid);
    
    p4est_locidx_t      facecode = 0;
    p4est_locidx_t      qtq = 0;
    int                 qtf = 0;
    p4est_locidx_t     *halves = NULL;
    
    p4est_quadrant_t   *quad = NULL;

    UserDataManager<someUserDataTypes>
      *userdata_mgr = static_cast<UserDataManager<someUserDataTypes> *> (solver->get_userdata_mgr());
    qdata_t            *ghost_data = &((userdata_mgr->m_ghost_data)[0]);

    SolverCore<qdata_t> *solverCore = static_cast<SolverCore<qdata_t> *>(solver->m_solver_core);
    
    /* initialize everything to nan */
    memset (n, -1, sizeof (quadrant_neighbor_t<qdata_t>));
    
    /*
     * get the neighbor information, 
     * each quadrant has P4EST_FACES possible neighbors.
     */
    facecode = P4EST_FACES * (tree->quadrants_offset + quadid) + face;
    qtq = mesh->quad_to_quad[facecode];
    qtf = mesh->quad_to_face[facecode];  /* face+orientation+2:1 encoded */
    
    /* qtf is negative when the neighbor is refined (see p4est_mesh_t doc) */
    n->is_hanging = qtf < 0;
    
    /* 
     * see p4est_mesh.h for the encoding in quad_to_face
     * this is valid whatever qtf is negative or positive.
     * Here n->face is the "true" face after decoding, i.e. in range 0..P4EST_FACES-1
     */
    n->face = (qtf + FACE_NEGATIVE_OFFSET) % P4EST_FACES;
    
    /* 
     * this is a quadrant on the domain boundary (not periodic)
     * see p4est routine mesh_iter_face in p4est_mesh.c to understand 
     * that if a face touches the outside boundary then corresponding
     * quad_to_quad entry is the inside quad, this explains why
     * here we test if qtq is equal to tree->quadrants_offset + quadid
     * (qtf >= 0) means same size or double size neighbor, i.e. not refined
     * testing qtf positivity is just a check; normally is enforced at mesh
     * creation by mesh_iter_face
     */
    if (qtq == (tree->quadrants_offset + quadid) && qtf >= 0) {
      quad = p4est_quadrant_array_index (&tree->quadrants, quadid);
      
      n->face = p4est_face_dual[face];
      n->level = quad->level;
      n->treeid = treeid;
      n->tree_boundary = 1;
      n->is.full.is_ghost = 1; 
      n->is.full.quad = quad;
      n->is.full.quadid_cum = tree->quadrants_offset + quadid;
      n->is.full.data = (qdata_t *) sc_mempool_alloc (p4est->user_data_pool);
      memset (n->is.full.data, 0, sizeof (qdata_t));
      
      /* call the user defined boundary function */
      solverCore->m_boundary_fn (p4est, treeid, quad, face, n->is.full.data);
      
    } else {
      
      /* we are not on the outside boundary */
      n->tree_boundary = 0;
      
      if (n->is_hanging) {
	
	/* 
	 * there is no unique neighbor quadrant, qtq points to an array of
	 * quadrants at half size.
	 */
	halves = (p4est_locidx_t *) sc_array_index (mesh->quad_to_half, qtq);
	
	/* loop over the half-sized neighbors */
	for (int i = 0; i < P4EST_HALF; ++i) {
	  quadrant_neighbor_data (p4est, ghost, ghost_data, halves[i], n, i);
	}
	
      } else {
	
	/*
	 * neighbor quadrant is not refined, i.e. same size or larger.
	 */
	quadrant_neighbor_data (p4est, ghost, ghost_data, qtq, n, 0);
	
      }
    }
  } // quadrant_get_face_neighbor
  
  // ====================================================================
  // ====================================================================
  /**
   * \brief Get the data from the current neighbor.
   *
   * If the neighbor is:
   * * a ghost quadrant, we return the ghost data
   * * a local quadrant, we return the user_data
   * * a boundary quadrant, we call the boundary condition in Solver.
   *
   * \param [in] mfn An initialized iterator.
   * \param [in] ghost_data The ghost data corresponding to the ghost layer.
   * \param [in,out] tree_boundary 0 if the quadrant is in the mesh, 1 if it is
   * on the boundary.
   *
   * See also routine p4est_mesh_face_neighbor_data in p4est_mesh.c
   *
   * \return The quadrant data.
   */
  static qdata_t *
  mesh_face_neighbor_data (p4est_mesh_face_neighbor_t * mfn,
			   qdata_t * ghost_data,
			   int *tree_boundary)
  {
    Solver             *solver = solver_from_p4est (mfn->p4est);
    
    p4est_locidx_t      qtq = mfn->current_qtq;
    p4est_locidx_t      lnq = mfn->mesh->local_num_quadrants;
    p4est_quadrant_t   *quad = NULL;
    qdata_t            *data = NULL;
    p4est_tree_t       *tree =
      p4est_tree_array_index (mfn->p4est->trees, mfn->which_tree);
    int                 face = 0;

    UserDataManager<someUserDataTypes>
      *userdata_mgr = static_cast<UserDataManager<someUserDataTypes> *> (solver->get_userdata_mgr());

    SolverCore<qdata_t> *solverCore = static_cast<SolverCore<qdata_t> *>(solver->m_solver_core);
    
    CANOP_ASSERT (qtq >= 0);
    
    /* this is a quadrant on the domain boundary */
    if (qtq == (tree->quadrants_offset + mfn->quadrant_id)) {
      
      if (tree_boundary != NULL) {
	*tree_boundary = 1;
      }
      
      quad = p4est_quadrant_array_index (&tree->quadrants, mfn->quadrant_id);
      data = (qdata_t *) sc_mempool_alloc (mfn->p4est->user_data_pool);
      memset (data, 0, sizeof (qdata_t));
      
      face = mfn->face + (mfn->subface > 0) - 1;
      solverCore->m_boundary_fn (mfn->p4est, mfn->which_tree, quad, face, data);
      
    } else {
      
      if (tree_boundary != NULL) {
	*tree_boundary = 0;
      }
      
      if (qtq < lnq) {            /* local quadrant */
	p4est_topidx_t      which_tree;
	which_tree = mfn->which_tree;
	quad =
	  p4est_mesh_quadrant_cumulative (mfn->p4est, qtq, &which_tree, NULL);
	data = userdata_mgr->quadrant_get_qdata (quad);
      } else {                      /* ghost quadrant */
	qtq -= lnq;
	data = &(ghost_data[qtq]);
      }
      
    }
    
    return data;
    
  } // mesh_face_neighbor_data

  // ====================================================================
  // ====================================================================
  /**
   * \brief Reconstruct limited gradients in the quadrant.
   *
   * For ADVECTION_UPWIND and BIFLUID:
   * The gradient is reconstructed for the conservative variables:
   *    alpha * rho1, (1 - alpha) * rho2, rho * u.
   *
   * For RAMSES, this is also known as slope computation using primitive (i.e.
   * reconstructed variables).
   *
   * After the reconstruction we use a limiter to make sure
   * the reconstructed value respects the maximum principle.
   */
  static void
  reconstruct_gradients (Solver * solver,
                         p4est_quadrant_t * quad,
                         p4est_locidx_t quadid,
                         p4est_topidx_t treeid,
                         int direction)
  {
    p4est_t            *p4est = solver->get_p4est();
    scalar_limiter_t    limiter = solver->m_limiter_fn;

    UserDataManager<someUserDataTypes>
    *userdata_mgr = static_cast<UserDataManager<someUserDataTypes> *> (solver->get_userdata_mgr());

    qdata_t            *data = userdata_mgr->quadrant_get_qdata (quad);
    //ConfigReader       *cfg = solver->m_cfg;

    qdatarecons_t       wn[2 * P4EST_HALF];
    qdatarecons_t       w; //, wl, wr;
    qdatarecons_t       delta[P4EST_HALF];
    quadrant_neighbor_t<qdata_t> n;

    double              dx = quadrant_length (quad);
    double              dxl = 0.0;
    double              dxr = 0.0;

    //UNUSED(limiter);

    /* initialize everything to 0 */
    memset (wn,    0, 2 * P4EST_HALF * sizeof (qdatarecons_t));
    memset (delta, 0,     P4EST_HALF * sizeof (qdatarecons_t));

    /*
     * to compute the reconstructed gradient in a cell, we look at the two
     * faces (left and right because of directional splitting). Since a face
     * can be shared with more than one neighbor, we suppose the face is
     * always refined. If it is not, we just copy the same information in the
     * 2 (or 4) virtual quadrants.
     *
     * So the basic idea is that we have 2 (or 4 neighbors):
     *
     *  o--------o------------------o--------o
     *  |        |                  |        |
     *  |   w_2  |                  |   w_3  |
     *  |        |                  |        |
     *  o--------o        w         o--------o
     *  |        |                  |        |
     *  |   w_0  |                  |   w_1  |
     *  |        |                  |        |
     *  o--------o------------------o--------o
     *
     *  and we compute a reconstructed gradient between (w_0, w, w_1) and
     *  (w_2, w, w_3) and then use minmod on those two values.
     */

    /* first we get all the w_i values from the neighbors */
    for (int face = 0; face < 2; ++face) {
      quadrant_get_face_neighbor (solver, quadid, treeid,
                                  direction_faces[direction][face], &n);

      /* given two neighbors, get the distance between their centers.
       * The three cases are:
       *      o-----o                 o-----o            o-----o-----o
       *      |     |                 |     |            |     |     |
       *      |  n  o---o             |  q  o---o        |  q  o  n  o
       *      |     | q |             |     | n |        |     |     |
       *      o-----o---o             o-----o---o        o-----o-----o
       *          1                       2                    3
       * and we have dx, the size of q. The distance is then:
       *      * in case 1:  dx / 2 + dx       = 3 / 2 * dx
       *      * in case 2:  dx / 2 + dx / 4   = 3 / 4 * dx
       *      * in case 3:  dx / 2 + dx / 2   = dx
       */
      if (face == 0) {
	dxl = 3.0 / (3.0 + n.level - quad->level) * dx;
      }
      else {
	dxr = 3.0 / (3.0 + n.level - quad->level) * dx;
      }
      
      /* get the data */
      if (n.is_hanging) {
	for (int i = 0; i < P4EST_HALF; ++i) {
	  userdata_mgr->reconstruct_variables (n.is.hanging.data[i]->w, wn[2 * i + face]);
	}
      } else {
	for (int i = 0; i < P4EST_HALF; ++i) {
	  userdata_mgr->reconstruct_variables (n.is.full.data->w,       wn[2 * i + face]);
        }
      }
      if (n.tree_boundary) {
        sc_mempool_free (p4est->user_data_pool, n.is.full.data);
      }
    }

    /* get the variables in the current quadrant */
    userdata_mgr->reconstruct_variables (data->w, w);

    userdata_mgr->compute_delta(w,wn, delta, limiter, dx, dxl, dxr);

    memcpy (&(data->delta[direction]),
            &(delta[0]), sizeof (qdatarecons_t));

  } // reconstruct_gradients

}; // class QuadrantNeighborUtils

#endif // QUADRANT_EXT_H_
