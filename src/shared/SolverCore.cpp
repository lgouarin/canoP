#include "SolverCore.h"

// =======================================================
// =======================================================
SolverCoreBase::SolverCoreBase(ConfigReader *cfg)
{
  
  /*
   * Initial condition setup.
   */
  m_init_name = cfg->config_read_std_string ("settings.initial_condition",
					     "unknown");
  
  /*
   * Boundary condition setup.
   */
  m_boundary_name = cfg->config_read_std_string ("settings.boundary",
						 "unknown");
  /*
   * Refine/Coarsen setup.
   */
  m_indicator_name = cfg->config_read_std_string ("settings.indicator",
						  "rho_gradient");  
  /*
   * Geometric refine setup.
   */
  m_geom_indicator_name = cfg->config_read_std_string ("settings.geometric_indicator",
						       "none");
  
} // SolverCoreBase::SolverCoreBase

// =======================================================
// =======================================================
SolverCoreBase::~SolverCoreBase()
{

} // SolverCoreBase::~SolverCoreBase

// =======================================================
// =======================================================
std::string& SolverCoreBase::get_init_name() {

  return m_init_name;

} // SolverCore::get_init_name

// =======================================================
// =======================================================
void SolverCoreBase::set_init_name(std::string name) {

  m_init_name = name;

} // SolverCore::get_init_name

// =======================================================
// =======================================================
p4est_init_t SolverCoreBase::get_init_fn() {

  return nullptr;  // will be setup in derived class

} // SolveCore::get_init_fn

// =======================================================
// =======================================================
p4est_refine_t SolverCoreBase::get_geom_indicator_fn() {

  return nullptr;  // will be setup in derived class

} // SolveCore::get_geom_indicator_fn

// =======================================================
// =======================================================
std::string& SolverCoreBase::get_boundary_name() {

  return m_boundary_name;

} // SolverCore::get_boundary_name

// =======================================================
// =======================================================
std::string& SolverCoreBase::get_indicator_name() {

  return m_indicator_name;

} // SolverCore::get_indicator_name

// =======================================================
// =======================================================
std::string& SolverCoreBase::get_geometric_indicator_name() {

  return m_geom_indicator_name;

} // SolverCore::get_geometric_indicator_name

