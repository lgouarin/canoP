#ifndef USERDATA_TYPES_H_
#define USERDATA_TYPES_H_

/**
 * Generic type list for typical CFD application.
 */
template<class some_qdata_t,
	 class some_qdatavar_t,
	 class some_qdatarecons_t >
struct UserDataTypes
{

  typedef some_qdata_t       qdata_t;       // Main data structure per cell
  typedef some_qdatavar_t    qdatavar_t;    // Conservative variables
  typedef some_qdatarecons_t qdatarecons_t; // Reconstructed or primitive variables
  
}; // UserDataTypes

#endif // USERDATA_TYPES_H_
