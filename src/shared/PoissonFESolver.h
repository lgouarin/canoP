
#ifndef POISSON_FE_SOLVER_H_
#define POISSON_FE_SOLVER_H_

/** Finite element based Poisson solver
 *
 * The Poisson problem is solved using Q1 finite elements, and a conjugate
 * gradient method.
 */

#ifdef USE_3D
#include <p4est_to_p8est.h>
#endif

#ifndef P4_TO_P8
#include <p4est.h>
#include <p4est_geometry.h>
#include <p4est_lnodes.h>
#else
#include <p8est.h>
#include <p8est_geometry.h>
#include <p8est_lnodes.h>
#endif

#include <functional>

/** Poisson solver feedback data. */
struct poisson_info_t {
  int iterations;   /**< Number of iterations done. */
  double residual;  /**< Final residual. */
};

/** Solve Poisson's equation using the given lnodes and data
 * \param [in]     p4est    Solve the PDE with the given mesh refinement.
 * \param [in]     lnodes   Use these Gauss-Lobatto nodes (degree 1).
 * \param [in]     bc       Boolean indicator, true for boundary nodes
 * \param [in]     rhs_in   Input RHS
 * \param [in,out] u_out    Solution (input: initial guess and boundary values)
 * \param [in]     imax     The maximum number of iterations.
 * \param [in]     tol      The convergence threshold (relative to the initial residual).
 * \param [out]    info     Feedback about number of iterations and final residual.
 *
 * \return Non-zero iff the solver converged.
 */
int
solve_poisson_lnodes (p4est_t * p4est, p4est_lnodes_t *lnodes,
        int8_t *bc, double *rhs_in, double *u_out,
        int imax, double tol, poisson_info_t *info = nullptr);

/** Execute the numerical part of the example: Solve Poisson's equation.
 * \param [in]  p4est    Solve the PDE with the given mesh refinement.
 * \param [out] info     Feedback about number of iterations and final residual.
 *
 * \return Non-zero iff the solver converged.
 */
int
solve_poisson (p4est_t * p4est, poisson_info_t *info = nullptr);

/** Compute the barycenter with respect to a given field.
 *
 * \param [in]  p4est       The forest.
 * \param [in]  geom        The geometry structure.
 * \param [in]  quad_getter The getter for the weight function.
 * \param [in]  quad_volume The getter for the quadrant volume.
 * \param [out] psum        The total weight (may be NULL).
 * \param [out] pcenter     The barycenter.
 */
void
compute_barycenter(p4est_t *p4est,
                   p4est_geometry_t *geom,
                   std::function<double(p4est_quadrant_t *)> quad_getter,
                   std::function<double(p4est_t *, p4est_topidx_t, p4est_quadrant_t *)> quad_volume,
                   double *psum,
                   double pcenter[3]);

/** Compute the average value of a given field.
 *
 * \param [in]  p4est       The forest.
 * \param [in]  quad_getter The getter for the field.
 * \param [in]  quad_volume The getter for the quadrant volume.
 *
 * \return The volume-weighted average.
 */
double
compute_average(p4est_t *p4est,
                std::function<double(p4est_quadrant_t *)> quad_getter,
                std::function<double(p4est_t *, p4est_topidx_t, p4est_quadrant_t *)> quad_volume);

/** Compute the value of a monopole expansion at a given point.
 *
 * \param [in]  sum    The total weight.
 * \param [in]  center The barycenter.
 * \param [in]  XYZ    The coordinates of the point.
 *
 * \return The value of the monopole expansion.
 *
 * \see compute_barycenter
 */
double
monopole_expansion(double sum, double center[3], double XYZ[3]);

/** Compute the L2 norm of a node vector.
 *
 * \param [in]  p4est  The forest.
 * \param [in]  lnodes The nodes structure.
 * \param [in]  u      The input vector.
 *
 * \return The L2 norm u^T M u where M is the mass matrix.
 */
double
nodes_L2_norm (p4est_t * p4est, p4est_lnodes_t * lnodes, double *u);

#endif /* POISSON_FE_SOLVER_H_ */

