#include "RiemannSolverFactoryRamses.h"

#include <cmath>  /* for fmax, fmin, sqrt */

#define UNUSED(x) ((void)(x))

namespace canop {

namespace ramses {

using QdataVar_t    = QdataVar;
using QdataRecons_t = QdataRecons;
using Params_t      = RamsesPar;


/** 
 * Riemann solver, equivalent to riemann_hll in RAMSES (see file
 * godunov_utils.f90 in RAMSES).
 * 
 * This is the HYDRO only version. The MHD version is in file riemann_mhd.h
 *
 * Reference : E.F. Toro, Riemann solvers and numerical methods for
 * fluid dynamics, Springer, chapter 10 (The HLL and HLLC Riemann solver).
 *
 * @param[in] qleft left state (reconstructed)
 * @param[in] qright right state (reconstructed)
 * @param[out] flux  flux (conservative variables)
 * @param[in] params a constant reference to a RamsesPar
 * @param[in,out] extra extra parameter (unused in hll solver)
 */
void riemann_ramses_hll(QdataRecons_t   qleft, 
			QdataRecons_t   qright, 
			QdataVar_t     *flux,
			const Params_t &params,
			double         *extra)
{
  UNUSED(extra);

  // 1D HLL Riemann solver
  
  // constants
  //const double smallp = params.smallc*params.smallc/params.gamma0;
  const double entho = 1.0 / (params.gamma0 - 1.0);

  // Maximum wave speed
  double rl = fmax(qleft.rho, params.smallr);
  double pl = fmax(qleft.P, rl*params.smallp);
  double ul =      qleft.velocity[IX];

  double rr = fmax(qright.rho, params.smallr);
  double pr = fmax(qright.P, rr*params.smallp);
  double ur =      qright.velocity[IX];

  double cl = sqrt(params.gamma0*pl/rl);
  double cr = sqrt(params.gamma0*pr/rr);

  // Compute HLL wave speed
  double SL = fmin(fmin(ul,ur) - fmax(cl,cr), 0.0);
  double SR = fmax(fmax(ul,ur) + fmax(cl,cr), 0.0);

  // Compute conservative variables
  QdataRecons_t uleft, uright;
  uleft.rho  = qleft.rho;
  uright.rho = qright.rho;
  uleft.P    = qleft.P  * entho + 0.5*qleft.rho  * qleft.velocity[IX] * qleft.velocity[IX];
  uright.P   = qright.P * entho + 0.5*qright.rho * qright.velocity[IX]*qright.velocity[IX];
  uleft.P   += 0.5*qleft.rho  * qleft.velocity[IY]  * qleft.velocity[IY];
  uright.P  += 0.5*qright.rho * qright.velocity[IY] * qright.velocity[IY];
#ifdef USE_3D
  uleft.P   += 0.5*qleft.rho  * qleft.velocity[IZ]  * qleft.velocity[IZ];
  uright.P  += 0.5*qright.rho * qright.velocity[IZ] * qright.velocity[IZ];
#endif
  uleft.velocity[IX]  = qleft.rho * qleft.velocity[IX];
  uright.velocity[IX] = qright.rho* qright.velocity[IX];

  // Other advected quantities
  uleft.velocity[IY]  = qleft.rho  * qleft.velocity[IY];
  uright.velocity[IY] = qright.rho * qright.velocity[IY];
#ifdef USE_3D
  uleft.velocity[IZ]  = qleft.rho  * qleft.velocity[IZ];
  uright.velocity[IZ] = qright.rho * qright.velocity[IZ];
#endif

  // Compute left and right fluxes
  QdataRecons_t fleft, fright;
  fleft.rho  = uleft .velocity[IX];
  fright.rho = uright.velocity[IX];
  fleft .P = qleft .velocity[IX] * ( uleft .P + qleft .P);
  fright.P = qright.velocity[IX] * ( uright.P + qright.P);
  fleft .velocity[IX] = uleft .velocity[IX] * qleft .velocity[IX] + qleft .P;
  fright.velocity[IX] = uright.velocity[IX] * qright.velocity[IX] + qright.P;

  // Other advected quantities
  fleft .velocity[IY] = fleft .rho * qleft .velocity[IY];
  fright.velocity[IY] = fright.rho * qright.velocity[IY];
#ifdef USE_3D
  fleft .velocity[IZ] = fleft .rho * qleft .velocity[IZ];
  fright.velocity[IZ] = fright.rho * qright.velocity[IZ];
#endif

  // Compute HLL fluxes  
  flux->rho = (SR * fleft.rho - 
	       SL * fright.rho + 
	       SR * SL * (uright.rho - uleft.rho) ) / (SR-SL);
  flux->E_tot   = (SR * fleft.P - 
		   SL * fright.P + 
		   SR * SL * (uright.P - uleft.P) ) / (SR-SL);
  flux->rhoV[IX] = (SR * fleft.velocity[IX] - 
		    SL * fright.velocity[IX] + 
		    SR * SL * (uright.velocity[IX] - uleft.velocity[IX]) ) / (SR-SL);
  flux->rhoV[IY] = (SR * fleft.velocity[IY] - 
		    SL * fright.velocity[IY] + 
		    SR * SL * (uright.velocity[IY] - uleft.velocity[IY]) ) / (SR-SL);
  
#ifdef USE_3D
  flux->rhoV[IZ] = (SR * fleft.velocity[IZ] - 
		    SL * fright.velocity[IZ] + 
		    SR * SL * (uright.velocity[IZ] - uleft.velocity[IZ]) ) / (SR-SL);
#endif
  
} // riemann_ramses_hll

/** 
 * Riemann solver, equivalent to riemann_hllc in RAMSES (see file
 * godunov_utils.f90 in RAMSES).
 * 
 * Hydro ONLY.
 *
 * @param[in] qleft left state (reconstructed)
 * @param[in] qright right state (reconstructed)
 * @param[out] flux  flux (conservative variables)
 * @param[in] params a constant reference to a RamsesPar
 * @param[in,out] extra extra parameter 
 *
 * extra parameter is used :
 * - in input to specify coordinate system, 0 means cartesian, in which case
 *   pressure term in normal velocity is included
 * - in output to pass pressure term in normal velocity flux when using
 *   non-cartesian coordinates).
 */
void riemann_ramses_hllc(QdataRecons_t   qleft, 
			 QdataRecons_t   qright, 
			 QdataVar_t     *flux,
			 const Params_t &params,
			 double         *extra)
{

  int using_non_cartesian_coordinates = (*extra != 0);

  const double entho = 1.0 / (params.gamma0 - 1.0);
  
  // Left variables
  double rl = fmax(qleft.rho, params.smallr);
  double pl = fmax(qleft.P, rl*params.smallp);
  double ul =      qleft.velocity[IX];
    
  double ecinl = 0.5*rl*ul*ul;
  ecinl += 0.5*rl*qleft.velocity[IY]*qleft.velocity[IY];
#ifdef USE_3D
    ecinl += 0.5*rl*qleft.velocity[IZ]*qleft.velocity[IZ];
#endif

  double  etotl = pl*entho+ecinl;
  double  ptotl = pl;

  // Right variables
  double rr = fmax(qright.rho, params.smallr);
  double pr = fmax(qright.P, rr*params.smallp);
  double ur =      qright.velocity[IX];

  double ecinr = 0.5*rr*ur*ur;
  ecinr += 0.5*rr*qright.velocity[IY]*qright.velocity[IY];
#ifdef USE_3D
  ecinr += 0.5*rr*qright.velocity[IZ]*qright.velocity[IZ];
#endif
  
  double  etotr = pr*entho+ecinr;
  double  ptotr = pr;
    
  // Find the largest eigenvalues in the normal direction to the interface
  double cfastl = sqrt(fmax(params.gamma0*pl/rl,params.smallc*params.smallc));
  double cfastr = sqrt(fmax(params.gamma0*pr/rr,params.smallc*params.smallc));

  // Compute HLL wave speed
  double SL = fmin(ul,ur) - fmax(cfastl,cfastr);
  double SR = fmax(ul,ur) + fmax(cfastl,cfastr);

  // Compute lagrangian sound speed
  double rcl = rl*(ul-SL);
  double rcr = rr*(SR-ur);
    
  // Compute acoustic star state
  double ustar    = (rcr*ur   +rcl*ul   +  (ptotl-ptotr))/(rcr+rcl);
  double ptotstar = (rcr*ptotl+rcl*ptotr+rcl*rcr*(ul-ur))/(rcr+rcl);

  // Left star region variables
  double rstarl    = rl*(SL-ul)/(SL-ustar);
  double etotstarl = ((SL-ul)*etotl-ptotl*ul+ptotstar*ustar)/(SL-ustar);
    
  // Right star region variables
  double rstarr    = rr*(SR-ur)/(SR-ustar);
  double etotstarr = ((SR-ur)*etotr-ptotr*ur+ptotstar*ustar)/(SR-ustar);
    
  // Sample the solution at x/t=0
  double ro, uo, ptoto, etoto;
  if (SL > 0.0) {
    ro=rl;
    uo=ul;
    ptoto=ptotl;
    etoto=etotl;
  } else if (ustar > 0.0) {
    ro=rstarl;
    uo=ustar;
    ptoto=ptotstar;
    etoto=etotstarl;
  } else if (SR > 0.0) {
    ro=rstarr;
    uo=ustar;
    ptoto=ptotstar;
    etoto=etotstarr;
  } else {
    ro=rr;
    uo=ur;
    ptoto=ptotr;
    etoto=etotr;
  }
      
  // Compute the Godunov flux (in conservative variables)
  flux->rho = ro*uo;

  if (using_non_cartesian_coordinates) {
    // do not include pressure
    flux->rhoV[IX] = ro*uo*uo;
    *extra = ptoto;
  } else {
    // return the total flux
    flux->rhoV[IX] = ro*uo*uo+ptoto;
  }

  flux->E_tot = (etoto+ptoto)*uo;

  if (flux->rho > 0.0) {
    flux->rhoV[IY] = flux->rho * qleft.velocity[IY];
  } else {
    flux->rhoV[IY] = flux->rho * qright.velocity[IY];
  }
  
#ifdef USE_3D
    if (flux->rho > 0.0) {
      flux->rhoV[IZ] = flux->rho * qleft.velocity[IZ];
    } else {
      flux->rhoV[IZ] = flux->rho * qright.velocity[IZ];
    }
#endif

} // riemann_ramses_hllc

#ifdef USE_RAMSES_MHD

/*****************************************************************/
/********* TO BE REFACTORED : MHD not really implemented ! *******/
/*****************************************************************/

/**
 * Compute the fast magnetosonic velocity.
 * 
 * IU is index to Vnormal
 * IA is index to Bnormal
 * 
 * IV, IW are indexes to Vtransverse1, Vtransverse2,
 * IB, IC are indexes to Btransverse1, Btransverse2
 *
 * @param[in] qvar : state vector (reconstructed)
 * @param[in] gamma0 : contextual parameter
 */
double find_speed_fast(QdataRecons_t   qvar, 
		       double          gamma0,
		       GeomDir         dir)
{
   
  double d,p,a,b,c,b2,c2,d2,cf;

  d=qvar.rho;
  p=qvar.P; 
  a=qvar.Bx;
  b=qvar.By;
  c=qvar.Bz;

  b2 = a*a + b*b + c*c;
  c2 = gamma0 * p / d;
  d2 = 0.5 * (b2/d + c2);
  if (dir==IX)
    cf = sqrt( d2 + sqrt(d2*d2 - c2*a*a/d) );

  if (dir==IY)
    cf = sqrt( d2 + sqrt(d2*d2 - c2*b*b/d) );

  if (dir==IZ)
    cf = sqrt( d2 + sqrt(d2*d2 - c2*c*c/d) );

  return cf;
  
} // find_speed_fast

/** 
 * Riemann solver, equivalent to riemann_hlld in RAMSES (see file
 * godunov_utils.f90 in RAMSES/DUMSES).
 *
 * Reference :
 * <A HREF="http://www.sciencedirect.com/science/article/B6WHY-4FY3P80-7/2/426234268c96dcca8a828d098b75fe4e">
 * Miyoshi & Kusano, 2005, JCP, 208, 315 </A>
 *
 * \warning This version of HLLD integrates the pressure term in
 * flux[IU] (as in RAMSES). This probably could be modified in the
 * future (as it is done in DUMSES) to handle cylindrical / spherical
 * coordinate systems. For example, one could add a new ouput named qStar
 * to store star state, and that could be used to compute geometrical terms
 * outside this routine.
 *
 * @param[in] qleft : input left state (face reconstructed)
 * @param[in] qright : input right state (face reconstructed)
 * @param[out] flux  : output flux (in conservative variables)
 * @param[in,out] extra extra parameter (unused in hll solver)
 */
void riemann_ramses_hlld(QdataRecons_t   qleft,
			 QdataRecons_t   qright,
			 QdataVar_t     *flux,
			 const Params_t &params,
			 double         *extra)
{

  UNUSED(extra);

  // Constants
  const double entho = 1.0 / (params.gamma0 - 1.0);

  // Enforce continuity of normal component of magnetic field
  double a    = 0.5 * ( qleft.Bx + qright.Bx );
  double sgnm = (a >= 0) ? 1.0 : -1.0;
  qleft .Bx  = a; 
  qright.Bx  = a;
  
  // ISOTHERMAL
  double cIso = params.cIso;
  if (cIso > 0) {
    // recompute pressure
    qleft .P = qleft .rho*cIso*cIso;
    qright.P = qright.rho*cIso*cIso;
  } // end ISOTHERMAL

  // left variables
  double rl, pl, ul, vl, wl, bl, cl;
  rl = qleft.rho; //rl = FMAX(qleft.rho, static_cast<double>(params.smallr)    );  
  pl = qleft.P; //pl = FMAX(qleft.P, static_cast<double>(rl*params.smallp) ); 
  ul = qleft.velocity[IX];  
  vl = qleft.velocity[IY];  
  wl = qleft.velocity[IZ]; 
  bl = qleft.By;  
  cl = qleft.Bz;
  double ecinl = 0.5 * (ul*ul + vl*vl + wl*wl) * rl;
  double emagl = 0.5 * ( a*a  + bl*bl + cl*cl);
  double etotl = pl*entho + ecinl + emagl;
  double ptotl = pl + emagl;
  double vdotbl= ul*a + vl*bl + wl*cl;

  // right variables
  double rr, pr, ur, vr, wr, br, cr;
  rr = qright.rho; //rr = FMAX(qright.rho, static_cast<double>( params.smallr) );
  pr = qright.P; //pr = FMAX(qright.P, static_cast<double>( rr*params.smallp) ); 
  ur = qright.velocity[IX];
  vr = qright.velocity[IY];
  wr = qright.velocity[IZ]; 
  br = qright.By;  
  cr = qright.Bz;
  double ecinr = 0.5 * (ur*ur + vr*vr + wr*wr) * rr;
  double emagr = 0.5 * ( a*a  + br*br + cr*cr);
  double etotr = pr*entho + ecinr + emagr;
  double ptotr = pr + emagr;
  double vdotbr= ur*a + vr*br + wr*cr;

  // find the largest eigenvalues in the normal direction to the interface
  double cfastl = find_speed_fast(qleft, params.gamma0, IX);
  double cfastr = find_speed_fast(qright,params.gamma0, IX);

  // compute hll wave speed
  double sl = fmin(ul,ur) - fmax(cfastl,cfastr);
  double sr = fmax(ul,ur) + fmax(cfastl,cfastr);
  
  // compute lagrangian sound speed
  double rcl = rl * (ul-sl);
  double rcr = rr * (sr-ur);

  // compute acoustic star state
  double ustar   = (rcr*ur   +rcl*ul   +  (ptotl-ptotr))/(rcr+rcl);
  double ptotstar= (rcr*ptotl+rcl*ptotr+rcl*rcr*(ul-ur))/(rcr+rcl);

  // left star region variables
  double estar;
  double rstarl, el;
  rstarl = rl*(sl-ul)/(sl-ustar);
  estar  = rl*(sl-ul)*(sl-ustar)-a*a;
  el     = rl*(sl-ul)*(sl-ul   )-a*a;
  double vstarl, wstarl;
  double bstarl, cstarl;
  // not very good (should use a small energy cut-off !!!)
  if(a*a>0 && fabs(estar/(a*a)-1.0)<=1e-8) {
    vstarl=vl;
    bstarl=bl;
    wstarl=wl;
    cstarl=cl;
  } else {
    vstarl=vl-a*bl*(ustar-ul)/estar;
    bstarl=bl*el/estar;
    wstarl=wl-a*cl*(ustar-ul)/estar;
    cstarl=cl*el/estar;
  }
  double vdotbstarl = ustar*a+vstarl*bstarl+wstarl*cstarl;
  double etotstarl  = ((sl-ul)*etotl-ptotl*ul+ptotstar*ustar+a*(vdotbl-vdotbstarl))/(sl-ustar);
  double sqrrstarl  = sqrt(rstarl);
  double calfvenl   = fabs(a)/sqrrstarl; /* sqrrstarl should never be zero, but it might happen if border conditions are not OK !!!!!! */
  double sal        = ustar-calfvenl;

  // right star region variables
  double rstarr, er;
  rstarr = rr*(sr-ur)/(sr-ustar);
  estar  = rr*(sr-ur)*(sr-ustar)-a*a;
  er     = rr*(sr-ur)*(sr-ur   )-a*a;
  double vstarr, wstarr;
  double bstarr, cstarr;
  // not very good (should use a small energy cut-off !!!)
  if(a*a>0 && fabs(estar/(a*a)-1.0)<=1e-8) {
    vstarr=vr;
    bstarr=br;
    wstarr=wr;
    cstarr=cr;
  } else {
    vstarr=vr-a*br*(ustar-ur)/estar;
    bstarr=br*er/estar;
    wstarr=wr-a*cr*(ustar-ur)/estar;
    cstarr=cr*er/estar;
  }
  double vdotbstarr = ustar*a+vstarr*bstarr+wstarr*cstarr;
  double etotstarr  = ((sr-ur)*etotr-ptotr*ur+ptotstar*ustar+a*(vdotbr-vdotbstarr))/(sr-ustar);
  double sqrrstarr  = sqrt(rstarr);
  double calfvenr   = fabs(a)/sqrrstarr; /* sqrrstarr should never be zero, but it might happen if border conditions are not OK !!!!!! */
  double sar        = ustar+calfvenr;

  // double star region variables
  double vstarstar     = (sqrrstarl*vstarl+sqrrstarr*vstarr+
			 sgnm*(bstarr-bstarl)) / (sqrrstarl+sqrrstarr);
  double wstarstar     = (sqrrstarl*wstarl+sqrrstarr*wstarr+
			 sgnm*(cstarr-cstarl)) / (sqrrstarl+sqrrstarr);
  double bstarstar     = (sqrrstarl*bstarr+sqrrstarr*bstarl+
			 sgnm*sqrrstarl*sqrrstarr*(vstarr-vstarl)) / 
    (sqrrstarl+sqrrstarr);
  double cstarstar     = (sqrrstarl*cstarr+sqrrstarr*cstarl+
			 sgnm*sqrrstarl*sqrrstarr*(wstarr-wstarl)) /
    (sqrrstarl+sqrrstarr);
  double vdotbstarstar = ustar*a+vstarstar*bstarstar+wstarstar*cstarstar;
  double etotstarstarl = etotstarl-sgnm*sqrrstarl*(vdotbstarl-vdotbstarstar);
  double etotstarstarr = etotstarr+sgnm*sqrrstarr*(vdotbstarr-vdotbstarstar);
  
  // sample the solution at x/t=0
  double ro, uo, vo, wo, bo, co, ptoto, etoto, vdotbo;
  if(sl>0) { // flow is supersonic, return upwind variables
    ro=rl;
    uo=ul;
    vo=vl;
    wo=wl;
    bo=bl;
    co=cl;
    ptoto=ptotl;
    etoto=etotl;
    vdotbo=vdotbl;
  } else if (sal>0) {
    ro=rstarl;
    uo=ustar;
    vo=vstarl;
    wo=wstarl;
    bo=bstarl;
    co=cstarl;
    ptoto=ptotstar;
    etoto=etotstarl;
    vdotbo=vdotbstarl;
  } else if (ustar>0) {
    ro=rstarl;
    uo=ustar;
    vo=vstarstar;
    wo=wstarstar;
    bo=bstarstar;
    co=cstarstar;
    ptoto=ptotstar;
    etoto=etotstarstarl;
    vdotbo=vdotbstarstar;
  } else if (sar>0) {
    ro=rstarr;
    uo=ustar;
    vo=vstarstar;
    wo=wstarstar;
    bo=bstarstar;
    co=cstarstar;
    ptoto=ptotstar;
    etoto=etotstarstarr;
    vdotbo=vdotbstarstar;
  } else if (sr>0) {
    ro=rstarr;
    uo=ustar;
    vo=vstarr;
    wo=wstarr;
    bo=bstarr;
    co=cstarr;
    ptoto=ptotstar;
    etoto=etotstarr;
    vdotbo=vdotbstarr;
  } else { // flow is supersonic, return upwind variables
    ro=rr;
    uo=ur;
    vo=vr;
    wo=wr;
    bo=br;
    co=cr;
    ptoto=ptotr;
    etoto=etotr;
    vdotbo=vdotbr;
  }

  // compute the godunov flux
  flux->rho      = ro*uo;
  flux->E_tot    = (etoto+ptoto)*uo-a*vdotbo;
  flux->rhoV[IX] = ro*uo*uo-a*a+ptoto; /* *** WARNING *** : ptoto used here (this is only valid for cartesian geometry) ! */
  flux->rhoV[IY] = ro*uo*vo-a*bo;
  flux->rhoV[IZ] = ro*uo*wo-a*co;
  flux->BxL = 0.0;
  flux->ByL = bo*uo-a*vo;
  flux->BzL = co*uo-a*wo;
  
} // riemann_hlld

#endif /* USE_RAMSES_MHD */

// =======================================================
// ======CLASS RiemanSolverFactoryRamses IMPL ============
// =======================================================

RiemannSolverFactoryRamses::RiemannSolverFactoryRamses() :
  RiemannSolverFactory()
{

  // register the above callback's
  registerRiemannSolver("hll" , riemann_ramses_hll);
  registerRiemannSolver("hllc", riemann_ramses_hllc);
  
} // RiemannSolverFactoryRamses::RiemannSolverFactoryRamses

} // namespace ramses

} // namespace canop
