#ifndef SOLVER_RAMSES_H_
#define SOLVER_RAMSES_H_

#include "subcycling.h"
#include "Solver.h"
#include "userdata_ramses.h"
#include "RiemannSolverFactoryRamses.h"

#include <mpi.h>

class ConfigReader;

namespace canop {

namespace ramses {

/**
 * Concrete Solver class implementation for Ramses application.
 */
class SolverRamses : public Solver
{

  using qdata_t = Qdata;

public:
  SolverRamses(ConfigReader * cfg, MPI_Comm mpicomm, bool useP4estMemory);
  virtual ~SolverRamses();

  /**
   * Here we implement unsplit numerical scheme for Ramses Hydro.
   */
  void next_iteration_impl();
  double next_iteration_recursive(double t_start, double dt_max);
  void next_iteration_prepare();
  void next_iteration_do();

  void next_iteration_hydro_cylindrical();

  void write_hdf5(const std::string &filename);

  /**
   * Parameters specific to Ramses scheme.
   */
  RamsesPar ramsesPar;

  std::string         m_riemann_solver_name;
  using riemann_solver_t = RiemannSolverFactoryRamses::RiemannSolverCallback;
  riemann_solver_t    m_riemann_solver_fn;

  subcycle_info *m_subcycle;

  /**
   * Static creation method called by the solver factory.
   */
  static Solver* create(ConfigReader * cfg, MPI_Comm mpicomm, bool useP4estMemory)
  {
    SolverRamses* solver = new SolverRamses(cfg, mpicomm, useP4estMemory);

    return solver;
  }

private:
  /**
   * Custom version of base class version.
   */
  void read_config();

}; // class SolverRamses

} // namespace ramses

} // namespace canop

#endif // SOLVER_RAMSES_H_
