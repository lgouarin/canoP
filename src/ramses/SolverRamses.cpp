#include "SolverRamses.h"
#include "SolverCoreRamses.h"
#include "Statistics.h"
#include "UserDataManagerRamses.h"
#include "IteratorsRamses.h"

#include "IO_Writer.h"

#include "canoP_base.h"
#include "gravity.h"
#include "utils.h"

#include <map>

namespace canop {

namespace ramses {

// =======================================================
// =======================================================
SolverRamses::SolverRamses(ConfigReader * cfg, MPI_Comm mpicomm, bool useP4estMemory) :
  Solver(cfg,mpicomm,sizeof(SolverRamses::qdata_t),useP4estMemory)
{

  // setup solver core
  m_solver_core = new SolverCoreRamses(cfg);

  // setup UserData Manager
  m_userdata_mgr = new UserDataManagerRamses(useP4estMemory, ramsesPar);

  // we need to setup the right iterators
  m_pIter = new IteratorsRamses();

  // will be allocated in read_config if needed
  m_subcycle = nullptr;

  // solver init (this is were p4est_new is called)
  init();

  // only MPI rank 0 prints config (thanks to CANOP_GLOBAL_PRODUCTION macro)
  print_config();

} // SolverRamses::SolverRamses

// =======================================================
// =======================================================
SolverRamses::~SolverRamses()
{

  finalize();

  delete m_solver_core;

  delete m_userdata_mgr;

  delete m_pIter;

  if (m_subcycle != nullptr)
    delete m_subcycle;
} // SolverRamses::~SolverRamses

// =======================================================
// =======================================================
void
SolverRamses::next_iteration_impl()
{

  if (m_subcycle == nullptr) {
    next_iteration_prepare ();
    next_iteration_do ();

  } else {
    m_subcycle->reset(true);
    compute_time_step (); // update m_minlevel and m_maxlevel
    m_subcycle->min_level = m_minlevel;
    m_subcycle->max_level = m_maxlevel;
    m_subcycle->level = m_minlevel;

    next_iteration_recursive (m_t, -1.0);

  }

} // SolverRamses::next_iteration_impl

// =======================================================
// =======================================================
double
SolverRamses::next_iteration_recursive(double t_start, double dt_max)
{
  m_t = t_start; // m_t is only updated by Solver::next_iteration
  next_iteration_prepare ();

  // prevent m_dt from violating the coarser-level CFL
  if (dt_max > 0)
    m_dt = SC_MIN(m_dt, dt_max);

  dt_max = m_dt;

  CANOP_GLOBAL_INFOF("Enter subcycle: level %d, substep %d, dt = %lg\n",
                     m_subcycle->level, m_subcycle->substep, m_dt);

  if (m_subcycle->level < m_subcycle->max_level) {
    double dt_acc = 0;

    m_subcycle->level += 1;

    // note: this may be replaced by a loop
    dt_acc += next_iteration_recursive (t_start, dt_max);
    dt_acc += next_iteration_recursive (t_start + dt_acc, dt_max);

    m_subcycle->level -= 1;
    // ensure all levels advance to the same time
    m_dt = dt_acc;
    // restore the time to its starting value
    m_t = t_start;
  }

  next_iteration_do ();

  CANOP_GLOBAL_INFOF("Exit  subcycle: level %d, substep %d, dt = %lg\n",
                     m_subcycle->level, m_subcycle->substep, m_dt);

  if (m_subcycle->level == m_subcycle->max_level)
    m_subcycle->substep += 1;

  return m_dt;

} // SolverRamses::next_iteration_recursive

// =======================================================
// =======================================================
void
SolverRamses::next_iteration_prepare()
{

  p4est_t            *p4est = get_p4est ();
  p4est_ghost_t      *ghost = get_ghost ();

  // remove the point source contribution to the potential before updat
  if (ramsesPar.gravity.point_enabled && (m_iteration > 0 || m_restart_run_enabled))
    set_point_gravity(p4est, true);

  // call the Poisson solver to compute the gravitational acceleration
  if (ramsesPar.gravity.poisson_enabled)
    solve_poisson_gravity(p4est, nullptr); // the ghost layer needs full connectivity, not only face

  // set the static gravitational acceleration if needed
  if (ramsesPar.gravity.point_enabled)
    set_point_gravity(p4est, false);

  // compute the cfl condition and time step at the smallest level
  compute_time_step ();

  // Update ghost
  p4est_ghost_exchange_data (p4est, ghost, get_ghost_data_ptr() );

  /*
   * reconstruct gradients
   */
  CANOP_GLOBAL_INFO ("Compute slopes (Godunov 1st stage)\n");
  p4est_iterate (p4est,
		 ghost,
		 /* user data */ NULL,
		 m_pIter->iteratorsList[IT_ID::RECONSTRUCT_GRADIENT],
		 NULL,        // face_fn
#ifdef P4_TO_P8
		 NULL,        // edge_fn
#endif
		 NULL         // corner_fn
		 );

  // the following is not needed since trace is local to each cell
  // exchange the data again to get all the slopes
  //p4est_ghost_exchange_data (p4est, ghost, this->ghost_data);

  /*
   * compute trace : reconstruct face states wm/wp using slope
   * and perform half time step update
   */
  CANOP_GLOBAL_INFO ("Performing trace in all direction\n");
  p4est_iterate (p4est,
		 ghost,
		 NULL, /* NULL means all direction trace */
		 m_pIter->iteratorsList[IT_ID::TRACE],
		 NULL,          // face_fn
#ifdef P4_TO_P8
		 NULL,          // edge_fn
#endif
		 NULL           // corner_fn
		 );
  // exchange the data again to get all the trace-reconstructed values
  p4est_ghost_exchange_data (p4est, ghost, get_ghost_data_ptr() );

} // SolverRamses::next_iteration_prepare

// =======================================================
// =======================================================
void
SolverRamses::next_iteration_do()
{

  p4est_t            *p4est = get_p4est ();
  p4est_ghost_t      *ghost = get_ghost ();

  // update the values in all the cell with the required fluxes
  CANOP_GLOBAL_INFO ("Performing scheme update in all direction\n");
  p4est_iterate (p4est,
		 ghost,
		 NULL, /* NULL means all direction update */
		 m_pIter->iteratorsList[IT_ID::UPDATE],
		 NULL,          // face_fn
#ifdef P4_TO_P8
		 NULL,          // edge_fn
#endif
		 NULL           // corner_fn
		 );

  if (ramsesPar.gravity.enabled) {

    // update the values in all the cell with gravity source term
    CANOP_GLOBAL_INFO ("Performing update with gravity source term\n");
    p4est_iterate (p4est,
		   ghost,
		   NULL, /* NULL means all direction update */
		   m_pIter->iteratorsList[IT_ID::GRAVITY_SOURCE_TERM],
		   NULL,          // face_fn
#ifdef P4_TO_P8
		   NULL,          // edge_fn
#endif
		   NULL           // corner_fn
		   );
  }
  
  // update old = new for all the quads
  CANOP_GLOBAL_INFO ("Copying new into old\n");
  p4est_iterate (p4est,
		 ghost,
		 NULL,
		 m_pIter->iteratorsList[IT_ID::END_STEP],
		 NULL,          // face_fn
#ifdef P4_TO_P8
                 NULL,          // edge_fn
#endif
                 NULL           // corner_fn
		 );

  // make sure everything is up-to-date
  if (m_subcycle != nullptr)
    p4est_ghost_exchange_data (p4est, ghost, get_ghost_data_ptr());

} // SolverRamses::next_iteration_do


// =======================================================
// =======================================================
/*
 * Solver unsplit for Ramses hydro scheme in cylindrical coordinates.
 *
 */
void UNUSED_FUNCTION
SolverRamses::next_iteration_hydro_cylindrical()
{
  p4est_t            *p4est = get_p4est ();
  p4est_ghost_t      *ghost = get_ghost ();

  // compute the cfl condition and time step at the smallest level
  compute_time_step ();

  /*
   * reconstruct gradients
   */
  CANOP_GLOBAL_INFO ("Compute slopes (Godunov 1st stage)\n");
  p4est_iterate (p4est,
		 ghost,
		 /* user data */ NULL,
		 m_pIter->iteratorsList[IT_ID::RECONSTRUCT_GRADIENT],
		 NULL,        // face_fn
#ifdef P4_TO_P8
		 NULL,        // edge_fn
#endif
		 NULL         // corner_fn
		 );

  // the following is not needed since trace is local to each cell
  // exchange the data again to get all the slopes
  //p4est_ghost_exchange_data (p4est, ghost, get_ghost_data_ptr());

  /*
   * compute trace : reconstruct face states wm/wp using slope
   * and perform half time step update
   */
  CANOP_GLOBAL_INFO ("Performing trace in all direction\n");
  p4est_iterate (p4est,
		 ghost,
		 NULL, /* NULL means all direction trace */
		 m_pIter->iteratorsList[IT_ID::TRACE],
		 NULL,          // face_fn
#ifdef P4_TO_P8
		 NULL,          // edge_fn
#endif
		 NULL           // corner_fn
		 );
  // exchange the data again to get all the trace-reconstructed values
  p4est_ghost_exchange_data (p4est, ghost, get_ghost_data_ptr() );

  // update the values in all the cell with the required fluxes
  CANOP_GLOBAL_INFO ("Performing scheme update in all direction\n");
  p4est_iterate (p4est,
		 ghost,
		 NULL, /* NULL means all direction update */
		 m_pIter->iteratorsList[IT_ID::UPDATE],
		 NULL,          // face_fn
#ifdef P4_TO_P8
		 NULL,          // edge_fn
#endif
		 NULL           // corner_fn
		 );

  if (ramsesPar.gravity.enabled) {

    // update the values in all the cell with gravity source term
    CANOP_GLOBAL_INFO ("Performing update with gravity source term\n");
    p4est_iterate (p4est,
		   ghost,
		   NULL, /* NULL means all direction update */
		   m_pIter->iteratorsList[IT_ID::GRAVITY_SOURCE_TERM],
		   NULL,          // face_fn
#ifdef P4_TO_P8
		   NULL,          // edge_fn
#endif
		   NULL           // corner_fn
		   );
  }

  // update old = new for all the quads
  CANOP_GLOBAL_INFO ("Copying new into old\n");
  p4est_iterate (p4est,
		 ghost,
		 NULL,
		 m_pIter->iteratorsList[IT_ID::END_STEP],
		 NULL,          // face_fn
#ifdef P4_TO_P8
                 NULL,          // edge_fn
#endif
                 NULL           // corner_fn
		 );

} // SolverRamses::next_iteration_hydro_cylindrical

// =======================================================
// =======================================================
void SolverRamses::write_hdf5(const std::string &filename)
{

  qdata_getter_t      getter = NULL; // callback to get actual data
  qdata_vector_getter_t vgetter = nullptr; // callback to get vector data
  // when position is needed
  qdata_field_type    ftype;
  hid_t               output_type =
    (m_single_precision ? H5T_NATIVE_FLOAT : H5T_NATIVE_DOUBLE);

  // open the new file and write our stuff
  m_writer->open (filename);
  m_writer->write_header (m_t);

  for (int i = 0; i < m_num_write_variables; ++i) {
    ftype = m_userdata_mgr->qdata_type_byname (m_write_variables[i]);

    if (ftype == QDATA_FIELD_SCALAR) {
      getter = m_userdata_mgr->qdata_getter_byname (m_write_variables[i]);
      if (getter != NULL) {
        m_writer->write_quadrant_attribute (m_write_variables[i],
                        m_userdata_mgr,
                        getter,
                        output_type);
      } else {
        // print warning
        CANOP_GLOBAL_INFOF ("Unrecognized getter: \"%s\"\n",
                            m_write_variables[i].c_str());
      }
    }

    else if (ftype == QDATA_FIELD_VECTOR) {
      vgetter = m_userdata_mgr->qdata_vgetter_byname (m_write_variables[i]);
      if (vgetter != nullptr) {
        m_writer->write_quadrant_attribute (m_write_variables[i],
                        m_userdata_mgr,
                        3,
                        vgetter,
                        output_type);
      }
    }
  }

  // close the file
  m_writer->write_footer ();
  m_writer->close ();

} //  SolverRamses::write_hdf5

// =======================================================
// =======================================================
void SolverRamses::read_config()
{
  // fisrt run base class method
  Solver::read_config();

  // read Ramses scheme specific parameters
  // #ifdef USE_RAMSES_HYDRO
  m_riemann_solver_name =
    m_cfg->config_read_std_string ("settings.riemann_solver", "hllc");
  // #elif defined(USE_RAMSES_MHD)
  //   m_riemann_solver_name =
  //     m_cfg->config_read_string ("settings.riemann_solver", "hlld");
  // #endif

  m_riemann_solver_fn =
    RiemannSolverFactoryRamses::Instance().get(m_riemann_solver_name);

  if (m_cfg->config_read_int("settings.subcycle", 0))
    m_subcycle = new subcycle_info;

  /*
   * Initialize ramses parameters.
   */
  ramsesPar.gamma0 = m_cfg->config_read_double ("settings.gamma0", 1.666);
  ramsesPar.smallc = m_cfg->config_read_double ("settings.smallc", 1e-8);
  ramsesPar.smallr = m_cfg->config_read_double ("settings.smallr", 1e-8);
  ramsesPar.smallp = m_cfg->config_read_double ("settings.smallp", 1e-7);
  ramsesPar.cIso   = m_cfg->config_read_double ("settings.cIso",   0.0);

  ramsesPar.gravity.static_enabled = m_cfg->config_read_double ("settings.static_gravity_enabled", 0);
  ramsesPar.gravity.point_enabled = m_cfg->config_read_double ("settings.point_gravity_enabled", 0);
  ramsesPar.gravity.poisson_enabled = m_cfg->config_read_int ("settings.poisson_gravity_enabled", 0);
  ramsesPar.gravity.enabled = ramsesPar.gravity.static_enabled ||
                              ramsesPar.gravity.point_enabled ||
                              ramsesPar.gravity.poisson_enabled;

  if (ramsesPar.gravity.enabled) {
    ramsesPar.gravity.static_x = m_cfg->config_read_double ("static_gravity_field.gravity_x", -0.1);
    ramsesPar.gravity.static_y = m_cfg->config_read_double ("static_gravity_field.gravity_y",  0.0);
    ramsesPar.gravity.static_z = m_cfg->config_read_double ("static_gravity_field.gravity_z",  0.0);

    ramsesPar.gravity.point_Gmass   = m_cfg->config_read_double ("point_gravity.Gmass",  1.0);
    ramsesPar.gravity.point_pos_x   = m_cfg->config_read_double ("point_gravity.pos_x",  0.5);
    ramsesPar.gravity.point_pos_y   = m_cfg->config_read_double ("point_gravity.pos_y",  0.5);
    ramsesPar.gravity.point_pos_z   = m_cfg->config_read_double ("point_gravity.pos_z",  0.5);
    ramsesPar.gravity.point_soften  = m_cfg->config_read_double ("point_gravity.soften", 0.0);

    ramsesPar.gravity.poisson_imax = m_cfg->config_read_int ("poisson_gravity.imax", 10000);
    ramsesPar.gravity.poisson_tol = m_cfg->config_read_double ("poisson_gravity.tol", 1e-4);
    ramsesPar.gravity.poisson_reuse_potential = m_cfg->config_read_int ("poisson_gravity.reuse_potential", 1);

    // read boundary conditions
    {
      static std::map<std::string, RamsesGravBC> boundary_map {
        {"neumann", RamsesGravBC::Neumann},
        {"periodic", RamsesGravBC::Periodic},
        {"monopole", RamsesGravBC::Monopole},
      };

      auto bound_str = m_cfg->config_read_std_string ("poisson_gravity.boundary", "neumann");
      auto bound_it = boundary_map.find (bound_str);
      if (bound_it != boundary_map.end())
        ramsesPar.gravity.poisson_boundary = bound_it->second;
      else {
        CANOP_GLOBAL_LERRORF ("Unrecognized Poisson boundary condition: \"%s\"\n",
                              bound_str.c_str());
        ramsesPar.gravity.poisson_boundary = RamsesGravBC::Neumann;
      }
    }
  }

  /* specific to cartesian / cylindrical scheme */
  ramsesPar.cartesian_enabled = 1; /* default */

  ramsesPar.cylindrical_enabled =
    m_cfg->config_read_int ( "geometry.cylindrical.enabled", 0);
  if (ramsesPar.cylindrical_enabled) {
    /* need to disabled cartesian and unstructured */
    ramsesPar.cartesian_enabled = 0;
  }

  ramsesPar.unstructured_enabled =
    m_cfg->config_read_int ( "geometry.unstructured.enabled", 0);
  if (ramsesPar.unstructured_enabled) {
    /* need to disabled cartesian and cylindrical */
    ramsesPar.cartesian_enabled = 0;
    ramsesPar.cylindrical_enabled = 0;
  }

  ramsesPar.cylindrical.nbTrees_theta = m_cfg->config_read_int ( "geometry.cylindrical.nbTrees_theta", 8);
  ramsesPar.cylindrical.nbTrees_z = m_cfg->config_read_int ( "geometry.cylindrical.nbTrees_z", 1);
  ramsesPar.cylindrical.rMin = m_cfg->config_read_double ( "geometry.cylindrical.rMin", 0.1);
  ramsesPar.cylindrical.rMax = m_cfg->config_read_double ( "geometry.cylindrical.rMax", 1.0);
  ramsesPar.cylindrical.Lr = ramsesPar.cylindrical.rMax-ramsesPar.cylindrical.rMin;
  ramsesPar.cylindrical.Ltheta = 2*M_PI;
  ramsesPar.cylindrical.Lz = m_cfg->config_read_double ( "geometry.cylindrical.Lz", 1.0);

  ramsesPar.cylindrical.zPeriodic = m_cfg->config_read_int ( "geometry.cylindrical.zPeriodic", 0);

  /* add timers for the Poisson solver, if enabled */
  if (ramsesPar.gravity.poisson_enabled) {
    Statistics::add_timer("t_grav_interp1", Statistics::timer_accum::mean, Statistics::timer_accum::sum);
    if(ramsesPar.gravity.poisson_boundary == RamsesGravBC::Monopole)
      Statistics::add_timer("t_grav_boundary", Statistics::timer_accum::mean, Statistics::timer_accum::sum);
    Statistics::add_timer("t_grav_solve", Statistics::timer_accum::mean, Statistics::timer_accum::sum);
    Statistics::add_timer("t_grav_interp2", Statistics::timer_accum::mean, Statistics::timer_accum::sum);
    Statistics::add_timer("t_grav_min", Statistics::timer_accum::min, Statistics::timer_accum::min);
    Statistics::add_timer("t_grav_max", Statistics::timer_accum::max, Statistics::timer_accum::max);
    Statistics::add_timer("t_grav_mean", Statistics::timer_accum::mean, Statistics::timer_accum::sum);
  }
} // SolverRamses::read_config

} // namespace ramses

} // namespace canop
