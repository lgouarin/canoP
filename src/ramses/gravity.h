#ifndef RAMSES_GRAVITY_H_
#define RAMSES_GRAVITY_H_

#ifdef USE_3D
#include <p4est_to_p8est.h>
#endif

#ifndef P4_TO_P8
#include <p4est.h>
#else
#include <p8est.h>
#endif

#include "PoissonFESolver.h"

namespace canop {
namespace ramses {

/**
 * Solve the Poisson equation for gravity.
 *
 * The units are chosen such that 4pi G = 1.
 *
 * \param[in]  p4est the forest
 * \param[in]  ghost the associated ghost layer
 * \param[out] info  feedback about number of iterations and final residual
 *
 * \return Non-zero iff the solver converged
 */
int
solve_poisson_gravity(p4est_t *p4est, p4est_ghost_t *ghost,
                      poisson_info_t *info = nullptr);

/**
 * Add a point-source contribution to the gravity fields.
 *
 * See the point_gravity_* members of ramsesPar.
 *
 * \param[in] p4est   the forest.
 * \param[in] reverse if true, remove the contribution instead of adding it.
 */
void
set_point_gravity (p4est_t *p4est, bool reverse = false);

} // namespace ramses
} // namespace canop

#endif // RAMSES_GRAVITY_H_
