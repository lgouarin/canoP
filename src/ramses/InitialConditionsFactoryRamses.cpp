#include "InitialConditionsFactoryRamses.h"

#include "SolverRamses.h"
#include "UserDataManagerRamses.h"
#include "quadrant_utils.h"

#include "ramses/userdata_ramses.h"

namespace canop {

namespace ramses {

using qdata_t               = Qdata;
using qdata_variables_t     = QdataVar;
using qdata_reconstructed_t = QdataRecons;

struct state2d_t {
  double data[4];
};

void get_quadrant2d_init_states(int problemId,
				state2d_t *primVarState0,
				state2d_t *primVarState1,
				state2d_t *primVarState2,
				state2d_t *primVarState3);


// =======================================================
// =======INITIAL CONDITIONS CALLBACKS====================
// =======================================================

/**************************************************************************/
/* Ramses : blast                                                         */
/**************************************************************************/
/**
 *
 * \brief blast
 *
 * The initial solution for rho is a disk which center is (xc,yc,zc) and
 * radius R.
 * Initial velocity is zero.
 *
 * See http://www.astro.princeton.edu/~jstone/Athena/tests/blast/blast.html
 */
void
init_ramses_blast (p4est_t * p4est,
		   p4est_topidx_t which_tree,
		   p4est_quadrant_t * quad)
{

  SolverRamses *solver = static_cast<SolverRamses*>(solver_from_p4est (p4est));

  UserDataManagerRamses
    *userdata_mgr = static_cast<UserDataManagerRamses *> (solver->get_userdata_mgr());

  /* geometry (if NULL, it means cartesian by default) */
  p4est_geometry_t   *geom        = solver->get_geom_compute();

  /* get config reader */
  ConfigReader    *cfg = solver->m_cfg;

  RamsesPar &ramsesPar = solver->ramsesPar;

  /* now start genuine initialization */
  qdata_variables_t   w;
  memset (&w, 0, sizeof (qdata_variables_t));

  // get the coordinates of the center of the quad
  double  XYZ[3] = { 0, 0, 0 };
  double  x,y,z;
  UNUSED (z);

  // get the physical coordinates of the center of the quad
  quadrant_center_vertex (p4est->connectivity, geom, which_tree, quad, XYZ);

  x = XYZ[0];
  y = XYZ[1];
  z = XYZ[2];

  // disk center, radius
  double x_c;
  double y_c;
  double z_c;
  double radius;

  x_c = cfg->config_read_double ("blast.x_c", 1.0);
  y_c = cfg->config_read_double ("blast.y_c", 1.0);
#ifdef USE_3D
  z_c = cfg->config_read_double ("blast.z_c", 1.0);
#endif
  UNUSED (z_c);

  radius = cfg->config_read_double ("blast.radius", 0.25);
  double radius2 = radius*radius;

  // density
  double density_in  = cfg->config_read_double ("blast.d_in",  1.0);
  double density_out = cfg->config_read_double ("blast.d_out", 1.0);

  // pressure
  double pressure_in  = cfg->config_read_double ("blast.p_in",  10.0);
  double pressure_out = cfg->config_read_double ("blast.p_out", 0.1);

  // velocity
  double velocity_x = cfg->config_read_double ("blast.velocity_x", 0.0);
  double velocity_y = cfg->config_read_double ("blast.velocity_y", 0.0);
  double velocity_z = cfg->config_read_double ("blast.velocity_z", 0.0);

  UNUSED (velocity_z);

  // define coordinates relative to the local tree
  double xl = x;
  double yl = y;
  double zl = z;
  
  // are we doing a replicated problem (useful for weak scaling)
  // this option is only interesting when using the brick connectivity
  bool replicated_problem_enabled = cfg->config_read_int ("settings.replicated_problem", 0);
  
  // if replicated problem is enabled, in each tree, we replicate the problem
  // we weak scaling is obtained when using a number of MPI process that
  // is a multiple of the number of trees
  if (replicated_problem_enabled == 1) {

    double tmp;
    xl = modf(x,&tmp);
    yl = modf(y,&tmp);

#ifdef USE_3D
    zl = modf(z,&tmp);
#endif // USE_3D

  } // end if replicated_problem_enabled
  
  double d2 = 
    SC_SQR(xl-x_c) + 
    SC_SQR(yl-y_c);
#ifdef USE_3D
  d2 += SC_SQR(zl-z_c);
#endif

  if (d2 < radius2) {

    w.rho = density_in;
    //+ d2 * (density_out-density_in)/radius2;
    //0.0 * (   ((double) (rand()) )/(RAND_MAX+1.0)*2-1  );
    w.rhoV[0] = w.rho*velocity_x;
    w.rhoV[1] = w.rho*velocity_y;
#ifdef USE_3D
    w.rhoV[2] = w.rho*velocity_z;
#endif

#ifdef USE_3D
    w.E_tot = pressure_in / (ramsesPar.gamma0-1.0) +
      0.5 * ( SC_SQR(w.rhoV[0]) +
	      SC_SQR(w.rhoV[1]) +
	      SC_SQR(w.rhoV[2]) ) / w.rho;
#else
    w.E_tot = pressure_in / (ramsesPar.gamma0-1.0) +
      0.5 * ( SC_SQR(w.rhoV[0]) +
	      SC_SQR(w.rhoV[1]) ) / w.rho;
#endif

  } else {
    w.rho = density_out + 0.0 * (   ((double) (rand()) )/(RAND_MAX+1.0)*2-1  );
    w.rhoV[0] = 0;
    w.rhoV[1] = 0;
#ifdef USE_3D
    w.rhoV[2] = 0;
#endif
    w.E_tot = pressure_out / (ramsesPar.gamma0-1.0);
  }

  // finally set quadrant's userdata
  userdata_mgr->quadrant_set_variables (quad, &w);
  userdata_mgr->quadrant_print_qdata (SC_LP_INFO, quad);

} // init_ramses_blast

/**************************************************************************/
/* Ramses : jet                                                           */
/**************************************************************************/
/**
 *
 * \brief jet
 *
 * Initial velocity is zero. Fluid at rest.
 * Border condition is inflow from the left.
 * Connectivity should be brick.
 *
 */
void
init_ramses_jet (p4est_t * p4est,
		 p4est_topidx_t which_tree,
		 p4est_quadrant_t * quad)
{

  SolverRamses *solver = static_cast<SolverRamses*>(solver_from_p4est (p4est));

  UserDataManagerRamses
    *userdata_mgr = static_cast<UserDataManagerRamses *> (solver->get_userdata_mgr());

  /* geometry (if NULL, it means cartesian by default) */
  p4est_geometry_t *geom = solver->get_geom_compute();

  /* get config reader */
  ConfigReader    *cfg = solver->m_cfg;

  RamsesPar &ramsesPar = solver->ramsesPar;

  /* now start genuine initialization */
  qdata_variables_t   w;
  memset (&w, 0, sizeof (qdata_variables_t));

  // get the coordinates of the center of the quad
  double  XYZ[3] = { 0, 0, 0 };
  double  x,y,z;
  UNUSED (z);

  // get the physical coordinates of the center of the quad
  quadrant_center_vertex (p4est->connectivity, geom, which_tree, quad, XYZ);

  x = XYZ[0];
  y = XYZ[1];
  z = XYZ[2];


  double rho  = cfg->config_read_double ("init_state.rho", 1.0);
  double P    = cfg->config_read_double ("init_state.pressure", 1.0);
  double vx   = cfg->config_read_double ("init_state.u", 0.0);
  double vy   = cfg->config_read_double ("init_state.v", 0.0);
  double vz   = cfg->config_read_double ("init_state.w", 0.0);
  UNUSED (vz);

  double xCenterJet = cfg->config_read_double ("border_inflow.xCenterJet", 0.0);
  double yCenterJet = cfg->config_read_double ("border_inflow.yCenterJet", 0.5);
  double radiusJet = cfg->config_read_double ("border_inflow.radiusJet", 0.1);
  double r2 = radiusJet*radiusJet;

  if (SC_SQR(x-xCenterJet)+SC_SQR(y-yCenterJet) < r2) {
    w.rho = 1.1;
  } else {
    w.rho = rho;
  }
  w.rhoV[0] = w.rho*vx;
  w.rhoV[1] = w.rho*vy;
#ifdef USE_3D
  w.rhoV[2] = w.rho*vz;
#endif

#ifdef USE_3D
  w.E_tot = P / (ramsesPar.gamma0-1.0) +
    0.5 * ( SC_SQR(w.rhoV[0]) +
	    SC_SQR(w.rhoV[1]) +
	    SC_SQR(w.rhoV[2]) ) / w.rho;
#else
  w.E_tot = P / (ramsesPar.gamma0-1.0) +
    0.5 * ( SC_SQR(w.rhoV[0]) +
	    SC_SQR(w.rhoV[1]) ) / w.rho;
#endif

  // finally set quadrant's userdata
  userdata_mgr->quadrant_set_variables (quad, &w);
  userdata_mgr->quadrant_print_qdata (SC_LP_INFO, quad);

} // init_ramses_jet

/**************************************************************************/
/* Ramses : Kelvin-Helmholtz                                              */
/**************************************************************************/
/**
 *
 * \brief Kelvin-Helmholtz instability.
 *
 * See http://www.astro.princeton.edu/~jstone/Athena/tests/kh/kh.html
 *
 * See also article by Robertson et al:
 * "Computational Eulerian hydrodynamics and Galilean invariance",
 * B.E. Robertson et al, Mon. Not. R. Astron. Soc., 401, 2463-2476, (2010).
 *
 */
void
init_ramses_kelvin_helmholtz (p4est_t * p4est,
			      p4est_topidx_t which_tree,
			      p4est_quadrant_t * quad)
{

  SolverRamses *solver = static_cast<SolverRamses*>(solver_from_p4est (p4est));

  UserDataManagerRamses
    *userdata_mgr = static_cast<UserDataManagerRamses *> (solver->get_userdata_mgr());

  /* get config reader */
  ConfigReader    *cfg = solver->m_cfg;

  RamsesPar &ramsesPar = solver->ramsesPar;

  /* geometry (if NULL, it means cartesian by default) */
  p4est_geometry_t *geom = solver->get_geom_compute();

  // density
  double density_in  = cfg->config_read_double ("kelvin_helmholtz.d_in",  1.0);
  double density_out = cfg->config_read_double ("kelvin_helmholtz.d_out", 1.0);

  // pressure
  double pressure  = cfg->config_read_double ("kelvin_helmholtz.pressure",  10.0);

  // perturbation sine / random
  int perturbation = cfg->config_read_int ("kelvin_helmholtz.perturbation_sine", 0);
  int p_sine = (perturbation > 0 ) ? 1 : 0;

  perturbation = cfg->config_read_int ("kelvin_helmholtz.perturbation_sine_robertson", 0);
  int p_sine_robertson = (perturbation > 0 ) ? 1 : 0;

  perturbation = cfg->config_read_int ("kelvin_helmholtz.perturbation_rand", 0);
  int p_rand = (perturbation > 0 ) ? 1 : 0;

  // flow velocities
  double vflow_in  = cfg->config_read_double ("kelvin_helmholtz.vflow_in",  -0.5);
  double vflow_out = cfg->config_read_double ("kelvin_helmholtz.vflow_out",  0.5);

  if (p_rand) {
    // choose a different random seed per mpi rank
    int seed = cfg->config_read_int ("kelvin_helmholtz.rand_seed", 12);
    srand(seed* (p4est->mpirank+1));
  }

  double amplitude = cfg->config_read_double ("kelvin_helmholtz.amplitude", 0.1);

  double outer_size = cfg->config_read_double ("kelvin_helmholtz.outer_size", 0.2);
  double inner_size = cfg->config_read_double ("kelvin_helmholtz.inner_size", 0.2);

  /* now start genuine initialization */
  qdata_variables_t   w;
  memset (&w, 0, sizeof (qdata_variables_t));

  /* physical space coordinates */
  double  XYZ[3] = { 0, 0, 0 };
  double  x,y,z;
  UNUSED (z);

  // get the physical coordinates of the center of the quad
  quadrant_center_vertex (p4est->connectivity, geom, which_tree, quad, XYZ);

  x = XYZ[0];
  y = XYZ[1];
  z = XYZ[2];

  /* are we using cartesian / cylindrical coordinate system ? */
  if (ramsesPar.cylindrical_enabled) {

    double r, theta;
    r     = XYZ[0];
    theta = XYZ[1];

    /*
     * Here, entering 2D cylindrical geometry
     */

    // get inner and outer radius
    double R1, R2;
    R1 = ramsesPar.cylindrical.rMin;
    R2 = ramsesPar.cylindrical.rMax;

    double R = (R1+R2)*0.5;
    UNUSED (R);

    int    n     = cfg->config_read_int   ("kelvin_helmholtz.mode", 2);
    double w0    = cfg->config_read_double("kelvin_helmholtz.w0", 0.1);
    double delta = cfg->config_read_double("kelvin_helmholtz.delta", 0.03);
    UNUSED (delta);

    double rho1 = density_in;
    double rho2 = density_out;

    // orthoradial velocity
    double v1 = vflow_in;
    double v2 = vflow_out;

    // angular velocity
    double Omega1 = v1/R1;
    double Omega2 = v2/R2;

#if 1

    if (r < R) {

      w.rho     = rho1;
      w.rhoV[0] = w.rho * w0 * sin(n*theta) * exp(-(r-R)*(r-R)/(2*R*R*delta));   // v radial
      w.rhoV[1] = w.rho * Omega1 * r;                   // v orthoradial
#ifdef USE_3D
      w.rhoV[2] = 0;
#endif
      w.E_tot   = (pressure) / (ramsesPar.gamma0-1.0) +
	0.5 * ( SC_SQR(w.rhoV[0]) + SC_SQR(w.rhoV[1]) ) / w.rho;

    } else {

      w.rho     = rho2;
      w.rhoV[0] = w.rho * w0 * sin(n*theta) * exp(-(r-R)*(r-R)/(2*R*R*delta));   // v radial
      w.rhoV[1] = w.rho * Omega2 * r;                   // v orthoradial
#ifdef USE_3D
      w.rhoV[2] = 0;
#endif
      w.E_tot   = (pressure) / (ramsesPar.gamma0-1.0) +
	0.5 * ( SC_SQR(w.rhoV[0]) + SC_SQR(w.rhoV[1]) ) / w.rho;

    }

#endif /* 1 */

#if 0 /* DISABLED */
    // if r << R, ramp = 1
    // if r >> R, ramp = 0
    double ramp = 1.0 / ( 1.0 + exp( 2*(r-R)/delta ) );

    // take care : in shell2d geometry/connectivity
    // first  direction is orthoradial
    // second direction is radial
    w.rho     = ramp*rho1 + (1-ramp)*rho2;
    w.rhoV[0] = w.rho * (ramp*v1 + (1-ramp)*v2);                   // v orthoradial
    w.rhoV[1] = w.rho * w0 * sin(n*theta) * 4 * ramp * (1-ramp);   // v radial
#ifdef USE_3D
    w.rhoV[2] = 0;
#endif
    w.E_tot   = pressure / (ramsesPar.gamma0-1.0) +
      0.5 * ( SC_SQR(w.rhoV[0]) + SC_SQR(w.rhoV[1]) ) / w.rho;
#endif /* DISABLED */

#if 0 /* DISABLED */
    delta *= (R2-R1);

    double r1 = R1 + 0.25*(R2-R1);
    double r2 = R1 + 0.75*(R2-R1);

    double ramp =
      1.0 / ( 1.0 + exp( 2*(r-r1)/delta ) ) +
      1.0 / ( 1.0 + exp( 2*(r2-r)/delta ) );

    w.rho     = rho1 + ramp*(rho2-rho1);
    w.rhoV[0] = w.rho * (v1 + ramp*(v2-v1)); // v orthoradial
    w.rhoV[1] = w.rho * w0 * sin(n*theta);   // v radial
    w.E_tot   = pressure / (ramsesPar.gamma0-1.0) +
      0.5 * ( SC_SQR(w.rhoV[0]) + SC_SQR(w.rhoV[1]) ) / w.rho;
#endif /* DISABLED */

  } else if (ramsesPar.cartesian_enabled) {

    /*
     * Regular cartesian geometry
     */
#ifndef USE_3D

  if (p_rand) {

    if ( y < 0.25 || y > 0.75) {

      w.rho     = density_out;
      w.rhoV[0] = density_out *	(vflow_out + amplitude * (1.0*rand()/RAND_MAX-0.5));
      w.rhoV[1] = density_out * (0         + amplitude * (1.0*rand()/RAND_MAX-0.5));
      w.E_tot   = pressure / (ramsesPar.gamma0-1.0) +
	0.5 * ( SC_SQR(w.rhoV[0]) + SC_SQR(w.rhoV[1]) ) / w.rho;

    } else {

      w.rho     = density_in;
      w.rhoV[0] = density_in * (vflow_in  + amplitude * (1.0*rand()/RAND_MAX-0.5));
      w.rhoV[1] = density_in * (0         + amplitude * (1.0*rand()/RAND_MAX-0.5));
      w.E_tot   = pressure / (ramsesPar.gamma0-1.0) +
	0.5 * ( SC_SQR(w.rhoV[0]) + SC_SQR(w.rhoV[1]) ) / w.rho;

    }

  } else if (p_sine) {

    if ( fabs(y-0.5) > outer_size ) {

      w.rho     = density_out;
      w.rhoV[0] = density_out * vflow_out;
      w.rhoV[1] = density_out * amplitude * sin(2*M_PI*x);
      w.E_tot   = pressure / (ramsesPar.gamma0-1.0) +
	0.5 * ( SC_SQR(w.rhoV[0]) + SC_SQR(w.rhoV[1]) ) / w.rho;

    } else if ( fabs(y-0.5) <= inner_size ) {

      w.rho     = density_in;
      w.rhoV[0] = density_in * vflow_in;
      w.rhoV[1] = density_in * amplitude  * sin(2*M_PI*x);
      w.E_tot   = pressure / (ramsesPar.gamma0-1.0) +
	0.5 * ( SC_SQR(w.rhoV[0]) + SC_SQR(w.rhoV[1]) ) / w.rho;

    } else { // interpolate

      double yCenter = 0.5;
      double ySize = 1.0;
      double interpSize = outer_size-inner_size;

      double rho_slope = (density_out - density_in) / (interpSize * ySize);
      double u_slope   = (vflow_out   - vflow_in)   / (interpSize * ySize);

      double deltaY;
      double deltaRho;
      double deltaU;

      if (y > yCenter) {
	deltaY   = y-(yCenter+inner_size*ySize);
	deltaRho = rho_slope*deltaY;
	deltaU   = u_slope*deltaY;
      } else {
	deltaY   = y-(yCenter-inner_size*ySize);
	deltaRho = -rho_slope*deltaY;
	deltaU   = -u_slope*deltaY;
      }

      w.rho     = density_in + deltaRho;
      w.rhoV[0] = density_in * (vflow_in  + deltaU);
      w.rhoV[1] = density_in * amplitude  * sin(2*M_PI*x);
      w.E_tot   = pressure / (ramsesPar.gamma0-1.0) +
	0.5 * ( SC_SQR(w.rhoV[0]) + SC_SQR(w.rhoV[1]) ) / w.rho;

    }

  } else if (p_sine_robertson) {

    // perturbation mode number
    double inner_size = cfg->config_read_double ("kelvin_helmholtz.inner_size", 0.2);
    UNUSED (inner_size);

    int    n     = cfg->config_read_int   ("kelvin_helmholtz.mode", 2);
    double w0    = cfg->config_read_double("kelvin_helmholtz.w0", 0.1);
    double delta = cfg->config_read_double("kelvin_helmholtz.delta", 0.03);

    double rho1 = density_in;
    double rho2 = density_out;

    double v1 = vflow_in;
    double v2 = vflow_out;

    double y1 = 0.25;
    double y2 = 0.75;

    double ramp =
      1.0 / ( 1.0 + exp( 2*(y-y1)/delta ) ) +
      1.0 / ( 1.0 + exp( 2*(y2-y)/delta ) );

    w.rho     = rho1 + ramp*(rho2-rho1);
    w.rhoV[0] = w.rho * (v1 + ramp*(v2-v1));
    w.rhoV[1] = w.rho * w0 * sin(n*M_PI*x);
    w.E_tot   = pressure / (ramsesPar.gamma0-1.0) +
      0.5 * ( SC_SQR(w.rhoV[0]) + SC_SQR(w.rhoV[1]) ) / w.rho;

  }

#else // 3D

  if (p_rand) {

    if ( z < 0.25 || z > 0.75) {

      w.rho     = density_out;
      w.rhoV[0] = density_out *	(vflow_out + amplitude * (1.0*rand()/RAND_MAX-0.5));
      w.rhoV[1] = density_out * (0         + amplitude * (1.0*rand()/RAND_MAX-0.5));
      w.rhoV[2] = density_out * (0         + amplitude * (1.0*rand()/RAND_MAX-0.5));
      w.E_tot   = pressure / (ramsesPar.gamma0-1.0) +
	0.5 * ( SC_SQR(w.rhoV[0]) +
		SC_SQR(w.rhoV[1]) +
		SC_SQR(w.rhoV[2])) / w.rho;

    } else {

      w.rho     = density_in;
      w.rhoV[0] = density_in * (vflow_in  + amplitude * (1.0*rand()/RAND_MAX-0.5));
      w.rhoV[1] = density_in * (0         + amplitude * (1.0*rand()/RAND_MAX-0.5));
      w.rhoV[2] = density_in * (0         + amplitude * (1.0*rand()/RAND_MAX-0.5));
      w.E_tot   = pressure / (ramsesPar.gamma0-1.0) +
	0.5 * ( SC_SQR(w.rhoV[0]) +
		SC_SQR(w.rhoV[1]) +
		SC_SQR(w.rhoV[2])) / w.rho;

    }

  } else if (p_sine) {

    if ( fabs(z-0.5) > outer_size ) {

      w.rho     = density_out;
      w.rhoV[0] = density_out * vflow_out;
      w.rhoV[1] = 0;
      w.rhoV[2] = density_out * amplitude * sin(2*M_PI*x);
      w.E_tot   = pressure / (ramsesPar.gamma0-1.0) +
    	0.5 * ( SC_SQR(w.rhoV[0]) +
		SC_SQR(w.rhoV[1]) +
		SC_SQR(w.rhoV[2]) ) / w.rho;

    } else if ( fabs(z-0.5) <= inner_size ) {

      w.rho     = density_in;
      w.rhoV[0] = density_in * vflow_in;
      w.rhoV[1] = 0;
      w.rhoV[2] = density_in * amplitude  * sin(2*M_PI*x);
      w.E_tot   = pressure / (ramsesPar.gamma0-1.0) +
    	0.5 * ( SC_SQR(w.rhoV[0]) +
		SC_SQR(w.rhoV[1]) +
		SC_SQR(w.rhoV[2]) ) / w.rho;

    } else { // interpolate

      double zCenter = 0.5;
      double zSize = 1.0;
      double interpSize = outer_size-inner_size;

      double rho_slope = (density_out - density_in) / (interpSize * zSize);
      double u_slope   = (vflow_out   - vflow_in)   / (interpSize * zSize);

      double deltaZ;
      double deltaRho;
      double deltaU;

      if (z > zCenter) {
	deltaZ   = z-(zCenter+inner_size*zSize);
	deltaRho = rho_slope*deltaZ;
	deltaU   = u_slope*deltaZ;
      } else {
	deltaZ   = z-(zCenter-inner_size*zSize);
	deltaRho = -rho_slope*deltaZ;
	deltaU   = -u_slope*deltaZ;
      }

      w.rho     = density_in + deltaRho;
      w.rhoV[0] = density_in * (vflow_in  + deltaU);
      w.rhoV[1] = 0;
      w.rhoV[2] = density_in * amplitude  * sin(2*M_PI*x);
      w.E_tot   = pressure / (ramsesPar.gamma0-1.0) +
	0.5 * ( SC_SQR(w.rhoV[0]) +
		SC_SQR(w.rhoV[1]) +
		SC_SQR(w.rhoV[2]) ) / w.rho;

    }

  } else if (p_sine_robertson) {

    // perturbation mode number
    int    n     = cfg->config_read_int   ("kelvin_helmholtz.mode", 2);
    double w0    = cfg->config_read_double("kelvin_helmholtz.w0", 0.1);
    double delta = cfg->config_read_double("kelvin_helmholtz.delta", 0.03);

    double rho1 = density_in;
    double rho2 = density_out;

    double v1x = vflow_in;
    double v2x = vflow_out;

    double v1y = vflow_in/2;
    double v2y = vflow_out/2;

    double z1 = 0.25;
    double z2 = 0.75;

    double ramp =
      1.0 / ( 1.0 + exp( 2*(z-z1)/delta ) ) +
      1.0 / ( 1.0 + exp( 2*(z2-z)/delta ) );

    w.rho     = rho1 + ramp*(rho2-rho1);
    w.rhoV[0] = w.rho * (v1x + ramp*(v2x-v1x));
    w.rhoV[1] = w.rho * (v1y + ramp*(v2y-v1y)); //w.rho * w0 * cos(n*M_PI*x);
    w.rhoV[2] = w.rho * w0 * sin(n*M_PI*x) * sin(n*M_PI*y);
    w.E_tot   = pressure / (ramsesPar.gamma0-1.0) +
      0.5 * ( SC_SQR(w.rhoV[0]) +
	      SC_SQR(w.rhoV[1]) +
	      SC_SQR(w.rhoV[2]) ) / w.rho;

  }

#endif

  } /* end cartesian / cylindrical */

  /* fill the quad ... */
  userdata_mgr->quadrant_set_variables (quad, &w);
  userdata_mgr->quadrant_print_qdata (SC_LP_INFO, quad);

} /* init_ramses_kelvin_helmholtz */

void primToCons_2D(state2d_t *state,
		   double _gamma0);

/**************************************************************************/
/* Ramses : piecewise constant in 2D quadrant                             */
/**************************************************************************/
/**
 *
 * \brief Setup piecewise constant initial data in 4 quadrants (2D only).
 *
 * There are 19 different possible configurations.
 *
 * This initial conditions are written for a unit square domain, i.e.
 * connectivity must be "unit".
 *
 * See article: Lax and Liu, "Solution of two-dimensional riemann
 * problems of gas dynamics by positive schemes",SIAM journal on
 * scientific computing, 1998, vol. 19, no2, pp. 319-340
 *
 * See also C. W. Schulz-Rinne and J. P. Collins and H. M. Glaz,
 * "Numerical Solution of the Riemann Problem for Two-Dimensional
 * Gas Dynamics", SIAM J. Sci. Comput., vol 14, pp 1394, 1993.
 *
 */
void
init_ramses_piecewise_constant_quadrant2d (p4est_t * p4est,
					   p4est_topidx_t which_tree,
					   p4est_quadrant_t * quad)
{

  SolverRamses *solver = static_cast<SolverRamses*>(solver_from_p4est (p4est));

  UserDataManagerRamses
    *userdata_mgr = static_cast<UserDataManagerRamses *> (solver->get_userdata_mgr());

  /* geometry (if NULL, it means cartesian by default) */
  p4est_geometry_t *geom = solver->get_geom_compute();

  /* get config reader */
  ConfigReader    *cfg = solver->m_cfg;

  RamsesPar &ramsesPar = solver->ramsesPar;

  /* now start genuine initialization */
  qdata_variables_t   w;
  memset (&w, 0, sizeof (qdata_variables_t));

  // get the coordinates of the center of the quad
  double  XYZ[3] = { 0, 0, 0 };
  double  x,y,z;
  UNUSED (z);

  // get the physical coordinates of the center of the quad
  quadrant_center_vertex (p4est->connectivity, geom, which_tree, quad, XYZ);

  x = XYZ[0];
  y = XYZ[1];
  z = XYZ[2];

  // which configuration ?
  int configNb = cfg->config_read_int ("piecewise_constant_quadrant2d.configNb", 1);

  // piecewise constant states
  state2d_t state1, state2, state3, state4;

  // transition point
  double xt = cfg->config_read_double ("piecewise_constant_quadrant2d.x", 0.5);
  double yt = cfg->config_read_double ("piecewise_constant_quadrant2d.y", 0.5);

  // retrieve the piecewise constant states associated
  // to the given configuration
  get_quadrant2d_init_states(configNb, &state1, &state2, &state3, &state4);

  if (x < xt) {
    if (y < yt) {
      // use state 3
      primToCons_2D(&state3, ramsesPar.gamma0);
      w.rho     = state3.data[ID];
      w.rhoV[0] = state3.data[IU];
      w.rhoV[1] = state3.data[IV];
      w.E_tot   = state3.data[IP];

    } else {
      // use state 2
      primToCons_2D(&state2, ramsesPar.gamma0);
      w.rho     = state2.data[ID];
      w.rhoV[0] = state2.data[IU];
      w.rhoV[1] = state2.data[IV];
      w.E_tot   = state2.data[IP];
    }
  } else {
    if (y < yt) {
      // use state 4
      primToCons_2D(&state4, ramsesPar.gamma0);
      w.rho     = state4.data[ID];
      w.rhoV[0] = state4.data[IU];
      w.rhoV[1] = state4.data[IV];
      w.E_tot   = state4.data[IP];

    } else {
      // use state 1
      primToCons_2D(&state1, ramsesPar.gamma0);
      w.rho     = state1.data[ID];
      w.rhoV[0] = state1.data[IU];
      w.rhoV[1] = state1.data[IV];
      w.E_tot   = state1.data[IP];
    }
  }

  /* if (x < xt) { */
  /*   if (y < yt) { */
  /*     // use state 1 */
  /*     primToCons_2D(&state1, ramsesPar.gamma0); */
  /*     w.rho     = state1.data[ID]; */
  /*     w.rhoV[0] = state1.data[IU]; */
  /*     w.rhoV[1] = state1.data[IV]; */
  /*     w.E_tot   = state1.data[IP]; */

  /*   } else { */
  /*     // use state 4 */
  /*     primToCons_2D(&state4, ramsesPar.gamma0); */
  /*     w.rho     = state4.data[ID]; */
  /*     w.rhoV[0] = state4.data[IU]; */
  /*     w.rhoV[1] = state4.data[IV]; */
  /*     w.E_tot   = state4.data[IP]; */
  /*   } */
  /* } else { */
  /*   if (y < yt) { */
  /*     // use state 2 */
  /*     primToCons_2D(&state2, ramsesPar.gamma0); */
  /*     w.rho     = state2.data[ID]; */
  /*     w.rhoV[0] = state2.data[IU]; */
  /*     w.rhoV[1] = state2.data[IV]; */
  /*     w.E_tot   = state2.data[IP]; */

  /*   } else { */
  /*     // use state 3 */
  /*     primToCons_2D(&state3, ramsesPar.gamma0); */
  /*     w.rho     = state3.data[ID]; */
  /*     w.rhoV[0] = state3.data[IU]; */
  /*     w.rhoV[1] = state3.data[IV]; */
  /*     w.E_tot   = state3.data[IP]; */
  /*   } */
  /* } */

  // finally set quadrant's userdata
  userdata_mgr->quadrant_set_variables (quad, &w);
  userdata_mgr->quadrant_print_qdata (SC_LP_INFO, quad);

} // init_ramses_piecewise_constant_quadrant2d

/**************************************************************************/
/**************************************************************************/
/**
 * \param[in]  problemId problem id (between 1 and 19)
 * \param[out] primVarState primitive variable state number 0
 * \param[out] primVarState primitive variable state number 1
 * \param[out] primVarState primitive variable state number 2
 * \param[out] primVarState primitive variable state number 3
 */
void
get_quadrant2d_init_states(int problemId,
			   state2d_t *primVarState0,
			   state2d_t *primVarState1,
			   state2d_t *primVarState2,
			   state2d_t *primVarState3)
{

  switch(problemId) {
  case 1:
    // Config 1
    primVarState0->data[ID] = 1.0;
    primVarState0->data[IU] = 0.0;
    primVarState0->data[IV] = 0.0;
    primVarState0->data[IP] = 1.0;

    primVarState1->data[ID] = 0.5197;
    primVarState1->data[IU] =-0.7259;
    primVarState1->data[IV] = 0.0;
    primVarState1->data[IP] = 0.4;

    primVarState2->data[ID] = 0.1072;
    primVarState2->data[IU] =-0.7259;
    primVarState2->data[IV] =-1.4045;
    primVarState2->data[IP] = 0.0439;

    primVarState3->data[ID] = 0.2579;
    primVarState3->data[IU] = 0.0;
    primVarState3->data[IV] =-1.4045;
    primVarState3->data[IP] = 0.15;
    break;

  case 2:
    // Config 2
    primVarState0->data[ID] = 1.0;
    primVarState0->data[IU] = 0.0;
    primVarState0->data[IV] = 0.0;
    primVarState0->data[IP] = 1.0;

    primVarState1->data[ID] = 0.5197;
    primVarState1->data[IU] =-0.7259;
    primVarState1->data[IV] = 0.0;
    primVarState1->data[IP] = 0.4;

    primVarState2->data[ID] = 1.0;
    primVarState2->data[IU] =-0.7259;
    primVarState2->data[IV] =-0.7259;
    primVarState2->data[IP] = 1.0;

    primVarState3->data[ID] = 0.5197;
    primVarState3->data[IU] = 0.0;
    primVarState3->data[IV] =-0.7259;
    primVarState3->data[IP] = 0.4;
    break;

  case 3:
    // Config 3
    primVarState0->data[ID] = 1.5;
    primVarState0->data[IU] = 0.0;
    primVarState0->data[IV] = 0.0;
    primVarState0->data[IP] = 1.5;

    primVarState1->data[ID] = 0.5323;
    primVarState1->data[IU] = 1.206;
    primVarState1->data[IV] = 0.0;
    primVarState1->data[IP] = 0.3;

    primVarState2->data[ID] = 0.138;
    primVarState2->data[IU] = 1.206;
    primVarState2->data[IV] = 1.206;
    primVarState2->data[IP] = 0.029;

    primVarState3->data[ID] = 0.5323;
    primVarState3->data[IU] = 0.0;
    primVarState3->data[IV] = 1.206;
    primVarState3->data[IP] = 0.3;
    break;

  case 4:
    // Config 4
    primVarState0->data[ID] = 1.1;
    primVarState0->data[IU] = 0.0;
    primVarState0->data[IV] = 0.0;
    primVarState0->data[IP] = 1.1;

    primVarState1->data[ID] = 0.5065;
    primVarState1->data[IU] = 0.8939;
    primVarState1->data[IV] = 0.0;
    primVarState1->data[IP] = 0.35;

    primVarState2->data[ID] = 1.1;
    primVarState2->data[IU] = 0.8939;
    primVarState2->data[IV] = 0.8939;
    primVarState2->data[IP] = 1.1;

    primVarState3->data[ID] = 0.5065;
    primVarState3->data[IU] = 0.0;
    primVarState3->data[IV] = 0.8939;
    primVarState3->data[IP] = 0.35;
    break;

  case 5:
    // Config 5
    primVarState0->data[ID] = 1.0;
    primVarState0->data[IU] =-0.75;
    primVarState0->data[IV] =-0.5;
    primVarState0->data[IP] = 1.0;

    primVarState1->data[ID] = 2.0;
    primVarState1->data[IU] =-0.75;
    primVarState1->data[IV] = 0.5;
    primVarState1->data[IP] = 1.0;

    primVarState2->data[ID] = 1.0;
    primVarState2->data[IU] = 0.75;
    primVarState2->data[IV] = 0.5;
    primVarState2->data[IP] = 1.0;

    primVarState3->data[ID] = 3.0;
    primVarState3->data[IU] = 0.75;
    primVarState3->data[IV] =-0.5;
    primVarState3->data[IP] = 1.0;
    break;

  case 6:
    // Config 6
    primVarState0->data[ID] = 1.0;
    primVarState0->data[IU] = 0.75;
    primVarState0->data[IV] =-0.5;
    primVarState0->data[IP] = 1.0;

    primVarState1->data[ID] = 2.0;
    primVarState1->data[IU] = 0.75;
    primVarState1->data[IV] = 0.5;
    primVarState1->data[IP] = 0.5;

    primVarState2->data[ID] = 1.0;
    primVarState2->data[IU] =-0.75;
    primVarState2->data[IV] = 0.5;
    primVarState2->data[IP] = 1.0;

    primVarState3->data[ID] = 3.0;
    primVarState3->data[IU] =-0.75;
    primVarState3->data[IV] =-0.5;
    primVarState3->data[IP] = 1.0;
    break;

  case 7:
    // Config 7
    primVarState0->data[ID] = 1.0;
    primVarState0->data[IU] = 0.1;
    primVarState0->data[IV] = 0.1;
    primVarState0->data[IP] = 1.0;

    primVarState1->data[ID] = 0.5197;
    primVarState1->data[IU] =-0.6259;
    primVarState1->data[IV] = 0.1;
    primVarState1->data[IP] = 0.4;

    primVarState2->data[ID] = 0.8;
    primVarState2->data[IU] = 0.1;
    primVarState2->data[IV] = 0.1;
    primVarState2->data[IP] = 0.4;

    primVarState3->data[ID] = 0.5197;
    primVarState3->data[IU] = 0.1;
    primVarState3->data[IV] =-0.6259;
    primVarState3->data[IP] = 0.4;
    break;

  case 8:
    // Config 8
    primVarState0->data[ID] = 0.5197;
    primVarState0->data[IU] = 0.1;
    primVarState0->data[IV] = 0.1;
    primVarState0->data[IP] = 0.4;

    primVarState1->data[ID] = 1.0;
    primVarState1->data[IU] =-0.6259;
    primVarState1->data[IV] = 0.1;
    primVarState1->data[IP] = 1.0;

    primVarState2->data[ID] = 0.8;
    primVarState2->data[IU] = 0.1;
    primVarState2->data[IV] = 0.1;
    primVarState2->data[IP] = 1.0;

    primVarState3->data[ID] = 1.0;
    primVarState3->data[IU] = 0.1;
    primVarState3->data[IV] =-0.6259;
    primVarState3->data[IP] = 1.0;
    break;

  case 9:
    // Config 9
    primVarState0->data[ID] = 1.0;
    primVarState0->data[IU] = 0.0;
    primVarState0->data[IV] = 0.3;
    primVarState0->data[IP] = 1.0;

    primVarState1->data[ID] = 2.0;
    primVarState1->data[IU] = 0.0;
    primVarState1->data[IV] =-0.3;
    primVarState1->data[IP] = 1.0;

    primVarState2->data[ID] = 1.039;
    primVarState2->data[IU] = 0.0;
    primVarState2->data[IV] =-0.8133;
    primVarState2->data[IP] = 0.4;

    primVarState3->data[ID] = 0.5197;
    primVarState3->data[IU] = 0.0;
    primVarState3->data[IV] =-0.4259;
    primVarState3->data[IP] = 0.4;
    break;

  case 10:
    // Config 10
    primVarState0->data[ID] = 1.0;
    primVarState0->data[IU] = 0.0;
    primVarState0->data[IV] = 0.4297;
    primVarState0->data[IP] = 1.0;

    primVarState1->data[ID] = 0.5;
    primVarState1->data[IU] = 0.0;
    primVarState1->data[IV] = 0.6076;
    primVarState1->data[IP] = 1.0;

    primVarState2->data[ID] = 0.2281;
    primVarState2->data[IU] = 0.0;
    primVarState2->data[IV] =-0.6076;
    primVarState2->data[IP] = 0.3333;

    primVarState3->data[ID] = 0.4562;
    primVarState3->data[IU] = 0.0;
    primVarState3->data[IV] =-0.4259;
    primVarState3->data[IP] = 0.3333;
    break;

  case 11:
    // Config 11
    primVarState0->data[ID] = 1.0;
    primVarState0->data[IU] = 0.1;
    primVarState0->data[IV] = 0.0;
    primVarState0->data[IP] = 1.0;

    primVarState1->data[ID] = 0.5313;
    primVarState1->data[IU] = 0.8276;
    primVarState1->data[IV] = 0.0;
    primVarState1->data[IP] = 0.4;

    primVarState2->data[ID] = 0.8;
    primVarState2->data[IU] = 0.1;
    primVarState2->data[IV] = 0.0;
    primVarState2->data[IP] = 0.4;

    primVarState3->data[ID] = 0.5313;
    primVarState3->data[IU] = 0.1;
    primVarState3->data[IV] = 0.7276;
    primVarState3->data[IP] = 0.4;
    break;

  case 12:
    // Config 12
    primVarState0->data[ID] = 0.5313;
    primVarState0->data[IU] = 0.0;
    primVarState0->data[IV] = 0.0;
    primVarState0->data[IP] = 0.4;

    primVarState1->data[ID] = 1.0;
    primVarState1->data[IU] = 0.7276;
    primVarState1->data[IV] = 0.0;
    primVarState1->data[IP] = 1.0;

    primVarState2->data[ID] = 0.8;
    primVarState2->data[IU] = 0.0;
    primVarState2->data[IV] = 0.0;
    primVarState2->data[IP] = 1.0;

    primVarState3->data[ID] = 1.0;
    primVarState3->data[IU] = 0.0;
    primVarState3->data[IV] = 0.7276;
    primVarState3->data[IP] = 1.0;

    break;

  case 13:
    // Config 13
    primVarState0->data[ID] = 1.0;
    primVarState0->data[IU] = 0.0;
    primVarState0->data[IV] =-0.3;
    primVarState0->data[IP] = 1.0;

    primVarState1->data[ID] = 2.0;
    primVarState1->data[IU] = 0.0;
    primVarState1->data[IV] = 0.3;
    primVarState1->data[IP] = 1.0;

    primVarState2->data[ID] = 1.0625;
    primVarState2->data[IU] = 0.0;
    primVarState2->data[IV] = 0.8145;
    primVarState2->data[IP] = 0.4;

    primVarState3->data[ID] = 0.5313;
    primVarState3->data[IU] = 0.0;
    primVarState3->data[IV] = 0.4276;
    primVarState3->data[IP] = 0.4;
    break;

  case 14:
    // Config 14
    primVarState0->data[ID] = 2.0;
    primVarState0->data[IU] = 0.0;
    primVarState0->data[IV] =-0.5606;
    primVarState0->data[IP] = 8.0;

    primVarState1->data[ID] = 1.0;
    primVarState1->data[IU] = 0.0;
    primVarState1->data[IV] =-1.2172;
    primVarState1->data[IP] = 8.0;

    primVarState2->data[ID] = 0.4736;
    primVarState2->data[IU] = 0.0;
    primVarState2->data[IV] = 1.2172;
    primVarState2->data[IP] = 2.6667;

    primVarState3->data[ID] = 0.9474;
    primVarState3->data[IU] = 0.0;
    primVarState3->data[IV] = 1.1606;
    primVarState3->data[IP] = 2.6667;
    break;

  case 15:
    // Config 15
    primVarState0->data[ID] = 1.0;
    primVarState0->data[IU] = 0.1;
    primVarState0->data[IV] =-0.3;
    primVarState0->data[IP] = 1.0;

    primVarState1->data[ID] = 0.5197;
    primVarState1->data[IU] =-0.6259;
    primVarState1->data[IV] =-0.3;
    primVarState1->data[IP] = 0.4;

    primVarState2->data[ID] = 0.8;
    primVarState2->data[IU] = 0.1;
    primVarState2->data[IV] =-0.3;
    primVarState2->data[IP] = 0.4;

    primVarState3->data[ID] = 0.5313;
    primVarState3->data[IU] = 0.1;
    primVarState3->data[IV] = 0.4276;
    primVarState3->data[IP] = 0.4;
    break;

  case 16:
    // Config 16
    primVarState0->data[ID] = 0.5313;
    primVarState0->data[IU] = 0.1;
    primVarState0->data[IV] = 0.1;
    primVarState0->data[IP] = 0.4;

    primVarState1->data[ID] = 1.0222;
    primVarState1->data[IU] =-0.6179;
    primVarState1->data[IV] = 0.1;
    primVarState1->data[IP] = 1.0;

    primVarState2->data[ID] = 0.8;
    primVarState2->data[IU] = 0.1;
    primVarState2->data[IV] = 0.1;
    primVarState2->data[IP] = 1.0;

    primVarState3->data[ID] = 1.0;
    primVarState3->data[IU] = 0.1;
    primVarState3->data[IV] = 0.8276;
    primVarState3->data[IP] = 1.0;
    break;

  case 17:
    // Config 17
    primVarState0->data[ID] = 1.0;
    primVarState0->data[IU] = 0.0;
    primVarState0->data[IV] =-0.4;
    primVarState0->data[IP] = 1.0;

    primVarState1->data[ID] = 2.0;
    primVarState1->data[IU] = 0.0;
    primVarState1->data[IV] =-0.3;
    primVarState1->data[IP] = 1.0;

    primVarState2->data[ID] = 1.0625;
    primVarState2->data[IU] = 0.0;
    primVarState2->data[IV] = 0.2145;
    primVarState2->data[IP] = 0.4;

    primVarState3->data[ID] = 0.5197;
    primVarState3->data[IU] = 0.0;
    primVarState3->data[IV] =-1.1259;
    primVarState3->data[IP] = 0.4;
    break;

  case 18:
    // Config 18
    primVarState0->data[ID] = 1.0;
    primVarState0->data[IU] = 0.0;
    primVarState0->data[IV] = 1.0;
    primVarState0->data[IP] = 1.0;

    primVarState1->data[ID] = 2.0;
    primVarState1->data[IU] = 0.0;
    primVarState1->data[IV] =-0.3;
    primVarState1->data[IP] = 1.0;

    primVarState2->data[ID] = 1.0625;
    primVarState2->data[IU] = 0.0;
    primVarState2->data[IV] = 0.2145;
    primVarState2->data[IP] = 0.4;

    primVarState3->data[ID] = 0.5197;
    primVarState3->data[IU] = 0.0;
    primVarState3->data[IV] = 0.2741;
    primVarState3->data[IP] = 0.4;
    break;

  case 19:
    // Config 19
    primVarState0->data[ID] = 1.0;
    primVarState0->data[IU] = 0.0;
    primVarState0->data[IV] = 0.3;
    primVarState0->data[IP] = 1.0;

    primVarState1->data[ID] = 2.0;
    primVarState1->data[IU] = 0.0;
    primVarState1->data[IV] =-0.3;
    primVarState1->data[IP] = 1.0;

    primVarState2->data[ID] = 1.0625;
    primVarState2->data[IU] = 0.0;
    primVarState2->data[IV] = 0.2145;
    primVarState2->data[IP] = 0.4;

    primVarState3->data[ID] = 0.5197;
    primVarState3->data[IU] = 0.0;
    primVarState3->data[IV] =-0.4259;
    primVarState3->data[IP] = 0.4;
    break;

  default:
    // return config 1
    primVarState0->data[ID] = 1.0;
    primVarState0->data[IU] = 0.0;
    primVarState0->data[IV] = 0.0;
    primVarState0->data[IP] = 1.0;

    primVarState1->data[ID] = 0.5197;
    primVarState1->data[IU] =-0.7259;
    primVarState1->data[IV] = 0.0;
    primVarState1->data[IP] = 0.4;

    primVarState2->data[ID] = 0.1072;
    primVarState2->data[IU] =-0.7259;
    primVarState2->data[IV] =-1.4045;
    primVarState2->data[IP] = 0.0439;

    primVarState3->data[ID] = 0.2579;
    primVarState3->data[IU] = 0.0;
    primVarState3->data[IV] =-1.4045;
    primVarState3->data[IP] = 0.15;
    break;
  } // end switch

} // get_quadrant2d_init_states


/**************************************************************************/
/**************************************************************************/
/**
 * primitive variables (rho, u, v, p) to conservative variables (rho,
 * rhou, rhov, E_total)
 * @param[in,out] state input (prim variables) / output (cons variables)
 * @param[in] _gamma0 specific heat ratio
 */
void primToCons_2D(state2d_t *state,
		   double _gamma0)
{

  double rho = state->data[ID];
  double p   = state->data[IP];
  double u   = state->data[IU];
  double v   = state->data[IV];

  state->data[IU] *= rho; // rho*u
  state->data[IV] *= rho; // rho*v

  state->data[IP] = p/(_gamma0-1.0) + rho*(u*u+v*v)*0.5;

} // primToCons_2D

/**************************************************************************/
/* Ramses : implode                                                       */
/**************************************************************************/
/**
 * The Hydrodynamical Implosion Test.
 * see
 * http://www.astro.princeton.edu/~jstone/Athena/tests/implode/Implode.html
 * for a description of such initial conditions.
 *
 * see also article : Liska, R., & Wendroff, B., "Comparison of Several difference schemes on 1D and 2D Test problems for the Euler equations", http://www-troja.fjfi.cvut.cz/~liska/CompareEuler/compare8/
 *
 */
void
init_ramses_implode (p4est_t * p4est,
		     p4est_topidx_t which_tree,
		     p4est_quadrant_t * quad)
{

  SolverRamses *solver = static_cast<SolverRamses*>(solver_from_p4est (p4est));

  UserDataManagerRamses
    *userdata_mgr = static_cast<UserDataManagerRamses *> (solver->get_userdata_mgr());

  /* geometry (if NULL, it means cartesian by default) */
  p4est_geometry_t *geom = solver->get_geom_compute();

  /* get config reader */
  ConfigReader    *cfg = solver->m_cfg;
  UNUSED (cfg);

  RamsesPar &ramsesPar = solver->ramsesPar;

  /* now start genuine initialization */
  qdata_variables_t   w;
  memset (&w, 0, sizeof (qdata_variables_t));

  // get the coordinates of the center of the quad
  double  XYZ[3] = { 0, 0, 0 };
  double  x,y,z;
  UNUSED (z);

  // initialize density perturbation amplitude */
  double amplitude = cfg->config_read_double ("implode.amplitude", 0.0);

  // random seed
  int    seed      = cfg->config_read_int    ("implode.rand_seed", 12);
  srand(seed * (p4est->mpirank+1) );

  /* are we using cartesian / cylindrical coordinate system ? */
  if (ramsesPar.cylindrical_enabled) {

    // get the physical coordinates of the center of the quad
    quadrant_center_vertex (p4est->connectivity, geom, which_tree, quad, XYZ);

    double r, theta, z;
    r     = XYZ[0];
    theta = XYZ[1];
    z     = XYZ[2];
    UNUSED(r);
    UNUSED(theta);
    UNUSED(z);

    double x,y;
    x = r*cos(theta);
    y = r*sin(theta);
    UNUSED(x);
    UNUSED(y);

    double rMin = ramsesPar.cylindrical.rMin;
    double rMax = ramsesPar.cylindrical.rMax;
    double Lr   = ramsesPar.cylindrical.Lr;
    UNUSED(Lr);
    UNUSED(rMax);


#ifdef USE_3D

    if (r > rMin+0.25*Lr) {

      w.rho     = 1.0 + amplitude * (1.0*rand()/RAND_MAX-0.5);
      w.rhoV[0] = 0.0;
      w.rhoV[1] = 0.0;
      w.rhoV[2] = 0.0;
      w.E_tot = 1.0 / (ramsesPar.gamma0-1.0) +
	0.5 * ( SC_SQR(w.rhoV[0]) +
		SC_SQR(w.rhoV[1]) +
		SC_SQR(w.rhoV[2]) ) / w.rho;

    } else {

      w.rho     = 0.125 + amplitude * (1.0*rand()/RAND_MAX-0.5);
      w.rhoV[0] = 0.0;
      w.rhoV[1] = 0.0;
      w.rhoV[2] = 0.0;
      w.E_tot = 0.14 / (ramsesPar.gamma0-1.0) +
	0.5 * ( SC_SQR(w.rhoV[0]) +
		SC_SQR(w.rhoV[1]) +
		SC_SQR(w.rhoV[2]) ) / w.rho;

    }

#else // 2D

    if (/*r > rMin+0.25*Lr*/ x > (rMin+rMax)*0.5) {

      w.rho     = 1.0 + amplitude * (1.0*rand()/RAND_MAX-0.5);
      w.rhoV[0] = 0.0;
      w.rhoV[1] = 0.0;

      w.E_tot = 1.0 / (ramsesPar.gamma0-1.0) +
	0.5 * ( SC_SQR(w.rhoV[0]) +
		SC_SQR(w.rhoV[1]) ) / w.rho;

    } else {

      w.rho     = 0.125 + amplitude * (1.0*rand()/RAND_MAX-0.5);
      w.rhoV[0] = 0.0;
      w.rhoV[1] = 0.0;

      w.E_tot = 0.14 / (ramsesPar.gamma0-1.0) +
	0.5 * ( SC_SQR(w.rhoV[0]) +
		SC_SQR(w.rhoV[1]) ) / w.rho;

    }

#endif // 2D/3D

  } else { // cartesian

    // get the physical coordinates of the center of the quad
    quadrant_center_vertex (p4est->connectivity, geom, which_tree, quad, XYZ);

    x = XYZ[0];
    y = XYZ[1];
    z = XYZ[2];

#ifdef USE_3D

    if (x+y+z > 0.5) {

      w.rho     = 1.0 + amplitude * (1.0*rand()/RAND_MAX-0.5);
      w.rhoV[0] = 0.0;
      w.rhoV[1] = 0.0;
      w.rhoV[2] = 0.0;
      w.E_tot = 1.0 / (ramsesPar.gamma0-1.0) +
	0.5 * ( SC_SQR(w.rhoV[0]) +
		SC_SQR(w.rhoV[1]) +
		SC_SQR(w.rhoV[2]) ) / w.rho;

    } else {

      w.rho     = 0.125 + amplitude * (1.0*rand()/RAND_MAX-0.5);
      w.rhoV[0] = 0.0;
      w.rhoV[1] = 0.0;
      w.rhoV[2] = 0.0;
      w.E_tot = 0.14 / (ramsesPar.gamma0-1.0) +
	0.5 * ( SC_SQR(w.rhoV[0]) +
		SC_SQR(w.rhoV[1]) +
		SC_SQR(w.rhoV[2]) ) / w.rho;

    }

#else // 2D

    if (x+y > 0.5) {

      w.rho     = 1.0 + amplitude * (1.0*rand()/RAND_MAX-0.5);
      w.rhoV[0] = 0.0;
      w.rhoV[1] = 0.0;

      w.E_tot = 1.0 / (ramsesPar.gamma0-1.0) +
	0.5 * ( SC_SQR(w.rhoV[0]) +
		SC_SQR(w.rhoV[1]) ) / w.rho;

    } else {

      w.rho     = 0.125 + amplitude * (1.0*rand()/RAND_MAX-0.5);
      w.rhoV[0] = 0.0;
      w.rhoV[1] = 0.0;

      w.E_tot = 0.14 / (ramsesPar.gamma0-1.0) +
	0.5 * ( SC_SQR(w.rhoV[0]) +
		SC_SQR(w.rhoV[1]) ) / w.rho;

    }

#endif // 2D/3D
  }

  // finally set quadrant's userdata
  userdata_mgr->quadrant_set_variables (quad, &w);
  userdata_mgr->quadrant_print_qdata (SC_LP_INFO, quad);

} // init_ramses_implode

/**************************************************************************/
/* Ramses : shock-bubble                                                  */
/**************************************************************************/
/**
 * Shock bubble test.
 *
 * See for example:
 * http://amroc.sourceforge.net/examples/euler/2d/html/shbubble_n.htm
 */
void
init_ramses_shock_bubble (p4est_t * p4est,
			  p4est_topidx_t which_tree,
			  p4est_quadrant_t * quad)
{

  SolverRamses *solver = static_cast<SolverRamses*>(solver_from_p4est (p4est));

  UserDataManagerRamses
    *userdata_mgr = static_cast<UserDataManagerRamses *> (solver->get_userdata_mgr());

  /* geometry (if NULL, it means cartesian by default) */
  p4est_geometry_t *geom = solver->get_geom_compute();

  /* get config reader */
  ConfigReader    *cfg = solver->m_cfg;

  RamsesPar &ramsesPar = solver->ramsesPar;

  /* now start genuine initialization */
  qdata_variables_t   w;
  memset (&w, 0, sizeof (qdata_variables_t));

  // get the coordinates of the center of the quad
  double  XYZ[3] = { 0, 0, 0 };
  double  x,y,z;
  UNUSED (z);

  // get the physical coordinates of the center of the quad
  quadrant_center_vertex (p4est->connectivity, geom, which_tree, quad, XYZ);

  x = XYZ[0];
  y = XYZ[1];
  z = XYZ[2];

  // initial front location (x position)
  double x_front = cfg->config_read_double ("shock_bubble.x_front", 1.0);

  // post shock
  double rho_post = cfg->config_read_double ("shock_bubble.post.rho", 3.81);
  double pressure_post = cfg->config_read_double ("shock_bubble.post.pressure", 10.0);
  double u_post = cfg->config_read_double ("shock_bubble.post.u", 2.85);
  double v_post = cfg->config_read_double ("shock_bubble.post.v", 0.0);
  double w_post = cfg->config_read_double ("shock_bubble.post.w", 0.0);
  UNUSED (w_post);

  // pre shock
  double rho_pre = cfg->config_read_double ("shock_bubble.pre.rho", 1.0);
  double pressure_pre = cfg->config_read_double ("shock_bubble.pre.pressure", 1.0);
  double u_pre = cfg->config_read_double ("shock_bubble.pre.u", 0.0);
  double v_pre = cfg->config_read_double ("shock_bubble.pre.v", 0.0);
  double w_pre = cfg->config_read_double ("shock_bubble.pre.w", 0.0);
  UNUSED (w_pre);

  // bubble
  double rho_bubble = cfg->config_read_double ("shock_bubble.bubble.rho", 0.1);
  double pressure_bubble = cfg->config_read_double ("shock_bubble.bubble.pressure", 1.0);
  double u_bubble = cfg->config_read_double ("shock_bubble.bubble.u", 0.0);
  double v_bubble = cfg->config_read_double ("shock_bubble.bubble.v", 0.0);
  double w_bubble = cfg->config_read_double ("shock_bubble.bubble.w", 0.0);
  UNUSED (w_bubble);

  double x_bubble = cfg->config_read_double ("shock_bubble.bubble.x", 0.4);
  double y_bubble = cfg->config_read_double ("shock_bubble.bubble.y", 0.0);
  double z_bubble = cfg->config_read_double ("shock_bubble.bubble.z", 0.0);
  UNUSED (z_bubble);


  double radius_bubble = cfg->config_read_double ("shock_bubble.bubble.radius", 0.1);
  radius_bubble *= radius_bubble;

  double r = (x-x_bubble)*(x-x_bubble) + (y-y_bubble)*(y-y_bubble);
#ifdef USE_3D
  r += (z-z_bubble)*(z-z_bubble);
#endif // USE_3D

  if (x < x_front) {
    // /////////////////////
    // set post-shock state
    // /////////////////////
    w.rho     = rho_post;
    w.rhoV[0] = rho_post*u_post;
    w.rhoV[1] = rho_post*v_post;
#ifdef USE_3D
    w.rhoV[2] = rho_post*w_post;
#endif // USE_3D

#ifdef USE_3D
    w.E_tot = pressure_post / (ramsesPar.gamma0-1.0) +
      0.5 * ( SC_SQR(w.rhoV[0]) +
	      SC_SQR(w.rhoV[1]) +
	      SC_SQR(w.rhoV[2]) ) / w.rho;
#else
    w.E_tot = pressure_post / (ramsesPar.gamma0-1.0) +
      0.5 * ( SC_SQR(w.rhoV[0]) +
	      SC_SQR(w.rhoV[1]) ) / w.rho;
#endif // USE_3D

  } else {

    if (r < radius_bubble) {

      // /////////////////////
      // set bubble state
      // /////////////////////
      w.rho     = rho_bubble;
      w.rhoV[0] = rho_bubble*u_bubble;
      w.rhoV[1] = rho_bubble*v_bubble;
#ifdef USE_3D
      w.rhoV[2] = rho_bubble*w_bubble;
#endif // USE_3D

#ifdef USE_3D
      w.E_tot = pressure_bubble / (ramsesPar.gamma0-1.0) +
	0.5 * ( SC_SQR(w.rhoV[0]) +
		SC_SQR(w.rhoV[1]) +
		SC_SQR(w.rhoV[2]) ) / w.rho;
#else
      w.E_tot = pressure_bubble / (ramsesPar.gamma0-1.0) +
	0.5 * ( SC_SQR(w.rhoV[0]) +
		SC_SQR(w.rhoV[1]) ) / w.rho;
#endif // USE_3D

    } else {

      // /////////////////////
      // set pre-shock state
      // /////////////////////
      w.rho     = rho_pre;
      w.rhoV[0] = rho_pre*u_pre;
      w.rhoV[1] = rho_pre*v_pre;
#ifdef USE_3D
      w.rhoV[2] = rho_pre*w_pre;
#endif // USE_3D

#ifdef USE_3D
      w.E_tot = pressure_pre / (ramsesPar.gamma0-1.0) +
	0.5 * ( SC_SQR(w.rhoV[0]) +
		SC_SQR(w.rhoV[1]) +
		SC_SQR(w.rhoV[2]) ) / w.rho;
#else
      w.E_tot = pressure_pre / (ramsesPar.gamma0-1.0) +
	0.5 * ( SC_SQR(w.rhoV[0]) +
		SC_SQR(w.rhoV[1]) ) / w.rho;
#endif // USE_3D

    } // end pre-shock

  }

  // finally set quadrant's userdata
  userdata_mgr->quadrant_set_variables (quad, &w);
  userdata_mgr->quadrant_print_qdata (SC_LP_INFO, quad);

} // init_ramses_shock_bubble

/**************************************************************************/
/* Ramses : wedge                                                         */
/**************************************************************************/
/**
 * Wedge (ramp) test
 *
 * See for example:
 * http://amroc.sourceforge.net/examples/euler/2d/html/ramp_n.htm
 */
void
init_ramses_wedge (p4est_t * p4est,
		   p4est_topidx_t which_tree,
		   p4est_quadrant_t * quad)
{

  SolverRamses *solver = static_cast<SolverRamses*>(solver_from_p4est (p4est));

  UserDataManagerRamses
    *userdata_mgr = static_cast<UserDataManagerRamses *> (solver->get_userdata_mgr());

  /* geometry (if NULL, it means cartesian by default) */
  p4est_geometry_t *geom = solver->get_geom_compute();

  /* get config reader */
  ConfigReader    *cfg = solver->m_cfg;
  UNUSED (cfg);

  RamsesPar &ramsesPar = solver->ramsesPar;

  /* now start genuine initialization */
  qdata_variables_t   w;
  memset (&w, 0, sizeof (qdata_variables_t));

  // get the coordinates of the center of the quad
  double  XYZ[3] = { 0, 0, 0 };
  double  x,y,z;
  UNUSED (z);

  // get the physical coordinates of the center of the quad
  quadrant_center_vertex (p4est->connectivity, geom, which_tree, quad, XYZ);

  x = XYZ[0];
  y = XYZ[1];
  z = XYZ[2];

  // initial front location (x position, angle)
  // angle is between x axis and front
  // equation of front at initial time:
  // y = slope_f*(x-x_f)
  double x_f     = cfg->config_read_double ("wedge.front_x", 0.1);
  double angle_f = cfg->config_read_double ("wedge.front_angle", M_PI/3.0);
  double slope_f = tan(angle_f);

  if ( y > slope_f*(x-x_f) ) {

    double rho1 = cfg->config_read_double ("wedge.rho1", 8.0);
    double p1   = cfg->config_read_double ("wedge.p1", 116.5);
    double u1   = cfg->config_read_double ("wedge.u1",  8.25*cos(angle_f-M_PI/2.0));
    double v1   = cfg->config_read_double ("wedge.v1",  8.25*sin(angle_f-M_PI/2.0));
    double w1   = cfg->config_read_double ("wedge.w1",  0.0);
    UNUSED (w1);

    w.rho     = rho1;
    w.rhoV[0] = rho1*u1;
    w.rhoV[1] = rho1*v1;
#ifdef USE_3D
    w.rhoV[2] = rho1*w1;
#endif

#ifdef USE_3D
    w.E_tot = p1 / (ramsesPar.gamma0-1.0) +
      0.5 * ( SC_SQR(w.rhoV[0]) +
	      SC_SQR(w.rhoV[1]) +
	      SC_SQR(w.rhoV[2]) ) / w.rho;
#else
    w.E_tot = p1 / (ramsesPar.gamma0-1.0) +
      0.5 * ( SC_SQR(w.rhoV[0]) +
	      SC_SQR(w.rhoV[1]) ) / w.rho;
#endif

  } else {

    double rho2 = cfg->config_read_double ("wedge.rho2", 1.4);
    double p2   = cfg->config_read_double ("wedge.p2",   1.0);
    double u2   = cfg->config_read_double ("wedge.u2",   0.0);
    double v2   = cfg->config_read_double ("wedge.v2",   0.0);
    double w2   = cfg->config_read_double ("wedge.w2",   0.0);
    UNUSED (w2);

    w.rho     = rho2;
    w.rhoV[0] = rho2*u2;
    w.rhoV[1] = rho2*v2;
#ifdef USE_3D
    w.rhoV[2] = rho2*w2;
#endif

#ifdef USE_3D
    w.E_tot = p2 / (ramsesPar.gamma0-1.0) +
      0.5 * ( SC_SQR(w.rhoV[0]) +
	      SC_SQR(w.rhoV[1]) +
	      SC_SQR(w.rhoV[2]) ) / w.rho;
#else
    w.E_tot = p2 / (ramsesPar.gamma0-1.0) +
      0.5 * ( SC_SQR(w.rhoV[0]) +
	      SC_SQR(w.rhoV[1]) ) / w.rho;
#endif

  }

  // finally set quadrant's userdata
  userdata_mgr->quadrant_set_variables (quad, &w);
  userdata_mgr->quadrant_print_qdata (SC_LP_INFO, quad);

} // init_ramses_wedge

/**************************************************************************/
/* Ramses : forward_facing_step                                           */
/**************************************************************************/
/**
 * Forward Facing step test.
 *
 * See for example:
 * http://amroc.sourceforge.net/examples/euler/2d/html/ffstep_n.htm
 */
void
init_ramses_forward_facing_step (p4est_t * p4est,
				 p4est_topidx_t which_tree,
				 p4est_quadrant_t * quad)
{

  SolverRamses *solver = static_cast<SolverRamses*>(solver_from_p4est (p4est));

  UserDataManagerRamses
    *userdata_mgr = static_cast<UserDataManagerRamses *> (solver->get_userdata_mgr());

  /* geometry (if NULL, it means cartesian by default) */
  p4est_geometry_t *geom = solver->get_geom_compute();

  /* get config reader */
  ConfigReader    *cfg = solver->m_cfg;
  UNUSED (cfg);

  RamsesPar &ramsesPar = solver->ramsesPar;

  /* now start genuine initialization */
  qdata_variables_t   w;
  memset (&w, 0, sizeof (qdata_variables_t));

  // get the coordinates of the center of the quad
  double  XYZ[3] = { 0, 0, 0 };
  double  x,y,z;
  UNUSED (x);
  UNUSED (y);
  UNUSED (z);

  // get the physical coordinates of the center of the quad
  quadrant_center_vertex (p4est->connectivity, geom, which_tree, quad, XYZ);

  x = XYZ[0];
  y = XYZ[1];
  z = XYZ[2];

  double rho  = cfg->config_read_double ("border_inflow.rho", 1.4);
  double P    = cfg->config_read_double ("border_inflow.pressure", 1.0);
  double vx   = cfg->config_read_double ("border_inflow.u", 3.0);
  double vy   = cfg->config_read_double ("border_inflow.v", 0.0);
  double vz   = cfg->config_read_double ("border_inflow.w", 0.0);
  UNUSED (vz);


  w.rho     = rho;
  w.rhoV[0] = rho*vx;
  w.rhoV[1] = rho*vy;
#ifdef USE_3D
  w.rhoV[2] = rho*vz;
#endif

#ifdef USE_3D
  w.E_tot = P / (ramsesPar.gamma0-1.0) +
    0.5 * ( SC_SQR(w.rhoV[0]) +
	    SC_SQR(w.rhoV[1]) +
	    SC_SQR(w.rhoV[2]) ) / w.rho;
#else
  w.E_tot = P / (ramsesPar.gamma0-1.0) +
    0.5 * ( SC_SQR(w.rhoV[0]) +
	    SC_SQR(w.rhoV[1]) ) / w.rho;
#endif

  // finally set quadrant's userdata
  userdata_mgr->quadrant_set_variables (quad, &w);
  userdata_mgr->quadrant_print_qdata (SC_LP_INFO, quad);

} // init_ramses_forward_facing_step

/**************************************************************************/
/* Ramses : backward_facing_step                                           */
/**************************************************************************/
/**
 * Backward Facing step test.
 *
 * See for example:
 * http://amroc.sourceforge.net/examples/euler/2d/html/bfstep_n.htm
 *
 */
void
init_ramses_backward_facing_step (p4est_t * p4est,
				  p4est_topidx_t which_tree,
				  p4est_quadrant_t * quad)
{

  SolverRamses *solver = static_cast<SolverRamses*>(solver_from_p4est (p4est));

  UserDataManagerRamses
    *userdata_mgr = static_cast<UserDataManagerRamses *> (solver->get_userdata_mgr());

  /* geometry (if NULL, it means cartesian by default) */
  p4est_geometry_t *geom = solver->get_geom_compute();

  /* get config reader */
  ConfigReader    *cfg = solver->m_cfg;
  UNUSED (cfg);

  RamsesPar &ramsesPar = solver->ramsesPar;

  /* now start genuine initialization */
  qdata_variables_t   w;
  memset (&w, 0, sizeof (qdata_variables_t));

  // get the coordinates of the center of the quad
  double  XYZ[3] = { 0, 0, 0 };
  double  x,y,z;
  UNUSED (x);
  UNUSED (y);
  UNUSED (z);

  // get the physical coordinates of the center of the quad
  quadrant_center_vertex (p4est->connectivity, geom, which_tree, quad, XYZ);

  x = XYZ[0];
  y = XYZ[1];
  z = XYZ[2];

  double rho  = cfg->config_read_double ("init_state.rho", 1.0);
  double P    = cfg->config_read_double ("init_state.pressure", 1.0);
  double vx   = cfg->config_read_double ("init_state.u", 0.0);
  double vy   = cfg->config_read_double ("init_state.v", 0.0);
  double vz   = cfg->config_read_double ("init_state.w", 0.0);
  UNUSED (vz);


  w.rho     = rho;
  w.rhoV[0] = rho*vx;
  w.rhoV[1] = rho*vy;
#ifdef USE_3D
  w.rhoV[2] = rho*vz;
#endif

#ifdef USE_3D
  w.E_tot = P / (ramsesPar.gamma0-1.0) +
    0.5 * ( SC_SQR(w.rhoV[0]) +
	    SC_SQR(w.rhoV[1]) +
	    SC_SQR(w.rhoV[2]) ) / w.rho;
#else
  w.E_tot = P / (ramsesPar.gamma0-1.0) +
    0.5 * ( SC_SQR(w.rhoV[0]) +
	    SC_SQR(w.rhoV[1]) ) / w.rho;
#endif

  // finally set quadrant's userdata
  userdata_mgr->quadrant_set_variables (quad, &w);
  userdata_mgr->quadrant_print_qdata (SC_LP_INFO, quad);

} // init_ramses_backward_facing_step

/**************************************************************************/
/* Ramses : Rayleigh-Taylor                                               */
/* here we assume that 0<x<3; heavy fluid is above light fluid            */
/**************************************************************************/
/**
 *
 * \brief Rayleigh-Taylor instability.
 *
 * See http://www.astro.princeton.edu/~jstone/Athena/tests/rt/rt.html
 * for a description of such initial conditions
 *
 */
void
init_ramses_rayleigh_taylor (p4est_t * p4est,
			     p4est_topidx_t which_tree,
			     p4est_quadrant_t * quad)
{

  SolverRamses *solver = static_cast<SolverRamses*>(solver_from_p4est (p4est));

  UserDataManagerRamses
    *userdata_mgr = static_cast<UserDataManagerRamses *> (solver->get_userdata_mgr());

  /* get config reader */
  ConfigReader    *cfg = solver->m_cfg;

  RamsesPar &ramsesPar = solver->ramsesPar;

  /* geometry (if NULL, it means cartesian by default) */
  p4est_geometry_t *geom = solver->get_geom_compute();

  /* now start genuine initialization */
  qdata_variables_t   w;
  memset (&w, 0, sizeof (qdata_variables_t));

  // get the coordinates of the center of the quad
  double  XYZ[3] = { 0, 0, 0 };
  double  x,y,z;
  UNUSED (z);

  // get the physical coordinates of the center of the quad
  quadrant_center_vertex (p4est->connectivity, geom, which_tree, quad, XYZ);

  x = XYZ[0];
  y = XYZ[1];
  z = XYZ[2];

  // density
  double d0 = cfg->config_read_double ("rayleigh_taylor.d0", 1.0);
  double d1 = cfg->config_read_double ("rayleigh_taylor.d1", 2.0);

  // static gravity field
  double grav_x = ramsesPar.gravity.static_x;
  double grav_y = ramsesPar.gravity.static_y;
  double grav_z = ramsesPar.gravity.static_z;
  UNUSED (grav_z);

  // pressure
  double pressure = cfg->config_read_double ("rayleigh_taylor.pressure", 1.0);

  // type of perturbation
  int randomEnabled = cfg->config_read_int    ("rayleigh_taylor.randomEnabled", 0);
  double amplitude =  cfg->config_read_double ("rayleigh_taylor.amplitude",     0.01);
  double mode =       cfg->config_read_int    ("rayleigh_taylor.perturb_mode",   1);
  double width =      cfg->config_read_double ("rayleigh_taylor.perturb_width", 0.05);

  // random number generator (only used if randomEnabled is different of 0)
  int seed = cfg->config_read_int ("rayleigh_taylor.rand_seed", 12);
  srand(seed* (p4est->mpirank+1));

  double Lx = 3.0;
  double Ly = 1.0;
  double Lz = 1.0;
  UNUSED (Lx);
  UNUSED (Ly);
  UNUSED (Lz);

  // interface location
  double x0 = cfg->config_read_double ("rayleigh_taylor.x0", 1.5);
  double y0 = cfg->config_read_double ("rayleigh_taylor.y0", 0.5);
  double z0 = cfg->config_read_double ("rayleigh_taylor.z0", 0.5);
  UNUSED (z0);

#ifdef USE_3D

  if (x<x0) { // light fluid

    w.rho     = d0;

  } else { // heavy fluid

    w.rho     = d1;

  }

  if (randomEnabled) {
    w.rhoV[0] = w.rho * amplitude * (1.0*rand()/RAND_MAX-0.5);
  } else {
    /*w.rhoV[0] = w.rho * amplitude *
      (1+cos(4.5*M_PI*(x-x0)/Lx))*
      (1+cos(2.0*M_PI*(y-y0)/Ly))*
      (1+cos(2.0*M_PI*(z-z0)/Lz))/4;*/
    w.rhoV[0] = amplitude *
      cos(mode*2*M_PI*(y-y0)/Ly) *
      cos(mode*2*M_PI*(z-z0)/Lz) *
      exp(-(x-x0)*(x-x0)/2/width/width);

  }
  w.rhoV[1] = 0.0;
  w.rhoV[2] = 0.0;
  w.E_tot   = pressure / (ramsesPar.gamma0-1.0) +
    w.rho*(grav_x*(x-x0) + grav_y*(y-y0) + grav_z*(z-z0)) / (ramsesPar.gamma0-1.0) +
    0.5 * ( SC_SQR(w.rhoV[0]) +
	    SC_SQR(w.rhoV[1]) +
	    SC_SQR(w.rhoV[2]) ) / w.rho;

#else // 2D

  if (x<x0) { // light fluid

    w.rho     = d0;

  } else { // heavy fluid

    w.rho     = d1;

  }

  if (randomEnabled) {
    w.rhoV[0] = w.rho * amplitude * (1.0*rand()/RAND_MAX-0.5);
  } else {
    /*w.rhoV[0] = w.rho * amplitude *
      (1+cos(4.5*M_PI*(x-x0)/Lx))*
      (1+cos(2.0*M_PI*(y-y0)/Ly))/4;*/
    w.rhoV[0] = amplitude * cos(mode*2*M_PI*(y-y0)/Ly) * exp(-(x-x0)*(x-x0)/2/width/width);

  }
  w.rhoV[1] = 0.0;
  w.E_tot   = pressure / (ramsesPar.gamma0-1.0) +
    w.rho*(grav_x*(x-x0) + grav_y*(y-y0)) / (ramsesPar.gamma0-1.0) +
    0.5 * ( SC_SQR(w.rhoV[0]) +
	    SC_SQR(w.rhoV[1])) / w.rho;


#endif // 2D/3D

  // finally set quadrant's userdata
  userdata_mgr->quadrant_set_variables (quad, &w);
  userdata_mgr->quadrant_print_qdata (SC_LP_INFO, quad);

} // init_ramses_rayleigh_taylor

/**************************************************************************/
/* Ramses : isentropic vortex                                             */
/**************************************************************************/
/**
 *
 * \brief isentropic vortex advection test
 *
 * See  
 * https://www.cfd-online.com/Wiki/2-D_vortex_in_isentropic_flow
 * https://hal.archives-ouvertes.fr/hal-01485587/document
 *
 */
void
init_ramses_isentropic_vortex (p4est_t * p4est,
			       p4est_topidx_t which_tree, 
			       p4est_quadrant_t * quad)
{

  SolverRamses *solver = static_cast<SolverRamses*>(solver_from_p4est (p4est));

  UserDataManagerRamses
    *userdata_mgr = static_cast<UserDataManagerRamses *> (solver->get_userdata_mgr());

  /* geometry (if NULL, it means cartesian by default) */
  p4est_geometry_t   *geom        = solver->get_geom_compute();

  /* get config reader */
  ConfigReader    *cfg = solver->m_cfg;

  RamsesPar &ramsesPar = solver->ramsesPar;
  
  /* now start genuine initialization */
  qdata_variables_t   w;
  memset (&w, 0, sizeof (qdata_variables_t));
  
  // get the coordinates of the center of the quad
  double  XYZ[3] = { 0, 0, 0 };
  double  x,y,z;
  UNUSED (z);

  // get the physical coordinates of the center of the quad
  quadrant_center_vertex (p4est->connectivity, geom, which_tree, quad, XYZ);

  // x,y,z should be in 0 - 1 (uint connectivity)
  x = XYZ[0];
  y = XYZ[1];
  z = XYZ[2];

  // geometry
  const double xmin = cfg->config_read_double ("isentropic_vortex.xmin", -5.0);
  const double ymin = cfg->config_read_double ("isentropic_vortex.ymin", -5.0);
  const double zmin = cfg->config_read_double ("isentropic_vortex.zmin", -5.0);
  const double xmax = cfg->config_read_double ("isentropic_vortex.xmax",  5.0);
  const double ymax = cfg->config_read_double ("isentropic_vortex.ymax",  5.0);
  const double zmax = cfg->config_read_double ("isentropic_vortex.zmax",  5.0);

  // rescale coordinates
  x = (xmax-xmin)*x + xmin;
  y = (ymax-ymin)*y + ymin;
  z = (zmax-zmin)*z + zmin;

  // ambient flow
  const double rho_a = cfg->config_read_double ("isentropic_vortex.density_ambient",1.0);
  const double T_a   = cfg->config_read_double ("isentropic_vortex.temperature_ambient",1.0);
  const double u_a   = cfg->config_read_double ("isentropic_vortex.vx_ambient",1.0);
  const double v_a   = cfg->config_read_double ("isentropic_vortex.vy_ambient",1.0);
  const double w_a   = cfg->config_read_double ("isentropic_vortex.vz_ambient",1.0);
  
  // vortex center
  const double vortex_x = cfg->config_read_double ("isentropic_vortex.center_x",0.0);
  const double vortex_y = cfg->config_read_double ("isentropic_vortex.center_y",0.0);
  const double vortex_z = cfg->config_read_double ("isentropic_vortex.center_z",0.0);

  const double beta = cfg->config_read_double ("isentropic_vortex.strength", 5.0);

  // relative coordinates versus vortex center
  const double xp = x - vortex_x;
  const double yp = y - vortex_y;
  const double r  = sqrt(xp*xp + yp*yp);

  const double du = - yp * beta / (2 * M_PI) * exp(0.5*(1.0-r*r));
  const double dv =   xp * beta / (2 * M_PI) * exp(0.5*(1.0-r*r));
  const double dw =   0.0;
  
  const double T = T_a - (ramsesPar.gamma0-1)*beta*beta/(8*ramsesPar.gamma0*M_PI*M_PI)*exp(1.0-r*r);
  const double rho = rho_a*pow(T/T_a,1.0/(ramsesPar.gamma0-1));

  w.rho = rho;
  w.rhoV[0] = w.rho*(u_a+du);
  w.rhoV[1] = w.rho*(v_a+dv);
#ifdef USE_3D
  w.rhoV[2] = w.rho*(w_a+dw);
#endif
  
#ifdef USE_3D
  w.E_tot = rho*T / (ramsesPar.gamma0-1.0) + 
    0.5 * ( SC_SQR(w.rhoV[0]) + 
	    SC_SQR(w.rhoV[1]) + 
	    SC_SQR(w.rhoV[2]) ) / w.rho;
#else
  w.E_tot = rho*T / (ramsesPar.gamma0-1.0) +
    0.5 * ( SC_SQR(w.rhoV[0]) + 
	    SC_SQR(w.rhoV[1]) ) / w.rho;
#endif
  
  // finally set quadrant's userdata
  userdata_mgr->quadrant_set_variables (quad, &w);
  userdata_mgr->quadrant_print_qdata (SC_LP_INFO, quad);
  
} // init_ramses_isentropic_vortex

/**************************************************************************/
/* Ramses : Jeans                                                         */
/**************************************************************************/
/**
 *
 * \brief Jeans instability.
 *
 * The initial solution for rho is a sphere which center is (0.5, 0.5, 0.5) and
 * radius jeans.length / 2. The code units are chosen such that 4pi G = 1
 * Initial velocity is zero.
 */
void
init_ramses_jeans (p4est_t * p4est,
		   p4est_topidx_t which_tree,
		   p4est_quadrant_t * quad)
{

  SolverRamses *solver = static_cast<SolverRamses*>(solver_from_p4est (p4est));

  UserDataManagerRamses
    *userdata_mgr = static_cast<UserDataManagerRamses *> (solver->get_userdata_mgr());

  /* geometry (if NULL, it means cartesian by default) */
  p4est_geometry_t   *geom        = solver->get_geom_compute();

  /* get config reader */
  ConfigReader    *cfg = solver->m_cfg;

  RamsesPar &ramsesPar = solver->ramsesPar;

  /* now start genuine initialization */
  qdata_variables_t   w;
  memset (&w, 0, sizeof (qdata_variables_t));

  // get the coordinates of the center of the quad
  double  XYZ[3] = { 0, 0, 0 };
  double  x,y,z;
  UNUSED (z);

  // get the physical coordinates of the center of the quad
  quadrant_center_vertex (p4est->connectivity, geom, which_tree, quad, XYZ);

  x = XYZ[0];
  y = XYZ[1];
  z = XYZ[2];

  // shpere center, radius
  double x_c = cfg->config_read_double ("jeans.centerx", 0.5);
  double y_c = cfg->config_read_double ("jeans.centery", 0.5);
#ifdef USE_3D
  double z_c = cfg->config_read_double ("jeans.centerz", 0.5);
#endif

  double ljeans = cfg->config_read_double ("jeans.length", 0.5);
  double radius = cfg->config_read_double ("jeans.radius", 0.5 * ljeans);
  //double radius = 0.5 * ljeans;
  double radius2 = radius*radius;

  // density
  double density_in  = cfg->config_read_double ("jeans.d_in",  1.0);
  double density_out = cfg->config_read_double ("jeans.d_out", 0.1);

  // pressure
  double pressure_in  = SC_SQR(density_in  * ljeans / (2 * M_PI)) / ramsesPar.gamma0;
  double pressure_out = pressure_in * density_out / density_in;

  double d2 = SC_SQR(x - x_c)
            + SC_SQR(y - y_c)
#ifdef USE_3D
            + SC_SQR(z - z_c)
#endif
            ;

  if (d2 < radius2) {
    w.rho = density_in;
    w.E_tot = pressure_in  / (ramsesPar.gamma0-1.0);
  } else {
    w.rho = density_out;
    w.E_tot = pressure_out / (ramsesPar.gamma0-1.0);
  }

  w.rhoV[0] = 0.0;
  w.rhoV[1] = 0.0;
#ifdef USE_3D
  w.rhoV[2] = 0.0;
#endif

  // finally set quadrant's userdata
  userdata_mgr->quadrant_set_variables (quad, &w);
  userdata_mgr->quadrant_print_qdata (SC_LP_INFO, quad);

} // init_ramses_jeans


// =======================================================
// =======CLASS InitialConditionsFactoryRamses IMPL ======
// =======================================================
InitialConditionsFactoryRamses::InitialConditionsFactoryRamses() :
  InitialConditionsFactory()
{

  // register the above callback's
  registerIC("blast" ,	              init_ramses_blast);
  registerIC("jet" ,	              init_ramses_jet);
  registerIC("kelvin_helmholtz" ,     init_ramses_kelvin_helmholtz);
  registerIC("piecewise_constant_quadrant2d" , init_ramses_piecewise_constant_quadrant2d);
  registerIC("implode" ,              init_ramses_implode);
  registerIC("shock_bubble" ,         init_ramses_shock_bubble);
  registerIC("wedge" ,                init_ramses_wedge);
  registerIC("rayleigh_taylor" ,      init_ramses_rayleigh_taylor);
  registerIC("forward_facing_step" ,  init_ramses_forward_facing_step);
  registerIC("backward_facing_step" , init_ramses_backward_facing_step);
  registerIC("isentropic_vortex" ,    init_ramses_isentropic_vortex);
  registerIC("jeans" ,                init_ramses_jeans);

} // InitialConditionsFactoryRamses

} // namespace ramses

} // namespace canop
