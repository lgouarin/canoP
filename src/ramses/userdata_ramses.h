#ifndef USERDATA_RAMSES_H_
#define USERDATA_RAMSES_H_

#ifdef USE_3D
#include <p4est_to_p8est.h>
#endif

#ifndef P4_TO_P8
#include <p4est.h>
#else
#include <p8est.h>
#endif

namespace canop {

namespace ramses {

enum GeomDir {
  IX = 0,
  IY = 1,
  IZ = 2
};

//! hydro/MHD field indexes
enum ComponentIndex {
  ID=0,  /*!< ID Density field index */
  IP=1,  /*!< IP Pressure field index (primitive variables) */
  IE=1,  /*!< IE Energy field index (conservative variables) */
  IU=2,  /*!< X velocity / momentum index */
  IV=3,  /*!< Y velocity / momentum index */
  IW=4,  /*!< Z velocity / momentum index */
  IBX=5, /*!< X component of magnetic field */
  IBY=6, /*!< Y component of magnetic field */
  IBZ=7, /*!< Z component of magnetic field */
  IA=5,  /*!< X component of magnetic field */
  IB=6,  /*!< Y component of magnetic field */
  IC=7   /*!< Z component of magnetic field */
};

#if defined(USE_RAMSES_MHD)
/* rho + E_total + rho*velocity + Bx,By,Bz left + Bx,By,Bz right */
constexpr int QDATA_NUM_VARIABLES = 2 + P4EST_DIM + 3 + 3;
#else // USE_RAMSES_HYDRO
/* rho + E_total + rho*velocity */
constexpr int QDATA_NUM_VARIABLES = 2 + P4EST_DIM;
#endif

/**
 * Some parameters specific to Ramses scheme in cylindrical coordinates.
 *
 * More precisely, these parameters are used to initialize the p4est connectivity.
 */
struct RamsesCylPar
{

  /*
   * cylindrical scheme :
   * - one tree along radial direction
   * - number of tree along theta and z is configurable
   */
  int nbTrees_theta; //!< number of tree along theta (connectivity)
  int nbTrees_z;     //!< number of tree along z     (connectivity)
  double rMin, rMax; //!< lowest radius, highest radius
  double Lr;         //!< physical
  double Ltheta;
  double Lz;
  int zPeriodic;

}; // struct RamsesCylPar


/**
 * Boundary conditions for the Poisson solver
 */
enum class RamsesGravBC
{
  Neumann,
  Periodic,
  Monopole,
};

/**
 * Gravity-specific parameters.
 */
struct RamsesGravPar
{
  int    enabled;                     //!< non-zero iff any of the *_enabled is non-zero

  int    static_enabled;              //!< enable uniform gravity field
  double static_x;                    //!< uniform gravity x component
  double static_y;                    //!< uniform gravity y component
  double static_z;                    //!< uniform gravity z component

  int    point_enabled;               //!< enable point gravity source
  double point_Gmass;                 //!< G * mass of the gravity source
  double point_pos_x;                 //!< x position of the source
  double point_pos_y;                 //!< y position of the source
  double point_pos_z;                 //!< z position of the source
  double point_soften;                //!< softening length to prevent divergence of the potential near the source

  int    poisson_enabled;             //!< enable self-gravity
  int    poisson_imax;                //!< maximum number of iterations for the Poisson solver
  double poisson_tol;                 //!< target tolerance for the Poisson solver
  int    poisson_reuse_potential;     //!< reuse the previous potential as an initial guess
  RamsesGravBC poisson_boundary;      //!< boundary conditions

};

/**
 * Ramses scheme specific parameters.
 */
struct  RamsesPar
{

  double gamma0;
  double smallp;
  double smallc;
  double smallr;
  double cIso;

  RamsesGravPar gravity;

  int cartesian_enabled;
  int cylindrical_enabled;
  int unstructured_enabled;
  RamsesCylPar cylindrical;

}; // struct RamsesPar

/**
 * \brief Vector of conserved variables.
 *
 *  We have
 *      rho is density
 *      rhoV = (u * rho, v * rho, w * rho)
 *      E_tot
 *      BxL, ByL, BzL : magnetic field on left faces
 *      BxR, ByR, BzR : magnetic field on right faces
 */
struct QdataVar
{
  double              rho;
  double              rhoV[P4EST_DIM];
  double              E_tot;

  inline
  QdataVar () : rho(-1.0), E_tot(0.0) {
    for (int i = 0; i<P4EST_DIM; ++i)
      rhoV[i] = 0.0;
  }
}; // struct QdataVar

#ifndef P4_TO_P8
inline void QDATA_INFOF(const QdataVar& w) {
  printf("rho %g | E_tot %g | rho_u %g | rho_v %g\n",
	 w.rho, w.E_tot, w.rhoV[0], w.rhoV[1]);
}
#else /* 3D */
inline void QDATA_INFOF(const QdataVar& w) {
  printf("rho %g | E_tot %g | rho_u %g | rho_v %g | rho_w %g\n",
	 w.rho, w.E_tot, w.rhoV[0], w.rhoV[1], w.rhoV[2]);
}
#endif /* P4_TO_P8 */

/**
 * Reconstructed variables (trace); primitive variables.
 */
struct QdataRecons
{

  double              rho;                 // density
  double              velocity[P4EST_DIM]; // velocity field
  double              P;                   // local pressure
  double              c;                   // local speed of sound

  inline
  QdataRecons () : rho(-1), P(0.0), c(0.0) {
    for (int i = 0; i<P4EST_DIM; i++)
      velocity[i] = 0.0;
  }

}; // struct QdataRecons

/**
 * Gravity solver variables
 */
struct QdataGrav
{
  double              phi; // Potential
  double              accel[P4EST_DIM]; // Grav. acceleration (- grad phi)

  inline
  QdataGrav () : phi(0.0) {
    for (int i = 0; i < P4EST_DIM; ++i)
      accel[i] = 0.0;
  }

}; // struct QdataGrav

/**
 * This is the main data structure (one by quadrant/cell).
 */
struct Qdata
{
  QdataVar   w;        /*!< variable at the current time */
  QdataVar   wnext;    /*!< variable at t + dt */

  QdataGrav  wgrav;    /*!< gravity solver variables */

  QdataRecons delta[P4EST_DIM];    /*!< reconstructed gradients / slopes, one per direction. */

  QdataRecons wm[P4EST_DIM];  /*!< trace state at left  cell face */
  QdataRecons wp[P4EST_DIM];  /*!< trace state at right cell face */

}; // struct Qdata

} // namespace ramses

} // namespace canop

#endif // USERDATA_RAMSES_H_
