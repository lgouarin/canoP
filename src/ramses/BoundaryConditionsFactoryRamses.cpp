#include "BoundaryConditionsFactoryRamses.h"

#include "SolverRamses.h"
#include "UserDataManagerRamses.h"
#include "quadrant_utils.h"

// remember that qdata_t is here a concrete type defined in userdata_ramses.h

#include "ramses/userdata_ramses.h"
#include "ConfigReader.h"

namespace canop {

namespace ramses {

using qdata_t               = Qdata; 
using qdata_reconstructed_t = QdataRecons;

// =======================================================
// ======BOUNDARY CONDITIONS CALLBACKS====================
// =======================================================


// =======================================================
// =======================================================
/*
 * TODO: maybe just make the boundary functions return a qdata_variables_t
 * that they have initialized and then we do the copying around.
 *
 * TODO: for order 2, how does delta have to be initialized? does it?
 */
void
bc_ramses_dirichlet (p4est_t * p4est,
		     p4est_topidx_t which_tree,
		     p4est_quadrant_t * q,
		     int face,
		     qdata_t * ghost_qdata)
{
  
  UNUSED (p4est);
  UNUSED (which_tree);
  UNUSED (q);
  UNUSED (face);
  UNUSED (ghost_qdata);
  
} // bc_ramses_dirichlet

// =======================================================
// =======================================================
void
bc_ramses_neuman (p4est_t * p4est,
		  p4est_topidx_t which_tree,
		  p4est_quadrant_t * q,
		  int face,
		  qdata_t * ghost_qdata)
{
  UNUSED (p4est);
  UNUSED (which_tree);
  UNUSED (face);

  // retrieve our Solver, UserDataManager and finaly our quadrant data
  SolverRamses    *solver = static_cast<SolverRamses*>(solver_from_p4est (p4est));

  UserDataManagerRamses
    *userdata_mgr = static_cast<UserDataManagerRamses *> (solver->get_userdata_mgr());

  qdata_t            *qdata = userdata_mgr->quadrant_get_qdata (q);

  
  /* config parameter handle */
  //RamsesPar       &ramsesPar = solver->ramsesPar;
  
  /* only copy the state variables */
  //qdata_set_variables (data, &(qdata->w));

  /* 
   * copy the entire qdata is necessary for ramses scheme
   * so that we have the slope and MUSCL-Hancock extrapoled states
   * and can perform the trace operation
   */
  userdata_mgr->qdata_copy(ghost_qdata, qdata);

  /* wp / wm should also be initialized */
  {
    qdata_reconstructed_t w_prim;

    /* get the primitive variables in the current quadrant */
    userdata_mgr->reconstruct_variables (ghost_qdata->w, w_prim);

    ghost_qdata->wm[IX] = w_prim;
    ghost_qdata->wp[IX] = w_prim;
    ghost_qdata->wm[IY] = w_prim;
    ghost_qdata->wp[IY] = w_prim;
#ifdef USE_3D
    ghost_qdata->wm[IZ] = w_prim;
    ghost_qdata->wp[IZ] = w_prim;
#endif
  }

  /* set slopes to zero */
  userdata_mgr->qdata_reconstructed_zero(&(ghost_qdata->delta[IX]));
  userdata_mgr->qdata_reconstructed_zero(&(ghost_qdata->delta[IY]));
#ifdef USE_3D
  userdata_mgr->qdata_reconstructed_zero(&(ghost_qdata->delta[IZ]));
#endif // USE_3D

} // bc_ramses_neuman

// =======================================================
// =======================================================
void
bc_ramses_reflective (p4est_t * p4est,
		      p4est_topidx_t which_tree,
		      p4est_quadrant_t * q,
		      int face,
		      qdata_t * ghost_qdata)
{
  UNUSED (p4est);
  UNUSED (which_tree);

  /* get face normal direction */
  int                 direction = face / 2;

  /* get current cell data */
  // retrieve our Solver, UserDataManager and finaly our quadrant data
  SolverRamses    *solver = static_cast<SolverRamses*>(solver_from_p4est (p4est));
  UserDataManagerRamses
    *userdata_mgr = static_cast<UserDataManagerRamses *> (solver->get_userdata_mgr());
  qdata_t            *qdata = userdata_mgr->quadrant_get_qdata (q);

  /* copy current into the ghost outside-boundary cell */
  userdata_mgr->qdata_copy(ghost_qdata, qdata);

  /* reverse the normal velocity */
  ghost_qdata->w.rhoV[direction]     = -ghost_qdata->w.rhoV[direction];
  ghost_qdata->wnext.rhoV[direction] = -ghost_qdata->wnext.rhoV[direction];

  // slopes in normal direction change sign
  userdata_mgr->qdata_reconstructed_change_sign( &(ghost_qdata->delta[direction]) );

  // swap wm and wp in normal direction:
  // so that qm on the right side is the same as qp on the left side
  userdata_mgr->qdata_reconstructed_swap( &(ghost_qdata->wp[direction]), &(ghost_qdata->wm[direction]) );
  ghost_qdata->wp[direction].velocity[direction] = -ghost_qdata->wp[direction].velocity[direction]; 
  ghost_qdata->wm[direction].velocity[direction] = -ghost_qdata->wm[direction].velocity[direction]; 

} // bc_ramses_reflexive

// =======================================================
// =======================================================
void
bc_ramses_hse (p4est_t * p4est,
	       p4est_topidx_t which_tree,
	       p4est_quadrant_t * q,
	       int face,
	       qdata_t * ghost_qdata)
{
  UNUSED (p4est);
  UNUSED (which_tree);
  UNUSED (face);

  /* config parameter handle */
  SolverRamses         *solver = static_cast<SolverRamses*>(solver_from_p4est (p4est));
  UserDataManagerRamses
    *userdata_mgr = static_cast<UserDataManagerRamses *> (solver->get_userdata_mgr());

  ConfigReader         *cfg  = solver->m_cfg;
  p4est_connectivity_t *conn = p4est->connectivity;
  UNUSED(cfg);
  UNUSED(conn);

  RamsesPar &ramsesPar = solver->ramsesPar;

  /* get current cell data */
  qdata_t            *qdata = userdata_mgr->quadrant_get_qdata (q);

  /* copy current into the ghost outside-boundary cell */
  userdata_mgr->qdata_copy(ghost_qdata, qdata);

  /* set slopes to zero */
  userdata_mgr->qdata_reconstructed_zero(&(ghost_qdata->delta[IX]));
  userdata_mgr->qdata_reconstructed_zero(&(ghost_qdata->delta[IY]));
#ifdef USE_3D
  userdata_mgr->qdata_reconstructed_zero(&(ghost_qdata->delta[IZ]));
#endif // USE_3D

  /* */
  double rho_base = qdata->w.rho;
  double rhovx = qdata->w.rhoV[IX];
  double rhovy = qdata->w.rhoV[IY];

  // kinetic energy
  double ekin_base = 0.5 * ( rhovx*rhovx + rhovy*rhovy );
#ifdef USE_3D
  double rhovz = qdata->w.rhoV[IZ];
  ekin_base += 0.5 * ( rhovz*rhovz );
#endif
  ekin_base /= rho_base;

  // internal energy
  double eint_base = (qdata->w.E_tot - ekin_base)/rho_base;

  // retrieve pressure
  double pressure_base;
  //double c;
  //eos(rho_base, eint_base, &pressure_base, &c, ramsesPar.gamma0, ramsesPar.smallp);
  pressure_base = (ramsesPar.gamma0-1.0) * rho_base * eint_base;

  // assume constant density, compute pressure in ghost cell
  // static gravity field
  double grav_x = ramsesPar.gravity.static_x;
  double grav_y = ramsesPar.gravity.static_y;
  double grav_z = ramsesPar.gravity.static_z;
  UNUSED (grav_z);

  double dx, dy, dz;
  quadrant_get_dx_dy_dz(conn, NULL, q, which_tree, &dx, &dy, &dz);
  UNUSED (dz);

  double deltaPressure;
  // delta Pressure = rho * g * delta_z
  if (       face == 0) {
    deltaPressure =  -rho_base * grav_x * dx;
  } else if (face == 1) {
    deltaPressure =   rho_base * grav_x * dx;
  } else if (face == 2) {
    deltaPressure =  -rho_base * grav_y * dy;
  } else if (face == 3) {
    deltaPressure =   rho_base * grav_y * dy;
  } else if (face == 4) {
    deltaPressure =  -rho_base * grav_z * dz;
  } else if (face == 5) {
    deltaPressure =   rho_base * grav_z * dz;
  }
  double pressure_ghost = pressure_base + deltaPressure;
  double energy_ghost = pressure_ghost/(ramsesPar.gamma0-1);

  ghost_qdata->w.E_tot     = energy_ghost + ekin_base;
  ghost_qdata->wnext.E_tot = energy_ghost + ekin_base;

  /* wp / wm should also be initialized */
  {
    qdata_reconstructed_t w_prim;

    /* get the primitive variables in the current quadrant */
    userdata_mgr->reconstruct_variables (ghost_qdata->w, w_prim);
    
    ghost_qdata->wm[IX] = w_prim;
    ghost_qdata->wp[IX] = w_prim;
    ghost_qdata->wm[IY] = w_prim;
    ghost_qdata->wp[IY] = w_prim;
#ifdef USE_3D
    ghost_qdata->wm[IZ] = w_prim;
    ghost_qdata->wp[IZ] = w_prim;
#endif
  }

} // bc_ramses_hse

// =======================================================
// =======================================================
void
bc_ramses_open_tube (p4est_t * p4est,
		     p4est_topidx_t which_tree,
		     p4est_quadrant_t * q,
		     int face,
		     qdata_t * ghost_qdata)
{
  UNUSED (p4est);
  UNUSED (which_tree);

  if (face == 0 || face == 1) {
    bc_ramses_neuman (p4est, which_tree, q, face, ghost_qdata);
  }
  else {
    bc_ramses_reflective (p4est, which_tree, q, face, ghost_qdata);
  }
} // bc_ramses_open_tube

// =======================================================
// =======================================================
void
bc_ramses_open_box (p4est_t * p4est,
		    p4est_topidx_t which_tree,
		    p4est_quadrant_t * q,
		    int face,
		    qdata_t * ghost_qdata)
{
  UNUSED (p4est);
  UNUSED (which_tree);

  if (face == 3) {
    bc_ramses_neuman (p4est, which_tree, q, face, ghost_qdata);
  }
  else {
    bc_ramses_reflective (p4est, which_tree, q, face, ghost_qdata);
  }
} // bc_ramses_open_box

// =======================================================
// =======================================================
void
bc_ramses_dirichlet_periodic(p4est_t * p4est,
			     p4est_topidx_t which_tree,
			     p4est_quadrant_t * q,
			     int face,
			     qdata_t * ghost_qdata)
{

  UNUSED (which_tree);
  UNUSED (q);
  UNUSED (face);
  
  SolverRamses       *solver = static_cast<SolverRamses*>( solver_from_p4est (p4est) );
  
  UserDataManagerRamses
    *userdata_mgr = static_cast<UserDataManagerRamses *> (solver->get_userdata_mgr());

  qdata_t            *qdata = userdata_mgr->quadrant_get_qdata(q);
  ConfigReader       *cfg = solver->m_cfg;
  UNUSED(cfg);
  RamsesPar          &ramsesPar = solver->ramsesPar;

  userdata_mgr->qdata_copy(ghost_qdata, qdata);

  double density=0, vflow=0, pressure=0;

  if (face == 2 || face == 3) {
    // we shouldn't be here, since the connectivity used should enforce this
    // we could print a warning ...
    return;
  }

  if (face == 0) { // inner border

    density  = cfg->config_read_double("border_inner.density",2.0);
    vflow    = cfg->config_read_double("border_inner.vflow",-0.5);
    pressure = cfg->config_read_double("border_inner.pressure",2.5);

  } else if (face == 1) { // outer border

    density  = cfg->config_read_double("border_outer.density",1.0);
    vflow    = cfg->config_read_double("border_outer.vflow",0.5);
    pressure = cfg->config_read_double("border_outer.pressure",2.5);

  }

  ghost_qdata->w.rho     = density;
  ghost_qdata->w.rhoV[0] = 0;             // radial 
  ghost_qdata->w.rhoV[1] = density*vflow; // orthoradial
#ifdef USE_3D
  ghost_qdata->w.rhoV[2] = 0;
#endif /* USE_3D */

  ghost_qdata->w.E_tot   = pressure / (ramsesPar.gamma0-1.0);
#ifdef USE_3D
  ghost_qdata->w.E_tot +=
    0.5 * ( SC_SQR(ghost_qdata->w.rhoV[0]) + 
	    SC_SQR(ghost_qdata->w.rhoV[1]) + 
	    SC_SQR(ghost_qdata->w.rhoV[2]) ) / ghost_qdata->w.rho;
#else
  ghost_qdata->w.E_tot +=
    0.5 * ( SC_SQR(ghost_qdata->w.rhoV[0]) + 
	    SC_SQR(ghost_qdata->w.rhoV[1]) ) / ghost_qdata->w.rho;
#endif

  /* wp / wm should also be initialized */
  {
    qdata_reconstructed_t w_prim;

    /* get the primitive variables in the current quadrant */
    userdata_mgr->reconstruct_variables (ghost_qdata->w, w_prim);
    
    ghost_qdata->wm[IX] = w_prim;
    ghost_qdata->wp[IX] = w_prim;
    ghost_qdata->wm[IY] = w_prim;
    ghost_qdata->wp[IY] = w_prim;
#ifdef USE_3D
    ghost_qdata->wm[IZ] = w_prim;
    ghost_qdata->wp[IZ] = w_prim;
#endif
  }

  if (face == 0) { // inner border

    //ghost_qdata->wm[IY] = qdata->wp[IY];

  } else if (face == 1) { // outer border

    //ghost_qdata->wp[IY] = qdata->wm[IY];

  }

} // bc_ramses_dirichlet_periodic

// =======================================================
// =======================================================
void
bc_ramses_shock_bubble(p4est_t * p4est,
		       p4est_topidx_t which_tree,
		       p4est_quadrant_t * q,
		       int face,
		       qdata_t * ghost_qdata)
{

  UNUSED (which_tree);
  UNUSED (q);
  UNUSED (face);

  SolverRamses       *solver = static_cast<SolverRamses*>( solver_from_p4est (p4est) );
  UserDataManagerRamses
    *userdata_mgr = static_cast<UserDataManagerRamses *> (solver->get_userdata_mgr());

  qdata_t            *qdata = userdata_mgr->quadrant_get_qdata(q);
  ConfigReader       *cfg = solver->m_cfg;
  RamsesPar          &ramsesPar = solver->ramsesPar;

  /* copy current cell data into the ghost outside-boundary cell */
  userdata_mgr->qdata_copy(ghost_qdata, qdata);

  if (face == 0) { // inflow

    double rho = cfg->config_read_double ("border_inflow.rho", 3.81);
    double P   = cfg->config_read_double ("border_inflow.pressure", 10.0);
    double u   = cfg->config_read_double ("border_inflow.u", 2.85);
    double v   = cfg->config_read_double ("border_inflow.v", 0.0);
    double w   = cfg->config_read_double ("border_inflow.w", 0.0);
    UNUSED (w);

    ghost_qdata->w.rho = rho;
    ghost_qdata->w.rhoV[0] = rho*u;
    ghost_qdata->w.rhoV[1] = rho*v;
#ifdef USE_3D
    ghost_qdata->w.rhoV[2] = rho*w;
    ghost_qdata->w.E_tot = P / (ramsesPar.gamma0-1.0) +
      0.5 * ( SC_SQR(rho*u) + 
	      SC_SQR(rho*v) + 
	      SC_SQR(rho*w) ) / rho;
#else
    ghost_qdata->w.E_tot = P / (ramsesPar.gamma0-1.0) +
      0.5 * ( SC_SQR(rho*u) + 
	      SC_SQR(rho*v) ) / rho;
    
#endif

    ghost_qdata->wnext = ghost_qdata->w;

    // set slopes to zero
    userdata_mgr->qdata_reconstructed_zero(&(ghost_qdata->delta[IX]));
    userdata_mgr->qdata_reconstructed_zero(&(ghost_qdata->delta[IY]));
#ifdef USE_3D
    userdata_mgr->qdata_reconstructed_zero(&(ghost_qdata->delta[IZ]));
#endif // USE_3D

    // reconstructed state (trace)
    qdata_reconstructed_t wr;
    wr.rho = rho;
    wr.velocity[IX] = u;
    wr.velocity[IY] = v;
#ifdef USE_3D
    wr.velocity[IZ] = w;
#endif
    wr.P = P;

    userdata_mgr->qdata_reconstructed_copy(&(ghost_qdata->wm[IX]), &wr);
    userdata_mgr->qdata_reconstructed_copy(&(ghost_qdata->wm[IY]), &wr);
    userdata_mgr->qdata_reconstructed_copy(&(ghost_qdata->wp[IX]), &wr);
    userdata_mgr->qdata_reconstructed_copy(&(ghost_qdata->wp[IY]), &wr);
#ifdef USE_3D
    userdata_mgr->qdata_reconstructed_copy(&(ghost_qdata->wm[IZ]), &wr);
    userdata_mgr->qdata_reconstructed_copy(&(ghost_qdata->wp[IZ]), &wr);
#endif

  } // end face 0 (inflow)


  if (face == 1) { // outflow

    // already taken care of by qdata_copy
    
    // reflective ?
    //bc_ramses_reflective (p4est, which_tree, q, face, ghost_qdata);

  } // end face 1


  if (face == 2 || face == 3) { // reflective

    bc_ramses_reflective (p4est, which_tree, q, face, ghost_qdata);
    
  } // end face 2

} // bc_ramses_shock_bubble

// =======================================================
// =======================================================
void
bc_ramses_inflow(p4est_t * p4est,
		 p4est_topidx_t which_tree,
		 p4est_quadrant_t * q,
		 int face,
		 qdata_t * ghost_qdata)
{

  UNUSED (which_tree);
  UNUSED (q);
  UNUSED (face);

  SolverRamses    *solver = static_cast<SolverRamses*>(solver_from_p4est (p4est));

  UserDataManagerRamses
    *userdata_mgr = static_cast<UserDataManagerRamses *> (solver->get_userdata_mgr());

  qdata_t         *qdata = userdata_mgr->quadrant_get_qdata(q);
  ConfigReader    *cfg = solver->m_cfg;
  RamsesPar       &ramsesPar = solver->ramsesPar;

  /* copy current cell data into the ghost outside-boundary cell */
  userdata_mgr->qdata_copy(ghost_qdata, qdata);

  /* one can specify the inflow face in the parameter file */
  int faceInflow = cfg->config_read_int ("border_inflow.face", 0);

  if (face == faceInflow && which_tree == 0) { // inflow

    double rho = cfg->config_read_double ("border_inflow.rho", 3.81);
    double P   = cfg->config_read_double ("border_inflow.pressure", 10.0);
    double u   = cfg->config_read_double ("border_inflow.u", 2.85);
    double v   = cfg->config_read_double ("border_inflow.v", 0.0);
    double w   = cfg->config_read_double ("border_inflow.w", 0.0);
    UNUSED (w);

    ghost_qdata->w.rho = rho;
    ghost_qdata->w.rhoV[0] = rho*u;
    ghost_qdata->w.rhoV[1] = rho*v;
#ifdef USE_3D
    ghost_qdata->w.rhoV[2] = rho*w;
    ghost_qdata->w.E_tot = P / (ramsesPar.gamma0-1.0) +
      0.5 * ( SC_SQR(rho*u) + 
	      SC_SQR(rho*v) + 
	      SC_SQR(rho*w) ) / rho;
#else
    ghost_qdata->w.E_tot = P / (ramsesPar.gamma0-1.0) +
      0.5 * ( SC_SQR(rho*u) + 
	      SC_SQR(rho*v) ) / rho;
    
#endif

    ghost_qdata->wnext = ghost_qdata->w;

    // set slopes to zero
    userdata_mgr->qdata_reconstructed_zero(&(ghost_qdata->delta[IX]));
    userdata_mgr->qdata_reconstructed_zero(&(ghost_qdata->delta[IY]));
#ifdef USE_3D
    userdata_mgr->qdata_reconstructed_zero(&(ghost_qdata->delta[IZ]));
#endif // USE_3D

    // reconstructed state (trace)
    qdata_reconstructed_t wr;
    wr.rho = rho;
    wr.velocity[IX] = u;
    wr.velocity[IY] = v;
#ifdef USE_3D
    wr.velocity[IZ] = w;
#endif
    wr.P = P;

    userdata_mgr->qdata_reconstructed_copy(&(ghost_qdata->wm[IX]), &wr);
    userdata_mgr->qdata_reconstructed_copy(&(ghost_qdata->wm[IY]), &wr);
    userdata_mgr->qdata_reconstructed_copy(&(ghost_qdata->wp[IX]), &wr);
    userdata_mgr->qdata_reconstructed_copy(&(ghost_qdata->wp[IY]), &wr);
#ifdef USE_3D
    userdata_mgr->qdata_reconstructed_copy(&(ghost_qdata->wm[IZ]), &wr);
    userdata_mgr->qdata_reconstructed_copy(&(ghost_qdata->wp[IZ]), &wr);
#endif

  } else { // reflective

    bc_ramses_reflective (p4est, which_tree, q, face, ghost_qdata);
    
  } 

} // bc_ramses_inflow

// =======================================================
// =======================================================
void
bc_ramses_forward_facing_step(p4est_t * p4est,
			      p4est_topidx_t which_tree,
			      p4est_quadrant_t * q,
			      int face,
			      qdata_t * ghost_qdata)
{

  UNUSED (which_tree);
  UNUSED (q);
  UNUSED (face);

  SolverRamses    *solver = static_cast<SolverRamses*>(solver_from_p4est (p4est));
    UserDataManagerRamses
    *userdata_mgr = static_cast<UserDataManagerRamses *> (solver->get_userdata_mgr());

  qdata_t         *qdata = userdata_mgr->quadrant_get_qdata(q);
  ConfigReader    *cfg = solver->m_cfg;
  RamsesPar       &ramsesPar = solver->ramsesPar;

  /* copy current cell data into the ghost outside-boundary cell */
  userdata_mgr->qdata_copy(ghost_qdata, qdata);

  if (face == 0) { // inflow

    double rho = cfg->config_read_double ("border_inflow.rho", 1.4);
    double P   = cfg->config_read_double ("border_inflow.pressure", 1.0);
    double u   = cfg->config_read_double ("border_inflow.u", 3.0);
    double v   = cfg->config_read_double ("border_inflow.v", 0.0);
    double w   = cfg->config_read_double ("border_inflow.w", 0.0);
    UNUSED (w);

    ghost_qdata->w.rho = rho;
    ghost_qdata->w.rhoV[0] = rho*u;
    ghost_qdata->w.rhoV[1] = rho*v;
#ifdef USE_3D
    ghost_qdata->w.rhoV[2] = rho*w;
    ghost_qdata->w.E_tot = P / (ramsesPar.gamma0-1.0) +
      0.5 * ( SC_SQR(rho*u) + 
	      SC_SQR(rho*v) + 
	      SC_SQR(rho*w) ) / rho;
#else
    ghost_qdata->w.E_tot = P / (ramsesPar.gamma0-1.0) +
      0.5 * ( SC_SQR(rho*u) + 
	      SC_SQR(rho*v) ) / rho;
    
#endif

    ghost_qdata->wnext = ghost_qdata->w;

    // set slopes to zero
    userdata_mgr->qdata_reconstructed_zero(&(ghost_qdata->delta[IX]));
    userdata_mgr->qdata_reconstructed_zero(&(ghost_qdata->delta[IY]));
#ifdef USE_3D
    userdata_mgr->qdata_reconstructed_zero(&(ghost_qdata->delta[IZ]));
#endif // USE_3D

    // reconstructed state (trace)
    qdata_reconstructed_t wr;
    wr.rho = rho;
    wr.velocity[IX] = u;
    wr.velocity[IY] = v;
#ifdef USE_3D
    wr.velocity[IZ] = w;
#endif
    wr.P = P;

    userdata_mgr->qdata_reconstructed_copy(&(ghost_qdata->wm[IX]), &wr);
    userdata_mgr->qdata_reconstructed_copy(&(ghost_qdata->wm[IY]), &wr);
    userdata_mgr->qdata_reconstructed_copy(&(ghost_qdata->wp[IX]), &wr);
    userdata_mgr->qdata_reconstructed_copy(&(ghost_qdata->wp[IY]), &wr);
#ifdef USE_3D
    userdata_mgr->qdata_reconstructed_copy(&(ghost_qdata->wm[IZ]), &wr);
    userdata_mgr->qdata_reconstructed_copy(&(ghost_qdata->wp[IZ]), &wr);
#endif

  } else if (face==1 && (which_tree+1)%15 ==0) { // outflow

    userdata_mgr->qdata_copy(ghost_qdata, qdata);
    
  } else { // reflective (top, botton and step)

    bc_ramses_reflective (p4est, which_tree, q, face, ghost_qdata);
    
  } 

} // bc_ramses_forward_facing_step

// =======================================================
// =======================================================
void
bc_ramses_backward_facing_step(p4est_t * p4est,
			       p4est_topidx_t which_tree,
			       p4est_quadrant_t * q,
			       int face,
			       qdata_t * ghost_qdata)
{

  UNUSED (which_tree);
  UNUSED (q);
  UNUSED (face);

  SolverRamses    *solver = static_cast<SolverRamses*>(solver_from_p4est (p4est));
  UserDataManagerRamses
    *userdata_mgr = static_cast<UserDataManagerRamses *> (solver->get_userdata_mgr());
  
  qdata_t         *qdata = userdata_mgr->quadrant_get_qdata(q);
  ConfigReader    *cfg = solver->m_cfg;
  RamsesPar       &ramsesPar = solver->ramsesPar;

  /* copy current cell data into the ghost outside-boundary cell */
  userdata_mgr->qdata_copy(ghost_qdata, qdata);

  if (face == 0 && which_tree==0) { // inflow

    double rho = cfg->config_read_double ("border_inflow.rho", 1.4);
    double P   = cfg->config_read_double ("border_inflow.pressure", 1.0);
    double u   = cfg->config_read_double ("border_inflow.u", 3.0);
    double v   = cfg->config_read_double ("border_inflow.v", 0.0);
    double w   = cfg->config_read_double ("border_inflow.w", 0.0);
    UNUSED (w);

    ghost_qdata->w.rho = rho;
    ghost_qdata->w.rhoV[0] = rho*u;
    ghost_qdata->w.rhoV[1] = rho*v;
#ifdef USE_3D
    ghost_qdata->w.rhoV[2] = rho*w;
    ghost_qdata->w.E_tot = P / (ramsesPar.gamma0-1.0) +
      0.5 * ( SC_SQR(rho*u) + 
	      SC_SQR(rho*v) + 
	      SC_SQR(rho*w) ) / rho;
#else
    ghost_qdata->w.E_tot = P / (ramsesPar.gamma0-1.0) +
      0.5 * ( SC_SQR(rho*u) + 
	      SC_SQR(rho*v) ) / rho;
    
#endif

    ghost_qdata->wnext = ghost_qdata->w;

    // set slopes to zero
    userdata_mgr->qdata_reconstructed_zero(&(ghost_qdata->delta[IX]));
    userdata_mgr->qdata_reconstructed_zero(&(ghost_qdata->delta[IY]));
#ifdef USE_3D
    userdata_mgr->qdata_reconstructed_zero(&(ghost_qdata->delta[IZ]));
#endif // USE_3D

    // reconstructed state (trace)
    qdata_reconstructed_t wr;
    wr.rho = rho;
    wr.velocity[IX] = u;
    wr.velocity[IY] = v;
#ifdef USE_3D
    wr.velocity[IZ] = w;
#endif
    wr.P = P;

    userdata_mgr->qdata_reconstructed_copy(&(ghost_qdata->wm[IX]), &wr);
    userdata_mgr->qdata_reconstructed_copy(&(ghost_qdata->wm[IY]), &wr);
    userdata_mgr->qdata_reconstructed_copy(&(ghost_qdata->wp[IX]), &wr);
    userdata_mgr->qdata_reconstructed_copy(&(ghost_qdata->wp[IY]), &wr);
#ifdef USE_3D
    userdata_mgr->qdata_reconstructed_copy(&(ghost_qdata->wm[IZ]), &wr);
    userdata_mgr->qdata_reconstructed_copy(&(ghost_qdata->wp[IZ]), &wr);
#endif

  } else if (face==1) { // outflow

    userdata_mgr->qdata_copy(ghost_qdata, qdata);
    
  } else { // reflective (top, botton and step)

    bc_ramses_reflective (p4est, which_tree, q, face, ghost_qdata);
    
  } 

} // bc_ramses_backward_facing_step

// =======================================================
// =======================================================
void
bc_ramses_wedge(p4est_t * p4est,
		p4est_topidx_t which_tree,
		p4est_quadrant_t * q,
		int face,
		qdata_t * ghost_qdata)
{

  UNUSED (which_tree);
  UNUSED (q);
  UNUSED (face);

  SolverRamses       *solver = static_cast<SolverRamses*>(solver_from_p4est (p4est));
  UserDataManagerRamses
    *userdata_mgr = static_cast<UserDataManagerRamses *> (solver->get_userdata_mgr());
  
  p4est_geometry_t   *geom        = solver->get_geom_compute();
  qdata_t            *qdata       = userdata_mgr->quadrant_get_qdata(q);
  ConfigReader       *cfg         = solver->m_cfg;
  RamsesPar          &ramsesPar   = solver->ramsesPar;
  
  /* copy current cell data into the ghost outside-boundary cell */
  // this is outflow (default behavior)
  userdata_mgr->qdata_copy(ghost_qdata, qdata);

  // get the coordinates of the center of the quad
  double  XYZ[3] = { 0, 0, 0 };
  double  x,y,z;
  UNUSED (x);
  UNUSED (y);
  UNUSED (z);
  
  // get the physical coordinates of the center of the quad
  quadrant_center_vertex (p4est->connectivity, geom, which_tree, q, XYZ);
  
  x = XYZ[0];
  y = XYZ[1];
  z = XYZ[2];

  // total time
  double t = solver->m_t;
  UNUSED(t);

  double x_f     = cfg->config_read_double ("wedge.front_x", 0.1);
  double angle_f = cfg->config_read_double ("wedge.front_angle", M_PI/3.0);
  double slope_f = tan(angle_f);

  // shock wave speed projected on x-axis
  double shock_speed = cfg->config_read_double ("shock_speed", 1.0);
  shock_speed /= cos(M_PI/2.0-angle_f);

  double delta_x = shock_speed*t;

  // inflow
  if ( ( face == 0 ) || 
       ( face == 2 && (x < x_f) ) ||
       ( face == 3 && (x < x_f + y/slope_f + delta_x) ) ) {

    double rho = cfg->config_read_double ("wedge.rho1", 8.0);
    double P   = cfg->config_read_double ("wedge.p1", 116.5);
    double u   = cfg->config_read_double ("wedge.u1", 8.25*cos(angle_f-M_PI/2.0));
    double v   = cfg->config_read_double ("wedge.v1", 8.25*sin(angle_f-M_PI/2.0));
    double w   = cfg->config_read_double ("wedge.w1",  0.0);
    UNUSED (w);

    ghost_qdata->w.rho = rho;
    ghost_qdata->w.rhoV[0] = rho*u;
    ghost_qdata->w.rhoV[1] = rho*v;
#ifdef USE_3D
    ghost_qdata->w.rhoV[2] = rho*w;
    ghost_qdata->w.E_tot = P / (ramsesPar.gamma0-1.0) +
      0.5 * ( SC_SQR(rho*u) + 
	      SC_SQR(rho*v) + 
	      SC_SQR(rho*w) ) / rho;
#else
    ghost_qdata->w.E_tot = P / (ramsesPar.gamma0-1.0) +
      0.5 * ( SC_SQR(rho*u) + 
	      SC_SQR(rho*v) ) / rho;
    
#endif

    ghost_qdata->wnext = ghost_qdata->w;

    // set slopes to zero
    userdata_mgr->qdata_reconstructed_zero(&(ghost_qdata->delta[IX]));
    userdata_mgr->qdata_reconstructed_zero(&(ghost_qdata->delta[IY]));
#ifdef USE_3D
    userdata_mgr->qdata_reconstructed_zero(&(ghost_qdata->delta[IZ]));
#endif // USE_3D

    // reconstructed state (trace)
    qdata_reconstructed_t wr;
    wr.rho = rho;
    wr.velocity[IX] = u;
    wr.velocity[IY] = v;
#ifdef USE_3D
    wr.velocity[IZ] = w;
#endif
    wr.P = P;

    userdata_mgr->qdata_reconstructed_copy(&(ghost_qdata->wm[IX]), &wr);
    userdata_mgr->qdata_reconstructed_copy(&(ghost_qdata->wm[IY]), &wr);
    userdata_mgr->qdata_reconstructed_copy(&(ghost_qdata->wp[IX]), &wr);
    userdata_mgr->qdata_reconstructed_copy(&(ghost_qdata->wp[IY]), &wr);
#ifdef USE_3D
    userdata_mgr->qdata_reconstructed_copy(&(ghost_qdata->wm[IZ]), &wr);
    userdata_mgr->qdata_reconstructed_copy(&(ghost_qdata->wp[IZ]), &wr);
#endif

  } // end face inflow

  // reflective
  if (face == 2 && (x >= x_f) ) { 

    bc_ramses_reflective (p4est, which_tree, q, face, ghost_qdata);

  } // end face reflective

} // bc_ramses_wedge

// =======================================================
// =======================================================
void
bc_ramses_jet(p4est_t * p4est,
	      p4est_topidx_t which_tree,
	      p4est_quadrant_t * q,
	      int face,
	      qdata_t * ghost_qdata)
{

  UNUSED (which_tree);
  UNUSED (q);
  UNUSED (face);

  SolverRamses       *solver = static_cast<SolverRamses*>(solver_from_p4est (p4est));
  UserDataManagerRamses
    *userdata_mgr = static_cast<UserDataManagerRamses *> (solver->get_userdata_mgr());
  
  qdata_t            *qdata  = userdata_mgr->quadrant_get_qdata(q);
  ConfigReader       *cfg    = solver->m_cfg;
  p4est_geometry_t   *geom   = solver->get_geom_compute ();
  RamsesPar       &ramsesPar = solver->ramsesPar;
  
  UNUSED(geom);
  
  /* copy current cell data into the ghost outside-boundary cell */
  userdata_mgr->qdata_copy(ghost_qdata, qdata);

  /* one can specify the inflow face in the parameter file */
  int faceInflow = cfg->config_read_int ("border_inflow.face", 0);
  double xCenterJet = cfg->config_read_double ("border_inflow.xCenterJet", 0.0);
  double yCenterJet = cfg->config_read_double ("border_inflow.yCenterJet", 0.5);
  double radiusJet  = cfg->config_read_double ("border_inflow.radiusJet", 0.1);
  double r2 = radiusJet*radiusJet;
  
  if (face == faceInflow && which_tree == 0) { // inflow

    // get the physical coordinates of the center of the quad
    double x,y,z;
    double  XYZ[3] = { 0, 0, 0 };
    quadrant_center_vertex (p4est->connectivity, geom, which_tree, q, XYZ);
    
    x = XYZ[0];
    y = XYZ[1];
    z = XYZ[2];
    UNUSED(z);
    
    double rho = cfg->config_read_double ("border_inflow.rho", 3.81);
    double P   = cfg->config_read_double ("border_inflow.pressure", 10.0);
    double u   = cfg->config_read_double ("border_inflow.u", 2.85);
    double v   = cfg->config_read_double ("border_inflow.v", 0.0);
    double w   = cfg->config_read_double ("border_inflow.w", 0.0);
    UNUSED (w);

    if (SC_SQR(x-xCenterJet)+SC_SQR(y-yCenterJet) < r2) {
      ghost_qdata->w.rho = rho;
      ghost_qdata->w.rhoV[0] = rho*u;
      ghost_qdata->w.rhoV[1] = rho*v;
#ifdef USE_3D
      ghost_qdata->w.rhoV[2] = rho*w;
      ghost_qdata->w.E_tot = P / (ramsesPar.gamma0-1.0) +
	0.5 * ( SC_SQR(rho*u) + 
		SC_SQR(rho*v) + 
		SC_SQR(rho*w) ) / rho;
#else
      ghost_qdata->w.E_tot = P / (ramsesPar.gamma0-1.0) +
	0.5 * ( SC_SQR(rho*u) + 
		SC_SQR(rho*v) ) / rho;
      
#endif
      
      ghost_qdata->wnext = ghost_qdata->w;
      
      // set slopes to zero
      userdata_mgr->qdata_reconstructed_zero(&(ghost_qdata->delta[IX]));
      userdata_mgr->qdata_reconstructed_zero(&(ghost_qdata->delta[IY]));
#ifdef USE_3D
      userdata_mgr->qdata_reconstructed_zero(&(ghost_qdata->delta[IZ]));
#endif // USE_3D
      
      // reconstructed state (trace)
      qdata_reconstructed_t wr;
      wr.rho = rho;
      wr.velocity[IX] = u;
      wr.velocity[IY] = v;
#ifdef USE_3D
      wr.velocity[IZ] = w;
#endif
      wr.P = P;
      
      userdata_mgr->qdata_reconstructed_copy(&(ghost_qdata->wm[IX]), &wr);
      userdata_mgr->qdata_reconstructed_copy(&(ghost_qdata->wm[IY]), &wr);
      userdata_mgr->qdata_reconstructed_copy(&(ghost_qdata->wp[IX]), &wr);
      userdata_mgr->qdata_reconstructed_copy(&(ghost_qdata->wp[IY]), &wr);
#ifdef USE_3D
      userdata_mgr->qdata_reconstructed_copy(&(ghost_qdata->wm[IZ]), &wr);
      userdata_mgr->qdata_reconstructed_copy(&(ghost_qdata->wp[IZ]), &wr);
#endif
    } else {
      // neumann 
    }
    
  } else { // reflective

    bc_ramses_reflective (p4est, which_tree, q, face, ghost_qdata);
    
  } 

} // bc_ramses_jet

// =======================================================
// ======CLASS BoundaryConditionsFactoryRamses IMPL ======
// =======================================================
BoundaryConditionsFactoryRamses::BoundaryConditionsFactoryRamses() :
  BoundaryConditionsFactory<Qdata>()
{

  // register the above callback's
  registerBoundaryConditions("dirichlet" , bc_ramses_dirichlet);
  registerBoundaryConditions("neuman"    , bc_ramses_neuman);
  registerBoundaryConditions("reflective", bc_ramses_reflective);
  registerBoundaryConditions("hse"       , bc_ramses_hse);
  registerBoundaryConditions("wedge"     , bc_ramses_wedge);
  registerBoundaryConditions("open_tube" , bc_ramses_open_tube);
  registerBoundaryConditions("open_box"  , bc_ramses_open_box);
  registerBoundaryConditions("dirichlet_periodic", bc_ramses_dirichlet_periodic);
  registerBoundaryConditions("shock_bubble", bc_ramses_shock_bubble);
  registerBoundaryConditions("inflow"      , bc_ramses_inflow);
  registerBoundaryConditions("forward_facing_step", bc_ramses_forward_facing_step);
  registerBoundaryConditions("backward_facing_step", bc_ramses_backward_facing_step);
  registerBoundaryConditions("jet"       , bc_ramses_jet);


} // BoundaryConditionsFactoryRamses::BoundaryConditionsFactoryRamses

} // namespace ramses

} // namespace canop
