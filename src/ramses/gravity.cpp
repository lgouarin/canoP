
#ifdef USE_3D
#include <p4est_to_p8est.h>
#endif

#ifndef P4_TO_P8
#include <p4est_bits.h>
#include <p4est_lnodes.h>
#else
#include <p8est_bits.h>
#include <p8est_lnodes.h>
#endif

#include "Lnode_Utils.h"
#include "PoissonFESolver.h"
#include "SolverRamses.h"
#include "Statistics.h"
#include "UserDataManagerRamses.h"

#include <cmath>
#include <functional>
#include <mpi.h>

#include "gravity.h"

namespace canop {
namespace ramses {

// TODO: put this somewhere else?
// XXX: cubic geometry only
static double get_quadrant_volume(p4est_t * p4est,
                                  p4est_topidx_t which_tree,
                                  p4est_quadrant_t * quad)
{

  UNUSED(p4est);
  UNUSED(which_tree);

  /* assume here cartesian geometry */
  double dx = 1.0*P4EST_QUADRANT_LEN(quad->level) / P4EST_ROOT_LEN;

#ifndef P4_TO_P8
  return dx*dx;
#else
  return dx*dx*dx;
#endif

} // get_quadrant_volume

// TODO: put this somewhere else?
// XXX: cubic geometry only
static double get_quadrant_scale(p4est_t * p4est,
                                 p4est_topidx_t which_tree,
                                 p4est_quadrant_t * quad)
{

  UNUSED(p4est);
  UNUSED(which_tree);

  /* assume here cartesian geometry */
  double dx = 1.0*P4EST_QUADRANT_LEN(quad->level) / P4EST_ROOT_LEN;

  return dx;
} // get_quadrant_scale


int
solve_poisson_gravity(p4est_t *p4est, p4est_ghost_t *ghost_,
                      poisson_info_t *info)
{
  SolverRamses *solver = static_cast<SolverRamses*>(solver_from_p4est(p4est));
  UserDataManagerRamses *userdata_mgr = static_cast<UserDataManagerRamses *>(solver->get_userdata_mgr());
  p4est_geometry_t *geom = solver->get_geom_compute();
  p4est_mesh_t *mesh = solver->get_mesh();

  p4est_lnodes_t *lnodes;

  clock_t t_start_tot, t_start;
  double time;

  double *rho_nodes;
  double *phi_nodes;
  int8_t *bc = nullptr;

  int     converged;

  // Fetch Poisson solver parameters
  int imax = solver->ramsesPar.gravity.poisson_imax;
  double tol = solver->ramsesPar.gravity.poisson_tol;

  t_start_tot = clock ();

  // Create a ghost layer if needed
  p4est_ghost_t *ghost;
  if(ghost_ == nullptr)
    ghost = p4est_ghost_new(p4est, P4EST_CONNECT_FULL);
  else
    ghost = ghost_;


  // Create a node numbering for continuous linear finite elements.
  lnodes = p4est_lnodes_new(p4est, ghost, 1);

  t_start = clock();

  // Interpolate the density field onto the nodes
  rho_nodes = CANOP_ALLOC(double, lnodes->num_local_nodes);
  auto rho_getter = std::bind(&UserDataManagerRamses::qdata_get_rho,
                              userdata_mgr,
                              std::placeholders::_1);
  interp_quad_to_lnodes(p4est, lnodes, rho_getter, get_quadrant_volume, rho_nodes);

  // Flag boundaries
  if(solver->ramsesPar.gravity.poisson_boundary != RamsesGravBC::Periodic) {
    bc = CANOP_ALLOC(int8_t, lnodes->num_local_nodes);
    lnodes_flag_boundaries(p4est, mesh, lnodes, bc);
  }

  // Make an initial guess for the potential
  phi_nodes = CANOP_ALLOC(double, lnodes->num_local_nodes);
  if(solver->ramsesPar.gravity.poisson_reuse_potential) {
    auto phi_getter = std::bind(&UserDataManagerRamses::qdata_get_grav_phi,
                                userdata_mgr,
                                std::placeholders::_1);
    interp_quad_to_lnodes(p4est, lnodes, phi_getter, get_quadrant_volume, phi_nodes);

    // Invert the sign of the guessed potential
    for(p4est_locidx_t i = 0; i < lnodes->num_local_nodes; ++i)
      phi_nodes[i] = -phi_nodes[i];
  }
  else {
    memset(phi_nodes, 0, lnodes->num_local_nodes * sizeof(double));
  }

  time = (double) (clock () - t_start) / CLOCKS_PER_SEC;
  if (solver->m_stats != nullptr)
    solver->m_stats->statistics_timing_byname ("t_grav_interp1", time);

  // Set boundary conditions
  if(solver->ramsesPar.gravity.poisson_boundary == RamsesGravBC::Monopole) {
    t_start = clock();

    double total_mass;
    double barycenter[3];
    compute_barycenter(p4est, geom, rho_getter, get_quadrant_volume,
                       &total_mass, barycenter);
    // Pass the total mass and barycenter to the monopole function
    auto monopole = std::bind(monopole_expansion, total_mass, barycenter,
                              std::placeholders::_1);
    lnodes_set_boundaries(p4est, geom, lnodes, bc, monopole, phi_nodes);

    // debug info
    CANOP_GLOBAL_PRODUCTIONF("monopole expansion: mass = %g, center = (%f, %f, %f)\n",
        total_mass, barycenter[0], barycenter[1], barycenter[2]);

    time = (double) (clock () - t_start) / CLOCKS_PER_SEC;
    if (solver->m_stats != nullptr)
      solver->m_stats->statistics_timing_byname ("t_grav_boundary", time);
  }

  // Handle periodic boundary conditions: remove average density
  if(solver->ramsesPar.gravity.poisson_boundary == RamsesGravBC::Periodic) {
    double rho_avg = compute_average(p4est, rho_getter, get_quadrant_volume);

    for(p4est_locidx_t i = 0; i < lnodes->num_local_nodes; ++i)
      rho_nodes[i] -= rho_avg;
  }

  // Solve Poisson equation using finite elements
  t_start = clock();
  converged = solve_poisson_lnodes(p4est, lnodes, bc, rho_nodes, phi_nodes, imax, tol, info);
  // XXX: phi_nodes actually contains -phi / (4pi G)

  time = (double) (clock () - t_start) / CLOCKS_PER_SEC;
  if (solver->m_stats != nullptr)
    solver->m_stats->statistics_timing_byname ("t_grav_solve", time);

  // Interpolate the acceleration and potential back to quadrants
  t_start = clock();
  auto acc_setter = std::bind(&UserDataManagerRamses::qdata_set_grav_acc_v,
                              userdata_mgr,
                              std::placeholders::_1, std::placeholders::_2);
  auto phi_setter = std::bind(&UserDataManagerRamses::qdata_set_grav_phi,
                              userdata_mgr,
                              std::placeholders::_1, std::placeholders::_2);
  gradient_lnodes_to_quad(p4est, lnodes, get_quadrant_scale, phi_nodes, acc_setter);
  // Put the correct sign to the potential
  for(p4est_locidx_t i = 0; i < lnodes->num_local_nodes; ++i)
    phi_nodes[i] = -phi_nodes[i];
  interp_lnodes_to_quad(p4est, lnodes, phi_nodes, phi_setter);

  time = (double) (clock () - t_start) / CLOCKS_PER_SEC;
  if (solver->m_stats != nullptr)
    solver->m_stats->statistics_timing_byname ("t_grav_interp2", time);

  // Clean up
  CANOP_FREE(rho_nodes);
  CANOP_FREE(bc);
  CANOP_FREE(phi_nodes);

  p4est_lnodes_destroy(lnodes);
  if(ghost_ == nullptr) // we created a ghost layer
    p4est_ghost_destroy(ghost);

  time = (double) (clock () - t_start_tot) / CLOCKS_PER_SEC;
  if (solver->m_stats != nullptr) {
    solver->m_stats->statistics_timing_byname ("t_grav_min", time);
    solver->m_stats->statistics_timing_byname ("t_grav_max", time);
    solver->m_stats->statistics_timing_byname ("t_grav_mean", time);
  }

  return converged;

}

void
point_gravity_callback (p4est_iter_volume_info_t *info, void *user_data)
{

  SolverRamses         *solver = static_cast<SolverRamses*>(solver_from_p4est(info->p4est));
  p4est_connectivity_t *conn = info->p4est->connectivity;
  p4est_geometry_t     *geom = solver->get_geom_compute();

  UserDataManagerRamses
    *userdata_mgr = static_cast<UserDataManagerRamses *> (solver->get_userdata_mgr());

  RamsesPar       &ramsesPar = solver->ramsesPar;

  p4est_quadrant_t     *quad = info->quad;
  p4est_topidx_t        treeid = info->treeid;
  Qdata *qdata = userdata_mgr->quadrant_get_qdata (quad);

  bool                  reverse = *static_cast<bool *>(user_data);
  double                rev_sign = reverse? -1.0 : 1.0;

  double                xyz[3] = {0., 0., 0.}, center[3] = {0., 0., 0.};
  double                radius_soft;

  double Gmass = ramsesPar.gravity.point_Gmass;
  double soften = ramsesPar.gravity.point_soften;
  center[0] = ramsesPar.gravity.point_pos_x;
  center[1] = ramsesPar.gravity.point_pos_y;
#ifdef P4_TO_P8
  center[2] = ramsesPar.gravity.point_pos_z;
#endif

  quadrant_center_vertex (conn, geom, treeid, quad, xyz);

  radius_soft = std::sqrt(
        SC_SQR(xyz[0] - center[0])
      + SC_SQR(xyz[1] - center[1])
#ifdef P4_TO_P8
      + SC_SQR(xyz[2] - center[2])
#endif
      + SC_SQR(soften)
      );

  qdata->wgrav.phi -= rev_sign * Gmass / radius_soft;
  for (int i = 0; i < P4EST_DIM; ++i)
    qdata->wgrav.accel[i] -= rev_sign * Gmass / SC_SQR(radius_soft) * (xyz[i] - center[i]) / radius_soft;
} // point_gravity_callback

void
set_point_gravity (p4est_t *p4est, bool reverse)
{
  p4est_iterate (p4est,
                 nullptr,                // ghost
                 &reverse,               // user data
                 point_gravity_callback, // volume
                 nullptr,                // face
#ifdef P4_TO_P8
                 nullptr,                // edge
#endif
                 nullptr                 // corner
                 );
} // set_point_gravity

} // namespace ramses
} // namespace canop
