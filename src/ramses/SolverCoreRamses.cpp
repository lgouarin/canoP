#include "SolverCoreRamses.h"

#include "InitialConditionsFactoryRamses.h"
#include "BoundaryConditionsFactoryRamses.h"
#include "IndicatorFactoryRamses.h"

#include "ConfigReader.h"

namespace canop {

namespace ramses {

// =======================================================
// =======================================================
SolverCoreRamses::SolverCoreRamses(ConfigReader *cfg) :
  SolverCore<Qdata>(cfg)
{

  /*
   * Initial condition callback setup.
   */
  InitialConditionsFactoryRamses IC_Factory;
  m_init_fn = IC_Factory.callback_byname (m_init_name);

  /*
   * Boundary condition callback setup.
   */
  BoundaryConditionsFactoryRamses BC_Factory;
  m_boundary_fn = BC_Factory.callback_byname (m_boundary_name);

  /*
   * Refine/Coarsen callback setup.
   */
  IndicatorFactoryRamses indic_Factory;
  m_indicator_fn = indic_Factory.callback_byname (m_indicator_name);

  /*
   * Geometric refine callback setup.
   */
  GeometricIndicatorFactory geom_indic_Factory;
  m_geom_indicator_fn = geom_indic_Factory.callback_byname (m_geom_indicator_name);

  
} // SolverCoreRamses::SolverCoreRamses


// =======================================================
// =======================================================
SolverCoreRamses::~SolverCoreRamses()
{
} // SolverCoreRamses::~SolverCoreRamses

} // namespace ramses

} // namespace canop
