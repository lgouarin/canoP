#ifndef INITIAL_CONDITIONS_FACTORY_RAMSES_H_
#define INITIAL_CONDITIONS_FACTORY_RAMSES_H_

#include "InitialConditionsFactory.h"
#include "ramses/userdata_ramses.h"

namespace canop {

namespace ramses {

/**
 *
 */
class InitialConditionsFactoryRamses : public InitialConditionsFactory
{

  using qdata_t = Qdata;
  
public:
  InitialConditionsFactoryRamses();
  virtual ~InitialConditionsFactoryRamses() {};
  
}; // class InitialConditionsFactoryRamses

} // namespace ramses

} // namespace canop

#endif // INITIAL_CONDITIONS_FACTORY_RAMSES_H_
