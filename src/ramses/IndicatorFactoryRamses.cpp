#include "IndicatorFactoryRamses.h"
#include "utils.h" // for UNUSED macro
#include "quadrant_utils.h"

#include <cmath>


namespace canop {

namespace ramses {

// remember that Qdata is a concrete type defined in userdata_ramses.h
using qdata_t = Qdata;


// =======================================================
// =======================================================
double
indicator_ramses_khokhlov (qdata_t * cella, qdata_t * cellb,
			   indicator_info_t * info)
{
  UNUSED (info);
  double              rho[2] = { cella->w.rho, cellb->w.rho };
  
  /* equivalent to indicator_rho_gradient */
  return indicator_scalar_gradient (rho[0], rho[1]);
  
} // indicator_ramses_khokhlov

// =======================================================
// =======================================================
double
indicator_ramses_rho_gradient (qdata_t * cella, qdata_t * cellb,
			       indicator_info_t * info)
{
  UNUSED (info);
  double              rho[2] = {
    cella->w.rho, cellb->w.rho
  };

  return indicator_scalar_gradient (rho[0], rho[1]);
  
} // indicator_ramses_rho_gradient

// =======================================================
// =======================================================
double
indicator_ramses_jeans (qdata_t * cella, qdata_t * cellb,
                        indicator_info_t * info)
{
  UNUSED(cellb);
  double rho, csound2, ljeans;
  double dx;

  RamsesPar *ramsesPar =
    static_cast<RamsesPar*>(info->user_data);

  rho = std::fmax(cella->w.rho, ramsesPar->smallr);

  // Compute sound speed
  csound2 = cella->w.E_tot; // Kinetic + thermal energy
  for(size_t i = 0; i < P4EST_DIM; ++i)
    csound2 -= 0.5 * SC_SQR(cella->w.rhoV[i]) / rho;
  csound2 *= ramsesPar->gamma0 - 1; // Pressure
  csound2 *= ramsesPar->gamma0 / rho; // Sound speed

  csound2 = std::fmax(csound2, SC_SQR(ramsesPar->smallc));

  // XXX: this assumes 4pi G = 1 in code units
  ljeans = 2 * M_PI * std::sqrt(csound2 / rho);

  dx = quadrant_length(info->quad_current);

  return dx / ljeans;

} // indicator_ramses_rho_gradient

// =======================================================
// ======CLASS IndicatorFactoryRamses IMPL ===============
// =======================================================

IndicatorFactoryRamses::IndicatorFactoryRamses() :
  IndicatorFactory<Qdata>()
{

  // register the above callback's
  registerIndicator("khokhlov"    , indicator_ramses_khokhlov);
  registerIndicator("rho_gradient", indicator_ramses_rho_gradient);
  registerIndicator("jeans",        indicator_ramses_jeans);
  
} // IndicatorFactoryRamses::IndicatorFactoryRamses

} // namespace ramses

} // namespace canop
