#ifndef ITERATORS_RAMSES_H_
#define ITERATORS_RAMSES_H_

#include "Iterators.h"

namespace canop {

namespace ramses {

/*
 * NB: if you need to add new iterators:
 * 1. resize vector iterator_list_t
 * 2. create a new enum to ease indexing this vector
 * 3. add new iterator to the vector in fill_iterators_list method.
 */

class IteratorsRamses : public IteratorsBase
{

public:
  IteratorsRamses();
  virtual ~IteratorsRamses();

  void fill_iterators_list();

};  // class IteratorsRamses

} // namespace ramses

} // namesapce canop

#endif // ITERATORS_RAMSES_H_
