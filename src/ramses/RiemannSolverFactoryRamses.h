#ifndef RIEMANN_SOLVER_FACTORY_RAMSES_H_
#define RIEMANN_SOLVER_FACTORY_RAMSES_H_

#include "RiemannSolverFactory.h"

#include "ramses/userdata_ramses.h"

namespace canop {

namespace ramses {

/*
 *
 * TODO: [enhancement] Params_t is an alias to a RamsesPar.
 * Maybe could that be a RiemannPar (to be defined) smaller, that contains
 * only what's really needed.
 *
 */

/**
 * Concrete implement of a riemann solver factory for the Ramses scheme.
 *
 * Valid riemann_solver names are "hll" and "hllc".
 * use method registerRiemannSolver to add another one.
 */
class RiemannSolverFactoryRamses : public RiemannSolverFactory<QdataRecons,
							       QdataVar,
							       RamsesPar>
{

  //using QdataVar_t    = canoP::ramses::QdataVar;
  //using QdataRecons_t = canoP::ramses::QdataRecons;
  //using Params_t      = canoP::ramses::RamsesPar;
    
private:
  // make constructor private -- this class is singleton 
  RiemannSolverFactoryRamses();
  RiemannSolverFactoryRamses(const RiemannSolverFactoryRamses&) = delete; // non construction-copyable
  RiemannSolverFactoryRamses& operator=(const RiemannSolverFactoryRamses &) {return *this;} // non-copyable

public:
  ~RiemannSolverFactoryRamses() {};

  static RiemannSolverFactoryRamses& Instance()
  {
    static RiemannSolverFactoryRamses instance;
    return instance;
  }
  
}; // class RiemannSolverFactoryRamses

} // namespace ramses

} // namespace canop

#endif // RIEMANN_SOLVER_FACTORY_RAMSES_H_
