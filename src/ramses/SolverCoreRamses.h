#ifndef SOLVER_CORE_RAMSES_H_
#define SOLVER_CORE_RAMSES_H_

#include "SolverCore.h"

#include "userdata_ramses.h"

class ConfigReader;

namespace canop {

namespace ramses {

/**
 * This is were Initial and Border Conditions callbacks are setup.
 */
class SolverCoreRamses : public SolverCore<Qdata>
{

public:
  SolverCoreRamses(ConfigReader *cfg);
  ~SolverCoreRamses();

  /**
   * Static creation method called by the solver factory.
   */
  static SolverCoreBase* create(ConfigReader *cfg)
  {
    return new SolverCoreRamses(cfg);
  }
  
}; // SolverCoreRamses

} // namespace ramses

} // namespace canop

#endif // SOLVER_CORE_RAMSES_H_
