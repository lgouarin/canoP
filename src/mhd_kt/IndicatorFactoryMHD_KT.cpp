#include "IndicatorFactoryMHD_KT.h"
#include "utils.h" // for UNUSED macro

namespace canop {

namespace mhd_kt {

// remember that Qdata is a concrete type defined in userdata_mhd_kt.h
using qdata_t = Qdata;


// =======================================================
// =======================================================
double
indicator_mhd_kt_khokhlov (qdata_t * cella, qdata_t * cellb,
			   indicator_info_t * info)
{
  UNUSED (info);
  double              U[2] = { cella->w.rho, cellb->w.rho };
  
  /* equivalent to indicator_U_gradient */
  return indicator_scalar_gradient (U[0], U[1]);
  
} // indicator_mhd_kt_khokhlov

// =======================================================
// =======================================================
double
indicator_mhd_kt_U_gradient (qdata_t * cella, qdata_t * cellb,
			     indicator_info_t * info)
{
  UNUSED (info);
  double              U[2] = { cella->w.rhoU, cellb->w.rhoU };

  return indicator_scalar_gradient (U[0], U[1]);
  
} // indicator_mhd_kt_U_gradient

// =======================================================
// ======CLASS IndicatorFactoryMHD_KT IMPL ===============
// =======================================================

IndicatorFactoryMHD_KT::IndicatorFactoryMHD_KT() :
  IndicatorFactory<Qdata>()
{

  // register the above callback's
  registerIndicator("khokhlov"    , indicator_mhd_kt_khokhlov);
  registerIndicator("U_gradient"  , indicator_mhd_kt_U_gradient);
  registerIndicator("rho_gradient", indicator_mhd_kt_khokhlov);
  
} // IndicatorFactoryMHD_KT::IndicatorFactoryMHD_KT

} // namespace mhd_kt

} // namespace canoP
