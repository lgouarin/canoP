#ifndef INDICATOR_FACTORY_MHD_KT_H_
#define INDICATOR_FACTORY_MHD_KT_H_

#include "IndicatorFactory.h"

#include "mhd_kt/userdata_mhd_kt.h"

namespace canop {

namespace mhd_kt {

/**
 * Concrete implement of an indicator factory for the MHD_KT scheme.
 *
 * Valid indicator names are "khokhlov" and "U_gradient".
 * use method registerIndicator to add another one.
 */
class IndicatorFactoryMHD_KT : public IndicatorFactory<Qdata>
{

public:
  IndicatorFactoryMHD_KT();
  virtual ~IndicatorFactoryMHD_KT() {};

}; // class IndicatorFactoryMHD_KT

} // namespace mhd_kt

} // namespace canoP

#endif // INDICATOR_FACTORY_MHD_KT_H_
