#include "BoundaryConditionsFactoryMHD_KT.h"

#include "SolverMHD_KT.h"
#include "UserDataManagerMHD_KT.h"
#include "quadrant_utils.h"

// remember that qdata_t is here a concrete type defined in userdata_mhd_kt.h

#include "mhd_kt/userdata_mhd_kt.h"
#include "ConfigReader.h"

namespace canop {

namespace mhd_kt {

using qdata_t               = Qdata; 
using qdata_reconstructed_t = QdataRecons;

// =======================================================
// ======BOUNDARY CONDITIONS CALLBACKS====================
// =======================================================


// =======================================================
// =======================================================
/*
 * TODO: maybe just make the boundary functions return a qdata_variables_t
 * that they have initialized and then we do the copying around.
 *
 * TODO: for order 2, how does delta have to be initialized? does it?
 */
void
bc_mhd_kt_dirichlet (p4est_t * p4est,
		     p4est_topidx_t which_tree,
		     p4est_quadrant_t * q,
		     int face,
		     qdata_t * ghost_qdata)
{
  
  UNUSED (p4est);
  UNUSED (which_tree);
  UNUSED (q);
  UNUSED (face);
  UNUSED (ghost_qdata);
  
} // bc_mhd_kt_dirichlet

// =======================================================
// =======================================================
void
bc_mhd_kt_neuman (p4est_t * p4est,
		  p4est_topidx_t which_tree,
		  p4est_quadrant_t * q,
		  int face,
		  qdata_t * ghost_qdata)
{
  UNUSED (p4est);
  UNUSED (which_tree);
  UNUSED (face);

  // retrieve our Solver, UserDataManager and finaly our quadrant data
  SolverMHD_KT    *solver = static_cast<SolverMHD_KT*>(solver_from_p4est (p4est));

  UserDataManagerMHD_KT
    *userdata_mgr = static_cast<UserDataManagerMHD_KT *> (solver->get_userdata_mgr());

  qdata_t            *qdata = userdata_mgr->quadrant_get_qdata (q);

  /* 
   * copy the entire qdata
   */
  userdata_mgr->qdata_copy(ghost_qdata, qdata);

} // bc_mhd_kt_neuman

// =======================================================
// ======CLASS BoundaryConditionsFactoryMHD_KT IMPL ======
// =======================================================
BoundaryConditionsFactoryMHD_KT::BoundaryConditionsFactoryMHD_KT() :
  BoundaryConditionsFactory<Qdata>()
{

  // register the above callback's
  registerBoundaryConditions("dirichlet" , bc_mhd_kt_dirichlet);
  registerBoundaryConditions("neuman"    , bc_mhd_kt_neuman);

} // BoundaryConditionsFactoryMHD_KT::BoundaryConditionsFactoryMHD_KT

} // namespace mhd_kt

} // namespace canoP
