#include "QuadrantNeighborUtils.h"
#include "quadrant_utils.h"

#ifndef P4_TO_P8
#include <p4est_bits.h>
#include <p4est_mesh.h>
#include <p4est_geometry.h>
#else
#include <p8est_bits.h>
#include <p8est_mesh.h>
#include <p8est_geometry.h>
#endif

#include <sc.h> // for SC_LP_ERROR macro
#include <cmath>
#include <iostream>

#include "SolverMHD_KT.h"
#include "SolverCoreMHD_KT.h"
#include "SolverWrap.h"
#include "IteratorsMHD_KT.h"
#include "UserDataManagerMHD_KT.h"
#include "userdata_mhd_kt.h"

#include "IndicatorFactoryMHD_KT.h"
#include "Euler.h"
#include "mhd.h"
//#include "RiemannSolverFactoryMHD_KT.h"

namespace canop {

namespace mhd_kt {

// get userdata specific stuff
using qdata_t               = Qdata;
using qdata_variables_t     = QdataVar;
using qdata_reconstructed_t = QdataRecons;

using UserDataTypesMHD_KT = UserDataTypes<Qdata,
					  QdataVar,
					  QdataRecons>;

using QuadrantNeighborUtilsMHD_KT = QuadrantNeighborUtils<UserDataTypesMHD_KT>;

constexpr int IT_IDD::LIMITER;
constexpr int IT_IDD::BFIELD_FACE;
constexpr int IT_IDD::UPDATE_B_DIFFUSIVE;
constexpr int IT_IDD::DIV_B;
constexpr int IT_IDD::TRANSP;
constexpr int IT_IDD::LIMITER_GLM_SUBCYCLED;
constexpr int IT_IDD::UPDATE_GLM_SUBCYCLED;
constexpr int IT_IDD::COPY_MAG_FIELD;

// =======================================================
// =======================================================
static void
iterator_start_step (p4est_iter_volume_info_t * info,
		     void *user_data)
{
  UNUSED (user_data);

  // retrieve our Solver, UserDataManager and finaly our quadrant data
  SolverMHD_KT *solver = static_cast<SolverMHD_KT*>(solver_from_p4est (info->p4est));

  UserDataManagerMHD_KT
    *userdata_mgr = static_cast<UserDataManagerMHD_KT *> (solver->get_userdata_mgr());

  userdata_mgr->quadrant_copy_w_to_wnext(info->quad);
  
} // iterator_start_step

// =======================================================
// =======================================================
static void iterator_end_step (p4est_iter_volume_info_t * info,
			       void *user_data)
{
  UNUSED (user_data);

  // retrieve our Solver, UserDataManager and finaly our quadrant data
  SolverMHD_KT    *solver = static_cast<SolverMHD_KT*>(solver_from_p4est (info->p4est));

  UserDataManagerMHD_KT
    *userdata_mgr = static_cast<UserDataManagerMHD_KT *> (solver->get_userdata_mgr());

  userdata_mgr->quadrant_copy_wnext_to_w(info->quad);
  
} // iterator_end_step

// =======================================================
// =======================================================
static void iterator_mhd_kt_copy_mag_field (p4est_iter_volume_info_t * info,
				     void *user_data)
{
  UNUSED (user_data);

  // retrieve our Solver, UserDataManager and finaly our quadrant data
  SolverMHD_KT    *solver = static_cast<SolverMHD_KT*>(solver_from_p4est (info->p4est));
  
  UserDataManagerMHD_KT
    *userdata_mgr = static_cast<UserDataManagerMHD_KT *> (solver->get_userdata_mgr());
  
  userdata_mgr->quadrant_copy_wnext_to_w_mag(info->quad);
  
} // iterator_mhd_kt_copy_mag_field

// =======================================================
// =======================================================
static void iterator_mark_adapt (p4est_iter_volume_info_t * info,
				 void *user_data)
{
  UNUSED (user_data);

  p4est_t            *p4est = info->p4est;
  Solver             *solver = solver_from_p4est (info->p4est);
  SolverWrap         *solverWrap = solver->m_wrap;
  UserDataManagerMHD_KT
    *userdata_mgr = static_cast<UserDataManagerMHD_KT *> (solver->get_userdata_mgr());

  //SolverMHD_KT       *solverMHD_KT = static_cast<SolverMHD_KT *>(solver);
  SolverCoreMHD_KT   *solverCore = static_cast<SolverCoreMHD_KT *>(solver->m_solver_core);
  
  p4est_ghost_t      *ghost  = solver->get_ghost();
  p4est_mesh_t       *mesh   = solver->get_mesh();
  qdata_t            *ghost_data = static_cast<qdata_t *>(userdata_mgr->get_ghost_data_ptr());
  qdata_t            *qdata;
  int                 level = info->quad->level;

  int                 boundary = 0;
  int                 face = 0;
  double              eps = 0.0;
  double              eps_max = -1.0;

  p4est_quadrant_t   *neighbor = nullptr;
  qdata_t            *ndata = nullptr;

  //indicator_t         indicator = solver->m_indicator_fn;
  indicator_info_t    iinfo;
  iinfo.face = 0;
  iinfo.epsilon = solver->m_epsilon_refine;
  iinfo.quad_current = info->quad;
  //iinfo.user_data = static_cast<void*>(&(solverMHD_KT->mhd_ktPar));

  /* init face neighbor iterator */
  p4est_mesh_face_neighbor_t mfn;
  p4est_mesh_face_neighbor_init2 (&mfn, p4est, ghost, mesh,
                                  info->treeid, info->quadid);

  /* loop over neighbors */
  neighbor = p4est_mesh_face_neighbor_next (&mfn, NULL, NULL, NULL, NULL);
  while (neighbor) {
    ndata = QuadrantNeighborUtilsMHD_KT::mesh_face_neighbor_data (&mfn, ghost_data, &boundary);

    /* mfn.face is initialized to 0 by p4est_mesh_face_neighbor_init2 and
     * is incremented:
     *  * when calling p4est_mesh_face_neighbor_next and the neighbor has
     *    smaller or equal level
     *  * when calling p4est_mesh_face_neighbor_next and the neighbor has a
     *    bigger level, but only after seeing the 2 (or 4) smaller neighbors.
     */
    iinfo.face = mfn.face + (mfn.subface > 0) - 1;
    iinfo.quad_neighbor = neighbor;

    /* compute the indicator */
    qdata = userdata_mgr->quadrant_get_qdata (info->quad);
    eps = solverCore->m_indicator_fn (qdata, ndata, &iinfo);
    eps_max = SC_MAX (eps_max, eps);

    /* if the neighbor is on the boundary, the data was allocated on the fly,
     * so we have to deallocate it.
     */
    if (boundary == 1) {
      sc_mempool_free (info->p4est->user_data_pool, ndata);
    }

    neighbor = p4est_mesh_face_neighbor_next (&mfn, NULL, NULL, &face, NULL);

  } // end while. all neighbors visited.

  /* we suppose that epsilon_refine >= epsilon_coarsen */
  if (level > solver->m_min_refine && eps_max < solver->m_epsilon_coarsen) {
    solverWrap->mark_coarsen(info->treeid, info->quadid);
  } else if (level < solver->m_max_refine && eps_max > solver->m_epsilon_refine) {
    solverWrap->mark_refine(info->treeid, info->quadid);
  } else {
    return;
  }

} // iterator_mark_adapt
// =======================================================
// =======================================================
static double compute_speed_mhd(const qdata_variables_t &qdata, double gamma_adiab)
{
  
  double Ekin= 0.5*((qdata.rhoU*qdata.rhoU+qdata.rhoV*qdata.rhoV)/(qdata.rho));
  double Emag = (qdata.Bx*qdata.Bx+qdata.By*qdata.By)/(8.0*M_PI);
#ifdef P4_TO_P8
  Ekin +=0.5*qdata.rhoW*qdata.rhoW/(qdata.rho);
  Emag +=(qdata.Bz*qdata.Bz)/(8.0*M_PI);
#endif
  double Eint=qdata.E-Ekin-Emag;

  double pressure = (gamma_adiab-1)*Eint;
  double speedsound=sqrt(gamma_adiab*pressure/(qdata.rho));
  double speedmhd=0.0; 
  speedmhd = sqrt(speedsound*speedsound +(qdata.Bx*qdata.Bx+qdata.By*qdata.By)/(4.0*M_PI*qdata.rho));
#ifdef P4_TO_P8
  speedmhd = sqrt(speedsound*speedsound +(qdata.Bx*qdata.Bx+qdata.By*qdata.By+qdata.Bz*qdata.Bz)/(4.0*M_PI*qdata.rho));
#endif   

  return speedmhd;
}

// =======================================================
// =======================================================
double compute_speed_sound(const qdata_variables_t &qdata, double gamma_adiab)
{
  
  double Ekin= 0.5*((qdata.rhoU*qdata.rhoU+qdata.rhoV*qdata.rhoV)/(qdata.rho));
  double Emag = (qdata.Bx*qdata.Bx+qdata.By*qdata.By)/(8.0*M_PI);
#ifdef P4_TO_P8
  Ekin +=0.5*qdata.rhoW*qdata.rhoW/(qdata.rho); 
  Emag +=(qdata.Bz*qdata.Bz)/(8.0*M_PI);
#endif
  double Eint=qdata.E-Ekin-Emag;
  
  double pressure = (gamma_adiab-1)*Eint; 
  double speedsound=sqrt(gamma_adiab*pressure/(qdata.rho));

  return speedsound;
}
// =======================================================
// =======================================================
double compute_ami_mhd(const qdata_variables_t &qdataL,
		       const qdata_variables_t &qdataR,
		       int dir,
		       double gamma)
{

  // comute spedd of sound
  double cL  = compute_speed_sound(qdataL,gamma);
  double cR  = compute_speed_sound(qdataR,gamma);
  double cL2 = cL*cL;
  double cR2 = cR*cR;

  // Compute alfven speed
  double vaL = sqrt((qdataL.Bx*qdataL.Bx+qdataL.By*qdataL.By)/(4.0*M_PI*qdataL.rho));
  double vaR = sqrt((qdataR.Bx*qdataR.Bx+qdataR.By*qdataR.By)/(4.0*M_PI*qdataR.rho));

#ifdef P4_TO_P8
  vaL = sqrt((qdataL.Bx*qdataL.Bx+qdataL.By*qdataL.By+qdataL.Bz*qdataL.Bz)/(4.0*M_PI*qdataL.rho));
  vaR = sqrt((qdataR.Bx*qdataR.Bx+qdataR.By*qdataR.By+qdataR.Bz*qdataR.Bz)/(4.0*M_PI*qdataR.rho)) ;
#endif
  
  double vaL2 = vaL*vaL;
  double vaR2 = vaR*vaR;
  double ami  = 0.0;

  if (dir==IX) {

    double amiL = sqrt(0.5*(vaL2 + cL2 + sqrt((vaL2+cL2)*(vaL2+cL2) - qdataL.Bx*qdataL.Bx*cL2/(M_PI*(qdataL.rho)))));
    double amiR = sqrt(0.5*(vaR2 + cR2 + sqrt((vaR2+cR2)*(vaR2+cR2) - qdataR.Bx*qdataR.Bx*cR2/(M_PI*(qdataR.rho)))));
    ami = SC_MAX( fabs(qdataL.rhoU/qdataL.rho) + amiL,
		  fabs(qdataR.rhoU/qdataR.rho) + amiR);
    
  }

  else if (dir==IY) {
    
    double amiL = sqrt(0.5*(vaL2 + cL2 + sqrt((vaL2+cL2)*(vaL2+cL2) - qdataL.By*qdataL.By*cL2/(M_PI*(qdataL.rho)))));
    double amiR = sqrt(0.5*(vaR2 + cR2 + sqrt((vaR2+cR2)*(vaR2+cR2) - qdataR.By*qdataR.By*cR2/(M_PI*(qdataR.rho)))));
    ami = SC_MAX( fabs(qdataL.rhoV/qdataL.rho) + amiL,
		  fabs(qdataR.rhoV/qdataR.rho) + amiR);
    
  }
  
#ifdef P4_TO_P8
  else if (dir==IZ) {

    double amiL = sqrt(0.5*(vaL2 + cL2 + sqrt((vaL2+cL2)*(vaL2+cL2) - qdataL.Bz*qdataL.Bz*cL2/(M_PI*(qdataL.rho)))));
    double amiR = sqrt(0.5*(vaR2 + cR2 + sqrt((vaR2+cR2)*(vaR2+cR2) - qdataR.Bz*qdataR.Bz*cR2/(M_PI*(qdataR.rho)))));
    ami = SC_MAX( fabs(qdataL.rhoW/qdataL.rho) + amiL,
		  fabs(qdataR.rhoW/qdataR.rho) + amiR);
    
  }   
#endif
  return ami;
  
} // compute_ami_mhd


// =======================================================
// =======================================================
double compute_ami_euler(const qdata_variables_t &qdataL,
			 const qdata_variables_t &qdataR,
			 int dir,
			 double gamma)
{

  // comute spedd of sound
  double cL = compute_speed_sound(qdataL,gamma);
  double cR = compute_speed_sound(qdataR,gamma);
  double ami=0.0;
  
  if (dir==IX) {
    ami = SC_MAX(qdataL.rhoU/qdataL.rho+cL,qdataR.rhoU/qdataR.rho+cR);
  }
  else if (dir==IY) {
    ami = SC_MAX(qdataL.rhoV/qdataL.rho+cL,qdataR.rhoV/qdataR.rho+cR);
  }
#ifdef P4_TO_P8
  else if (dir==IZ) {
    ami = SC_MAX(qdataL.rhoW/qdataL.rho+cL,qdataR.rhoW/qdataR.rho+cR);
  }    
#endif
  return ami;
} //

// =======================================================
// =======================================================
/**
 * \brief Compute the time step as dt.
 *
 * An iterator over all the cells to compute this minimum.
 * The result is stored in the user_data.
 */
static void iterator_time_step (p4est_iter_volume_info_t * info,
				void *user_data)
{

  UNUSED (user_data);

  /* get forest structure */
  
  SolverMHD_KT
    *solver = static_cast<SolverMHD_KT*>(solver_from_p4est (info->p4est));
  
  UserDataManagerMHD_KT
    *userdata_mgr = static_cast<UserDataManagerMHD_KT *> (solver->get_userdata_mgr());

  double             *min_dt = (double *) user_data;

  qdata_t            *data = userdata_mgr->quadrant_get_qdata (info->quad);

  double dx,dy,dz;
  double U,V,W;
  UNUSED(dz);
  UNUSED(W);
  
  ConfigReader       *cfg = solver->m_cfg;

  double gamma = cfg->config_read_double ("settings.gamma0", 5.0/3);

  dx = dy = dz = quadrant_length (info->quad);
  //double c = compute_speed_sound(data->w, gamma);
  double cf = compute_speed_mhd(data->w, gamma);

  U=data->w.rhoU/(data->w.rho);
  V=data->w.rhoV/(data->w.rho);
#ifdef P4_TO_P8
  W=data->w.rhoW/(data->w.rho);
#endif

  *min_dt = SC_MIN(*min_dt, dx/(fabs(U)+cf) );
  *min_dt = SC_MIN(*min_dt, dy/(fabs(V)+cf) );
#ifdef P4_TO_P8
  *min_dt = SC_MIN(*min_dt, dz/(fabs(W)+cf) );
#endif

  /* also compute the min and max levels in the mesh */
  solver->m_minlevel = SC_MIN (solver->m_minlevel, info->quad->level);
  solver->m_maxlevel = SC_MAX (solver->m_maxlevel, info->quad->level);
  
} // iterator_time_step

// =======================================================
// =======================================================
/**
 * \brief Compute and store magnetic field at faces.
 *
 * For each cell (leaf), compute Bxfy and Byfx from cell-center magnetic field components. 
 *
 */
static void
iterator_mhd_kt_compute_Bfield_face (p4est_iter_volume_info_t * info, 
				     void *user_data)
{

  /* get forest structure */
  p4est_t              *p4est = info->p4est;
  
  /* get solver and geometry (only for unstructured) */
  SolverMHD_KT
    *solver = static_cast<SolverMHD_KT*>(solver_from_p4est (p4est));

  UserDataManagerMHD_KT
    *userdata_mgr = static_cast<UserDataManagerMHD_KT *> (solver->get_userdata_mgr());
  
  int                *direction = (int *) user_data;
  double              dx  , dy  , dz  ; // current  cell
  double              dx_n, dy_n, dz_n; // neighbor cell
  double              dx_min;
  
  p4est_quadrant_t   *quad   = info->quad;
  p4est_locidx_t      quadid = info->quadid;
  p4est_topidx_t      treeid = info->treeid;
  
  /* 2 states: one for each side of the face */
  qdata_t            *data[2] = { NULL, NULL };

  /* the face Id in range 0..P4EST_FACES-1 */
  int                 qf[2] = { -1, -1 };

  double              scale = 0.0;
  quadrant_neighbor_t<qdata_t> n;

  /* geometrical terms */
  int                 dir_start = (direction == NULL ? 0 : *direction);
  int                 dir_end = (direction == NULL ? P4EST_DIM : *direction + 1);

  double              Bxfy, Byfx; // 2D
#ifdef P4_TO_P8
  double              Byfz, Bzfy; // 3D only
  double              Bzfx, Bxfz; // 3D only
  UNUSED(Byfz);
  UNUSED(Bzfy);
  UNUSED(Bzfx);
  UNUSED(Bxfz);
#endif // P4_TO_P8
  
  UNUSED (scale);
  UNUSED (dx_min);

  /* get current cell data */
  data[CELL_CURRENT] = userdata_mgr->quadrant_get_qdata (quad);
  
  /* current cell length / location */

  //quadrant_get_dx_dy_dz(conn, NULL, quad, treeid, &dx, &dy, &dz);
  dx = dy = dz = quadrant_length (info->quad);

  // for each dir
  /* loop over all quadrant's faces */
  for( int dir = dir_start; dir < dir_end; ++dir) {

    // we always look at faces +X, +Y, +Z
    
    // get the right face in the given direction
    qf[CELL_CURRENT] = QuadrantNeighborUtilsBase::direction_faces[dir][1];      
    
    /* 
     * get the Qdata of the neighbors on this face 
     * It is important to realize here that, if face touches the outside
     * boundary, there will be a call to our boundary_fn callback
     * which will fill the wm, wp states
     */
    QuadrantNeighborUtilsMHD_KT::quadrant_get_face_neighbor (solver,
							     quadid,
							     treeid,
							     qf[CELL_CURRENT],
							     &n);
    qf[CELL_NEIGHBOR] = n.face;

    /* 
     *
     */
    if (n.is_hanging) {

      dx_n = dy_n = dz_n = dx*0.5;

      // compute average neighbor magnetic component
      double Baverage=0;
      for (int i = 0; i < P4EST_HALF; ++i) {
	
	/* perform update */
	data[CELL_NEIGHBOR] = n.is.hanging.data[i];

	if (dir ==IX)
	  Baverage += data[CELL_NEIGHBOR]->w.By;
	if (dir ==IY)
	  Baverage += data[CELL_NEIGHBOR]->w.Bx;

      } // end for i=0..P4EST_HELF
      Baverage /= P4EST_HALF;

      // now interpolation
      // 2D
      if (dir == IX) {
	// compute Byfx as a linear interpolation
	Byfx = (dx*data[CELL_CURRENT]->w.By + dx_n*Baverage) / (dx+dx_n);
      } else {
	// compute Byfx as a linear interpolation
	Bxfy = (dx*data[CELL_CURRENT]->w.Bx + dx_n*Baverage) / (dx+dx_n);
      }      
      
    } else {  // not hanging

      // check if neighbor larger than current cell
      if (n.level < info->quad->level)
	dx_n = dy_n = dz_n = dx*2;
      else
	dx_n = dy_n = dz_n = dx;

      data[CELL_NEIGHBOR] = n.is.full.data;

      // 2D
      if (dir == IX) {
	// compute Byfx as a linear interpolation
	Byfx = (dx*(data[CELL_CURRENT]->w.By) + dx_n*(data[CELL_NEIGHBOR]->w.By) ) / (dx+dx_n);
        
      } else {
	// compute Bxfy as a linear interpolation
	Bxfy = (dx*(data[CELL_CURRENT]->w.Bx) + dx_n*(data[CELL_NEIGHBOR]->w.Bx)) / (dx+dx_n);
      }
      
    } // end if (n.is_hanging)

    /* 
     * if cell is on the outside boundary, we need to free the extra "ghost" 
     * quadrant
     */
    if (n.tree_boundary) {
      sc_mempool_free (info->p4est->user_data_pool, n.is.full.data);
    }
    
  } // end for dir

  data[CELL_CURRENT]->w.Bxfy = Bxfy;
  data[CELL_CURRENT]->w.Byfx = Byfx;


#ifdef P4_TO_P8
  // data[CELL_CURRENT]->w.Bzfx = Bzfx;
  // data[CELL_CURRENT]->w.Bzfy = Bzfy;  
  // data[CELL_CURRENT]->w.Bxfz = Bxfz;
  // data[CELL_CURRENT]->w.Byfz = Byfz;
#endif // P4_TO_P8
  
} // iterator_mhd_kt_compute_Bfield_face

// =======================================================
// =======================================================
/**
 * \brief Compute transport properties
 *
 *
 */
static void
iterator_mhd_kt_transp (p4est_iter_volume_info_t * info, 
			  void *user_data)
{
  UNUSED(info);
  UNUSED(user_data);
  
#ifdef USE_MUTATIONPP

  /* get forest structure */
  p4est_t              *p4est = info->p4est;
  
  /* get solver and geometry (only for unstructured) */
  SolverMHD_KT       *solver = static_cast<SolverMHD_KT*>(solver_from_p4est (p4est));

  UserDataManagerMHD_KT
    *userdata_mgr = static_cast<UserDataManagerMHD_KT *> (solver->get_userdata_mgr());

  qdata_t            *data[2] = { NULL, NULL};

  int                *direction = (int *) user_data;
  
  p4est_quadrant_t   *quad   = info->quad;
  

  /* get current cell data */
  data[CELL_CURRENT] = userdata_mgr->quadrant_get_qdata (quad);
  
  /* current cell length / location */

  //quadrant_get_dx_dy_dz(conn, NULL, quad, treeid, &dx, &dy, &dz);
  //dx = dy = dz = quadrant_length (info->quad);
  
  double gamma = solver->m_cfg->config_read_double ("settings.gamma0", 5.0/3);
  

  double T0 = 10000; // Temperature in Kelvin
  double P0 = 100000; // Pressure in Pascal
  //double rho0 = 0.00139274; // Density in kg.m-3 for a perfect gas Helium-Hydrogen mixture  
  
  double Ekin= 0.5*((data[CELL_CURRENT]->w.rhoU*data[CELL_CURRENT]->w.rhoU+data[CELL_CURRENT]->w.rhoV*data[CELL_CURRENT]->w.rhoV)/(data[CELL_CURRENT]->w.rho));
  double Emag = (data[CELL_CURRENT]->w.Bx*data[CELL_CURRENT]->w.Bx+data[CELL_CURRENT]->w.By*data[CELL_CURRENT]->w.By)/(8.0*M_PI);
#ifdef P4_TO_P8
  Ekin +=0.5*data[CELL_CURRENT]->w.rhoW*data[CELL_CURRENT]->w.rhoW/(data[CELL_CURRENT]->w.rho);
  Emag +=(data[CELL_CURRENT]->w.Bz*data[CELL_CURRENT]->w.Bz)/(8.0*M_PI);
#endif
  double Eint=data[CELL_CURRENT]->w.E-Ekin-Emag;

  double pressure = (gamma-1)*Eint;
  double temperature = pressure/(data[CELL_CURRENT]->w.rho);

  double T=T0*temperature;
  double P=P0*pressure;

  solver->m_mix->setState(&T,&P);
  // std::cout << std::setw(13) << Solver.m_mix->parallelElectronThermalConductivity();
  data[CELL_CURRENT]->w.lam_h= solver->m_mix->heavyThermalConductivity();
  data[CELL_CURRENT]->w.eta_h= solver->m_mix->viscosity();
  data[CELL_CURRENT]->w.lam_e= solver->m_mix->parallelElectronThermalConductivity();

  data[CELL_CURRENT]->wnext.lam_h=data[CELL_CURRENT]->w.lam_h ;
  data[CELL_CURRENT]->wnext.eta_h=data[CELL_CURRENT]->w.eta_h ;
  data[CELL_CURRENT]->wnext.lam_e=data[CELL_CURRENT]->w.lam_e ;
#endif // USE_MUTATIONPP
  
} // iterator_mhd_kt_transp


// =======================================================
// =======================================================
/**
 * \brief Update div(B) in the Qdata.
 *
 *
 */
static void
iterator_mhd_kt_div_b (p4est_iter_volume_info_t * info, 
			  void *user_data)
{

  /* get forest structure */
  p4est_t              *p4est = info->p4est;
  
  /* get solver and geometry (only for unstructured) */
  SolverMHD_KT       *solver = static_cast<SolverMHD_KT*>(solver_from_p4est (p4est));

  UserDataManagerMHD_KT
    *userdata_mgr = static_cast<UserDataManagerMHD_KT *> (solver->get_userdata_mgr());


  int                *direction = (int *) user_data;
  double              dx  , dy  , dz  ; // current  cell
  //double              dx_L, dy_L, dz_L; // left neighbor cell
  double              dx_R, dy_R, dz_R; // right neighbor cell
  
  p4est_quadrant_t   *quad   = info->quad;
  p4est_locidx_t      quadid = info->quadid;
  p4est_topidx_t      treeid = info->treeid;

  /* 2 states: one for each neighbor */
  qdata_t            *data[2] = { NULL, NULL };
  
  /* the face Id in range 0..P4EST_FACES-1 */
  int                 qfL[2] = { -1, -1 };
  int                 qfR[2] = { -1, -1 };

  // left and right neighor along a given direction
  quadrant_neighbor_t<qdata_t> nL, nR;

  /* geometrical terms */
  int                 dir_start = (direction == NULL ? 0 : *direction);
  int                 dir_end = (direction == NULL ? P4EST_DIM : *direction + 1);

  /* get current cell data */
  data[CELL_CURRENT] = userdata_mgr->quadrant_get_qdata (quad);
  
  /* current cell length / location */

  //quadrant_get_dx_dy_dz(conn, NULL, quad, treeid, &dx, &dy, &dz);
  dx = dy = dz = quadrant_length (info->quad);

  double dBxdx   ;
  double dBydy   ;
#ifdef P4_TO_P8
  double dBzdz;
#endif // P4_TO_P8

  /* loop over all quadrant's faces */
  for( int dir = dir_start; dir < dir_end; ++dir) {
    
    // get both left and right neighbors
    qfL[CELL_CURRENT] = QuadrantNeighborUtilsBase::direction_faces[dir][0];      
    qfR[CELL_CURRENT] = QuadrantNeighborUtilsBase::direction_faces[dir][1];      
    
    /* 
     * get the Qdata of the neighbors on this face 
     * It is important to realize here that, if face touches the outside
     * boundary, there will be a call to our boundary_fn callback
     * which will fill the wm, wp states
     */
    QuadrantNeighborUtilsMHD_KT::quadrant_get_face_neighbor (solver,
							     quadid,
							     treeid,
							     qfL[CELL_CURRENT],
							     &nL);
    QuadrantNeighborUtilsMHD_KT::quadrant_get_face_neighbor (solver,
							     quadid,
							     treeid,
							     qfR[CELL_CURRENT],
							     &nR);
    qfL[CELL_NEIGHBOR] = nL.face;
    qfR[CELL_NEIGHBOR] = nR.face;


    if (nR.is_hanging) {

      dx_R = dy_R = dz_R = dx*0.5;
      
      // looking for the upper right neighbor - index 1
      if (dir==IX) {
	double tmp = 0;
	for (int i = 0; i < P4EST_HALF; ++i) {
	  tmp += nR.is.hanging.data[i]->w.Bx;
	}
	dBxdx = ((tmp/P4EST_HALF - data[CELL_CURRENT]->w.Bx) / (dx_R+dx)) * 2;
      }
      if (dir==IY) {
	double tmp = 0;
	for (int i = 0; i < P4EST_HALF; ++i) {
	  tmp += nR.is.hanging.data[i]->w.By;
	}
	dBydy = ((tmp/P4EST_HALF - data[CELL_CURRENT]->w.By) / (dx_R+dx)) * 2;
      }
#ifdef P4_TO_P8
      if (dir==IZ) {
	double tmp = 0;
	for (int i = 0; i < P4EST_HALF; ++i) {
	  tmp += nR.is.hanging.data[i]->w.Bz;
	}
	dBzdz = ((tmp/P4EST_HALF - data[CELL_CURRENT]->w.Bz) / (dx_R+dx)) * 2;
      }
      
#endif // P4_TO_P8
      
    } else {  // not hanging

      // check if neighbor larger than current cell
      if (nR.level < info->quad->level)
	dx_R = dy_R = dz_R = dx*2;
      else
	dx_R = dy_R = dz_R = dx;
      
      // looking for the upper right neighbor - index 1
      if (dir==IX) {
	dBxdx = ((nR.is.full.data->w.Bx - data[CELL_CURRENT]->w.Bx) / (dx_R+dx)) * 2;
      }
      if (dir==IY) {
	dBydy = ((nR.is.full.data->w.By - data[CELL_CURRENT]->w.By) / (dx_R+dx)) * 2;
      }
#ifdef P4_TO_P8
      if (dir==IZ) {
	dBzdz = ((nR.is.full.data->w.Bz - data[CELL_CURRENT]->w.Bz) / (dx_R+dx)) * 2;
      }      
#endif // P4_TO_P8
      
    } // end if (nR.is_hanging)

   data[CELL_CURRENT]->wnext.divB = dBxdx + dBydy;
#ifdef P4_TO_P8
   data[CELL_CURRENT]->wnext.divB += dBzdz;
#endif // P4_TO_P8
   
    /* 
     * if cell is on the outside boundary, we need to free the extra "ghost" 
     * quadrant
     */
    if (nL.tree_boundary) {
      sc_mempool_free (info->p4est->user_data_pool, nL.is.full.data);
    }
    if (nR.tree_boundary) {
      sc_mempool_free (info->p4est->user_data_pool, nR.is.full.data);
    }    
  } // end for dir

} // iterator_mhd_kt_div_b

// =======================================================
// =======================================================
/**
 * \brief Update magnetic field (cell-center) with diffusive div B=0 constraint.
 *
 * This iterator must called after iterator_mhd_kt_update; this is why
 * we update directly wnext and NOT w.
 *
 * Only called when using the diffusive divergence cleaning method.
 *
 */
static void
iterator_mhd_kt_update_b_diffusive (p4est_iter_volume_info_t * info, 
				    void *user_data)
{

  /* get forest structure */
  p4est_t              *p4est = info->p4est;
  
  /* get solver and geometry (only for unstructured) */
  SolverMHD_KT       *solver = static_cast<SolverMHD_KT*>(solver_from_p4est (p4est));

  UserDataManagerMHD_KT
    *userdata_mgr = static_cast<UserDataManagerMHD_KT *> (solver->get_userdata_mgr());


  int                *direction = (int *) user_data;
  double              dx  , dy  , dz  ; // current  cell
  double              dx_L, dy_L, dz_L; // left neighbor cell
  double              dx_R, dy_R, dz_R; // right neighbor cell
  
  p4est_quadrant_t   *quad   = info->quad;
  p4est_locidx_t      quadid = info->quadid;
  p4est_topidx_t      treeid = info->treeid;

  /* 2 states: one for each neighbor */
  qdata_t            *data[2] = { NULL, NULL };
  
  /* the face Id in range 0..P4EST_FACES-1 */
  int                 qfL[2] = { -1, -1 };
  int                 qfR[2] = { -1, -1 };

  // left and right neighor along a given direction
  quadrant_neighbor_t<qdata_t> nL, nR;

  /* geometrical terms */
  int                 dir_start = (direction == NULL ? 0 : *direction);
  int                 dir_end = (direction == NULL ? P4EST_DIM : *direction + 1);

  /* get current cell data */
  data[CELL_CURRENT] = userdata_mgr->quadrant_get_qdata (quad);
  
  /* current cell length / location */

  //quadrant_get_dx_dy_dz(conn, NULL, quad, treeid, &dx, &dy, &dz);
  dx = dy = dz = quadrant_length (info->quad);

  // to update Bx : dBxdx   , dByfxdyC, dBzfxdzC
  // to update By : dBxfydxC, dBydy   , dBzfydzC
  // to update Bz : dBxfzdxC, dByfzdyC, dBzdz
  double dBxdx   , dByfxdyC, dBzfxdzC;
  double dBxfydxC, dBydy   , dBzfydzC;
  UNUSED(dBzfxdzC); UNUSED(dBzfydzC);
#ifdef P4_TO_P8
  double dBxfzdxC, dByfzdyC, dBzdz;
  UNUSED(dBxfzdxC); UNUSED(dByfzdyC); UNUSED(dBzdz);
#endif // P4_TO_P8
  
  // intermediate varaibles
  double BxfyL, BxfyR;
  double ByfxL, ByfxR;
  
  double Br  = 0.0;
  double Bl  = 0.0;
  double ami = 0.0;
  double gamma = solver->m_cfg->config_read_double ("settings.gamma0", 5.0/3);

  /* loop over all quadrant's faces */
  for( int dir = dir_start; dir < dir_end; ++dir) {
    
    // get both left and right neighbors
    qfL[CELL_CURRENT] = QuadrantNeighborUtilsBase::direction_faces[dir][0];      
    qfR[CELL_CURRENT] = QuadrantNeighborUtilsBase::direction_faces[dir][1];      
    
    /* 
     * get the Qdata of the neighbors on this face 
     * It is important to realize here that, if face touches the outside
     * boundary, there will be a call to our boundary_fn callback
     * which will fill the wm, wp states
     */
    QuadrantNeighborUtilsMHD_KT::quadrant_get_face_neighbor (solver,
							     quadid,
							     treeid,
							     qfL[CELL_CURRENT],
							     &nL);
    QuadrantNeighborUtilsMHD_KT::quadrant_get_face_neighbor (solver,
							     quadid,
							     treeid,
							     qfR[CELL_CURRENT],
							     &nR);
    qfL[CELL_NEIGHBOR] = nL.face;
    qfR[CELL_NEIGHBOR] = nR.face;

    /* 
     * in2d, compute : 
     *   - Bxfy gradient along x
     *   - Byfx gradient along y
     */
    if (nL.is_hanging) {

      dx_L = dy_L = dz_L = dx*0.5;
      
      // looking for the upper right neighbor - index 1
      if (dir==IX)
	BxfyL = nL.is.hanging.data[1]->w.Bxfy;
      if (dir==IY)
	ByfxL = nL.is.hanging.data[1]->w.Byfx;
      
    } else {  // not hanging

      // check if neighbor larger than current cell
      if (nL.level < info->quad->level)
	dx_L = dy_L = dz_L = dx*2;
      else
	dx_L = dy_L = dz_L = dx;
      
      // looking for the upper right neighbor - index 1
      if (dir==IX)
	BxfyL = nL.is.full.data->w.Bxfy;
      if (dir==IY)
	ByfxL = nL.is.full.data->w.Byfx;

    } // end if (nL.is_hanging)

    if (nR.is_hanging) {

      dx_R = dy_R = dz_R = dx*0.5;
      
      // looking for the upper right neighbor - index 1
      if (dir==IX) {
	BxfyR = nR.is.hanging.data[1]->w.Bxfy;
	double tmp = 0;
	for (int i = 0; i < P4EST_HALF; ++i) {
	  tmp += nR.is.hanging.data[1]->w.Bx;
	}
	dBxdx = ((tmp/P4EST_HALF - data[CELL_CURRENT]->w.Bx) / (dx_R+dx)) * 2;
      }
      if (dir==IY) {
	ByfxR = nR.is.hanging.data[1]->w.Byfx;
	double tmp = 0;
	for (int i = 0; i < P4EST_HALF; ++i) {
	  tmp += nR.is.hanging.data[1]->w.By;
	}
	dBydy = ((tmp/P4EST_HALF - data[CELL_CURRENT]->w.By) / (dx_R+dx)) * 2;
      }
      
    } else {  // not hanging

      // check if neighbor larger than current cell
      if (nR.level < info->quad->level)
	dx_R = dy_R = dz_R = dx*2;
      else
	dx_R = dy_R = dz_R = dx;
      
      // looking for the upper right neighbor - index 1
      if (dir==IX) {
	BxfyR = nR.is.full.data->w.Bxfy;
	dBxdx = ((nR.is.full.data->w.Bx - data[CELL_CURRENT]->w.Bx) / (dx_R+dx)) * 2;
      }
      if (dir==IY) {
	ByfxR = nR.is.full.data->w.Byfx;
	dBydy = ((nR.is.full.data->w.By - data[CELL_CURRENT]->w.By) / (dx_R+dx)) * 2;
      }

    } // end if (nR.is_hanging)

    // we are ready to compute centered gradients dBxdxC and dBydyC
    if (dir==IX)
      dBxfydxC = (BxfyR-BxfyL) / (dx_L*0.5+dx+dx_R*0.5);
    if (dir==IY)
      dByfxdyC = (ByfxR-ByfxL) / (dx_L*0.5+dx+dx_R*0.5);
    
    /* 
     * if cell is on the outside boundary, we need to free the extra "ghost" 
     * quadrant
     */
    if (nL.tree_boundary) {
      sc_mempool_free (info->p4est->user_data_pool, nL.is.full.data);
    }
    if (nR.tree_boundary) {
      sc_mempool_free (info->p4est->user_data_pool, nR.is.full.data);
    }    
  } // end for dir

  ////////////////////////////////////////////////////////////////////////////
  // ami limiter : TODO
  ////////////////////////////////////////////////////////////////////////////

  double dt = solver->m_dt;
  
  ConfigReader    *cfg = solver->m_cfg;
  double divB_damp =  cfg->config_read_double ("settings.divB_damp", 0.2);
  /* loop over all quadrant's faces */
  for( int dir = dir_start; dir < dir_end; ++dir) {

    // get right neighbors
    qfR[CELL_CURRENT] = QuadrantNeighborUtilsBase::direction_faces[dir][1];

    /* 
     * get the Qdata of the neighbors on this face 
     * It is important to realize here that, if face touches the outside
     * boundary, there will be a call to our boundary_fn callback
     * which will fill the wm, wp states
     */
    QuadrantNeighborUtilsMHD_KT::quadrant_get_face_neighbor (solver,
                                                             quadid,
                                                             treeid,
                                                             qfR[CELL_CURRENT],
                                                             &nR);
    qfR[CELL_NEIGHBOR] = nR.face;
    if (dir==IX) {
      // left
      Bl  = data[CELL_CURRENT]->w.Bx+data[CELL_CURRENT]->dw[dir].Bx;
        
      if (nR.is_hanging) {
	  
	// right
	double tmp = 0;
	for (int i = 0; i < P4EST_HALF; ++i) {
	  tmp +=  nR.is.hanging.data[i]->w.Bx-nR.is.hanging.data[i]->dw[dir].Bx;
	}   
	  
	Br  = tmp/P4EST_HALF;
	  
	ami = compute_ami_mhd( data[CELL_CURRENT]->w,
			       nR.is.hanging.data[1]->w,
			       dir,gamma);
      } else {
	
	// right
	Br  = nR.is.full.data->w.Bx-nR.is.full.data->dw[dir].Bx;
	  
	ami = compute_ami_mhd( data[CELL_CURRENT]->w,
			       nR.is.full.data->w,
			       dir,gamma);
      }
      
      // real update of current cell - outgoing flux
      data[CELL_CURRENT]->wnext.Bx -= 0.5*(dt/dx)*((-divB_damp*dx*dy/dt)*(dBxdx + dByfxdyC)-ami*(Br-Bl));
    }
    
    if (dir==IY) {
      // left
      Bl  = data[CELL_CURRENT]->w.By+data[CELL_CURRENT]->dw[dir].By;

      // right
      if (nR.is_hanging) {
	// right
	double tmp = 0;
	for (int i = 0; i < P4EST_HALF; ++i) {
	  tmp +=  nR.is.hanging.data[i]->w.By-nR.is.hanging.data[i]->dw[dir].By;
	}
	Br=tmp/P4EST_HALF;
            
	ami = compute_ami_mhd( data[CELL_CURRENT]->w,
			       nR.is.hanging.data[1]->w,
			       dir,gamma);

      } else {
	Br  = nR.is.full.data->w.By-nR.is.full.data->dw[dir].By;

	ami = compute_ami_mhd( data[CELL_CURRENT]->w,
			       nR.is.full.data->w,
			       dir,gamma);
      }
        
      data[CELL_CURRENT]->wnext.By -= 0.5*(dt/dx)*((-divB_damp*dx*dy/dt)*(dBxfydxC + dBydy)-ami*(Br-Bl)); // AMI ???

    }
    
    if (nR.tree_boundary) {
      sc_mempool_free (info->p4est->user_data_pool, nR.is.full.data);
    }

  }// End for dir
  // now real update on neighbors on the Left
  for (int dir = dir_start; dir < dir_end; ++dir) {
    
    // get Right neighbors
    qfR[CELL_CURRENT] = QuadrantNeighborUtilsBase::direction_faces[dir][1];      
    
    /* 
     * get the Qdata of the neighbors on this face 
     * It is important to realize here that, if face touches the outside
     * boundary, there will be a call to our boundary_fn callback
     * which will fill the wm, wp states
     */
    QuadrantNeighborUtilsMHD_KT::quadrant_get_face_neighbor (solver,
							     quadid,
							     treeid,
							     qfR[CELL_CURRENT],
							     &nR);
    qfR[CELL_NEIGHBOR] = nR.face;

    if (nR.is_hanging) {
      
      dx_R = dy_R = dz_R = dx*0.5;
      
      // looking for the upper right neighbor - index 1
      if (dir==IX) {
        
        // left
        Bl  = data[CELL_CURRENT]->w.Bx+data[CELL_CURRENT]->dw[dir].Bx;
        // right
        double tmp = 0;
        for (int i = 0; i < P4EST_HALF; ++i) {
          tmp +=  nR.is.hanging.data[i]->w.Bx-nR.is.hanging.data[i]->dw[dir].Bx;
        }
        Br=tmp/P4EST_HALF;
        ami = compute_ami_mhd(     data[CELL_CURRENT]->w,
                                   nR.is.hanging.data[1]->w,
                                   dir,gamma);	


	for (int i = 0; i < P4EST_HALF; ++i) {
	  nR.is.hanging.data[i]->wnext.Bx +=0.5*(dt/dx)*((-divB_damp*dx_R*dy_R/dt)*(dBxdx + dByfxdyC)-ami*(Br-Bl)); // divide by dt strange
	}
	
      }
      if (dir==IY) {
        //left
        Bl  = data[CELL_CURRENT]->w.By+data[CELL_CURRENT]->dw[dir].By;
        //right 
        double tmp = 0;
        for (int i = 0; i < P4EST_HALF; ++i) {
          tmp +=  nR.is.hanging.data[i]->w.By-nR.is.hanging.data[i]->dw[dir].By;
        }
        Br=tmp/P4EST_HALF;
        ami = compute_ami_mhd(     data[CELL_CURRENT]->w,
                                   nR.is.hanging.data[1]->w,
                                   dir,gamma);
        for (int i = 0; i < P4EST_HALF; ++i) {
          nR.is.hanging.data[i]->wnext.By += 0.5*(dt/dx)*((-divB_damp*dx_R*dy_R/dt)*(dBxfydxC + dBydy)-ami*(Br-Bl)); // AMI ???
	}
      }

    } else {
      dx_R=dy_R=dx;
 
      if (dir==IX) {
	// left
	Bl  = data[CELL_CURRENT]->w.Bx+data[CELL_CURRENT]->dw[dir].Bx;

	// right
	Br  = nR.is.full.data->w.Bx-nR.is.full.data->dw[dir].Bx;

	ami = compute_ami_mhd( data[CELL_CURRENT]->w,
			       nR.is.full.data->w,
			       dir,gamma);


	nR.is.full.data->wnext.Bx += 0.5*(dt/dx)*((-divB_damp*dx*dy/dt)*(dBxdx + dByfxdyC)-ami*(Br-Bl)); // divide by dt strange
      }
      if (dir==IY) {
	// left
	Bl  = data[CELL_CURRENT]->w.By+data[CELL_CURRENT]->dw[dir].By;

	// right
	Br  = nR.is.full.data->w.By-nR.is.full.data->dw[dir].By;

	ami = compute_ami_mhd( data[CELL_CURRENT]->w,
			       nR.is.full.data->w,
			       dir,gamma);


	nR.is.full.data->wnext.By += 0.5*(dt/dx)*((-divB_damp*dx*dy/dt)*(dBxfydxC + dBydy)-ami*(Br-Bl)); // AMI ???
      }      

    }



    if (nR.tree_boundary) {
      sc_mempool_free (info->p4est->user_data_pool, nR.is.full.data);
    }
    
  } // end for dir
  
} // iterator_mhd_kt_update_b_diffusive

// =======================================================
// =======================================================
/**
 * \brief Compute and store local slopes.
 *
 * For each cell (leaf), compute a limiter gradient stored
 * dw[IX], dw[IY], dw[IZ].
 *
 */
static void
iterator_mhd_kt_limiter (p4est_iter_volume_info_t * info, 
			 void *user_data)
{

  /* get forest structure */
  p4est_t              *p4est = info->p4est;
  
  /* get solver and geometry (only for unstructured) */
  SolverMHD_KT       *solver = static_cast<SolverMHD_KT*>(solver_from_p4est (p4est));

  UserDataManagerMHD_KT
    *userdata_mgr = static_cast<UserDataManagerMHD_KT *> (solver->get_userdata_mgr());

  int                *direction = (int *) user_data;
  double              dx  , dy  , dz  ; // current  cell
  double              dx_min;
  
  p4est_quadrant_t   *quad   = info->quad;
  p4est_locidx_t      quadid = info->quadid;
  p4est_topidx_t      treeid = info->treeid;

  /* 2 states: one for each side of the face */
  qdata_t            *data[2] = { NULL, NULL };

  /* the face Id in range 0..P4EST_FACES-1 */
  int                 qf[2] = { -1, -1 };

  double              scale = 0.0;
  quadrant_neighbor_t<qdata_t> n;

  /* geometrical terms */
  int                 dir_start = (direction == NULL ? 0 : *direction);
  int                 dir_end = (direction == NULL ? P4EST_DIM : *direction + 1);

  double              dxL, dxR;
  qdata_variables_t   delta_dataL, delta_dataR;

    
  UNUSED (scale);
  UNUSED (dx_min);

  /* get current cell data */
  data[CELL_CURRENT] = userdata_mgr->quadrant_get_qdata (quad);
  
  /* current cell length / location */

  //quadrant_get_dx_dy_dz(conn, NULL, quad, treeid, &dx, &dy, &dz);
  dx = dy = dz = quadrant_length (info->quad);
  dxL = dx;
  dxR = dx; 

  // slope 
  
  // for each dir, compute dw
  /* loop over all quadrant's faces */
  for( int dir = dir_start; dir < dir_end; ++dir) {
  
    for (int face = 0; face < 2; ++face) {

      /* get the current face in the given direction */
      qf[CELL_CURRENT] = QuadrantNeighborUtilsBase::direction_faces[dir][face];      
      
      /* 
       * get the Qdata of the neighbors on this face 
       * It is important to realize here that, if face touches the outside
       * boundary, there will be a call to our boundary_fn callback
       * which will fill the wm, wp states
       */
      QuadrantNeighborUtilsMHD_KT::quadrant_get_face_neighbor (solver,
							       quadid,
							       treeid,
							       qf[CELL_CURRENT],
							       &n);
      qf[CELL_NEIGHBOR] = n.face;

      /* 
       *
       */
      if (n.is_hanging) {
 
	/* perform update */
	if (face==0) {
	  dxL=0.5*dx;
	  delta_dataL.set_zero();
	  for (int i = 0; i < P4EST_HALF; ++i){
	    data[CELL_NEIGHBOR] = n.is.hanging.data[i]; 
	    delta_dataL -= data[CELL_NEIGHBOR]->w;
	  }
	  delta_dataL /= P4EST_HALF;
	  delta_dataL += data[CELL_CURRENT]->w;
	  delta_dataL /= 0.5*(dxL+dx);
	    
	} else {
	  dxR=0.5*dx; 
	  delta_dataR.set_zero(); 
	  for (int i = 0; i < P4EST_HALF; ++i){
	    data[CELL_NEIGHBOR] = n.is.hanging.data[i];
	    delta_dataR  += data[CELL_NEIGHBOR]->w;
	  }
	  delta_dataR /= P4EST_HALF;
	  delta_dataR -= data[CELL_CURRENT]->w;
	  delta_dataR /= 0.5*(dxR+dx);

	}
	
      } else {
	
	data[CELL_NEIGHBOR] = n.is.full.data;
	
	if (face==0) {
	  
	  dxL = n.level < info->quad->level ? 2*dx : dx;
          delta_dataL  = data[CELL_CURRENT]->w;
	  delta_dataL -= data[CELL_NEIGHBOR]->w;
	  delta_dataL /= 0.5*(dxL+dx);
	  
	} else {
	  
	  dxR = n.level < info->quad->level ? 2*dx : dx;
          delta_dataR  = data[CELL_NEIGHBOR]->w;
	  delta_dataR -= data[CELL_CURRENT]->w;
	  delta_dataR /= 0.5*(dxR+dx);
	  
	}
	
      } // end if (n.is_hanging)
      
      /* 
       * if cell is on the outside boundary, we need to free the extra "ghost" 
       * quadrant
       */
      if (n.tree_boundary) {
	sc_mempool_free (info->p4est->user_data_pool, n.is.full.data);
      }

    } // end for face


    auto compute_phi = solver->m_compute_phi;
    double R;

    R = delta_dataR.rho == 0 ? 1.0 : delta_dataL.rho/delta_dataR.rho;
    data[CELL_CURRENT]->dw[dir].rho  = 0.5 * compute_phi(R) * delta_dataR.rho * dx;

    R = delta_dataR.rhoU == 0 ? 1.0 : delta_dataL.rhoU/delta_dataR.rhoU;
    data[CELL_CURRENT]->dw[dir].rhoU = 0.5 * compute_phi(R) * delta_dataR.rhoU * dx;

    R = delta_dataR.rhoV == 0 ? 1.0 : delta_dataL.rhoV/delta_dataR.rhoV;
    data[CELL_CURRENT]->dw[dir].rhoV = 0.5 * compute_phi(R) * delta_dataR.rhoV * dx;
#ifdef P4_TO_P8
    R = delta_dataR.rhoW == 0 ? 1.0 : delta_dataL.rhoW/delta_dataR.rhoW;
    data[CELL_CURRENT]->dw[dir].rhoW = 0.5 * compute_phi(R) * delta_dataR.rhoW * dx;
#endif

    R = delta_dataR.E == 0 ? 1.0 : delta_dataL.E/delta_dataR.E;
    data[CELL_CURRENT]->dw[dir].E  = 0.5* compute_phi(R) * delta_dataR.E * dx;

    R = delta_dataR.Bx == 0 ? 1.0 : delta_dataL.Bx/delta_dataR.Bx;
    data[CELL_CURRENT]->dw[dir].Bx = 0.5* compute_phi(R) * delta_dataR.Bx * dx;

    R = delta_dataR.By == 0 ? 1.0 : delta_dataL.By/delta_dataR.By;
    data[CELL_CURRENT]->dw[dir].By = 0.5* compute_phi(R) * delta_dataR.By * dx;
    
#ifdef P4_TO_P8
    R = delta_dataR.Bz == 0 ? 1.0 : delta_dataL.Bz/delta_dataR.Bz;
    data[CELL_CURRENT]->dw[dir].Bz = 0.5* compute_phi(R) * delta_dataR.Bz * dx;
#endif  
    
    R = delta_dataR.psi == 0 ? 1.0 : delta_dataL.psi/delta_dataR.psi;
    data[CELL_CURRENT]->dw[dir].psi = 0.5* compute_phi(R) * delta_dataR.psi * dx;

  } // end for dir

  
} // iterator_mhd_kt_limiter

// =======================================================
// =======================================================
/**
 * \brief Compute and store local slopes.
 *
 * For each cell (leaf), compute a limiter gradient stored
 * dw[IX], dw[IY], dw[IZ].
 *
 * Compute gradient only for mag field and psi
 *
 */
static void
iterator_mhd_kt_limiter_glm_subcycled (p4est_iter_volume_info_t * info, 
				       void *user_data)
{

  // TODO
  UNUSED(info);
  UNUSED(user_data);
  
} // iterator_mhd_kt_limiter_glm_subcycled

// =======================================================
// =======================================================
/**
 * \brief Update the value in a quadrant using the MHD_KT scheme.
 *
 * This is were the Riemann solver enters into play.
 *
 * Update actually happens on both side of the interface:
 * - when morton index  (actually quadid) of current cell is smaller than 
 *   that of neighbor
 * - when current cell is regular and neighbor is ghost
 */
static void
iterator_mhd_kt_update (p4est_iter_volume_info_t * info, 
			void *user_data)
{

  /* get forest structure */
  p4est_t              *p4est = info->p4est;
  
  /* get solver and geometry (only for unstructured) */
  SolverMHD_KT       *solver = static_cast<SolverMHD_KT*>(solver_from_p4est (p4est));

  UserDataManagerMHD_KT
    *userdata_mgr = static_cast<UserDataManagerMHD_KT *> (solver->get_userdata_mgr());
  

  int                *direction = (int *) user_data;
  double              dx  , dy  , dz  ; // current  cell
  double              dx_min;
  
  p4est_quadrant_t   *quad   = info->quad;
  p4est_locidx_t      quadid = info->quadid;
  p4est_topidx_t      treeid = info->treeid;

  /* 2 states: one for each side of the face */
  qdata_t            *data[2] = { NULL, NULL };

  /* the face Id in range 0..P4EST_FACES-1 */
  int                 qf[2] = { -1, -1 };

  double              scale = 0.0;
  quadrant_neighbor_t<qdata_t> n;

  // retrieve cleaning method
  div_b_cleaning_t cleaning = solver->div_b_method;
  
  /* geometrical terms */
  int                 dir_start = (direction == NULL ? 0 : *direction);
  int                 dir_end = (direction == NULL ? P4EST_DIM : *direction + 1);

  qdata_variables_t   dataL, dataR, dataC,Fx,Fy;
  
  double dt = solver->m_dt;
  double ami =0.0;
  double geom_scale = 1.0;
  
  // Coefficient for the GLM MHD system equation
  double ch = 0.0;
  double cp = 0.0;

  double gamma = solver->m_cfg->config_read_double ("settings.gamma0", 5.0/3);
  double cfl =   solver->m_cfg->config_read_double ("settings.cfl", 0.2);  

  UNUSED (scale);
  UNUSED (dx_min);

  /* get current cell data */
  data[CELL_CURRENT] = userdata_mgr->quadrant_get_qdata (quad);
  dataC = data[CELL_CURRENT]->w;
  
  /* current cell length / location */

  //quadrant_get_dx_dy_dz(conn, NULL, quad, treeid, &dx, &dy, &dz);
  dx = dy = dz = quadrant_length (info->quad);

  qdata_variables_t flux_tot;
  
  /* loop over all quadrant's faces */
  for( int dir = dir_start; dir < dir_end; ++dir) {
  
    for (int face = 0; face < 2; ++face) {

      /* get the current face in the given direction */
      qf[CELL_CURRENT] = QuadrantNeighborUtilsBase::direction_faces[dir][face];      
      
      /* 
       * get the Qdata of the neighbors on this face 
       * It is important to realize here that, if face touches the outside
       * boundary, there will be a call to our boundary_fn callback
       * which will fill the wm, wp states
       */
      QuadrantNeighborUtilsMHD_KT::quadrant_get_face_neighbor (solver,
							       quadid,
							       treeid,
							       qf[CELL_CURRENT],
							       &n);
      qf[CELL_NEIGHBOR] = n.face;

      /* 
       *
       */
      if (n.is_hanging) {

#ifdef P4_TO_P8
	geom_scale = 0.25;
#else
	geom_scale = 0.5;
#endif
	
	/* half neighbors interface loop */
	for (int i = 0; i < P4EST_HALF; ++i) {
	  	  
	  /* perform update */
	  data[CELL_NEIGHBOR] = n.is.hanging.data[i];

	  if (face==0) {

	    // left
	    dataL   = data[CELL_NEIGHBOR]->w;
            dataL  += data[CELL_NEIGHBOR]->dw[dir];

            // Right
            dataR   = data[CELL_CURRENT]->w;
            dataR  -= data[CELL_CURRENT]->dw[dir];

            ami = compute_ami_mhd( data[CELL_NEIGHBOR]->w,
				   data[CELL_CURRENT]->w,
				   dir,gamma);

	  } else {

	    // left
            dataL   = data[CELL_CURRENT]->w; 
            dataL  += data[CELL_CURRENT]->dw[dir];

            // Right
	    dataR   = data[CELL_NEIGHBOR]->w; 
            dataR  -= data[CELL_NEIGHBOR]->dw[dir];

            ami = compute_ami_mhd( data[CELL_CURRENT]->w,
				   data[CELL_NEIGHBOR]->w,
				   dir,gamma);
	  }


          ch = (0.1*cfl*dx)/dt ;
          cp = sqrt(0.18*ch); 
 
	  qdata_variables_t flux,tmp;
	  compute_flux_mhd(dataL,tmp,dir,gamma,ch,cleaning); flux += tmp;
	  compute_flux_mhd(dataR,tmp,dir,gamma,ch,cleaning); flux += tmp;
	  
	  tmp = dataR;
	  tmp -= dataL;
	  tmp *= ami;
	  
	  flux -= tmp;
	  flux *= 0.5;

	  flux *= geom_scale;
	  
          // Please remove the comments below for the div(B) diffusive method
	  //if(dir==IX){
	  //  flux.Bx=0.0; 
	  //}
	  
	  //if(dir==IY){	
	  //  flux.By=0.0;
	  //} 
	  
	  if (face==0)
	    flux_tot += flux;
	  else
	    flux_tot -= flux;
	  
	} /* end for i=0..P4EST_HELF */

      } else {

	geom_scale = 1.0;

	data[CELL_NEIGHBOR] = n.is.full.data;
	
	if (face==0) {
	  
	  /* full neighbor (same size or larger) */	  
	  // left
	  dataL  = data[CELL_NEIGHBOR]->w;
          dataL += data[CELL_NEIGHBOR]->dw[dir];
	  
	  // right
	  dataR  = data[CELL_CURRENT]->w;
          dataR -= data[CELL_CURRENT]->dw[dir];
           
          ami = compute_ami_mhd( data[CELL_NEIGHBOR]->w,
				 data[CELL_CURRENT]->w,
				 dir,gamma);
	  
	} else {
	  
	  // left
	  dataL  = data[CELL_CURRENT]->w;
          dataL += data[CELL_CURRENT]->dw[dir];
	  
	  // right
	  dataR  = data[CELL_NEIGHBOR]->w;
          dataR -= data[CELL_NEIGHBOR]->dw[dir];
	  ami = compute_ami_mhd( data[CELL_CURRENT]->w,
				 data[CELL_NEIGHBOR]->w,
				 dir,gamma);
	  
	}
        
        ch = (0.1*cfl*dx)/dt ;
        cp = sqrt(0.18*ch);	

        qdata_variables_t flux,tmp;
	compute_flux_mhd(dataL,tmp,dir,gamma,ch,cleaning); flux += tmp;
	compute_flux_mhd(dataR,tmp,dir,gamma,ch,cleaning); flux += tmp;
	
	tmp = dataR;
	tmp -= dataL;
	tmp *= ami;
	
	flux -= tmp;
	flux *= 0.5;
	
	flux *= geom_scale;

        // Please remove the comments below for the div(B) diffusive method
        //if(dir==IX){
	//  flux.Bx=0.0; 
	//}

	//if(dir==IY){	
	//  flux.By=0.0;
        //} 
	
	if (face==0)
	  flux_tot += flux;
	else
	  flux_tot -= flux;
	
      } // end if (n.is_hanging)
      
      /* 
       * if cell is on the outside boundary, we need to free the extra "ghost" 
       * quadrant
       */
      if (n.tree_boundary) {
	sc_mempool_free (info->p4est->user_data_pool, n.is.full.data);
      }

    } // end for face


  } // end for dir

  // now update
  {
    flux_tot *= (dt/dx);
    data[CELL_CURRENT]->wnext = data[CELL_CURRENT]->w;
    data[CELL_CURRENT]->wnext += flux_tot;
  }
  
  // Update source term in psi equation for maintaining div(B)=0
  if (solver->div_b_method == DIV_B_GLM) {
    data[CELL_CURRENT]->wnext.psi = data[CELL_CURRENT]->wnext.psi/(1 + dt * (ch*ch)/(cp*cp)) ; 
    //data[CELL_CURRENT]->wnext.psi = data[CELL_CURRENT]->wnext.psi * exp(- dt * (ch*ch)/(cp*cp)) ;
  }

} // iterator_mhd_kt_update

// =======================================================
// =======================================================
/**
 * \brief Update the value in a quadrant using the MHD_KT scheme.
 *
 * This is were the Riemann solver enters into play.
 *
 * Update actually happens on both side of the interface:
 * - when morton index  (actually quadid) of current cell is smaller than 
 *   that of neighbor
 * - when current cell is regular and neighbor is ghost
 */
static void
iterator_mhd_kt_update_glm_subcycled (p4est_iter_volume_info_t * info, 
				      void *user_data)
{

  /* get forest structure */
  p4est_t              *p4est = info->p4est;
  
  /* get solver and geometry (only for unstructured) */
  SolverMHD_KT       *solver = static_cast<SolverMHD_KT*>(solver_from_p4est (p4est));

  UserDataManagerMHD_KT
    *userdata_mgr = static_cast<UserDataManagerMHD_KT *> (solver->get_userdata_mgr());
  

  int                *direction = (int *) user_data;
  double              dx  , dy  , dz  ; // current  cell
  double              dx_min;
  
  p4est_quadrant_t   *quad   = info->quad;
  p4est_locidx_t      quadid = info->quadid;
  p4est_topidx_t      treeid = info->treeid;

  /* 2 states: one for each side of the face */
  qdata_t            *data[2] = { NULL, NULL };

  /* the face Id in range 0..P4EST_FACES-1 */
  int                 qf[2] = { -1, -1 };

  double              scale = 0.0;
  quadrant_neighbor_t<qdata_t> n;

  // retrieve cleaning method
  //div_b_cleaning_t cleaning = solver->div_b_method;
  
  /* geometrical terms */
  int                 dir_start = (direction == NULL ? 0 : *direction);
  int                 dir_end = (direction == NULL ? P4EST_DIM : *direction + 1);

  qdata_variables_t   dataL, dataR, dataC,Fx,Fy;
  
  double ami =0.0;
  double geom_scale = 1.0;
  
  // Coefficient for the GLM MHD system equation
  double ch = 0.0;
  double cp = 0.0;

  double gamma = solver->m_cfg->config_read_double ("settings.gamma0", 5.0/3);
  double cfl =   solver->m_cfg->config_read_double ("settings.cfl", 0.2);  
  int nb_subcycles = solver->m_cfg->config_read_int("settings.glm_nb_subcycles", 5);
  double dt = solver->m_dt / nb_subcycles;

  UNUSED (scale);
  UNUSED (dx_min);

  /* get current cell data */
  data[CELL_CURRENT] = userdata_mgr->quadrant_get_qdata (quad);
  dataC = data[CELL_CURRENT]->w;
  
  /* current cell length / location */

  //quadrant_get_dx_dy_dz(conn, NULL, quad, treeid, &dx, &dy, &dz);
  dx = dy = dz = quadrant_length (info->quad);

  qdata_variables_t flux_tot;
  
  /* loop over all quadrant's faces */
  for( int dir = dir_start; dir < dir_end; ++dir) {
  
    for (int face = 0; face < 2; ++face) {

      /* get the current face in the given direction */
      qf[CELL_CURRENT] = QuadrantNeighborUtilsBase::direction_faces[dir][face];      
      
      /* 
       * get the Qdata of the neighbors on this face 
       * It is important to realize here that, if face touches the outside
       * boundary, there will be a call to our boundary_fn callback
       * which will fill the wm, wp states
       */
      QuadrantNeighborUtilsMHD_KT::quadrant_get_face_neighbor (solver,
							       quadid,
							       treeid,
							       qf[CELL_CURRENT],
							       &n);
      qf[CELL_NEIGHBOR] = n.face;

      /* 
       *
       */
      if (n.is_hanging) {

#ifdef P4_TO_P8
	geom_scale = 0.25;
#else
	geom_scale = 0.5;
#endif
	
	/* half neighbors interface loop */
	for (int i = 0; i < P4EST_HALF; ++i) {
	  	  
	  /* perform update */
	  data[CELL_NEIGHBOR] = n.is.hanging.data[i];

	  if (face==0) {

	    // left
	    dataL   = data[CELL_NEIGHBOR]->w;
            dataL  += data[CELL_NEIGHBOR]->dw[dir];

            // Right
            dataR   = data[CELL_CURRENT]->w;
            dataR  -= data[CELL_CURRENT]->dw[dir];

            ami = compute_ami_mhd( data[CELL_NEIGHBOR]->w,
				   data[CELL_CURRENT]->w,
				   dir,gamma);

	  } else {

	    // left
            dataL   = data[CELL_CURRENT]->w; 
            dataL  += data[CELL_CURRENT]->dw[dir];

            // Right
	    dataR   = data[CELL_NEIGHBOR]->w; 
            dataR  -= data[CELL_NEIGHBOR]->dw[dir];

            ami = compute_ami_mhd( data[CELL_CURRENT]->w,
				   data[CELL_NEIGHBOR]->w,
				   dir,gamma);
	  }


          ch = (0.1*cfl*dx)/dt ;
          cp = sqrt(0.18*ch); 
 
	  qdata_variables_t flux,tmp;
	  compute_flux_mhd_glm(dataL,tmp,dir,gamma,ch); flux += tmp;
	  compute_flux_mhd_glm(dataR,tmp,dir,gamma,ch); flux += tmp;
	  
	  tmp = dataR;
	  tmp -= dataL;
	  tmp *= ami;
	  
	  flux -= tmp;
	  flux *= 0.5;

	  flux *= geom_scale;
	  
          // Please remove the comments below for the div(B) diffusive method
	  //if(dir==IX){
	  //  flux.Bx=0.0; 
	  //}
	  
	  //if(dir==IY){	
	  //  flux.By=0.0;
	  //} 
	  
	  if (face==0)
	    flux_tot += flux;
	  else
	    flux_tot -= flux;
	  
	} /* end for i=0..P4EST_HELF */

      } else {

	geom_scale = 1.0;

	data[CELL_NEIGHBOR] = n.is.full.data;
	
	if (face==0) {
	  
	  /* full neighbor (same size or larger) */	  
	  // left
	  dataL  = data[CELL_NEIGHBOR]->w;
          dataL += data[CELL_NEIGHBOR]->dw[dir];
	  
	  // right
	  dataR  = data[CELL_CURRENT]->w;
          dataR -= data[CELL_CURRENT]->dw[dir];
           
          ami = compute_ami_mhd( data[CELL_NEIGHBOR]->w,
				 data[CELL_CURRENT]->w,
				 dir,gamma);
	  
	} else {
	  
	  // left
	  dataL  = data[CELL_CURRENT]->w;
          dataL += data[CELL_CURRENT]->dw[dir];
	  
	  // right
	  dataR  = data[CELL_NEIGHBOR]->w;
          dataR -= data[CELL_NEIGHBOR]->dw[dir];
	  ami = compute_ami_mhd( data[CELL_CURRENT]->w,
				 data[CELL_NEIGHBOR]->w,
				 dir,gamma);
	  
	}
        
        ch = (0.1*cfl*dx)/dt ;
        cp = sqrt(0.18*ch);	

        qdata_variables_t flux,tmp;
	compute_flux_mhd_glm(dataL,tmp,dir,gamma,ch); flux += tmp;
	compute_flux_mhd_glm(dataR,tmp,dir,gamma,ch); flux += tmp;
	
	tmp = dataR;
	tmp -= dataL;
	tmp *= ami;
	
	flux -= tmp;
	flux *= 0.5;
	
	flux *= geom_scale;

        // Please remove the comments below for the div(B) diffusive method
        //if(dir==IX){
	//  flux.Bx=0.0; 
	//}

	//if(dir==IY){	
	//  flux.By=0.0;
        //} 
	
	if (face==0)
	  flux_tot += flux;
	else
	  flux_tot -= flux;
	
      } // end if (n.is_hanging)
      
      /* 
       * if cell is on the outside boundary, we need to free the extra "ghost" 
       * quadrant
       */
      if (n.tree_boundary) {
	sc_mempool_free (info->p4est->user_data_pool, n.is.full.data);
      }

    } // end for face


  } // end for dir

  // now update

  flux_tot *= (dt/dx);
  if (solver->subcycle_iter == nb_subcycles-1) {

    data[CELL_CURRENT]->wnext.Bx = data[CELL_CURRENT]->w.Bx + flux_tot.Bx;
    data[CELL_CURRENT]->wnext.By = data[CELL_CURRENT]->w.By + flux_tot.By;
#ifdef P4_TO_P8
    data[CELL_CURRENT]->wnext.Bz = data[CELL_CURRENT]->w.Bz + flux_tot.Bz;
#endif
    data[CELL_CURRENT]->wnext.psi = data[CELL_CURRENT]->w.psi + flux_tot.psi;

    // Update source term in psi equation for maintaining div(B)=0
    //data[CELL_CURRENT]->wnext.psi = data[CELL_CURRENT]->wnext.psi/(1 + dt * nb_subcycles * (ch*ch)/(cp*cp)) ; 
    data[CELL_CURRENT]->wnext.psi = data[CELL_CURRENT]->wnext.psi/(1 + dt * (ch*ch)/(cp*cp)) ; 


  } else {
    
    data[CELL_CURRENT]->w.Bx += flux_tot.Bx;
    data[CELL_CURRENT]->w.By += flux_tot.By;
#ifdef P4_TO_P8
    data[CELL_CURRENT]->w.Bz += flux_tot.Bz;
#endif
    data[CELL_CURRENT]->w.psi += flux_tot.psi;
    
  }
  
} // iterator_mhd_kt_update_glm_sybcycled

// =======================================================
// =======================================================

// =======================================================
// =======================================================

// =======================================================
// =======================================================
IteratorsMHD_KT::IteratorsMHD_KT() : IteratorsBase() {

  this->fill_iterators_list();
  
} // IteratorsMHD_KT::IteratorsMHD_KT

// =======================================================
// =======================================================
IteratorsMHD_KT::~IteratorsMHD_KT()
{
  
} // IteratorsMHD_KT::~IteratorsMHD_KT

// =======================================================
// =======================================================
void IteratorsMHD_KT::fill_iterators_list()
{

  iteratorsList[IT_IDD::START_STEP]          = iterator_start_step;
  iteratorsList[IT_IDD::END_STEP]            = iterator_end_step;
  iteratorsList[IT_IDD::MARK_ADAPT]          = iterator_mark_adapt;
  iteratorsList[IT_IDD::CFL_TIME_STEP]       = iterator_time_step;
  iteratorsList[IT_IDD::UPDATE]              = iterator_mhd_kt_update;
  iteratorsList[IT_IDD::LIMITER]             = iterator_mhd_kt_limiter;
  iteratorsList[IT_IDD::BFIELD_FACE]         = iterator_mhd_kt_compute_Bfield_face;
  iteratorsList[IT_IDD::UPDATE_B_DIFFUSIVE]  = iterator_mhd_kt_update_b_diffusive;
  iteratorsList[IT_IDD::DIV_B]               = iterator_mhd_kt_div_b;
  iteratorsList[IT_IDD::TRANSP]              = iterator_mhd_kt_transp;
  iteratorsList[IT_IDD::LIMITER_GLM_SUBCYCLED] = iterator_mhd_kt_limiter_glm_subcycled;
  iteratorsList[IT_IDD::UPDATE_GLM_SUBCYCLED]  = iterator_mhd_kt_update_glm_subcycled;
  iteratorsList[IT_IDD::COPY_MAG_FIELD]      = iterator_mhd_kt_copy_mag_field;

} // IteratorsMHD_KT::fill_iterators_list

} // namespace mhd_kt

} // namespace canoP
