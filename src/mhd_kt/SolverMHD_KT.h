#ifndef SOLVER_MHD_KT_H_
#define SOLVER_MHD_KT_H_

#include "Solver.h"
#include "userdata_mhd_kt.h"
#include "ScalarLimiterFactory.h"
#include "limiter.h"
#include "mhd.h"

#include <mpi.h>

#ifdef USE_MUTATIONPP
#include "mutation++/mutation++.h"
#endif

class ConfigReader;

namespace canop {

namespace mhd_kt {

/**
 * Concrete Solver class implementation for MHD_KT application.
 */
class SolverMHD_KT : public Solver
{

  using qdata_t = Qdata;
  using scalar_limiter_t = regular_limiter_t;

public:
  SolverMHD_KT(ConfigReader * cfg, MPI_Comm mpicomm, bool useP4estMemory);
  virtual ~SolverMHD_KT();

  //! scalar limiter
  regular_limiter_t m_compute_phi;
  
#ifdef USE_MUTATIONPP
  // transfort coefficient computation
  Mutation::Mixture* m_mix;
#endif

  //! which method to cleaning divergence
  div_b_cleaning_t div_b_method;

  //! subcycle iteration number
  int subcycle_iter;
  
  // redefine compute_time_step
  void compute_time_step();
  
  /**
   * Here we implement unsplit numerical scheme for MHD_KT Hydro.
   */
  void next_iteration_impl();

  /**
   * Static creation method called by the solver factory.
   */
  static Solver* create(ConfigReader * cfg, MPI_Comm mpicomm, bool useP4estMemory)
  {
    SolverMHD_KT* solver = new SolverMHD_KT(cfg, mpicomm, useP4estMemory);

    return solver;
  }

private:
  /**
   * Custom version of base class version.
   */
  void read_config();
  
}; // class SolverMHD_KT

} // namespace mhd_kt

} // namespace canoP

#endif // SOLVER_MHD_KT_H_
