#ifndef SOLVER_CORE_MHD_KT_H_
#define SOLVER_CORE_MHD_KT_H_

#include "SolverCore.h"

#include "userdata_mhd_kt.h"

class ConfigReader;

namespace canop {

namespace mhd_kt {

/**
 * This is were Initial and Border Conditions callbacks are setup.
 */
class SolverCoreMHD_KT : public SolverCore<Qdata>
{

public:
  SolverCoreMHD_KT(ConfigReader *cfg);
  ~SolverCoreMHD_KT();

  /**
   * Static creation method called by the solver factory.
   */
  static SolverCoreBase* create(ConfigReader *cfg)
  {
    return new SolverCoreMHD_KT(cfg);
  }
  
}; // SolverCoreMHD_KT

} // namespace mhd_kt

} // namespace canoP

#endif // SOLVER_CORE_MHD_KT_H_
