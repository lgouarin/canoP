#ifndef ITERATORS_MHD_KT_H_
#define ITERATORS_MHD_KT_H_

#include "Iterators.h"

namespace canop {

namespace mhd_kt {

/**
 * Adding new IDs to struct IT_ID.
 *
 * We start using number 32 (assuming the number of ID's in base class is
 * less than 32).
 *
 * TODO: enforce strongly that each new IDs below are initialized with
 * a number not already used in base class.
 */
struct IT_IDD : public IT_ID {

  static constexpr int LIMITER = 32;
  static constexpr int BFIELD_FACE = 33;
  static constexpr int UPDATE_B_DIFFUSIVE = 34;
  static constexpr int DIV_B = 35;
  static constexpr int TRANSP = 36;
  static constexpr int LIMITER_GLM_SUBCYCLED = 37;
  static constexpr int UPDATE_GLM_SUBCYCLED = 38;
  static constexpr int COPY_MAG_FIELD = 39;
  
};


/*
 * NB: if you need to add new iterators:
 * 1. resize vector iterator_list_t
 * 2. create a new enum to ease indexing this vector
 * 3. add new iterator to the vector in fill_iterators_list method.
 */

class IteratorsMHD_KT : public IteratorsBase
{

public:
  IteratorsMHD_KT();
  virtual ~IteratorsMHD_KT();

  void fill_iterators_list();
  
};  // class IteratorsMHD_KT

} // namespace mhd_kt

} // namespace canoP

#endif // ITERATORS_MHD_KT_H_
