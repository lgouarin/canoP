#include "Euler.h"

namespace canop {

namespace mhd_kt {

double  compute_pressure(const qdata_variables_t &qdata, double gamma)
{
  
  double Ekin= 0.5*((qdata.rhoU*qdata.rhoU+qdata.rhoV*qdata.rhoV)/(qdata.rho));
#ifdef P4_TO_P8
  Ekin +=0.5*qdata.rhoW*qdata.rhoW/(qdata.rho); 
#endif
  double Eint=qdata.E-Ekin;
  
  double pressure = (gamma-1)*Eint; 

  return pressure;

} // compute_pressure

void compute_flux(const qdata_variables_t &q,
		  qdata_variables_t &flux,
		  int dir, double gamma)
{
  // compute pressure
  double p = compute_pressure(q, gamma);

#ifdef P4_TO_P8
  
  if (dir == IX) {
    double u = q.rhoU/q.rho;
    
    flux.rho =    q.rhoU;     //   rho u
    flux.rhoU = u*q.rhoU+p;   // u rho u + p
    flux.rhoV = u*q.rhoV;     // u rho v
    flux.rhoW = u*q.rhoW;     // u rho w
    flux.E    = u*(q.E+p);    // u (E+p)
  }
  else if (dir == IY) {
    double v = q.rhoV/q.rho;
    
    flux.rho  =   q.rhoV;     //   rho v
    flux.rhoU = v*q.rhoU;     // v rho u
    flux.rhoV = v*q.rhoV+p;   // v rho v + p
    flux.rhoW = v*q.rhoW;     // v rho w
    flux.E    = v*(q.E+p);    // v (E+p)
  }
  else if (dir == IZ) {
    double w = q.rhoW/q.rho;
    
    flux.rho  =   q.rhoW;     //   rho w
    flux.rhoU = w*q.rhoU;     // w rho u
    flux.rhoV = w*q.rhoV;     // w rho v
    flux.rhoW = w*q.rhoW+p;   // w rho w + p
    flux.E    = w*(q.E+p);    // w (E+p)
  }

#else

  if (dir == IX) {
    flux.rho  = q.rhoU;                 // rho u
    flux.rhoU = q.rhoU*q.rhoU/q.rho+p;   // rho u^2 + p
    flux.rhoV = q.rhoU*q.rhoV/q.rho;     // rho u v
    flux.E    = q.rhoU/q.rho*(q.E+p); // u (E+p)  
  }
  else if (dir == IY) {
    flux.rho  = q.rhoV;                 // rho v
    flux.rhoU = q.rhoV*q.rhoU/q.rho;     // rho v u
    flux.rhoV = q.rhoV*q.rhoV/q.rho+p;   // rho v^2 + p
    flux.E    = q.rhoV/q.rho*(q.E+p); // v (E+p)
  }
  
#endif
  
} // compute_flux

} // namespace mhd_kt

} // namespace canoP
