#ifndef USERDATA_MANAGER_MFPLASMA_H_
#define USERDATA_MANAGER_MFPLASMA_H_

#include "UserDataManager.h"
#include "userdata_mfplasma.h"

namespace canop {

namespace mfplasma {

using UserDataTypesMfplasma = UserDataTypes<Qdata,
					   QdataVar,
					   QdataRecons>;


class UserDataManagerMfplasma : public UserDataManager<UserDataTypesMfplasma>
{
  
public:
  UserDataManagerMfplasma(bool useP4estMemory, MfplasmaPar &mfplasmaPar);
  virtual ~UserDataManagerMfplasma();

  virtual void qdata_set_variables (qdata_t * data,
				    qdatavar_t * w);
  
  virtual void quadrant_set_state_data (p4est_quadrant_t * dst,
					p4est_quadrant_t * src);

  virtual void quadrant_compute_mean(p4est_quadrant_t *qmean,
				     p4est_quadrant_t *quadout[P4EST_CHILDREN]);

  
  virtual void qdata_print_variables (int loglevel, qdata_t * data);

  virtual void quadrant_copy_w_to_wnext (p4est_quadrant_t * q);
  virtual void quadrant_copy_wnext_to_w (p4est_quadrant_t * q);
  
  double qdata_get_ne (p4est_quadrant_t * q);
  double qdata_get_ni (p4est_quadrant_t * q);

  void qdata_vget_rhoVe (p4est_quadrant_t * q, size_t dim, double * val);
  void qdata_vget_rhoVi (p4est_quadrant_t * q, size_t dim, double * val);

  //! get total charge
  double qdata_get_rhoc (p4est_quadrant_t * q);
  
  //! get electric potential
  double qdata_get_elec_phi (p4est_quadrant_t * q);

  void qdata_set_elec_phi (p4est_quadrant_t * q, double val);
  void qdata_set_elec_acc (p4est_quadrant_t * q, int direction, double val);
  void qdata_set_elec_acc_v (p4est_quadrant_t * q, double *vec);

  void qdata_vget_elec_field (p4est_quadrant_t * q, size_t dim, double * val);


  /**
   * Return a pointer to a member that is a qdata field "getter".
   */
  virtual qdata_getter_t qdata_getter_byname(const std::string &varName);

  virtual qdata_vector_getter_t
  qdata_vgetter_byname (const std::string &varName);

  /**
   * Return the field type for the given variable.
   */
  virtual qdata_field_type qdata_type_byname(const std::string &varName);

private:
  const MfplasmaPar &mfplasmaPar;

}; // UserDataManagerMfplasma

} // namespace mfplasma

} // namespace canop

#endif // USERDATA_MANAGER_MFPLASMA_H_
