
#include "SolverMfplasma.h"
#include "SolverCoreMfplasma.h"
#include "Statistics.h"
#include "UserDataManagerMfplasma.h"
#include "IteratorsMfplasma.h"

#include "IO_Writer.h"

#include "canoP_base.h"
#include "elec_poisson.h"
#include "utils.h"

namespace canop {

namespace mfplasma {

// =======================================================
// =======================================================
SolverMfplasma::SolverMfplasma(ConfigReader * cfg, MPI_Comm mpicomm, bool useP4estMemory) :
  Solver(cfg,mpicomm,sizeof(SolverMfplasma::qdata_t),useP4estMemory)
{

  // setup solver core
  m_solver_core = new SolverCoreMfplasma(cfg);

  // setup UserData Manager
  m_userdata_mgr = new UserDataManagerMfplasma(useP4estMemory, mfplasmaPar);

  // we need to setup the right iterators
  m_pIter = new IteratorsMfplasma();

  // solver init (this is were p4est_new is called)
  init();
  
  // only MPI rank 0 prints config (thanks to CANOP_GLOBAL_PRODUCTION macro)
  print_config();
  
} // SolverMfplasma::SolverMfplasma

// =======================================================
// =======================================================
SolverMfplasma::~SolverMfplasma()
{

  finalize();
  
  delete m_solver_core;

  delete m_userdata_mgr;

  delete m_pIter;
  
} // SolverMfplasma::~SolverMfplasma

// =======================================================
// =======================================================
void
SolverMfplasma::compute_time_step()
{

  p4est_t            *p4est = get_p4est();
  p4est_geometry_t   *geom  = get_geom_compute ();
  MPI_Comm            mpicomm = p4est->mpicomm;
  double              min_dt = 10;

  UNUSED (geom);

  // initialize the min / max current levels
  m_minlevel = P4EST_QMAXLEVEL;
  m_maxlevel = 0;

  // compute the min of dx
  // update the min / max current levels
  p4est_iterate (p4est, 
		 NULL, 
		 /* user data */ &min_dt, 
		 m_pIter->iteratorsList[IT_ID::CFL_TIME_STEP],  /* cell_fn */
		 NULL,                        /* face_fn */
#ifdef P4_TO_P8
                 NULL,                        /* edge_fn */
#endif
                 NULL                         /* corner_fn */
		 ); 

  // get the global maximum and minimum level -No risk on min_dt?-TODO
  MPI_Allreduce (MPI_IN_PLACE, &(m_minlevel), 1,
                 MPI_INT, MPI_MIN, mpicomm);
  MPI_Allreduce (MPI_IN_PLACE, &(m_maxlevel), 1,
                 MPI_INT, MPI_MAX, mpicomm);

  // update the min dt using the given courant number
  if (min_dt < 0) {
    m_dt = m_tmax;
  }
  else if (!ISFUZZYNULL (min_dt)) {
    m_dt = min_dt;
    m_dt = fmin (m_dt, fabs (m_tmax - m_t));
  }
  else {
    SC_ABORT ("min_dt is 0\n");
  }
  //take into account the source terms -TODO except the electric field, will be included when Poisson is implemented
  const bool& mag_on = mfplasmaPar.static_mag_field_enabled;
  const bool& iz_on = mfplasmaPar.ionization_enabled;
  if(mag_on){
    const double& Bx = mfplasmaPar.static_Bx;
    const double& By = mfplasmaPar.static_By;
    const double& Bz = mfplasmaPar.static_Bz;
    const double& epsilon = mfplasmaPar.epsilon;
    double B = sqrt(Bx*Bx+By*By+Bz*Bz);
    m_dt = fmin(m_dt,epsilon/B);
  }
  
  if(iz_on){
    const double& nu_iz = mfplasmaPar.nu_iz;
    m_dt = fmin(m_dt,1/nu_iz);    
  }
  
  const bool& poisson_on = mfplasmaPar.elec.poisson_enabled;
  if(poisson_on){
      const double& epsilon = mfplasmaPar.epsilon;
      const double& lambda = mfplasmaPar.elec.lambda;
      m_dt = fmin(m_dt,sqrt(1/(epsilon*lambda*lambda)));
  }

  // take the min over all the processes
  MPI_Allreduce (MPI_IN_PLACE, &(m_dt), 1, MPI_DOUBLE, MPI_MIN,
                 mpicomm);
  
} // SolverMfplasma::compute_time_step

// =======================================================
// =======================================================
void
SolverMfplasma::next_iteration_impl()
{

  p4est_t            *p4est = get_p4est ();
  p4est_ghost_t      *ghost = get_ghost ();

  // call the Poisson solver to compute the electric field acceleration
  if (mfplasmaPar.elec.poisson_enabled)
    solve_poisson_elec(p4est, nullptr); // the ghost layer needs full connectivity, not only face

  // compute the cfl condition and time step at the smallest level
  compute_time_step ();

  // Update ghost
  p4est_ghost_exchange_data (p4est, ghost, get_ghost_data_ptr() );
  
  /*
   * Perform update here:
   * - simple 1s order ?
   * - Runge-Kutta higher-order ?
   *
   * Whatever you'd like to be.
   *
   */
  CANOP_GLOBAL_INFO ("Perform numerical scheme time update\n");
  p4est_iterate (p4est,
		 ghost,
		 /* user data */ NULL,
		 m_pIter->iteratorsList[IT_ID::UPDATE],
		 NULL,        // face_fn
#ifdef P4_TO_P8
		 NULL,        // edge_fn
#endif
		 NULL         // corner_fn
		 );
  

  const bool& elec_on = mfplasmaPar.elec.enabled;
  const bool& mag_on = mfplasmaPar.static_mag_field_enabled;
  const bool& iz_on = mfplasmaPar.ionization_enabled;

  if (elec_on or mag_on or iz_on) {

    int source_term_center_enabled = m_cfg->config_read_int("settings.source_term_center_enabled", 0);
    int source_term_face_enabled = m_cfg->config_read_int("settings.source_term_face_enabled", 0);

    if (source_term_center_enabled) {

      CANOP_GLOBAL_INFO("Add source term (centered)\n");
      p4est_iterate(p4est, ghost,
                    /* user data */ NULL,
                    m_pIter->iteratorsList[IT_IDD::SOURCE_TERM_CENTER],
                    NULL, // face_fn
#ifdef P4_TO_P8
                    NULL, // edge_fn
#endif
                    NULL // corner_fn
        );

    } else if (source_term_face_enabled) {

      CANOP_GLOBAL_INFO("Add source term (centered)\n");
      p4est_iterate(p4est, ghost,
                    /* user data */ NULL,
                    m_pIter->iteratorsList[IT_IDD::SOURCE_TERM_FACE],
                    NULL, // face_fn
#ifdef P4_TO_P8
                    NULL, // edge_fn
#endif
                    NULL // corner_fn
        );

    }
  }

  // update old = new for all the quads
  CANOP_GLOBAL_INFO ("Copying new into old\n");
  p4est_iterate (p4est, 
		 ghost, 
		 NULL, 
		 m_pIter->iteratorsList[IT_ID::END_STEP], 
		 NULL,          // face_fn
#ifdef P4_TO_P8
                 NULL,          // edge_fn
#endif
                 NULL           // corner_fn
		 );
//  p4est_ghost_exchange_data (p4est, ghost, get_ghost_data_ptr());
  
} // SolverMfplasma::next_iteration_impl

// =======================================================
// =======================================================
void SolverMfplasma::read_config()
{
  // fisrt run base class method
  Solver::read_config();

  /*
   * Initialize mfplasma parameters.
   */
  mfplasmaPar.c_e = m_cfg->config_read_double ("settings.c_e", 1.0);
  mfplasmaPar.c_i = m_cfg->config_read_double ("settings.c_i", 1.0);
  
  mfplasmaPar.q_e = m_cfg->config_read_double ("settings.q_e", -1.0);
  mfplasmaPar.q_i = m_cfg->config_read_double ("settings.q_i", 1.0);

  mfplasmaPar.kappa   = m_cfg->config_read_double ("settings.kappa", 1.0);
  mfplasmaPar.epsilon = m_cfg->config_read_double ("settings.epsilon", 1.0);

  // electric field
  mfplasmaPar.elec.static_E_enabled = m_cfg->config_read_int ("settings.static_E_enabled", 0);

  mfplasmaPar.elec.poisson_enabled = m_cfg->config_read_int ("settings.poisson_elec_enabled", 0);
  mfplasmaPar.elec.enabled = mfplasmaPar.elec.static_E_enabled ||
                             mfplasmaPar.elec.poisson_enabled;
    
  if (mfplasmaPar.elec.enabled) {
    mfplasmaPar.elec.static_Ex = m_cfg->config_read_double ("static_elec_field.Ex",  0.0);
    mfplasmaPar.elec.static_Ey = m_cfg->config_read_double ("static_elec_field.Ey",  0.0);
    mfplasmaPar.elec.static_Ez = m_cfg->config_read_double ("static_elec_field.Ez",  0.0);

    mfplasmaPar.elec.poisson_imax = m_cfg->config_read_int ("poisson_elec.imax", 10000);
    mfplasmaPar.elec.poisson_tol = m_cfg->config_read_double ("poisson_elec.tol", 1e-4);
    mfplasmaPar.elec.poisson_reuse_potential = m_cfg->config_read_int ("poisson_elec.reuse_potential", 1);

    // read boundary conditions
    {
      static std::map<std::string, MfplasmaElecBC> boundary_map {
        {"neumann", MfplasmaElecBC::Neumann},
        {"dirichlet", MfplasmaElecBC::Dirichlet},
        {"periodic", MfplasmaElecBC::Periodic},
        {"monopole", MfplasmaElecBC::Monopole},
      };

      auto bound_str = m_cfg->config_read_std_string ("poisson_elec.boundary", "neumann");
      auto bound_it = boundary_map.find (bound_str);
      if (bound_it != boundary_map.end())
        mfplasmaPar.elec.poisson_boundary = bound_it->second;
      else {
        CANOP_GLOBAL_LERRORF ("Unrecognized Poisson boundary condition: \"%s\"\n",
                              bound_str.c_str());
        mfplasmaPar.elec.poisson_boundary = MfplasmaElecBC::Neumann;
      }
    }
  }

  mfplasmaPar.elec.lambda = m_cfg->config_read_double ("poisson_elec.lambda", 1e-2);

  // magnetic field

  mfplasmaPar.static_mag_field_enabled = m_cfg->config_read_int ("settings.static_mag_field_enabled", 1);
  mfplasmaPar.static_Bx = m_cfg->config_read_double ("settings.static_Bx", 0.0);
  mfplasmaPar.static_By = m_cfg->config_read_double ("settings.static_By", 0.0);
  mfplasmaPar.static_Bz = m_cfg->config_read_double ("settings.static_Bz", 1.0);
  
  mfplasmaPar.ionization_enabled = m_cfg->config_read_int ("settings.ionization_enabled", 1);
  mfplasmaPar.nu_iz = m_cfg->config_read_double ("settings.nu_iz", 1.0);
  
  mfplasmaPar.low_Mach_correction_enabled_e = m_cfg->config_read_int("settings.low_Mach_correction_enabled_e", 1);
  mfplasmaPar.low_Mach_correction_enabled_i = m_cfg->config_read_int("settings.low_Mach_correction_enabled_i", 0);
  mfplasmaPar.M_co = m_cfg->config_read_double ("settings.M_co", 0.01);

  /* add timers for the Poisson solver, if enabled */
  if (mfplasmaPar.elec.poisson_enabled) {
    Statistics::add_timer("t_elec_interp1", Statistics::timer_accum::mean, Statistics::timer_accum::sum);
    if(mfplasmaPar.elec.poisson_boundary == MfplasmaElecBC::Monopole)
      Statistics::add_timer("t_elec_boundary", Statistics::timer_accum::mean, Statistics::timer_accum::sum);
    Statistics::add_timer("t_elec_solve", Statistics::timer_accum::mean, Statistics::timer_accum::sum);
    Statistics::add_timer("t_elec_interp2", Statistics::timer_accum::mean, Statistics::timer_accum::sum);
    Statistics::add_timer("t_elec_min", Statistics::timer_accum::min, Statistics::timer_accum::min);
    Statistics::add_timer("t_elec_max", Statistics::timer_accum::max, Statistics::timer_accum::max);
    Statistics::add_timer("t_elec_mean", Statistics::timer_accum::mean, Statistics::timer_accum::sum);
  }

} // SolverMfplasma::read_config

} // namespace mfplasma

} // namespace canop
