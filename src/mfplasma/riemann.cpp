#include "riemann.h"

namespace canop {

namespace mfplasma {
    
// ===============================================================
// ===============================================================
    
double Liou_fct(double M_o, double M)
{
    return sqrt((1-M_o)*(1-M_o)*M*M+4*M*M)/(1+M_o*M_o);
}//Liou_fct

// ===============================================================
// ===============================================================
QdataCons riemann_llf_isoT(QdataPrim qleft, QdataPrim qright, double c,bool low_Mach_correction_enanbled, MfplasmaPar par)
{

  //============================
  // Compute maximum wave speed
  //============================
  double nl=qleft.n;
  double ul=qleft.u;
  double vl=qleft.v;
  double wl=qleft.w;
  double pl=qleft.n*c*c;
  
  double nr=qright.n;
  double ur=qright.u;
  double vr=qright.v;
  double wr=qright.w;
  double pr=qright.n*c*c;

  double cl= c;
  double cr= c;
  
  double cmax;
  if(low_Mach_correction_enanbled){
    double M_co = par.M_co;
    double M = (sqrt(nl)*ul+sqrt(nr)*nr)/(sqrt(nl)+sqrt(nr))*sqrt(par.epsilon);
    double M_o = fmin(1,fmax(M_co,M));
    double f_Liou = Liou_fct(M_o,M);
    cmax = (1+M_o)/2*fmax(fabs(ul)+f_Liou*cl,fabs(ur)+f_Liou*cr);
    //printf("low_Mach_correction_enabled_e");
  }else{
    cmax = fmax(fabs(ul)+cl,fabs(ur)+cr);
  }

  // Compute average velocity
  //qgdnv[IU] = HALF_F*(qleft[IU]+qright[IU]);

  //================================
  // Compute conservative variables
  //================================
  QdataCons uleft, uright;
  
  // mass density
  uleft .n = nl;
  uright.n = nr;

  // normal momentum
  uleft .nu = nl*ul;
  uright.nu = nr*ur;

  // transverse momentum
  uleft .nv = nl*vl;
  uright.nv = nr*vr;

  uleft .nw = nl*wl;
  uright.nw = nr*wr;

  //===============================
  // Compute left and right fluxes
  //===============================
  QdataCons fleft, fright;
  
  // mass density
  fleft .n = uleft .n * qleft .u;
  fright.n = uright.n * qright.u;

  // normal momentum
  fleft .nu = pl + uleft .nu * qleft .u;
  fright.nu = pr + uright.nu * qright.u;

  // transverse momentum
  fleft .nv = fleft .n * qleft .v;
  fright.nv = fright.n * qright.v;

  fleft .nw = fleft .n * qleft .w;
  fright.nw = fright.n * qright.w;

  //==============================
  // Compute Lax-Friedrich fluxes
  //==============================
  QdataCons flux = ( fleft + fright - (uright - uleft)*cmax ) * 0.5;

  return flux;

} //riemann_llf_isoT

// ===============================================================
// ===============================================================
QdataVar riemann_solver(const QdataVar& qleft,  // primitive variables
                        const QdataVar& qright, // primitive variables
                        const MfplasmaPar& par)
{
  QdataPrim qpL, qpR;
  QdataCons flux;

  QdataVar res;

  // electrons
  qpL.n = qleft.ne;
  qpL.u = qleft.nu_e;
  qpL.v = qleft.nv_e;
  qpL.w = qleft.nw_e;

  qpR.n = qright.ne;
  qpR.u = qright.nu_e;
  qpR.v = qright.nv_e;
  qpR.w = qright.nw_e;

  // check if isothermal eos...
  flux = riemann_llf_isoT(qpL, qpR, par.c_e,par.low_Mach_correction_enabled_e,par);

  res.ne   = flux.n;
  res.nu_e = flux.nu;
  res.nv_e = flux.nv;
  res.nw_e = flux.nw;

  // ions
  qpL.n = qleft.ni;
  qpL.u = qleft.nu_i;
  qpL.v = qleft.nv_i;
  qpL.w = qleft.nw_i;

  qpR.n = qright.ni;
  qpR.u = qright.nu_i;
  qpR.v = qright.nv_i;
  qpR.w = qright.nw_i;

  // check if isothermal eos...
  flux = riemann_llf_isoT(qpL, qpR, par.c_i,par.low_Mach_correction_enabled_i,par);

  res.ni   = flux.n;
  res.nu_i = flux.nu;
  res.nv_i = flux.nv;
  res.nw_i = flux.nw;

  return res;

} // riemann_solver

} // namespace mfplasma

} // namespace canop
