#ifndef ITERATORS_MFPLASMA_H_
#define ITERATORS_MFPLASMA_H_

#include "Iterators.h"


namespace canop {

namespace mfplasma {

/**
 * Adding new IDs to struct IT_ID.
 *
 * We start using number 32 (assuming the number of ID's in base class is
 * less than 32).
 *
 * TODO: enforce strongly that each new IDs below are initialized with
 * a number not already used in base class.
 */
struct IT_IDD : public IT_ID {

  static constexpr int SOURCE_TERM_CENTER = 32;
  static constexpr int SOURCE_TERM_FACE   = 33;

};

/*
 * NB: if you need to add new iterators:
 * 1. resize vector iterator_list_t
 * 2. create a new enum to ease indexing this vector
 * 3. add new iterator to the vector in fill_iterators_list method.
 */

class IteratorsMfplasma : public IteratorsBase {

public:
  IteratorsMfplasma();
  virtual ~IteratorsMfplasma();

  void fill_iterators_list();

}; // class IteratorsMfplasma

} // namespace mfplasma

} // namespace canop

#endif // ITERATORS_MFPLASMA_H_

