#ifndef SOLVER_CORE_MFPLASMA_H_
#define SOLVER_CORE_MFPLASMA_H_

#include "SolverCore.h"

#include "userdata_mfplasma.h"

class ConfigReader;

namespace canop {

namespace mfplasma {

/**
 * This is were Initial and Border Conditions callbacks are setup.
 */
class SolverCoreMfplasma : public SolverCore<Qdata>
{

public:
  SolverCoreMfplasma(ConfigReader *cfg);
  ~SolverCoreMfplasma();

  /**
   * Static creation method called by the solver factory.
   */
  static SolverCoreBase* create(ConfigReader *cfg)
  {
    return new SolverCoreMfplasma(cfg);
  }
  
}; // SolverCoreMfplasma

} // namespace mfplasma

} // namespace canop

#endif // SOLVER_CORE_MFPLASMA_H_
