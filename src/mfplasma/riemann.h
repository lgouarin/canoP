#ifndef MFPLASMA_RIEMANN_H_
#define MFPLASMA_RIEMANN_H_

#include "userdata_mfplasma.h"

namespace canop {

namespace mfplasma {
    
/*
 * Liou function, used for low Mach corrections
 */

double Liou_fct(double M_o, double M);

/**
 * Local Lax-Friedrich riemann solver (for ions or electrons).
 *
 * Reference : E.F. Toro, Riemann solvers and numerical methods for
 * fluid dynamics, Springer, chapter 10 (The HLL and HLLC Riemann solver).
 * See section 10.5.1, equation 10.43 which gives the expression of S+ 
 * denoted here as cmax.
 */
QdataCons riemann_llf_isoT(QdataPrim qL, QdataPrim qR, double c);

/**
 * Apply riemann solver on both electrons and ions.
 */
QdataVar riemann_solver(const QdataVar& qleft, 
                        const QdataVar& qright,
                        const MfplasmaPar& par);

} // namespace mfplasma

} // namespace canop

#endif // MFPLASMA_RIEMANN_H_
