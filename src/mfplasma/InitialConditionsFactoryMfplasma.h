#ifndef INITIAL_CONDITIONS_FACTORY_MFPLASMA_H_
#define INITIAL_CONDITIONS_FACTORY_MFPLASMA_H_

#include "InitialConditionsFactory.h"
#include "mfplasma/userdata_mfplasma.h"

namespace canop {

namespace mfplasma {

/**
 *
 */
class InitialConditionsFactoryMfplasma : public InitialConditionsFactory
{

  using qdata_t = Qdata;
  
public:
  InitialConditionsFactoryMfplasma();
  virtual ~InitialConditionsFactoryMfplasma() {};
  
}; // class InitialConditionsFactoryMfplasma

} // namespace mfplasma

} // namespace canop

#endif // INITIAL_CONDITIONS_FACTORY_MFPLASMA_H_
