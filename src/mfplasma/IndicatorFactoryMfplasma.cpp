#include "IndicatorFactoryMfplasma.h"
#include "utils.h" // for UNUSED macro

namespace canop {

namespace mfplasma {

// remember that Qdata is a concrete type defined in userdata_mfplasma.h
using qdata_t = Qdata;


// =======================================================
// =======================================================
double
indicator_mfplasma_density_gradient (qdata_t * cella, qdata_t * cellb,
                                     indicator_info_t * info)
{
  UNUSED (info);
  double              ne[2] = { cella->w.ne, cellb->w.ne };
  double              ni[2] = { cella->w.ni, cellb->w.ni };

  double eps_e = indicator_scalar_gradient (ne[0], ne[1]);
  double eps_i = indicator_scalar_gradient (ni[0], ni[1]);

  return SC_MAX(eps_e, eps_i);

} // indicator_mfplasma_density_gradient

// =======================================================
// ======CLASS IndicatorFactoryMfplasma IMPL ===============
// =======================================================

IndicatorFactoryMfplasma::IndicatorFactoryMfplasma() :
  IndicatorFactory<Qdata>()
{

  // register the above callback's
  registerIndicator("density_gradient"  , indicator_mfplasma_density_gradient);
  
} // IndicatorFactoryMfplasma::IndicatorFactoryMfplasma

} // namespace mfplasma

} // namespace canop
