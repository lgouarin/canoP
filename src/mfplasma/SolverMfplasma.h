#ifndef SOLVER_MFPLASMA_H_
#define SOLVER_MFPLASMA_H_

#include "Solver.h"
#include "userdata_mfplasma.h"

#include <mpi.h>

class ConfigReader;

namespace canop {

namespace mfplasma {

/**
 * Concrete Solver class implementation for Mfplasma application.
 */
class SolverMfplasma : public Solver
{

  using qdata_t = Qdata;
  
public:
  SolverMfplasma(ConfigReader * cfg, MPI_Comm mpicomm, bool useP4estMemory);
  virtual ~SolverMfplasma();

  // redefine compute_time_step
  void compute_time_step();
  
  /**
   * Here we implement unsplit numerical scheme for Mfplasma Hydro.
   */
  void next_iteration_impl();

  /**
   * Parameters specific to Mfplasma scheme.
   */
  MfplasmaPar mfplasmaPar;

  /**
   * Static creation method called by the solver factory.
   */
  static Solver* create(ConfigReader * cfg, MPI_Comm mpicomm, bool useP4estMemory)
  {
    SolverMfplasma* solver = new SolverMfplasma(cfg, mpicomm, useP4estMemory);

    return solver;
  }

private:
  /**
   * Custom version of base class version.
   */
  void read_config();
  
}; // class SolverMfplasma

} // namespace mfplasma

} // namespace canop

#endif // SOLVER_MFPLASMA_H_
