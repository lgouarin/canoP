#ifndef BOUNDARY_CONDITIONS_FACTORY_MFPLASMA_H_
#define BOUNDARY_CONDITIONS_FACTORY_MFPLASMA_H_

#include "BoundaryConditionsFactory.h"
#include "mfplasma/userdata_mfplasma.h"

namespace canop {

namespace mfplasma {

/**
 *
 */
class BoundaryConditionsFactoryMfplasma : public BoundaryConditionsFactory<Qdata>
{

  using qdata_t = Qdata;
  
public:
  BoundaryConditionsFactoryMfplasma();
  virtual ~BoundaryConditionsFactoryMfplasma() {};
  
}; // class BoundaryConditionsFactoryMfplasma

} // namespace mfplasma

} // namespace canop

#endif // BOUNDARY_CONDITIONS_FACTORY_MFPLASMA_H_
