#include "UserDataManagerMfplasma.h"
#include "canoP_base.h"
#include "eos.h"
#include "ScalarLimiterFactory.h"

namespace canop {

namespace mfplasma {

using mfplasma_getter_t = double (UserDataManagerMfplasma ::*) (p4est_quadrant_t * q);
using mfplasma_vgetter_t = void (UserDataManagerMfplasma ::*) (p4est_quadrant_t * q, size_t dim, double * val);

using scalar_limiter_t = ScalarLimiterFactory::scalar_limiter_t;

// ====================================================================
// ====================================================================
UserDataManagerMfplasma::UserDataManagerMfplasma(bool useP4estMemory,
						 MfplasmaPar &mfplasmaPar) :
  UserDataManager<UserDataTypesMfplasma>(),
  mfplasmaPar(mfplasmaPar)
{

  UNUSED(useP4estMemory);
  
} // UserDataManagerMfplasma::UserDataManagerMfplasma

// ====================================================================
// ====================================================================
UserDataManagerMfplasma::~UserDataManagerMfplasma()
{

} // UserDataManagerMfplasma::~UserDataManagerMfplasma

// ====================================================================
// ====================================================================
void
UserDataManagerMfplasma::qdata_set_variables (qdata_t * data,
                                              qdatavar_t * w)
{

  memcpy (&(data->w), w, sizeof (qdatavar_t));
  memcpy (&(data->wnext), w, sizeof (qdatavar_t));
  memset (&(data->welec), 0, sizeof (QdataElec));
  
} // UserDataManagerMfplasma::qdata_set_variables

// ====================================================================
// ====================================================================
void
UserDataManagerMfplasma::quadrant_set_state_data (p4est_quadrant_t * dst,
                                                  p4est_quadrant_t * src)
{

  qdata_t *data_dst = quadrant_get_qdata (dst);
  qdata_t *data_src = quadrant_get_qdata (src);

  memcpy (&(data_dst->w),     &(data_src->w),     sizeof (qdatavar_t));
  memcpy (&(data_dst->wnext), &(data_src->wnext), sizeof (qdatavar_t));
  memcpy (&(data_dst->welec), &(data_src->welec), sizeof (QdataElec));

} // UserDataManagerMfplasma::quadrant_set_state_variables

// ====================================================================
// ====================================================================
void
UserDataManagerMfplasma::quadrant_compute_mean(p4est_quadrant_t *qmean,
                                               p4est_quadrant_t *quadout[P4EST_CHILDREN])
{

  qdata_t     *child_data;
  qdatavar_t   mean; // initialized to zero
  QdataElec   gmean;
  //memset (&gmean, 0, sizeof (QdataElec));

  for (int8_t i = 0; i < P4EST_CHILDREN; ++i) {

    child_data = quadrant_get_qdata (quadout[i]);

    mean += child_data->w;

    gmean.phi += child_data->welec.phi / P4EST_CHILDREN;
    for (int j = 0; j < 3/*P4EST_DIM*/; ++j) {
      gmean.accel[j] += child_data->welec.accel[j] / P4EST_CHILDREN;
    }

  }

  mean /= P4EST_CHILDREN;

  // finally, copy all of that into qmean
  quadrant_set_variables (qmean, &mean);

  // set welec
  memcpy (&(quadrant_get_qdata(qmean)->welec), &gmean, sizeof (QdataElec));

} // quadrant_compute_mean

// ====================================================================
// ====================================================================
void
UserDataManagerMfplasma::qdata_print_variables (int loglevel, qdata_t * data)
{
  CANOP_LOGF (loglevel, "ne    = %-15.8lf %.8lf\n",
              data->w.ne, data->wnext.ne);
  CANOP_LOGF (loglevel, "ni    = %-15.8lf %.8lf\n",
              data->w.ni, data->wnext.ni);

} // UserDataManagerMfplasma::qdata_print_variables

// ====================================================================
// ====================================================================
void
UserDataManagerMfplasma::quadrant_copy_w_to_wnext (p4est_quadrant_t * q)
{
  qdata_t *data = quadrant_get_qdata (q);
  data->wnext = data->w;
  
} // UserDataManagerMfplasma::quadrant_copy_w_to_wnext

// ====================================================================
// ====================================================================
void
UserDataManagerMfplasma::quadrant_copy_wnext_to_w (p4est_quadrant_t * q)
{
  qdata_t *data = quadrant_get_qdata (q);
  data->w = data->wnext;
  
} // UserDataManagerMfplasma::quadrant_copy_wnext_to_w

// ====================================================================
// ====================================================================
double
UserDataManagerMfplasma::qdata_get_ne (p4est_quadrant_t * q)
{
  qdata_t *data = quadrant_get_qdata (q);

  return data->w.ne;
  
} // UserDataManagerMfplasma::qdata_get_U

// ====================================================================
// ====================================================================
double
UserDataManagerMfplasma::qdata_get_ni (p4est_quadrant_t * q)
{
  qdata_t *data = quadrant_get_qdata (q);

  return data->w.ni;
  
} // UserDataManagerMfplasma::qdata_get_V

// ====================================================================
// ====================================================================
void
UserDataManagerMfplasma::qdata_vget_rhoVe (p4est_quadrant_t * q, size_t dim, double * val)
{
  qdata_t *data = quadrant_get_qdata (q);

  val[IX] = data->w.nu_e;
  val[IY] = data->w.nv_e;
  if (dim==3) {
    val[IZ] = data->w.nw_e;
  }

} // UserDataManagerMfplasma::qdata_vget_rhoVe

// ====================================================================
// ====================================================================
void
UserDataManagerMfplasma::qdata_vget_rhoVi (p4est_quadrant_t * q, size_t dim, double * val)
{
  qdata_t *data = quadrant_get_qdata (q);

  val[IX] = data->w.nu_i;
  val[IY] = data->w.nv_i;
  if (dim==3) {
    val[IZ] = data->w.nw_i;
  }

} // UserDataManagerMfplasma::qdata_vget_rhoVi

// ====================================================================
// ====================================================================
double
UserDataManagerMfplasma::qdata_get_rhoc (p4est_quadrant_t * q)
{

  const double& q_e = mfplasmaPar.q_e;
  const double& q_i = mfplasmaPar.q_i;

  qdata_t *data = quadrant_get_qdata (q);

  return q_e*data->w.ne + q_i*data->w.ni;
  
} // UserDataManagerMfplasma::qdata_get_rhoc

// ====================================================================
// ====================================================================
double
UserDataManagerMfplasma::qdata_get_elec_phi (p4est_quadrant_t * q)
{
  qdata_t *data = quadrant_get_qdata (q);

  return data->welec.phi;
  
} // UserDataManagerMfplasma::qdata_get_elec_phi

// ====================================================================
// ====================================================================
void
UserDataManagerMfplasma::qdata_set_elec_phi (p4est_quadrant_t * q, double val)
{
  qdata_t *data = quadrant_get_qdata (q);

  data->welec.phi = val;

} // UserDataManagerMfplasma::qdata_set_elec_phi

// ====================================================================
// ====================================================================
void
UserDataManagerMfplasma::qdata_set_elec_acc (p4est_quadrant_t * q, int direction, double val)
{
  qdata_t *data = quadrant_get_qdata (q);

  //if(direction > 0 && direction < P4EST_DIM)
  data->welec.accel[direction] = val;

} // UserDataManagerMfplasma::qdata_set_elec_acc

// ====================================================================
// ====================================================================
void
UserDataManagerMfplasma::qdata_set_elec_acc_v (p4est_quadrant_t * q, double *vec)
{
  qdata_t *data = quadrant_get_qdata (q);

  for(size_t i = 0; i < P4EST_DIM; ++i)
    data->welec.accel[i] = vec[i];

  if (P4EST_DIM==2)
    data->welec.accel[IZ] = 0.0;

} // UserDataManagerMfplasma::qdata_set_elec_acc_v

// ====================================================================
// ====================================================================
void
UserDataManagerMfplasma::qdata_vget_elec_field (p4est_quadrant_t * q, size_t dim, double * val)
{
  qdata_t *data = quadrant_get_qdata (q);

  val[IX] = data->welec.accel[IX];
  val[IY] = data->welec.accel[IY];
  if (dim==3) {
    val[IZ] = data->welec.accel[IZ];
  } else {
    val[IZ] = 0.0;
  }

} // UserDataManagerMfplasma::qdata_vget_elec_field

// ====================================================================
// ====================================================================
qdata_getter_t
UserDataManagerMfplasma::qdata_getter_byname (const std::string &varName)
{

  qdata_getter_t base_getter = nullptr;
  mfplasma_getter_t getter = nullptr;
  
  if (!varName.compare("ne")) {
    getter = &UserDataManagerMfplasma::qdata_get_ne;
  }

  if (!varName.compare("ni")) {
    getter = &UserDataManagerMfplasma::qdata_get_ni;
  }
    
  if (!varName.compare("phi")) {
    getter = &UserDataManagerMfplasma::qdata_get_elec_phi;
  }

  if (getter != nullptr) {

    // convert to base class member function pointer
    base_getter = static_cast<qdata_getter_t>(getter);

  } else {

    // print warning
    CANOP_GLOBAL_INFOF ("Unrecognized getter: \"%s\"\n", varName.c_str());
    
  }
  
  return base_getter;
  
} // UserDataManagerMfplasma::qdata_getter_byname

// ====================================================================
// ====================================================================
qdata_vector_getter_t
UserDataManagerMfplasma::qdata_vgetter_byname (const std::string &varName)
{

  qdata_vector_getter_t base_getter = nullptr;
  mfplasma_vgetter_t getter = nullptr;

  if (!varName.compare("rhoVe")) {
    getter = &UserDataManagerMfplasma::qdata_vget_rhoVe;
  } else if (!varName.compare("rhoVi")) {
    getter = &UserDataManagerMfplasma::qdata_vget_rhoVi;
  } else if (!varName.compare("elec_field")) {
    getter = &UserDataManagerMfplasma::qdata_vget_elec_field;
  }

  if (getter != nullptr) {

    // convert to base class member function pointer
    base_getter = static_cast<qdata_vector_getter_t>(getter);

  } else {

    // print warning
    P4EST_GLOBAL_INFOF ("Unrecognized getter: \"%s\"\n", varName.c_str());

  }

  return base_getter;
  
} // UserDataManagerMfplasma::qdata_vgetter_byname

// ====================================================================
// ====================================================================
qdata_field_type
UserDataManagerMfplasma::qdata_type_byname(const std::string &varName)
{

  if (!varName.compare("ne") ||
      !varName.compare("ni") ||
      !varName.compare("phi") )
    return QDATA_FIELD_SCALAR;

  if (!varName.compare("rhoVe") ||
      !varName.compare("rhoVi") ||
      !varName.compare("elec_field") )
    return QDATA_FIELD_VECTOR;

  // print warning
  P4EST_GLOBAL_INFOF ("Unrecognized getter: \"%s\"\n", varName.c_str());
  return QDATA_FIELD_UNKNOWN;

} // UserDataManagerMfplasma::qdata_type_byname

} // namespace mfplasma

} // namespace canop
