#include "QuadrantNeighborUtils.h"
#include "quadrant_utils.h"

#ifndef P4_TO_P8
#include <p4est_bits.h>
#include <p4est_mesh.h>
#include <p4est_geometry.h>
#else
#include <p8est_bits.h>
#include <p8est_mesh.h>
#include <p8est_geometry.h>
#endif

#include <sc.h> // for SC_LP_ERROR macro
#include <cmath>

#include "SolverMfplasma.h"
#include "SolverCoreMfplasma.h"
#include "SolverWrap.h"
#include "IteratorsMfplasma.h"
#include "UserDataManagerMfplasma.h"
#include "userdata_mfplasma.h"

#include "IndicatorFactoryMfplasma.h"
//#include "RiemannSolverFactoryMfplasma.h"
#include "riemann.h"

namespace canop {

namespace mfplasma {

// get userdata specific stuff
using qdata_t               = Qdata;
using qdata_variables_t     = QdataVar;
using qdata_reconstructed_t = QdataRecons;

using UserDataTypesMfplasma = UserDataTypes<Qdata,
					   QdataVar,
					   QdataRecons>;

using QuadrantNeighborUtilsMfplasma = QuadrantNeighborUtils<UserDataTypesMfplasma>;

constexpr int IT_IDD::SOURCE_TERM_CENTER;
constexpr int IT_IDD::SOURCE_TERM_FACE;

// =======================================================
// =======================================================
/**
 * \brief Test whether the current quad should be skipped.
 *
 * This routine handles quandrant skipping for subcycling. A quadrant is skipped if:
 *  * subcycling is enabled, and
 *  * the quadrant level is not equal to the current subcycling level
 */
static bool
should_skip_quad(SolverMfplasma *solver, p4est_quadrant_t *quad)
{
  if (true /*solver->m_subcycle == nullptr*/)
    return false;

  // else
  //   return (quad->level != solver->m_subcycle->level);
} // should_skip_quad

// =======================================================
// =======================================================
/**
 * \brief Test whether the current interface should be skipped.
 *
 * This routine ensures that the update with fluxes through each face is done
 * only once per relevant subcycle.
 *
 * For non-subcycling solvers, a face is skipped if:
 *  * the neighbour is not a ghost, and
 *  * the current cumulative index is greater than the neighbor cumulative
 *    index -TODO this pars has been neglected when replacing should_skip functions by "true",
 *    must be correctly reimplemented in order not to compute twice fluxes
 *
 * For subcycling solvers, a face is skipped if:
 *  * the current cell is not at the current subcycling level, or
 *  * the neighbor cell is not a ghost, and
 *    + the neighbor cell is finer than the current cell
 *    + the current and neighbor cells have the same level and the current
 *      cumulative index is greater than the neighbor cumulative index, or
 */
static bool
should_skip_face(SolverMfplasma *solver, bool neighbor_is_ghost, bool hanging_face,
                 p4est_locidx_t quadid_cum_current, p4est_locidx_t quadid_cum_neighbor,
                 p4est_quadrant_t *quad_current, p4est_quadrant_t *quad_neighbor)
{
  if (true /*solver->m_subcycle == nullptr*/) {
    if (neighbor_is_ghost)
      return false;

    return (quadid_cum_current > quadid_cum_neighbor);
  }

  // else {
  //   if (quad_current->level != solver->m_subcycle->level)
  //     return true;

  //   if (neighbor_is_ghost)
  //     return false;

  //   if (hanging_face)
  //     return true;

  //   return (quad_current->level == quad_neighbor->level &&
  //       quadid_cum_current > quadid_cum_neighbor);

  // }
} // should_skip_face

/**
 * Convert conservative to primitive (electrons and ions)
 *
 * Probably the right place to apply Equation of state.
 */
static QdataVar
cons2prim(QdataVar qcons, MfplasmaPar par)
{

  QdataVar qprim;

  qprim.ne = qcons.ne;
  qprim.ni = qcons.ni;

  qprim.nu_e = qcons.nu_e/qcons.ne;
  qprim.nv_e = qcons.nv_e/qcons.ne;
  qprim.nw_e = qcons.nw_e/qcons.ne;

  qprim.nu_i = qcons.nu_i/qcons.ni;
  qprim.nv_i = qcons.nv_i/qcons.ni;
  qprim.nw_i = qcons.nw_i/qcons.ni;

  return qprim;

} // cons2prim

/**
 * Cartesian coordinates update.
 *
 * \param[in,out] data data from both sides
 * \param[in] face the face id of interface as seen from both sides
 * \param[in] dt   time step
 * \param[in] dS   interface surface (3D) or length (2D)
 * \param[in] dV   volume of both cells  
 * \param[in] solver solver structure to retrieve riemann solver routine
 *
 */
static void
mfplasma_update_face (qdata_t * data[2], 
                      int face[2], 
                      double dt, 
                      double dS,
                      double dV[2],
                      Solver * solver)
{

  SolverMfplasma *solverMfplasma = static_cast<SolverMfplasma *>(solver);
  //riemann_solver_t riemann_solver_fn = solverMfplasma->m_riemann_solver_fn; - TODO

  MfplasmaPar &mfplasmaPar = solverMfplasma->mfplasmaPar;

  double dtdx[2] = { dt*dS/dV[CELL_CURRENT], 
		     dt*dS/dV[CELL_NEIGHBOR]};
    
  /* we will solver Riemann problem associated to the input face data */
  QdataVar qleft, qright; // are primitive variables
  QdataVar flux;

  double extra = 0;

  int dir = face[CELL_CURRENT]/2;

  /* retrieve qleft / qright for the given face direction */
  if (face[CELL_CURRENT] % 2 == 0) { // we have a left interface
    
    // interface right state is in wm of face[CELL_CURRENT]
    qright = cons2prim(data[CELL_CURRENT]->w, mfplasmaPar);
    // interface left  state is in wp of face[CELL_NEIGHBOR]
    qleft  = cons2prim(data[CELL_NEIGHBOR]->w, mfplasmaPar);
    
  } else { // face[CELL_CURRENT] % 2 is 1  // we have a right interface
    
    // interface right state is in wm of face[CELL_NEIGHBOR]
    qright = cons2prim(data[CELL_NEIGHBOR]->w, mfplasmaPar);
    // interface left  state is in wp of face[CELL_CURRENT]
    qleft  = cons2prim(data[CELL_CURRENT]->w, mfplasmaPar);
    
  }

  /*
   * solve riemann problem and update current cell
   */
  if (dir == IX) {
    
    // solve riemann problem - TODO add parameter to switch the riemann solver type
    flux = riemann_solver(qleft, qright, mfplasmaPar);
    
    // update current cell
    if (face[CELL_CURRENT]%2 == 0) {
      
      data[CELL_CURRENT]->wnext  += flux * dtdx[0];
      data[CELL_NEIGHBOR]->wnext -= flux * dtdx[1];
      
    } else {

      data[CELL_CURRENT]->wnext  -= flux * dtdx[0];
      data[CELL_NEIGHBOR]->wnext += flux * dtdx[1];

    }
    
  } else if (dir == IY) {
        
    // need to swap direction IX and IY before entering riemann solver
    std::swap( qleft.nu_e,  qleft.nv_e );
    std::swap( qleft.nu_i,  qleft.nv_i );
    std::swap( qright.nu_e,  qright.nv_e );
    std::swap( qright.nu_i,  qright.nv_i );
    
    // solve riemann problem
    flux = riemann_solver(qleft, qright, mfplasmaPar);
    
    std::swap( flux.nu_e,  flux.nv_e );
    std::swap( flux.nu_i,  flux.nv_i );
    
    // update current cell
    if (face[CELL_CURRENT]%2 == 0) {

      data[CELL_CURRENT]->wnext  += flux * dtdx[0];
      data[CELL_NEIGHBOR]->wnext -= flux * dtdx[1];

    } else {

      data[CELL_CURRENT]->wnext  -= flux * dtdx[0];
      data[CELL_NEIGHBOR]->wnext += flux * dtdx[1];

    }
    
  } else if (dir == IZ) { // TODO 
        
    // // need to swap direction IX and IZ before entering riemann solver
    // std::swap( qleft.nu_e,  qleft.nw_e );
    // std::swap( qleft.nu_i,  qleft.nw_i );
    // std::swap( qright.nu_e,  qright.nw_e );
    // std::swap( qright.nu_i,  qright.nw_i );
    
    // // solve riemann problem
    // riemann_solver(qleft, qright, mfplasmaPar);
    
    // std::swap( flux.nu_e,  flux.nw_e );
    // std::swap( flux.nu_i,  flux.nw_i );
    
    // // update current cell
    // if (face[CELL_CURRENT]%2 == 0) {
    //   data[CELL_CURRENT]->wnext  += flux * dtdx[0];
    
    //   data[CELL_NEIGHBOR]->wnext -= flux * dtdx[1];
    // } else {
    //   data[CELL_CURRENT]->wnext  -= flux * dtdx[0];
    
    //   data[CELL_NEIGHBOR]->wnext += flux * dtdx[1];
    // }
    
  } /* end direction */
  
} // mfplasma_update_face

/**
 *
 */
static
QdataVar compute_source_term(const QdataVar &data, 
                             const QdataElec &qelec,
                             double dt,
                             const MfplasmaPar& mfplasmaPar) {

  QdataVar src;

  const bool& elec_static_on = mfplasmaPar.elec.static_E_enabled;
  const bool& elec_poisson_on = mfplasmaPar.elec.poisson_enabled;
  const bool& elec_on = mfplasmaPar.elec.enabled;

  double Ex = mfplasmaPar.elec.static_Ex;
  double Ey = mfplasmaPar.elec.static_Ey;
  double Ez = mfplasmaPar.elec.static_Ez;

  const bool& mag_on = mfplasmaPar.static_mag_field_enabled;
  const double& Bx = mfplasmaPar.static_Bx;
  const double& By = mfplasmaPar.static_By;
  const double& Bz = mfplasmaPar.static_Bz;

  const bool& iz_on = mfplasmaPar.ionization_enabled;
  const double& nu_iz = mfplasmaPar.nu_iz;
  
  const double& q_e = mfplasmaPar.q_e;
  const double& q_i = mfplasmaPar.q_i;

  const double& epsilon = mfplasmaPar.epsilon;
  if (elec_poisson_on) {

    // TODO scaling L Debye
    double lambda2 = mfplasmaPar.elec.lambda;
    lambda2 *= lambda2;

    Ex = -qelec.accel[IX]/lambda2;
    Ey = -qelec.accel[IY]/lambda2;
    if (P4EST_DIM==3)
      Ez = -qelec.accel[IZ]/lambda2;
    else
      Ez = 0.0;
  }

  // compute source terms
  src.ne = dt * data.ne * iz_on * nu_iz;
  
  src.nu_e = dt * q_e * data.ne / epsilon *
    (elec_on * Ex + mag_on * (data.nv_e * Bz - data.nw_e * By));
  
  src.nv_e = dt * q_e * data.ne / epsilon *
    (elec_on * Ey + mag_on * (data.nw_e * Bx - data.nu_e * Bz));
  
  src.nw_e = dt * q_e * data.ne / epsilon *
    (elec_on * Ez + mag_on * (data.nu_e * By - data.nv_e * Bx));
  
  src.ni = dt * data.ne * iz_on * nu_iz;
  src.nu_i = dt * q_i * data.ni *
    (elec_on * Ex + mag_on * (data.nv_i * Bz - data.nw_i * By));
  src.nv_i = dt * q_i * data.ni *
    (elec_on * Ey + mag_on * (data.nw_i * Bx - data.nu_i * Bz));
  src.nw_i = dt * q_i * data.ni *
    (elec_on * Ez + mag_on * (data.nu_i * By - data.nv_i * Bx));

  return src;

} // compute_source_term

// =======================================================
// =======================================================
static void
iterator_start_step (p4est_iter_volume_info_t * info,
		     void *user_data)
{
  UNUSED (user_data);

  // retrieve our Solver, UserDataManager and finaly our quadrant data
  SolverMfplasma *solver = static_cast<SolverMfplasma*>(solver_from_p4est (info->p4est));

  UserDataManagerMfplasma
    *userdata_mgr = static_cast<UserDataManagerMfplasma *> (solver->get_userdata_mgr());

  userdata_mgr->quadrant_copy_w_to_wnext(info->quad);
  
} // iterator_start_step

// =======================================================
// =======================================================
static void iterator_end_step (p4est_iter_volume_info_t * info,
			       void *user_data)
{
  UNUSED (user_data);

  // retrieve our Solver, UserDataManager and finaly our quadrant data
  SolverMfplasma    *solver = static_cast<SolverMfplasma*>(solver_from_p4est (info->p4est));

  UserDataManagerMfplasma
    *userdata_mgr = static_cast<UserDataManagerMfplasma *> (solver->get_userdata_mgr());

  userdata_mgr->quadrant_copy_wnext_to_w(info->quad);
  
} // iterator_end_step

// =======================================================
// =======================================================
static void iterator_mark_adapt (p4est_iter_volume_info_t * info,
				 void *user_data)
{
  UNUSED (user_data);

  p4est_t            *p4est = info->p4est;
  Solver             *solver = solver_from_p4est (info->p4est);
  SolverWrap         *solverWrap = solver->m_wrap;
  UserDataManagerMfplasma
    *userdata_mgr = static_cast<UserDataManagerMfplasma *> (solver->get_userdata_mgr());

  SolverCoreMfplasma   *solverCore = static_cast<SolverCoreMfplasma *>(solver->m_solver_core);
  
  p4est_ghost_t      *ghost  = solver->get_ghost();
  p4est_mesh_t       *mesh   = solver->get_mesh();
  qdata_t            *ghost_data = static_cast<qdata_t *>(userdata_mgr->get_ghost_data_ptr());
  qdata_t            *qdata;
  int                 level = info->quad->level;

  int                 boundary = 0;
  int                 face = 0;
  double              eps = 0.0;
  double              eps_max = -1.0;

  p4est_quadrant_t   *neighbor = nullptr;
  qdata_t            *ndata = nullptr;

  //indicator_t         indicator = solver->m_indicator_fn;
  indicator_info_t    iinfo;
  iinfo.face = 0;
  iinfo.epsilon = solver->m_epsilon_refine;
  iinfo.quad_current = info->quad;
  //iinfo.user_data = static_cast<void*>(&(solverMfplasma->mfplasmaPar));

  /* init face neighbor iterator */
  p4est_mesh_face_neighbor_t mfn;
  p4est_mesh_face_neighbor_init2 (&mfn, p4est, ghost, mesh,
                                  info->treeid, info->quadid);

  /* loop over neighbors */
  neighbor = p4est_mesh_face_neighbor_next (&mfn, NULL, NULL, NULL, NULL);
  while (neighbor) {
    ndata = QuadrantNeighborUtilsMfplasma::mesh_face_neighbor_data (&mfn, ghost_data, &boundary);

    /* mfn.face is initialized to 0 by p4est_mesh_face_neighbor_init2 and
     * is incremented:
     *  * when calling p4est_mesh_face_neighbor_next and the neighbor has
     *    smaller or equal level
     *  * when calling p4est_mesh_face_neighbor_next and the neighbor has a
     *    bigger level, but only after seeing the 2 (or 4) smaller neighbors.
     */
    iinfo.face = mfn.face + (mfn.subface > 0) - 1;
    iinfo.quad_neighbor = neighbor;

    /* compute the indicator */
    qdata = userdata_mgr->quadrant_get_qdata (info->quad);
    eps = solverCore->m_indicator_fn (qdata, ndata, &iinfo);
    eps_max = SC_MAX (eps_max, eps);

    /* if the neighbor is on the boundary, the data was allocated on the fly,
     * so we have to deallocate it.
     */
    if (boundary == 1) {
      sc_mempool_free (info->p4est->user_data_pool, ndata);
    }

    neighbor = p4est_mesh_face_neighbor_next (&mfn, NULL, NULL, &face, NULL);

  } // end while. all neighbors visited.

  /* we suppose that epsilon_refine >= epsilon_coarsen */
  if (level > solver->m_min_refine && eps_max < solver->m_epsilon_coarsen) {
    solverWrap->mark_coarsen(info->treeid, info->quadid);
  } else if (level < solver->m_max_refine && eps_max > solver->m_epsilon_refine) {
    solverWrap->mark_refine(info->treeid, info->quadid);
  } else {
    return;
  }

} // iterator_mark_adapt

// =======================================================
// =======================================================
/**
 * \brief Compute the time step as dt = minimum of sigma * dx * dx / nu.
 *
 * An iterator over all the cells to compute this minimum.
 * The result is stored in the user_data.
 */
static void iterator_time_step (p4est_iter_volume_info_t * info,
				void *user_data)
{

  UNUSED (user_data);

  /* get forest structure */
  SolverMfplasma         *solver = static_cast<SolverMfplasma*>(solver_from_p4est (info->p4est));
  //SolverWrap           *solverWrap = solver->m_wrap;
  
  /* experimental modif zone*/
  
  SolverMfplasma *solverMfplasma = static_cast<SolverMfplasma *>(solver);

  MfplasmaPar &mfplasmaPar = solverMfplasma->mfplasmaPar;

  UserDataManagerMfplasma
    *userdata_mgr = static_cast<UserDataManagerMfplasma *> (solver->get_userdata_mgr());

  p4est_quadrant_t   *quad   = info->quad;
  
  qdata_t *data = userdata_mgr->quadrant_get_qdata (quad);
  
  const double& ne = data->w.ne;
  const double& nu_e = data->w.nu_e;
  const double& nv_e = data->w.nv_e;
  const double& c_e = mfplasmaPar.c_e;
  
  const double& ni = data->w.ni;
  const double& nu_i = data->w.nu_i;
  const double& nv_i = data->w.nv_i;
  const double& c_i = mfplasmaPar.c_i;
  
  double cmax_e; 
  double cmax_i;
  double cmax;
  
  const bool& low_Mach_correction_enabled_e = mfplasmaPar.low_Mach_correction_enabled_e;
  const bool& low_Mach_correction_enabled_i = mfplasmaPar.low_Mach_correction_enabled_i;
  
  if(low_Mach_correction_enabled_e){
    double M_co = mfplasmaPar.M_co;
    double Mu = nu_e/ne*sqrt(mfplasmaPar.epsilon);
    double Mv = nv_e/ne*sqrt(mfplasmaPar.epsilon);
    double Mo_u = fmin(1,fmax(M_co,Mu)); //TODO problem when M<0?
    double Mo_v = fmin(1,fmax(M_co,Mv));
    double fLiou_u = Liou_fct(Mo_u,Mu);
    double fLiou_v = Liou_fct(Mo_v,Mv);
    cmax_e = fmax((1+Mo_u)/2*fabs(nu_e)/ne+fLiou_u*c_e,(1+Mo_v)/2*fabs(nv_e)/ne+fLiou_v*c_e);
  }else{
    cmax_e = fmax(nu_e,nv_e)/ne + c_e;
  }
  
  if(low_Mach_correction_enabled_i){
    double M_co = mfplasmaPar.M_co;
    double Mu = nu_i/ni*sqrt(mfplasmaPar.epsilon);
    double Mv = nv_i/ni*sqrt(mfplasmaPar.epsilon);
    double Mo_u = fmin(1,fmax(M_co,Mu)); //TODO problem when M<0?
    double Mo_v = fmin(1,fmax(M_co,Mv));
    double fLiou_u = Liou_fct(Mo_u,Mu);
    double fLiou_v = Liou_fct(Mo_v,Mv);
    cmax_i = fmax((1+Mo_u)/2*fabs(nu_i)/ni+fLiou_u*c_i,(1+Mo_v)/2*fabs(nv_i)/ni+fLiou_v*c_i);
  }else{
    cmax_i = fmax(nu_i,nv_i)/ni + c_i;
  }
  
  cmax = fmax(cmax_e,cmax_i);
  
  // get config reader
  ConfigReader    *cfg = solver->m_cfg;
  UNUSED (cfg);
  
  double CFL = cfg->config_read_double ("settings.CFL", 0.9); //probably better to do this in solver directly -TODO

  /* end of experimental modif zone*/
  
  double             *min_dt = (double *) user_data; //has been changed from min_dt

  double dx,dy,dz;
  dx = dy = dz = quadrant_length (info->quad);

  // compute dt in CURRENT CELL
  double dt = dx*CFL/(2*cmax); // might be replaced by 0.001 to simplify temporarily TODO

  *min_dt = SC_MIN (*min_dt, dt);

  /* also compute the min and max levels in the mesh */
  solver->m_minlevel = SC_MIN (solver->m_minlevel, info->quad->level);
  solver->m_maxlevel = SC_MAX (solver->m_maxlevel, info->quad->level);
  
} // iterator_time_step

// =======================================================
// =======================================================
/**
 * \brief Update the value in a quadrant using the Mfplasma scheme.
 *
 * This is were the Riemann solver enters into play.
 *
 * Update actually happens on both side of the interface:
 * - when morton index  (actually quadid) of current cell is smaller than 
 *   that of neighbor
 * - when current cell is regular and neighbor is ghost
 */
static void
iterator_mfplasma_update (p4est_iter_volume_info_t * info, 
                          void *user_data)
{

  /* get forest structure */
  p4est_t              *p4est = info->p4est;
  
  /* get solver and geometry (only for unstructured) */
  SolverMfplasma       *solver = static_cast<SolverMfplasma*>(solver_from_p4est (p4est));

  UserDataManagerMfplasma
    *userdata_mgr = static_cast<UserDataManagerMfplasma *> (solver->get_userdata_mgr());
  

  int                *direction = (int *) user_data;
  double              dx  , dy  , dz  ; // current  cell
  double              dx_n, dy_n, dz_n; // neighbor cell
  double              dx_min;

  p4est_quadrant_t   *quad   = info->quad;
  p4est_locidx_t      quadid = info->quadid;
  p4est_topidx_t      treeid = info->treeid;

  p4est_tree_t       *tree = p4est_tree_array_index (p4est->trees, treeid);

  p4est_locidx_t      quadid_cum = tree->quadrants_offset + quadid;

  double              dS; /* interface surface (3D) or length (2D) */
  double              dV[2]; /* cells volumes (3D) or area (2D) */

  /* 2 states: one for each side of the face */
  qdata_t            *data[2] = { NULL, NULL };

  /* the face Id in range 0..P4EST_FACES-1 */
  int                 qf[2] = { -1, -1 };

  double              scale = 0.0;
  quadrant_neighbor_t<qdata_t> n;

  /* geometrical terms */
  int                 dir_start = (direction == NULL ? 0 : *direction);
  int                 dir_end = (direction == NULL ? P4EST_DIM : *direction + 1);

  double              dxL, dxR;
  qdata_variables_t   dataL, dataR, dataC,Fx,Fy;
  
  double dt = solver->m_dt;
  //double nu = solver->m_cfg->config_read_double ("ramp.nu", 0.07);
  
  UNUSED (scale);
  UNUSED (dx_min);

  /* get current cell data */
  data[CELL_CURRENT] = userdata_mgr->quadrant_get_qdata (quad);
  
  /* current cell length / location */
  
  //quadrant_get_dx_dy_dz(conn, NULL, quad, treeid, &dx, &dy, &dz);
  dx = dy = dz = quadrant_length (info->quad);
  
  /* current cell volume */
#ifdef USE_3D
  dV[CELL_CURRENT] = dx*dy*dz;
#else
  dV[CELL_CURRENT] = dx*dy;
#endif
  
  /* loop over all quadrant's faces */
  for( int dir = dir_start; dir < dir_end; ++dir) {
    
    for (int face = 0; face < 2; ++face) {

      /* get the current face in the given direction */
      qf[CELL_CURRENT] = QuadrantNeighborUtilsBase::direction_faces[dir][face];      
      
      /* 
       * get the Qdata of the neighbors on this face 
       * It is important to realize here that, if face touches the outside
       * boundary, there will be a call to our boundary_fn callback
       * which will fill the wm, wp states
       */
      QuadrantNeighborUtilsMfplasma::quadrant_get_face_neighbor (solver,
                                                                 quadid,
                                                                 treeid,
                                                                 qf[CELL_CURRENT],
                                                                 &n);
      qf[CELL_NEIGHBOR] = n.face;
      
      /* 
       *
       */
      if (n.is_hanging) {
        
	/* half neighbors interface loop */
	for (int i = 0; i < P4EST_HALF; ++i) {
          
          dx_n = dy_n = dz_n = dx*0.5;
          
          dV[CELL_NEIGHBOR] = dx_n * dy_n;
#if USE_3D
          dV[CELL_NEIGHBOR] *= dz_n;
#endif
          
          /* common interface length is neighbor face area */
          dS = quadrant_face_area(dx_n, dy_n, dz_n, dir);
          
          
          /* perform update only when needed */
          if (!should_skip_face(solver, n.is.hanging.is_ghost[i],
                                true, quadid_cum, 
                                n.is.hanging.quadid_cum[i], 
                                quad,
                                n.is.hanging.quad[i])) {

            data[CELL_NEIGHBOR] = n.is.hanging.data[i];

            mfplasma_update_face(data, qf, solver->m_dt, dS, dV, solver);

          }

        } /* end for i=0..P4EST_HELF */

      } else {

          /* full neighbor (same size or larger) */
	  if (n.level < info->quad->level)
	    dx_n = dy_n = dz_n = quadrant_length (info->quad)*2;
	  else
	    dx_n = dy_n = dz_n = quadrant_length (info->quad);


	  dV[CELL_NEIGHBOR] = dx_n * dy_n;
#if USE_3D
	  dV[CELL_NEIGHBOR] *= dz_n;
#endif
	  
	  /* common interface length is current face area */
	  dS = quadrant_face_area(dx, dy, dz, dir);

          /* perform update only when needed */
          if (!should_skip_face(solver, n.is.full.is_ghost, 
                                false, quadid_cum,
                                n.is.full.quadid_cum, 
                                quad, 
                                n.is.full.quad)) {

            data[CELL_NEIGHBOR] = n.is.full.data;

            mfplasma_update_face(data, qf, solver->m_dt, dS, dV, solver);
          }

      } // end if (n.is_hanging)
      
      /* 
       * if cell is on the outside boundary, we need to free the extra "ghost" 
       * quadrant
       */
      if (n.tree_boundary) {
	sc_mempool_free (info->p4est->user_data_pool, n.is.full.data);
      }

    } // end for face
    
  } // end for dir
  
} // iterator_mfplasma_update

// =======================================================
// =======================================================
/**
 * \brief Update the value in a quadrant using the electricand magnetic field source terms..
 *
 */
static void
iterator_mfplasma_source_term_center (p4est_iter_volume_info_t * info, 
                                      void *user_data)
{

  /* get forest structure */
  p4est_t              *p4est = info->p4est;
  
  /* get solver and geometry (only for unstructured) */
  SolverMfplasma       *solver = static_cast<SolverMfplasma*>(solver_from_p4est (p4est));

  UserDataManagerMfplasma
    *userdata_mgr = static_cast<UserDataManagerMfplasma *> (solver->get_userdata_mgr());
  

  int                *direction = (int *) user_data;

  p4est_quadrant_t   *quad   = info->quad;
  p4est_locidx_t      quadid = info->quadid;
  p4est_topidx_t      treeid = info->treeid;

  p4est_tree_t       *tree = p4est_tree_array_index (p4est->trees, treeid);

  p4est_locidx_t      quadid_cum = tree->quadrants_offset + quadid;
  
  double dt = solver->m_dt;
  //double nu = solver->m_cfg->config_read_double ("ramp.nu", 0.07);
  
  /* get current cell data */
  qdata_t* qdata = userdata_mgr->quadrant_get_qdata (quad);
  QdataVar& data  = qdata->w;
  QdataVar& data_next = qdata->wnext;

  // source term
  MfplasmaPar &mfplasmaPar = solver->mfplasmaPar;
    
  QdataVar src = compute_source_term(qdata->w,
                                     qdata->welec,
                                     dt, mfplasmaPar);

// OLD IMPLEMENTATION
//      MfplasmaPar &mfplasmaPar = solver->mfplasmaPar;
//    
//      const bool& elec_on = mfplasmaPar.elec.static_E_enabled;
//      const double& Ex = mfplasmaPar.elec.static_Ex;
//      const double& Ey = mfplasmaPar.elec.static_Ey;
//      const double& Ez = mfplasmaPar.elec.static_Ez;
//    
//      const bool& mag_on = mfplasmaPar.static_mag_field_enabled;
//      const double& Bx = mfplasmaPar.static_Bx;
//      const double& By = mfplasmaPar.static_By;
//      const double& Bz = mfplasmaPar.static_Bz;
//    
//      const bool& iz_on = mfplasmaPar.ionization_enabled;
//      const double& nu_iz = mfplasmaPar.nu_iz;
//    
//      const double& q_e = mfplasmaPar.q_e;
//      const double& q_i = mfplasmaPar.q_i;
//    
//      const double& epsilon = mfplasmaPar.epsilon;
// compute source terms
//  src.ne   += dt*    data.ne*iz_on*nu_iz;
//  src.nu_e += dt*q_e*data.ne/epsilon*(elec_on*Ex+mag_on*(data.nv_e*Bz-data.nw_e*By));
//  src.nv_e += dt*q_e*data.ne/epsilon*(elec_on*Ey+mag_on*(data.nw_e*Bx - data.nu_e*Bz));
//  src.nw_e += dt*q_e*data.ne/epsilon*(elec_on*Ez+mag_on*(data.nu_e*By - data.nv_e*Bx));
//  
//  src.ni   += dt*    data.ne*iz_on*nu_iz;
//  src.nu_i += dt*q_i*data.ni*(elec_on*Ex+mag_on*(data.nv_i*Bz - data.nw_i*By));
//  src.nv_i += dt*q_i*data.ni*(elec_on*Ey+mag_on*(data.nw_i*Bx - data.nu_i*Bz));
//  src.nw_i += dt*q_i*data.ni*(elec_on*Ez+mag_on*(data.nu_i*By - data.nv_i*Bx));
    
  data_next += src; 

} // iterator_mfplasma_source_term_center

// =======================================================
// =======================================================
/**
 * \brief Update the value in a quadrant using the Mfplasma source term computed at face.
 */
static void
iterator_mfplasma_source_term_face (p4est_iter_volume_info_t * info, 
                                    void *user_data)
{

  /* get forest structure */
  p4est_t              *p4est = info->p4est;
  
  /* get solver and geometry (only for unstructured) */
  SolverMfplasma       *solver = static_cast<SolverMfplasma*>(solver_from_p4est (p4est));

  UserDataManagerMfplasma
    *userdata_mgr = static_cast<UserDataManagerMfplasma *> (solver->get_userdata_mgr());
  

  int                *direction = (int *) user_data;

  p4est_quadrant_t   *quad   = info->quad;
  p4est_locidx_t      quadid = info->quadid;
  p4est_topidx_t      treeid = info->treeid;

  p4est_tree_t       *tree = p4est_tree_array_index (p4est->trees, treeid);

  p4est_locidx_t      quadid_cum = tree->quadrants_offset + quadid;

  /* 2 states: one for each side of the face */
  qdata_t            *data[2] = { NULL, NULL };

  /* the face Id in range 0..P4EST_FACES-1 */
  int                 qf[2] = { -1, -1 };

  double              scale = 0.0;
  quadrant_neighbor_t<qdata_t> n;

  /* geometrical terms */
  int                 dir_start = (direction == NULL ? 0 : *direction);
  int                 dir_end = (direction == NULL ? P4EST_DIM : *direction + 1);

  
  double dt = solver->m_dt;
  //double nu = solver->m_cfg->config_read_double ("ramp.nu", 0.07);

  // source term accumulator
  QdataVar src;

  MfplasmaPar &mfplasmaPar = solver->mfplasmaPar;

  UNUSED (scale);

  /* get current cell data */
  data[CELL_CURRENT] = userdata_mgr->quadrant_get_qdata (quad);

  QdataVar src_term_cur = compute_source_term(data[CELL_CURRENT]->w, 
                                              data[CELL_CURRENT]->welec,
                                              dt, mfplasmaPar);


  
  /* loop over all quadrant's faces */
  for( int dir = dir_start; dir < dir_end; ++dir) {
    
    for (int face = 0; face < 2; ++face) {

      /* get the current face in the given direction */
      qf[CELL_CURRENT] = QuadrantNeighborUtilsBase::direction_faces[dir][face];      
      
      /* 
       * get the Qdata of the neighbors on this face 
       * It is important to realize here that, if face touches the outside
       * boundary, there will be a call to our boundary_fn callback
       * which will fill the wm, wp states
       */
      QuadrantNeighborUtilsMfplasma::quadrant_get_face_neighbor (solver,
                                                                 quadid,
                                                                 treeid,
                                                                 qf[CELL_CURRENT],
                                                                 &n);
      qf[CELL_NEIGHBOR] = n.face;
      
      /* 
       *
       */
    if (n.is_hanging) {
        
	/* half neighbors interface loop */
        for (int i = 0; i < P4EST_HALF; ++i) {
            
            /* perform update only when needed */
            if (true /* !should_skip_face(solver, n.is.hanging.is_ghost[i],
                                    true, quadid_cum, 
                                    n.is.hanging.quadid_cum[i], 
                                    quad,
                                    n.is.hanging.quad[i]) */) {

                data[CELL_NEIGHBOR] = n.is.hanging.data[i];

                //mfplasma_update_face(data, qf, solver->m_dt, dS, dV, solver);
                QdataVar src_term_neigh = compute_source_term(data[CELL_NEIGHBOR]->w,
                                                              data[CELL_NEIGHBOR]->welec,
                                                              
                                                              dt,mfplasmaPar);

                QdataVar src_average = (src_term_cur + src_term_neigh)*0.5;

                double weight = P4EST_DIM==2 ? 0.5 : 0.25;
                src += src_average * weight;

            }

         } /* end for i=0..P4EST_HELF */

      } else {

          /* perform update only when needed */
          if (true /*!should_skip_face(solver, n.is.full.is_ghost, 
                                false, quadid_cum,
                                n.is.full.quadid_cum, 
                                quad, 
                                n.is.full.quad)*/) {

            data[CELL_NEIGHBOR] = n.is.full.data;

            //mfplasma_update_face(data, qf, solver->m_dt, dS, dV, solver);
            QdataVar src_term_neigh = compute_source_term(data[CELL_NEIGHBOR]->w,data[CELL_NEIGHBOR]->welec,dt,mfplasmaPar);
            
            QdataVar src_average = (src_term_cur + src_term_neigh)*0.5;
            
            src += src_average;
            
          }

      } // end if (n.is_hanging)
      
      /* 
       * if cell is on the outside boundary, we need to free the extra "ghost" 
       * quadrant
       */
      if (n.tree_boundary) {
	sc_mempool_free (info->p4est->user_data_pool, n.is.full.data);
      }

    } // end for face
    
  } // end for dir

  // TODO - check p4est nb face par quad
  double quad_weight = P4EST_DIM == 2 ? 4.0 : 6.0;

  src /= quad_weight;

  data[CELL_NEIGHBOR]->wnext += src;

} // iterator_mfplasma_source_term_face

// =======================================================
// =======================================================

// =======================================================
// =======================================================

// =======================================================
// =======================================================
IteratorsMfplasma::IteratorsMfplasma() : IteratorsBase() {

  this->fill_iterators_list();
  
} // IteratorsMfplasma::IteratorsMfplasma

// =======================================================
// =======================================================
IteratorsMfplasma::~IteratorsMfplasma()
{
  
} // IteratorsMfplasma::~IteratorsMfplasma

// =======================================================
// =======================================================
void IteratorsMfplasma::fill_iterators_list()
{

  iteratorsList[IT_ID::START_STEP]          = iterator_start_step;
  iteratorsList[IT_ID::END_STEP]            = iterator_end_step;
  iteratorsList[IT_ID::MARK_ADAPT]          = iterator_mark_adapt;
  iteratorsList[IT_ID::CFL_TIME_STEP]       = iterator_time_step;
  iteratorsList[IT_ID::UPDATE]              = iterator_mfplasma_update;
  iteratorsList[IT_IDD::SOURCE_TERM_CENTER] = iterator_mfplasma_source_term_center;
  iteratorsList[IT_IDD::SOURCE_TERM_FACE]   = iterator_mfplasma_source_term_face;
  
} // IteratorsMfplasma::fill_iterators_list

} // namespace mfplasma

} // namespace canop
