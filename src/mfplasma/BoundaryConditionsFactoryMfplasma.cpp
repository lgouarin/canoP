#include "BoundaryConditionsFactoryMfplasma.h"

#include "SolverMfplasma.h"
#include "UserDataManagerMfplasma.h"
#include "quadrant_utils.h"

// remember that qdata_t is here a concrete type defined in userdata_mfplasma.h

#include "mfplasma/userdata_mfplasma.h"
#include "ConfigReader.h"

namespace canop {

namespace mfplasma {

using qdata_t               = Qdata; 
using qdata_reconstructed_t = QdataRecons;

// =======================================================
// ======BOUNDARY CONDITIONS CALLBACKS====================
// =======================================================


// =======================================================
// =======================================================
/*
 * TODO: maybe just make the boundary functions return a qdata_variables_t
 * that they have initialized and then we do the copying around.
 *
 * TODO: for order 2, how does delta have to be initialized? does it?
 */
void
bc_mfplasma_dirichlet (p4est_t * p4est,
		     p4est_topidx_t which_tree,
		     p4est_quadrant_t * q,
		     int face,
		     qdata_t * ghost_qdata)
{
  
  UNUSED (p4est);
  UNUSED (which_tree);
  UNUSED (q);
  UNUSED (face);
  UNUSED (ghost_qdata);
  
} // bc_mfplasma_dirichlet

// =======================================================
// =======================================================
void
bc_mfplasma_neuman (p4est_t * p4est,
		  p4est_topidx_t which_tree,
		  p4est_quadrant_t * q,
		  int face,
		  qdata_t * ghost_qdata)
{
  UNUSED (p4est);
  UNUSED (which_tree);
  UNUSED (face);

  // retrieve our Solver, UserDataManager and finaly our quadrant data
  SolverMfplasma    *solver = static_cast<SolverMfplasma*>(solver_from_p4est (p4est));

  UserDataManagerMfplasma
    *userdata_mgr = static_cast<UserDataManagerMfplasma *> (solver->get_userdata_mgr());

  qdata_t            *qdata = userdata_mgr->quadrant_get_qdata (q);

  /* 
   * copy the entire qdata
   */
  userdata_mgr->qdata_copy(ghost_qdata, qdata);

} // bc_mfplasma_neuman

// =======================================================
// =======================================================
void
bc_mfplasma_reflective (p4est_t * p4est,
		  p4est_topidx_t which_tree,
		  p4est_quadrant_t * q,
		  int face,
		  qdata_t * ghost_qdata)
{
  UNUSED (p4est);
  UNUSED (which_tree);
  UNUSED (face);

  // retrieve our Solver, UserDataManager and finaly our quadrant data
  SolverMfplasma    *solver = static_cast<SolverMfplasma*>(solver_from_p4est (p4est));

  UserDataManagerMfplasma
    *userdata_mgr = static_cast<UserDataManagerMfplasma *> (solver->get_userdata_mgr());

  qdata_t            *qdata = userdata_mgr->quadrant_get_qdata (q);
  
  int dir = face/2;
  /* 
   * copy the entire qdata
   */
  userdata_mgr->qdata_copy(ghost_qdata, qdata);
  //reverse speed
  if (dir == IX) {
    ghost_qdata->w.nu_e *=-1;
    ghost_qdata->w.nu_i *=-1;
  } else if (dir == IY) {
    ghost_qdata->w.nv_e *=-1;
    ghost_qdata->w.nv_i *=-1;
  } else if (dir == IZ) {
    ghost_qdata->w.nw_e *=-1;
    ghost_qdata->w.nw_i *=-1;
  } /* end direction */
  

} // bc_mfplasma_reflective

// =======================================================
// ======CLASS BoundaryConditionsFactoryMfplasma IMPL ======
// =======================================================
BoundaryConditionsFactoryMfplasma::BoundaryConditionsFactoryMfplasma() :
  BoundaryConditionsFactory<Qdata>()
{

  // register the above callback's
  registerBoundaryConditions("dirichlet" , bc_mfplasma_dirichlet);
  registerBoundaryConditions("neuman"    , bc_mfplasma_neuman);
  registerBoundaryConditions("reflective", bc_mfplasma_reflective);

} // BoundaryConditionsFactoryMfplasma::BoundaryConditionsFactoryMfplasma

} // namespace mfplasma

} // namespace canop
