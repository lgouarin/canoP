#ifndef MFPLASMA_ELEC_H_
#define MFPLASMA_ELEC_H_

#ifdef USE_3D
#include <p4est_to_p8est.h>
#endif

#ifndef P4_TO_P8
#include <p4est.h>
#else
#include <p8est.h>
#endif

#include "PoissonFESolver.h"

namespace canop {
namespace mfplasma {

/**
 * Solve the Poisson equation for electric field.
 *
 * The units are chosen such that epsilon_0 = 1.
 *
 * \param[in]  p4est the forest
 * \param[in]  ghost the associated ghost layer
 * \param[out] info  feedback about number of iterations and final residual
 *
 * \return Non-zero iff the solver converged
 */
int
solve_poisson_elec(p4est_t *p4est, p4est_ghost_t *ghost,
                   poisson_info_t *info = nullptr);

} // namespace mfplasma
} // namespace canop

#endif // MFPLASMA_ELEC_H_
