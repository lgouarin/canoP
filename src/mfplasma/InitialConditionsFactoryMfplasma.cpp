#include "InitialConditionsFactoryMfplasma.h"

#include "SolverMfplasma.h"
#include "UserDataManagerMfplasma.h"
#include "quadrant_utils.h"

#include "mfplasma/userdata_mfplasma.h"

namespace canop {

namespace mfplasma {

using qdata_t               = Qdata; 
using qdata_variables_t     = QdataVar; 
using qdata_reconstructed_t = QdataRecons;

// =======================================================
// =======INITIAL CONDITIONS CALLBACKS====================
// =======================================================

/**************************************************************************/
/* Mfplasma : discontinuity                                                         */
/**************************************************************************/
/**
 * Initial conditions is a discontinuity.
 *
 */
void
init_mfplasma_discontinuity (p4est_t * p4est,
                             p4est_topidx_t which_tree, 
                             p4est_quadrant_t * quad)
{

  SolverMfplasma *solver = static_cast<SolverMfplasma*>(solver_from_p4est (p4est));

  UserDataManagerMfplasma
    *userdata_mgr = static_cast<UserDataManagerMfplasma *> (solver->get_userdata_mgr());

  // geometry (if NULL, it means cartesian by default)
  p4est_geometry_t *geom = solver->get_geom_compute();

  // get config reader
  ConfigReader    *cfg = solver->m_cfg;
  UNUSED (cfg);

  // now start genuine initialization
  qdata_variables_t   w;
  
  // get the coordinates of the center of the quad
  double  XYZ[3] = { 0, 0, 0 };
  double  x,y,z;
  UNUSED (y);
  UNUSED (z);
  
  // get the physical coordinates of the center of the quad
  quadrant_center_vertex (p4est->connectivity, geom, which_tree, quad, XYZ);

  x = XYZ[0];
  y = XYZ[1];
  z = XYZ[2];

  double x_discon = cfg->config_read_double ("discontinuity.x", 0.5);

  if (x<x_discon) { 
    
    w.ne = cfg->config_read_double ("discontinuity.ne_L", 1.0);
    w.ni = cfg->config_read_double ("discontinuity.ni_L", 1.0);;

    w.nu_e = 0.0;
    w.nv_e = 0.0;
    w.nw_e = 0.0;

    w.nu_i = 0.0;
    w.nv_i = 0.0;
    w.nw_i = 0.0;

  } else {
    
    w.ne = cfg->config_read_double ("discontinuity.ne_R", 0.1);
    w.ni = cfg->config_read_double ("discontinuity.ni_R", 0.1);

    w.nu_e = 0.0;
    w.nv_e = 0.0;
    w.nw_e = 0.0;

    w.nu_i = 0.0;
    w.nv_i = 0.0;
    w.nw_i = 0.0;

  }
  
  // finally set quadrant's userdata
  userdata_mgr->quadrant_set_variables (quad, &w);
  userdata_mgr->quadrant_print_qdata (SC_LP_INFO, quad);
  
} // init_mfplasma_discontinuity

/**************************************************************************/
/* Mfplasma : Vertical discontinuity                                                         */
/**************************************************************************/
/**
 * Initial conditions is a vertical discontinuity.
 *
 */
void
init_mfplasma_vertical_discontinuity (p4est_t * p4est,
                             p4est_topidx_t which_tree, 
                             p4est_quadrant_t * quad)
{

  SolverMfplasma *solver = static_cast<SolverMfplasma*>(solver_from_p4est (p4est));

  UserDataManagerMfplasma
    *userdata_mgr = static_cast<UserDataManagerMfplasma *> (solver->get_userdata_mgr());

  // geometry (if NULL, it means cartesian by default)
  p4est_geometry_t *geom = solver->get_geom_compute();

  // get config reader
  ConfigReader    *cfg = solver->m_cfg;
  UNUSED (cfg);

  // now start genuine initialization
  qdata_variables_t   w;
  
  // get the coordinates of the center of the quad
  double  XYZ[3] = { 0, 0, 0 };
  double  x,y,z;
  UNUSED (x);
  UNUSED (z);
  
  // get the physical coordinates of the center of the quad
  quadrant_center_vertex (p4est->connectivity, geom, which_tree, quad, XYZ);

  x = XYZ[0];
  y = XYZ[1];
  z = XYZ[2];

  double y_discon = cfg->config_read_double ("Vdiscontinuity.y", 0.5);

  if (y<y_discon) { 
      
    w.ne = cfg->config_read_double ("Vdiscontinuity.ne_L", 1.0);
    w.ni = cfg->config_read_double ("Vdiscontinuity.ni_L", 1.0);;

    w.nu_e = 0.0;
    w.nv_e = 0.0;
    w.nw_e = 0.0;

    w.nu_i = 0.0;
    w.nv_i = 0.0;
    w.nw_i = 0.0;

  } else {
    
    w.ne = cfg->config_read_double ("discontinuity.ne_R", 0.1);
    w.ni = cfg->config_read_double ("discontinuity.ni_R", 0.1);

    w.nu_e = 0.0;
    w.nv_e = 0.0;
    w.nw_e = 0.0;

    w.nu_i = 0.0;
    w.nv_i = 0.0;
    w.nw_i = 0.0;

  }
  
  // finally set quadrant's userdata
  userdata_mgr->quadrant_set_variables (quad, &w);
  userdata_mgr->quadrant_print_qdata (SC_LP_INFO, quad);
  
} // init_mfplasma_vertical_discontinuity

/**************************************************************************/
/* Mfplasma : diag discontinuity                                                         */
/**************************************************************************/
/**
 * Initial conditions is a digonal discontinuity.
 *
 */
void
init_mfplasma_diag_discontinuity (p4est_t * p4est,
                             p4est_topidx_t which_tree, 
                             p4est_quadrant_t * quad)
{

  SolverMfplasma *solver = static_cast<SolverMfplasma*>(solver_from_p4est (p4est));

  UserDataManagerMfplasma
    *userdata_mgr = static_cast<UserDataManagerMfplasma *> (solver->get_userdata_mgr());

  // geometry (if NULL, it means cartesian by default)
  p4est_geometry_t *geom = solver->get_geom_compute();

  // get config reader
  ConfigReader    *cfg = solver->m_cfg;
  UNUSED (cfg);

  // now start genuine initialization
  qdata_variables_t   w;
  
  // get the coordinates of the center of the quad
  double  XYZ[3] = { 0, 0, 0 };
  double  x,y,z;
  UNUSED (z);
  
  // get the physical coordinates of the center of the quad
  quadrant_center_vertex (p4est->connectivity, geom, which_tree, quad, XYZ);

  x = XYZ[0];
  y = XYZ[1];
  z = XYZ[2];

  if (x<y) { 
    
    w.ne = cfg->config_read_double ("discontinuity.ne_L", 1.0);
    w.ni = cfg->config_read_double ("discontinuity.ni_L", 1.0);;

    w.nu_e = 0.0;
    w.nv_e = 0.0;
    w.nw_e = 0.0;

    w.nu_i = 0.0;
    w.nv_i = 0.0;
    w.nw_i = 0.0;

  } else {
    
    w.ne = cfg->config_read_double ("discontinuity.ne_R", 0.1);
    w.ni = cfg->config_read_double ("discontinuity.ni_R", 0.1);

    w.nu_e = 0.0;
    w.nv_e = 0.0;
    w.nw_e = 0.0;

    w.nu_i = 0.0;
    w.nv_i = 0.0;
    w.nw_i = 0.0;

  }
  
  // finally set quadrant's userdata
  userdata_mgr->quadrant_set_variables (quad, &w);
  userdata_mgr->quadrant_print_qdata (SC_LP_INFO, quad);
  
} // init_mfplasma_diag_discontinuity

/**************************************************************************/
/* Mfplasma : square discontinuity                                                         */
/**************************************************************************/
/**
 * Initial conditions is a square discontinuity.
 *
 */
void
init_mfplasma_square_discontinuity (p4est_t * p4est,
                             p4est_topidx_t which_tree, 
                             p4est_quadrant_t * quad)
{

  SolverMfplasma *solver = static_cast<SolverMfplasma*>(solver_from_p4est (p4est));

  UserDataManagerMfplasma
    *userdata_mgr = static_cast<UserDataManagerMfplasma *> (solver->get_userdata_mgr());

  // geometry (if NULL, it means cartesian by default)
  p4est_geometry_t *geom = solver->get_geom_compute();

  // get config reader
  ConfigReader    *cfg = solver->m_cfg;
  UNUSED (cfg);

  // now start genuine initialization
  qdata_variables_t   w;
  
  // get the coordinates of the center of the quad
  double  XYZ[3] = { 0, 0, 0 };
  double  x,y,z;
  UNUSED (z);
  
  // get the physical coordinates of the center of the quad
  quadrant_center_vertex (p4est->connectivity, geom, which_tree, quad, XYZ);

  x = XYZ[0];
  y = XYZ[1];
  z = XYZ[2];
  
  double x_c = cfg->config_read_double ("squareDiscontinuity.x_c", 0.5);
  double y_c = cfg->config_read_double ("squareDiscontinuity.y_c", 0.5);
  double a = cfg->config_read_double ("squareDiscontinuity.a", 0.4);

  double r = sqrt((x-x_c)*(x-x_c)+(y-y_c)*(y-y_c));

  if (r<0.3/*fabs(x-x_c)<=a/2 && fabs(y-y_c)<=a/2*/) { 
    
    w.ne = cfg->config_read_double ("squareDiscontinuity.ne_c", 1.0);
    w.ni = cfg->config_read_double ("squareDiscontinuity.ni_c", 1.0);;

    w.nu_e = 0.0;
    w.nv_e = 0.0;
    w.nw_e = 0.0;

    w.nu_i = 0.0;
    w.nv_i = 0.0;
    w.nw_i = 0.0;

  } else {
    
    w.ne = cfg->config_read_double ("squareDiscontinuity.ne_ext", 0.1);
    w.ni = cfg->config_read_double ("squareDiscontinuity.ni_ext", 0.1);

    w.nu_e = 0.0;
    w.nv_e = 0.0;
    w.nw_e = 0.0;

    w.nu_i = 0.0;
    w.nv_i = 0.0;
    w.nw_i = 0.0;

  }
  
  // finally set quadrant's userdata
  userdata_mgr->quadrant_set_variables (quad, &w);
  userdata_mgr->quadrant_print_qdata (SC_LP_INFO, quad);
  
} // init_mfplasma_square_discontinuity

// =======================================================
// =======CLASS InitialConditionsFactoryMfplasma IMPL ======
// =======================================================
InitialConditionsFactoryMfplasma::InitialConditionsFactoryMfplasma() :
  InitialConditionsFactory()
{

  // register the above callback's
  registerIC("discontinuity" ,            init_mfplasma_discontinuity);
  registerIC("vertical discontinuity" ,   init_mfplasma_vertical_discontinuity);
  registerIC("diagonal discontinuity" ,   init_mfplasma_diag_discontinuity);
  registerIC("square discontinuity" ,     init_mfplasma_square_discontinuity);
  
} // InitialConditionsFactoryMfplasma

} // namespace mfplasma

} // namespace canop
