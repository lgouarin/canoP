
#ifdef USE_3D
#include <p4est_to_p8est.h>
#endif

#ifndef P4_TO_P8
#include <p4est_bits.h>
#include <p4est_lnodes.h>
#else
#include <p8est_bits.h>
#include <p8est_lnodes.h>
#endif

#include "Lnode_Utils.h"
#include "PoissonFESolver.h"
#include "SolverMfplasma.h"
#include "Statistics.h"
#include "UserDataManagerMfplasma.h"

#include <cmath>
#include <functional>
#include <mpi.h>

#include "elec_poisson.h"

namespace canop {
namespace mfplasma {

// TODO: put this somewhere else?
// XXX: cubic geometry only
static double get_quadrant_volume(p4est_t * p4est,
                                  p4est_topidx_t which_tree,
                                  p4est_quadrant_t * quad)
{

  UNUSED(p4est);
  UNUSED(which_tree);

  /* assume here cartesian geometry */
  double dx = 1.0*P4EST_QUADRANT_LEN(quad->level) / P4EST_ROOT_LEN;

#ifndef P4_TO_P8
  return dx*dx;
#else
  return dx*dx*dx;
#endif

} // get_quadrant_volume

// TODO: put this somewhere else?
// XXX: cubic geometry only
static double get_quadrant_scale(p4est_t * p4est,
                                 p4est_topidx_t which_tree,
                                 p4est_quadrant_t * quad)
{

  UNUSED(p4est);
  UNUSED(which_tree);

  /* assume here cartesian geometry */
  double dx = 1.0*P4EST_QUADRANT_LEN(quad->level) / P4EST_ROOT_LEN;

  return dx;
} // get_quadrant_scale

static double set_dirichlet(double dirichlet_value, double XYZ[3])
{

  return dirichlet_value;

}


int
solve_poisson_elec(p4est_t *p4est, p4est_ghost_t *ghost_,
                   poisson_info_t *info)
{
  SolverMfplasma *solver = static_cast<SolverMfplasma*>(solver_from_p4est(p4est));
  UserDataManagerMfplasma *userdata_mgr = static_cast<UserDataManagerMfplasma *>(solver->get_userdata_mgr());
  p4est_geometry_t *geom = solver->get_geom_compute();
  p4est_mesh_t *mesh = solver->get_mesh();

  p4est_lnodes_t *lnodes;

  clock_t t_start_tot, t_start;
  double time;

  double *rhoc_nodes;
  double *phi_nodes;
  int8_t *bc = nullptr;

  int     converged;

  // Fetch Poisson solver parameters
  int imax = solver->mfplasmaPar.elec.poisson_imax;
  double tol = solver->mfplasmaPar.elec.poisson_tol;

  t_start_tot = clock ();

  // Create a ghost layer if needed
  p4est_ghost_t *ghost;
  if(ghost_ == nullptr)
    ghost = p4est_ghost_new(p4est, P4EST_CONNECT_FULL);
  else
    ghost = ghost_;


  // Create a node numbering for continuous linear finite elements.
  lnodes = p4est_lnodes_new(p4est, ghost, 1);

  t_start = clock();

  // Interpolate the total charge density field onto the nodes
  rhoc_nodes = CANOP_ALLOC(double, lnodes->num_local_nodes);
  auto rhoc_getter = std::bind(&UserDataManagerMfplasma::qdata_get_rhoc,
                               userdata_mgr,
                               std::placeholders::_1);
  interp_quad_to_lnodes(p4est, lnodes, rhoc_getter, get_quadrant_volume, rhoc_nodes);

  // Flag boundaries
  if(solver->mfplasmaPar.elec.poisson_boundary != MfplasmaElecBC::Periodic) {
    bc = CANOP_ALLOC(int8_t, lnodes->num_local_nodes);
    lnodes_flag_boundaries(p4est, mesh, lnodes, bc);
  }

  // Make an initial guess for the potential
  phi_nodes = CANOP_ALLOC(double, lnodes->num_local_nodes);
  if(solver->mfplasmaPar.elec.poisson_reuse_potential) {
    auto phi_getter = std::bind(&UserDataManagerMfplasma::qdata_get_elec_phi,
                                userdata_mgr,
                                std::placeholders::_1);
    interp_quad_to_lnodes(p4est, lnodes, phi_getter, get_quadrant_volume, phi_nodes);

    // Invert the sign of the guessed potential
    for(p4est_locidx_t i = 0; i < lnodes->num_local_nodes; ++i)
      phi_nodes[i] = -phi_nodes[i];
  }
  else {
    memset(phi_nodes, 0, lnodes->num_local_nodes * sizeof(double));
  }

  time = (double) (clock () - t_start) / CLOCKS_PER_SEC;
  if (solver->m_stats != nullptr)
    solver->m_stats->statistics_timing_byname ("t_elec_interp1", time);

  // Set boundary conditions
  if(solver->mfplasmaPar.elec.poisson_boundary == MfplasmaElecBC::Monopole) {
    t_start = clock();

    double total_charge;
    double barycenter[3];
    compute_barycenter(p4est, geom, rhoc_getter, get_quadrant_volume,
                       &total_charge, barycenter);
    // Pass the total charge and barycenter to the monopole function
    auto monopole = std::bind(monopole_expansion, total_charge, barycenter,
                              std::placeholders::_1);
    lnodes_set_boundaries(p4est, geom, lnodes, bc, monopole, phi_nodes);

    // debug info
    CANOP_GLOBAL_PRODUCTIONF("monopole expansion: mass = %g, center = (%f, %f, %f)\n",
        total_charge, barycenter[0], barycenter[1], barycenter[2]);

    time = (double) (clock () - t_start) / CLOCKS_PER_SEC;
    if (solver->m_stats != nullptr)
      solver->m_stats->statistics_timing_byname ("t_elec_boundary", time);
  }

  if(solver->mfplasmaPar.elec.poisson_boundary == MfplasmaElecBC::Dirichlet) {
    t_start = clock();

    ConfigReader    *cfg = solver->m_cfg;    
    double dirichlet_value = cfg->config_read_double("BC.dirichlet_value", 0.0);

    auto dirichlet = std::bind(set_dirichlet, dirichlet_value, std::placeholders::_1);

    lnodes_set_boundaries(p4est, geom, lnodes, bc, dirichlet, phi_nodes);

    time = (double) (clock () - t_start) / CLOCKS_PER_SEC;
    if (solver->m_stats != nullptr)
      solver->m_stats->statistics_timing_byname ("t_elec_boundary", time);
  }

  // Handle periodic boundary conditions: remove average density
  if(solver->mfplasmaPar.elec.poisson_boundary == MfplasmaElecBC::Periodic) {
    double rhoc_avg = compute_average(p4est, rhoc_getter, get_quadrant_volume);

    for(p4est_locidx_t i = 0; i < lnodes->num_local_nodes; ++i)
      rhoc_nodes[i] -= rhoc_avg;
  }

  // Solve Poisson equation using finite elements
  t_start = clock();
  converged = solve_poisson_lnodes(p4est, lnodes, bc, rhoc_nodes, phi_nodes, imax, tol, info);
  // XXX: phi_nodes actually contains -phi / (4pi G)

  time = (double) (clock () - t_start) / CLOCKS_PER_SEC;
  if (solver->m_stats != nullptr)
    solver->m_stats->statistics_timing_byname ("t_elec_solve", time);

  // Interpolate the acceleration and potential back to quadrants
  t_start = clock();
  auto acc_setter = std::bind(&UserDataManagerMfplasma::qdata_set_elec_acc_v,
                              userdata_mgr,
                              std::placeholders::_1, std::placeholders::_2);
  auto phi_setter = std::bind(&UserDataManagerMfplasma::qdata_set_elec_phi,
                              userdata_mgr,
                              std::placeholders::_1, std::placeholders::_2);
  gradient_lnodes_to_quad(p4est, lnodes, get_quadrant_scale, phi_nodes, acc_setter);
  // Put the correct sign to the potential
  for(p4est_locidx_t i = 0; i < lnodes->num_local_nodes; ++i)
    phi_nodes[i] = -phi_nodes[i];
  interp_lnodes_to_quad(p4est, lnodes, phi_nodes, phi_setter);

  time = (double) (clock () - t_start) / CLOCKS_PER_SEC;
  if (solver->m_stats != nullptr)
    solver->m_stats->statistics_timing_byname ("t_elec_interp2", time);

  // Clean up
  CANOP_FREE(rhoc_nodes);
  CANOP_FREE(bc);
  CANOP_FREE(phi_nodes);

  p4est_lnodes_destroy(lnodes);
  if(ghost_ == nullptr) // we created a ghost layer
    p4est_ghost_destroy(ghost);

  time = (double) (clock () - t_start_tot) / CLOCKS_PER_SEC;
  if (solver->m_stats != nullptr) {
    solver->m_stats->statistics_timing_byname ("t_elec_min", time);
    solver->m_stats->statistics_timing_byname ("t_elec_max", time);
    solver->m_stats->statistics_timing_byname ("t_elec_mean", time);
  }

  return converged;

} // solve_poisson_elec

} // namespace mfplasma
} // namespace canop
