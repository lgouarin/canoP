#include "SolverCoreMfplasma.h"

#include "InitialConditionsFactoryMfplasma.h"
#include "BoundaryConditionsFactoryMfplasma.h"
#include "IndicatorFactoryMfplasma.h"

#include "ConfigReader.h"

namespace canop {

namespace mfplasma {

// =======================================================
// =======================================================
SolverCoreMfplasma::SolverCoreMfplasma(ConfigReader *cfg) :
  SolverCore<Qdata>(cfg)
{

  /*
   * Initial condition callback setup.
   */
  InitialConditionsFactoryMfplasma IC_Factory;
  m_init_fn = IC_Factory.callback_byname (m_init_name);

  /*
   * Boundary condition callback setup.
   */
  BoundaryConditionsFactoryMfplasma BC_Factory;
  m_boundary_fn = BC_Factory.callback_byname (m_boundary_name);

  /*
   * Refine/Coarsen callback setup.
   */
  IndicatorFactoryMfplasma indic_Factory;
  m_indicator_fn = indic_Factory.callback_byname (m_indicator_name);

  /*
   * Geometric refine callback setup.
   */
  GeometricIndicatorFactory geom_indic_Factory;
  m_geom_indicator_fn = geom_indic_Factory.callback_byname (m_geom_indicator_name);

  
} // SolverCoreMfplasma::SolverCoreMfplasma


// =======================================================
// =======================================================
SolverCoreMfplasma::~SolverCoreMfplasma()
{
} // SolverCoreMfplasma::~SolverCoreMfplasma

} // namespace mfplasma

} // namespace canop
