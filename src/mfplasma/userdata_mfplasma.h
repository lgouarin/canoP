#ifndef USERDATA_MFPLASMA_H_
#define USERDATA_MFPLASMA_H_

#ifdef USE_3D
#include <p4est_to_p8est.h>
#endif

#ifndef P4_TO_P8
#include <p4est.h>
#else
#include <p8est.h>
#endif

namespace canop {

namespace mfplasma {

enum GeomDir {
  IX = 0,
  IY = 1,
  IZ = 2
};

/* number of variables <==> P4EST_DIM : one velocity per dimension */
constexpr int QDATA_NUM_VARIABLES = P4EST_DIM;

/**
 * Boundary conditions for the Poisson solver
 */
enum class MfplasmaElecBC
{
  Neumann,
  Dirichlet,
  Periodic,
  Monopole,
};

/**
 * Electric field Poisson solver specific parameters.
 */
struct MfplasmaElecPar
{
  int    enabled;                     //!< non-zero iff any of the *_enabled is non-zero

  int    static_E_enabled;            //!< enable uniform elec field
  double static_Ex;                   //!< uniform elec x component
  double static_Ey;                   //!< uniform elec y component
  double static_Ez;                   //!< uniform elec z component

  // int    point_enabled;               //!< enable point elec source
  // double point_Gmass;                 //!< G * mass of the elec source
  // double point_pos_x;                 //!< x position of the source
  // double point_pos_y;                 //!< y position of the source
  // double point_pos_z;                 //!< z position of the source
  // double point_soften;                //!< softening length to prevent divergence of the potential near the source

  int    poisson_enabled;             //!< enable self-elec
  int    poisson_imax;                //!< maximum number of iterations for the Poisson solver
  double poisson_tol;                 //!< target tolerance for the Poisson solver
  int    poisson_reuse_potential;     //!< reuse the previous potential as an initial guess
  MfplasmaElecBC poisson_boundary;    //!< boundary conditions

  double lambda;                      //!< Lambda Debye / L_0

}; // MfPlasmaElecPar

/**
 * Mfplasma scheme specific parameters.
 */
struct  MfplasmaPar
{ 
  
  // double gamma0;
  // double smallp;
  // double smallc;
  // double smallr;

  double c_i; //!< speed of sound ions
  double c_e; //!< speed of sound electrons
  
  double q_e; //!< charge electrons
  double q_i; //!< charge ions

  double kappa; //!< temperature ratio T_i/T_e
  double epsilon; //!< mass ration m_e/m_i

  MfplasmaElecPar elec;

  bool static_mag_field_enabled;
  double static_Bx;
  double static_By;
  double static_Bz;
  
  bool ionization_enabled;
  double nu_iz;
  
  bool low_Mach_correction_enabled_e;
  bool low_Mach_correction_enabled_i;
  
  double M_co;

  //int cartesian_enabled;
  //int cylindrical_enabled;
  //int unstructured_enabled;
  //MfplasmaCylPar cylindrical;
  
}; // struct MfplasmaPar

/**
 * \brief Vector of Mfplasma conservative variables.
 *
 */
struct QdataVar
{

  double ne, ni;
  double nu_e, nv_e, nw_e;
  double nu_i, nv_i, nw_i;

  inline
  QdataVar() : ne(0.0), ni(0.0),
               nu_e(0.0), nv_e(0.0), nw_e(0.0),
               nu_i(0.0), nv_i(0.0), nw_i(0.0)
  {}

  inline
  QdataVar &operator+=(const QdataVar& operand) {
    this->ne += operand.ne;
    this->ni += operand.ni;

    this->nu_e += operand.nu_e;
    this->nv_e += operand.nv_e;
    this->nw_e += operand.nw_e;

    this->nu_i += operand.nu_i;
    this->nv_i += operand.nv_i;
    this->nw_i += operand.nw_i;

    return *this;
  };
  
  inline
  QdataVar &operator-=(const QdataVar& operand) {
    this->ne -= operand.ne;
    this->ni -= operand.ni;

    this->nu_e -= operand.nu_e;
    this->nv_e -= operand.nv_e;
    this->nw_e -= operand.nw_e;

    this->nu_i -= operand.nu_i;
    this->nv_i -= operand.nv_i;
    this->nw_i -= operand.nw_i;

    return *this;
  };
  
  inline
  QdataVar &operator/=(double operand) {
    this->ne /= operand;
    this->ni /= operand;

    this->nu_e /= operand;
    this->nv_e /= operand;
    this->nw_e /= operand;

    this->nu_i /= operand;
    this->nv_i /= operand;
    this->nw_i /= operand;

    return *this;
  };

  inline
  QdataVar &operator*=(double operand) {
    this->ne *= operand;
    this->ni *= operand;

    this->nu_e *= operand;
    this->nv_e *= operand;
    this->nw_e *= operand;

    this->nu_i *= operand;
    this->nv_i *= operand;
    this->nw_i *= operand;

    return *this;
  };

  inline
  QdataVar operator* (double operand) {

    QdataVar res;

    res.ne   = this->ne   * operand;
    res.ni   = this->ni   * operand;

    res.nu_e = this->nu_e * operand;
    res.nv_e = this->nv_e * operand;
    res.nw_e = this->nw_e * operand;

    res.nu_i = this->nu_i * operand;
    res.nv_i = this->nv_i * operand;
    res.nw_i = this->nw_i * operand;

    return res;
  };

  inline
  QdataVar operator+ (const QdataVar& operand) {
    
    QdataVar res;

    res.ne += operand.ne;
    res.ni += operand.ni;

    res.nu_e += operand.nu_e;
    res.nv_e += operand.nv_e;
    res.nw_e += operand.nw_e;

    res.nu_i += operand.nu_i;
    res.nv_i += operand.nv_i;
    res.nw_i += operand.nw_i;

    return *this;
  };


}; // struct QdataVar

using QdataRecons = QdataVar;

/**
 * \brief single fluid primitive variables.
 *
 */
struct QdataPrim {

  double n;
  double u, v, w;

  inline
  QdataPrim () : n(0.0), u(0.0), v(0.0), w(0.0) {}

}; // QdataPrim

/**
 * \brief signle fluid conservative variables.
 *
 */
struct QdataCons {

  double n;
  double nu, nv, nw;

  inline
  QdataCons () : n(0.0), nu(0.0), nv(0.0), nw(0.0) {}

  inline
  QdataCons operator+ (const QdataCons& operand) {

    QdataCons res;

    res.n  = this->n  + operand.n;
    res.nu = this->nu + operand.nu;
    res.nv = this->nv + operand.nv;
    res.nw = this->nw + operand.nw;

    return res;
  };

  inline
  QdataCons operator- (const QdataCons& operand) {

    QdataCons res;

    res.n  = this->n  - operand.n;
    res.nu = this->nu - operand.nu;
    res.nv = this->nv - operand.nv;
    res.nw = this->nw - operand.nw;

    return res;
  };

  inline
  QdataCons operator* (const double operand) {

    QdataCons res;

    res.n  = this->n  * operand;
    res.nu = this->nu * operand;
    res.nv = this->nv * operand;
    res.nw = this->nw * operand;

    return res;
  };

}; // struct QdataCons

inline void QDATA_INFOF(const QdataVar& w) {
  printf("ne %g | ni %g\n",
	 w.ne,w.ni);
}

/**
 * Poisson (Electric filed) solver variables
 */
struct QdataElec
{
  double              phi; // Potential
  double              accel[3 /*P4EST_DIM*/]; // elec field. acceleration (-grad phi)

  inline
  QdataElec () : phi(0.0) {
    for (int i = 0; i < 3 /*P4EST_DIM*/; ++i)
      accel[i] = 0.0;
  }

}; // struct QdataElec

/**
 * This is the main data structure (one by quadrant/cell).
 */
struct Qdata
{
  QdataVar   w;        /*!< variable at the current time */
  QdataVar   wnext;    /*!< variable at t + dt */

  QdataElec  welec;    /*!< variables for Poisson solver */

}; // struct Qdata

} // namespace mfplasma

} // namespace canop

#endif // USERDATA_MFPLASMA_H_
