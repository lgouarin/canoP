#ifndef INDICATOR_FACTORY_MFPLASMA_H_
#define INDICATOR_FACTORY_MFPLASMA_H_

#include "IndicatorFactory.h"

#include "mfplasma/userdata_mfplasma.h"

namespace canop {

namespace mfplasma {

/**
 * Concrete implement of an indicator factory for the Mfplasma scheme.
 *
 * Valid indicator names are "khokhlov" and "U_gradient".
 * use method registerIndicator to add another one.
 */
class IndicatorFactoryMfplasma : public IndicatorFactory<Qdata>
{

public:
  IndicatorFactoryMfplasma();
  virtual ~IndicatorFactoryMfplasma() {};

}; // class IndicatorFactoryMfplasma

} // namespace mfplasma

} // namespace canop

#endif // INDICATOR_FACTORY_MFPLASMA_H_
