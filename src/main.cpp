#include <sc.h>
#include "canoP_base.h"
#include "ConfigReader.h"
#include "Solver.h"
#include "SolverFactory.h"

#include "IO_Writer.h"

#include <time.h>

/*
 * Have stack trace dumped automatically upon crash.
 */
#ifdef USE_BACKWARD_CPP
#include "backward.hpp"
#endif // USE_BACKWARD_CPP

int
main (int argc, char **argv)
{
  MPI_Comm            mpicomm;
  int                 mpisize, mpirank, mpiret;
  int                 log_verbosity;
  Solver             *solver;
  ConfigReader       *cfg;
  clock_t             t_start = 0;

  /* initialize MPI and p4est internals */
  mpiret = MPI_Init (&argc, &argv);
  SC_CHECK_MPI (mpiret);
  mpicomm = MPI_COMM_WORLD;
  mpiret = MPI_Comm_size (mpicomm, &mpisize);
  SC_CHECK_MPI (mpiret);
  mpiret = MPI_Comm_rank (mpicomm, &mpirank);
  SC_CHECK_MPI (mpiret);

  if (argc < 2) {
    if (mpirank == 0) {
      printf ("usage: %s CONFIGURATION_FILE LOG_VERBOSITY\n", argv[0]);
    }
    MPI_Finalize ();
    exit (0);
  }

  // get the verbosity for the logs
  if (argc == 3) {
    log_verbosity = config_log_verbosity (argv[2]);
  }
  else {
    log_verbosity = SC_LP_PRODUCTION;
  }

  // who is responsible for catching system signals : SC or BACKWARD_CPP ?
  // by default, we use SC, but we'd prefer BACKWARD_CPP if available
  int SC_CATCH_SIGNALS=1;
  int SC_PRINT_BACKTRACE=1;
#ifdef USE_BACKWARD_CPP
  SC_CATCH_SIGNALS=0;
  SC_PRINT_BACKTRACE=0;
#endif // USE_BACKWARD_CPP

  // init p4est stuff
  sc_init (mpicomm, SC_CATCH_SIGNALS, SC_PRINT_BACKTRACE, NULL, log_verbosity);
  p4est_init (NULL, log_verbosity);

  canoP_init (NULL, log_verbosity);

  // init configuration reader
  cfg = new ConfigReader (argv[1]);

  bool useP4estMemory = false; // should be read from cfg
  if (cfg->config_read_int ("settings.use_p4est_memory", 0))
    useP4estMemory = true;

  // retrieve solver name from settings
  const std::string solver_name = cfg->config_read_std_string("settings.solver_name", "Unknown");
  
  // the new way: use the solver factory
  // TO DO: read cfg to determine which string identifying
  // a solver to pass to SolverFactory's create method.
  solver = SolverFactory::Instance().create(solver_name,
					    cfg,
					    mpicomm,
					    useP4estMemory);
  
  t_start = (double) clock ();

  /*
   * perform the time loop
   */
  while ( !solver->finished() ) {
    solver->next_iteration();

    if ( solver->should_adapt () ) {
      solver->adapt(0);
    }

    if ( solver->should_write() ) {
      solver->save_solution();
    }

    if ( solver->should_write_restart() ) {
      solver->write_restart_file();
    }
  }

  /*
   * Display some minimalist statistics / monitoring information on screen.
   */
  double total_time = (double) (clock () - t_start) / CLOCKS_PER_SEC;
  double number_of_cell_update_per_second = 1.0 * solver->m_total_num_cell_update / total_time;
  CANOP_GLOBAL_ESSENTIAL  ("#######################################################\n");
  CANOP_GLOBAL_ESSENTIALF ("Number of cell-updates per seconds is : %f Mupdate-cell/s\n", number_of_cell_update_per_second/1e6);
  
  /*
   * Write detailed statistics to file.
   */
  solver->write_statistics();

  /* destroy our data structures */
  delete solver;
  delete cfg;

  /* clean up and exit */
  sc_finalize ();

  mpiret = MPI_Finalize ();
  SC_CHECK_MPI (mpiret);

  return 0;
  
} // main
