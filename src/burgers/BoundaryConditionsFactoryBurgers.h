#ifndef BOUNDARY_CONDITIONS_FACTORY_BURGERS_H_
#define BOUNDARY_CONDITIONS_FACTORY_BURGERS_H_

#include "BoundaryConditionsFactory.h"
#include "burgers/userdata_burgers.h"

namespace canop {

namespace burgers {

/**
 *
 */
class BoundaryConditionsFactoryBurgers : public BoundaryConditionsFactory<Qdata>
{

  using qdata_t = Qdata;
  
public:
  BoundaryConditionsFactoryBurgers();
  virtual ~BoundaryConditionsFactoryBurgers() {};
  
}; // class BoundaryConditionsFactoryBurgers

} // namespace burgers

} // namespace canop

#endif // BOUNDARY_CONDITIONS_FACTORY_BURGERS_H_
