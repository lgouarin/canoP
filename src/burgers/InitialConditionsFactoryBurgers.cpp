#include "InitialConditionsFactoryBurgers.h"

#include "SolverBurgers.h"
#include "UserDataManagerBurgers.h"
#include "quadrant_utils.h"

#include "burgers/userdata_burgers.h"

namespace canop {

namespace burgers {

using qdata_t               = Qdata; 
using qdata_variables_t     = QdataVar; 
using qdata_reconstructed_t = QdataRecons;

// =======================================================
// =======INITIAL CONDITIONS CALLBACKS====================
// =======================================================

/**************************************************************************/
/* Burgers : ramp                                                         */
/**************************************************************************/
/**
 * Initial conditions is a ramp.
 * See http://nbviewer.jupyter.org/github/numerical-mooc/numerical-mooc/blob/master/lessons/02_spacetime/02_04_1DBurgers.ipynb
 *
 * Note:
 * The solution has been rescaled to domain [0,1]^2 instead of [0,2pi]^2
 * This solution is bound to be used with the unit connectivity and periodic 
 * boundary conditions.
 */
void
init_burgers_ramp (p4est_t * p4est,
		   p4est_topidx_t which_tree, 
		   p4est_quadrant_t * quad)
{

  SolverBurgers *solver = static_cast<SolverBurgers*>(solver_from_p4est (p4est));

  UserDataManagerBurgers
    *userdata_mgr = static_cast<UserDataManagerBurgers *> (solver->get_userdata_mgr());

  // geometry (if NULL, it means cartesian by default)
  p4est_geometry_t *geom = solver->get_geom_compute();

  // get config reader
  ConfigReader    *cfg = solver->m_cfg;
  UNUSED (cfg);

  // now start genuine initialization
  qdata_variables_t   w;
  
  // get the coordinates of the center of the quad
  double  XYZ[3] = { 0, 0, 0 };
  double  x,y,z;
  UNUSED (y);
  UNUSED (z);

  // viscosity
  double nu = cfg->config_read_double ("ramp.nu", 0.07);
  
  // get the physical coordinates of the center of the quad
  quadrant_center_vertex (p4est->connectivity, geom, which_tree, quad, XYZ);
  
  x = XYZ[0];
  y = XYZ[1];
  z = XYZ[2];

  // minus pi**2/nu
  const double m_pi2_over_nu = -M_PI*M_PI/nu;
  
  double phi = exp(m_pi2_over_nu * x * x) + exp(m_pi2_over_nu * (x-1.0) * (x-1.0));

  double dphidx = m_pi2_over_nu * 2 * x * exp(m_pi2_over_nu * x * x) +
    m_pi2_over_nu * 2 * (x-1) * exp(m_pi2_over_nu * (x-1.0) * (x-1.0));
  
#ifdef USE_3D

  w.U = -nu/M_PI/phi * dphidx + 4;
  w.V = 0.0;
  w.W = 0.0;
  
#else // 2D
  
  w.U = (-nu/M_PI/phi * dphidx + 4);
  w.V = 0.0;
  
#endif // 2D/3D
  
  // finally set quadrant's userdata
  userdata_mgr->quadrant_set_variables (quad, &w);
  userdata_mgr->quadrant_print_qdata (SC_LP_INFO, quad);
  
} // init_burgers_ramp

// =======================================================
// =======CLASS InitialConditionsFactoryBurgers IMPL ======
// =======================================================
InitialConditionsFactoryBurgers::InitialConditionsFactoryBurgers() :
  InitialConditionsFactory()
{

  // register the above callback's
  registerIC("ramp" ,              init_burgers_ramp);
  
} // InitialConditionsFactoryBurgers

} // namespace burgers

} // namespace canop
