#ifndef SOLVER_CORE_BURGERS_H_
#define SOLVER_CORE_BURGERS_H_

#include "SolverCore.h"

#include "userdata_burgers.h"

class ConfigReader;

namespace canop {

namespace burgers {

/**
 * This is were Initial and Border Conditions callbacks are setup.
 */
class SolverCoreBurgers : public SolverCore<Qdata>
{

public:
  SolverCoreBurgers(ConfigReader *cfg);
  ~SolverCoreBurgers();

  /**
   * Static creation method called by the solver factory.
   */
  static SolverCoreBase* create(ConfigReader *cfg)
  {
    return new SolverCoreBurgers(cfg);
  }
  
}; // SolverCoreBurgers

} // namespace burgers

} // namespace canop

#endif // SOLVER_CORE_BURGERS_H_
