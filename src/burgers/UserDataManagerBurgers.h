#ifndef USERDATA_MANAGER_BURGERS_H_
#define USERDATA_MANAGER_BURGERS_H_

#include "UserDataManager.h"
#include "userdata_burgers.h"

namespace canop {

namespace burgers {

/* For this simple case, qdatavar_t and qdatarecons_t are just aliased to double */
using UserDataTypesBurgers = UserDataTypes<Qdata,
					   QdataVar,
					   QdataRecons>;


class UserDataManagerBurgers : public UserDataManager<UserDataTypesBurgers>
{
  
public:
  UserDataManagerBurgers(bool useP4estMemory);
  virtual ~UserDataManagerBurgers();

  virtual void qdata_set_variables (qdata_t * data,
				    qdatavar_t * w);
  
  virtual void quadrant_set_state_data (p4est_quadrant_t * dst,
					p4est_quadrant_t * src);

  virtual void quadrant_compute_mean(p4est_quadrant_t *qmean,
				     p4est_quadrant_t *quadout[P4EST_CHILDREN]);

  
  virtual void qdata_print_variables (int loglevel, qdata_t * data);

  virtual void quadrant_copy_w_to_wnext (p4est_quadrant_t * q);
  virtual void quadrant_copy_wnext_to_w (p4est_quadrant_t * q);
  
  double qdata_get_U (p4est_quadrant_t * q);
  double qdata_get_V (p4est_quadrant_t * q);
  double qdata_get_W (p4est_quadrant_t * q);
  
  /**
   * Return a pointer to a member that is a qdata field "getter".
   */
  virtual qdata_getter_t qdata_getter_byname(const std::string &varName);

  /**
   * Return the field type for the given variable.
   */
  virtual qdata_field_type qdata_type_byname(const std::string &varName);

}; // UserDataManagerBurgers

} // namespace burgers

} // namespace canop

#endif // USERDATA_MANAGER_BURGERS_H_
