#ifndef ITERATORS_BURGERS_H_
#define ITERATORS_BURGERS_H_

#include "Iterators.h"


namespace canop {

namespace burgers {

/*
 * NB: if you need to add new iterators:
 * 1. resize vector iterator_list_t
 * 2. create a new enum to ease indexing this vector
 * 3. add new iterator to the vector in fill_iterators_list method.
 */

class IteratorsBurgers : public IteratorsBase
{

public:
  IteratorsBurgers();
  virtual ~IteratorsBurgers();

  void fill_iterators_list();
  
};  // class IteratorsBurgers

} // namespace burgers

} // namespace canop

#endif // ITERATORS_BURGERS_H_
