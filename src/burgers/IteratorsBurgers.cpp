#include "QuadrantNeighborUtils.h"
#include "quadrant_utils.h"

#ifndef P4_TO_P8
#include <p4est_bits.h>
#include <p4est_mesh.h>
#include <p4est_geometry.h>
#else
#include <p8est_bits.h>
#include <p8est_mesh.h>
#include <p8est_geometry.h>
#endif

#include <sc.h> // for SC_LP_ERROR macro
#include <cmath>

#include "SolverBurgers.h"
#include "SolverCoreBurgers.h"
#include "SolverWrap.h"
#include "IteratorsBurgers.h"
#include "UserDataManagerBurgers.h"
#include "userdata_burgers.h"

#include "IndicatorFactoryBurgers.h"
//#include "RiemannSolverFactoryBurgers.h"

namespace canop {

namespace burgers {

// get userdata specific stuff
using qdata_t               = Qdata;
using qdata_variables_t     = QdataVar;
using qdata_reconstructed_t = QdataRecons;

using UserDataTypesBurgers = UserDataTypes<Qdata,
					   QdataVar,
					   QdataRecons>;

using QuadrantNeighborUtilsBurgers = QuadrantNeighborUtils<UserDataTypesBurgers>;

// =======================================================
// =======================================================
static void
iterator_start_step (p4est_iter_volume_info_t * info,
		     void *user_data)
{
  UNUSED (user_data);

  // retrieve our Solver, UserDataManager and finaly our quadrant data
  SolverBurgers *solver = static_cast<SolverBurgers*>(solver_from_p4est (info->p4est));

  UserDataManagerBurgers
    *userdata_mgr = static_cast<UserDataManagerBurgers *> (solver->get_userdata_mgr());

  userdata_mgr->quadrant_copy_w_to_wnext(info->quad);
  
} // iterator_start_step

// =======================================================
// =======================================================
static void iterator_end_step (p4est_iter_volume_info_t * info,
			       void *user_data)
{
  UNUSED (user_data);

  // retrieve our Solver, UserDataManager and finaly our quadrant data
  SolverBurgers    *solver = static_cast<SolverBurgers*>(solver_from_p4est (info->p4est));

  UserDataManagerBurgers
    *userdata_mgr = static_cast<UserDataManagerBurgers *> (solver->get_userdata_mgr());

  userdata_mgr->quadrant_copy_wnext_to_w(info->quad);
  
} // iterator_end_step

// =======================================================
// =======================================================
static void iterator_mark_adapt (p4est_iter_volume_info_t * info,
				 void *user_data)
{
  UNUSED (user_data);

  p4est_t            *p4est = info->p4est;
  Solver             *solver = solver_from_p4est (info->p4est);
  SolverWrap         *solverWrap = solver->m_wrap;
  UserDataManagerBurgers
    *userdata_mgr = static_cast<UserDataManagerBurgers *> (solver->get_userdata_mgr());

  SolverCoreBurgers   *solverCore = static_cast<SolverCoreBurgers *>(solver->m_solver_core);
  
  p4est_ghost_t      *ghost  = solver->get_ghost();
  p4est_mesh_t       *mesh   = solver->get_mesh();
  qdata_t            *ghost_data = static_cast<qdata_t *>(userdata_mgr->get_ghost_data_ptr());
  qdata_t            *qdata;
  int                 level = info->quad->level;

  int                 boundary = 0;
  int                 face = 0;
  double              eps = 0.0;
  double              eps_max = -1.0;

  p4est_quadrant_t   *neighbor = nullptr;
  qdata_t            *ndata = nullptr;

  //indicator_t         indicator = solver->m_indicator_fn;
  indicator_info_t    iinfo;
  iinfo.face = 0;
  iinfo.epsilon = solver->m_epsilon_refine;
  iinfo.quad_current = info->quad;
  //iinfo.user_data = static_cast<void*>(&(solverBurgers->burgersPar));

  /* init face neighbor iterator */
  p4est_mesh_face_neighbor_t mfn;
  p4est_mesh_face_neighbor_init2 (&mfn, p4est, ghost, mesh,
                                  info->treeid, info->quadid);

  /* loop over neighbors */
  neighbor = p4est_mesh_face_neighbor_next (&mfn, NULL, NULL, NULL, NULL);
  while (neighbor) {
    ndata = QuadrantNeighborUtilsBurgers::mesh_face_neighbor_data (&mfn, ghost_data, &boundary);

    /* mfn.face is initialized to 0 by p4est_mesh_face_neighbor_init2 and
     * is incremented:
     *  * when calling p4est_mesh_face_neighbor_next and the neighbor has
     *    smaller or equal level
     *  * when calling p4est_mesh_face_neighbor_next and the neighbor has a
     *    bigger level, but only after seeing the 2 (or 4) smaller neighbors.
     */
    iinfo.face = mfn.face + (mfn.subface > 0) - 1;
    iinfo.quad_neighbor = neighbor;

    /* compute the indicator */
    qdata = userdata_mgr->quadrant_get_qdata (info->quad);
    eps = solverCore->m_indicator_fn (qdata, ndata, &iinfo);
    eps_max = SC_MAX (eps_max, eps);

    /* if the neighbor is on the boundary, the data was allocated on the fly,
     * so we have to deallocate it.
     */
    if (boundary == 1) {
      sc_mempool_free (info->p4est->user_data_pool, ndata);
    }

    neighbor = p4est_mesh_face_neighbor_next (&mfn, NULL, NULL, &face, NULL);

  } // end while. all neighbors visited.

  /* we suppose that epsilon_refine >= epsilon_coarsen */
  if (level > solver->m_min_refine && eps_max < solver->m_epsilon_coarsen) {
    solverWrap->mark_coarsen(info->treeid, info->quadid);
  } else if (level < solver->m_max_refine && eps_max > solver->m_epsilon_refine) {
    solverWrap->mark_refine(info->treeid, info->quadid);
  } else {
    return;
  }

} // iterator_mark_adapt

// =======================================================
// =======================================================
/**
 * \brief Compute the time step as dt = minimum of sigma * dx * dx / nu.
 *
 * An iterator over all the cells to compute this minimum.
 * The result is stored in the user_data.
 */
static void iterator_time_step (p4est_iter_volume_info_t * info,
				void *user_data)
{

  UNUSED (user_data);

  /* get forest structure */
  SolverBurgers         *solver = static_cast<SolverBurgers*>(solver_from_p4est (info->p4est));
  //SolverWrap           *solverWrap = solver->m_wrap;

  double             *min_dx = (double *) user_data;

  double dx,dy,dz;
  dx = dy = dz = quadrant_length (info->quad);
  
  *min_dx = SC_MIN (*min_dx, dx);

  /* also compute the min and max levels in the mesh */
  solver->m_minlevel = SC_MIN (solver->m_minlevel, info->quad->level);
  solver->m_maxlevel = SC_MAX (solver->m_maxlevel, info->quad->level);
  
} // iterator_time_step

// static double
// compute_derivative(double uL, double uC, double uR,
// 		   double dxL, double dxC, double dxR,
// 		   int order)
// {
//   if (order==1) {
    
//     return (uR-uL)/(0.5*fabs(dxL)+dxC+0.5*dxR);
    
//   } else if (order==2) {
    
//     double alpha = 2*dxR / (dxR-dxL);
//     double beta  = 2*dxL / (dxL-dxR);
//     double gamma = -2.0;
//     return (alpha*uL+beta*uR+gamma*uC)/0.5/(alpha*dxL*dxL+beta*dxR*dxR);
    
//   }
  
//   return 0.0;
  
// } // compute_derivative

// =======================================================
// =======================================================

static qdata_variables_t
convective_flux_x(qdata_variables_t dataL,
		  qdata_variables_t dataR,
		  qdata_variables_t dataC,
		  double dxL, double dxC, double dxR)
{
  double FxxL, FxxR, FxyL, FxyR;
  
  // Compute face velocity
  qdata_variables_t dataPNL, dataPNR, deltaFx ;
  
  dataPNL.U = (0.5*(dataL.U)+dataC.U)/2;
  dataPNR.U = (0.5*(dataR.U)+dataC.U)/2;
  
  if ( fabs(dxL) < fabs(dxC) ) { // Left neighboor is hanging

    dataL.U=dataL.U/2;
    
    if( dataPNL.U > 0 ) {
      FxxL = 0.5*(dataL.U*dataL.U); 
      FxyL = 0.5*(dataL.U*dataL.V);
    } else {
      FxxL = 0.5*(dataC.U*dataC.U);
      FxyL = 0.5*(dataC.U*dataC.V);
    }
    
  } else if ( fabs(dxL) > fabs(dxC) ) {  //Left neighboor is full

    if ( dataPNL.U>0 ) {
      FxxL = 0.5*(0.5*(dataL.U*dataL.U)) ; 
      FxyL = 0.5*(0.5*(dataL.U*dataL.V));
    } else {
      FxxL = 0.5*(dataC.U*dataC.U);
      FxyL = 0.5*(dataC.U*dataC.V);
    }
    
  } else {              //Left neighboor is the same
    
    if ( dataPNL.U>0 ) {
      FxxL = 0.5*(dataL.U*dataL.U);  
      FxyL = 0.5*(dataL.U*dataL.V);
    } else {
      FxxL = 0.5*(dataC.U*dataC.U);
      FxyL = 0.5*(dataC.U*dataC.V);
    }
    
  } // End if Left neighboor
  
  
  if ( fabs(dxR) < fabs(dxC) ) { // Right neighboor is hanging

    dataR.U = dataR.U/2;
    
    if ( dataPNR.U>0 ) {
      FxxR = 0.5*(dataC.U*dataC.U);  
      FxyR = 0.5*(dataC.U*dataC.V);
    } else {
      FxxR = 0.5*(dataR.U*dataR.U);
      FxyR = 0.5*(dataR.U*dataR.V);
    }
    
  } else if ( fabs(dxR) > fabs(dxC) ) {  //Right neighboor is full

    if ( dataPNR.U>0 ) {
      FxxR = (0.5*(dataC.U*dataC.U));  
      FxyR = (0.5*(dataC.U*dataC.V));
    } else {
      FxxR = 0.5*(0.5*(dataR.U*dataR.U));
      FxyR = 0.5*(0.5*(dataR.U*dataR.V));
    }
    
  } else {              //Right neighboor is the same

    if ( dataPNR.U>0 ) {
      FxxR = 0.5*(dataC.U*dataC.U);  
      FxyR = 0.5*(dataC.U*dataC.V);
    } else {
      FxxR = 0.5*(dataR.U*dataR.U);
      FxyR = 0.5*(dataR.U*dataR.V);
    }
    
  } // End if Right neighboor
  
  deltaFx.U = FxxR-FxxL;
  deltaFx.V = FxyR-FxyL;
  
  return deltaFx;
}

// =======================================================
// =======================================================
static qdata_variables_t
convective_flux_y(qdata_variables_t dataL,
		  qdata_variables_t dataR,
		  qdata_variables_t dataC,
		  double dxL, double dxC, double dxR)
{
  double FyyL, FyyR, FyxL, FyxR;

  // Compute face velocity
  qdata_variables_t dataPNL, dataPNR, deltaFy ;

  dataPNL.V = (0.5*(dataL.V)+dataC.V)/2;
  dataPNR.V = (0.5*(dataR.V)+dataC.V)/2;

  if ( fabs(dxL) < fabs(dxC) ) { // Left neighboor is hanging

    dataL.V = dataL.V/2;

    if ( dataPNL.V>0 ) {      
      FyyL = 0.5*(dataL.V*dataL.V); 
      FyxL = 0.5*(dataL.U*dataL.V);
    } else {
      FyyL = 0.5*(dataC.V*dataC.V);
      FyxL = 0.5*(dataC.U*dataC.V);
    }
    
  } else if ( fabs(dxL) > fabs(dxC) ) {  //Left neighboor is full

    if ( dataPNL.V>0 ) {
      FyyL = 0.5*(0.5*(dataL.V*dataL.V));
      FyxL = 0.5*(0.5*(dataL.U*dataL.V));
    } else {
      FyyL = 0.5*(dataC.V*dataC.V);
      FyxL = 0.5*(dataC.U*dataC.V);
    }

  } else {              //Left neighboor is the same

    if ( dataPNL.V>0 ) {
      FyyL = 0.5*(dataL.V*dataL.V);  
      FyxL = 0.5*(dataL.U*dataL.V);
    } else {
      FyyL = 0.5*(dataC.V*dataC.V);
      FyxL = 0.5*(dataC.U*dataC.V);
    }
    
  } // End if Left neighboor

  if ( fabs(dxR) < fabs(dxC) ) { // Right neighboor is hanging

    dataR.V = dataR.V/2;

    if ( dataPNR.V>0 ) {
      FyyR = 0.5*(dataC.V*dataC.V);  
      FyxR = 0.5*(dataC.U*dataC.V);
    } else {
      FyyR = 0.5*(dataR.V*dataR.V);
      FyxR = 0.5*(dataR.U*dataR.V);
    }
    
  } else if ( fabs(dxR) > fabs(dxC) ) {  //Right neighboor is full

    if ( dataPNR.V>0 ) {
      FyyR = (0.5*(dataC.V*dataC.V));  
      FyxR = (0.5*(dataC.U*dataC.V));
    } else {
      FyyR = 0.5*(0.5*(dataR.V*dataR.V));
      FyxR = 0.5*(0.5*(dataR.U*dataR.V));
    }

  } else {              //Right neighboor is the same
    
    if ( dataPNR.V>0 ) {
      FyyR = 0.5*(dataC.V*dataC.V);  
      FyxR = 0.5*(dataC.U*dataC.V);
    } else {
      FyyR = 0.5*(dataR.V*dataR.V);
      FyxR = 0.5*(dataR.U*dataR.V);
    }
    
  } // End if Right neighboor

  deltaFy.U = FyxR-FyxL;
  deltaFy.V = FyyR-FyyL;

  return deltaFy;
  
} // convective_flux_y

// =======================================================
// =======================================================
/**
 * \brief Update the value in a quadrant using the Burgers scheme.
 *
 * This is were the Riemann solver enters into play.
 *
 * Update actually happens on both side of the interface:
 * - when morton index  (actually quadid) of current cell is smaller than 
 *   that of neighbor
 * - when current cell is regular and neighbor is ghost
 */
static void
iterator_burgers_update (p4est_iter_volume_info_t * info, 
			 void *user_data)
{

  /* get forest structure */
  p4est_t              *p4est = info->p4est;
  
  /* get solver and geometry (only for unstructured) */
  SolverBurgers       *solver = static_cast<SolverBurgers*>(solver_from_p4est (p4est));

  UserDataManagerBurgers
    *userdata_mgr = static_cast<UserDataManagerBurgers *> (solver->get_userdata_mgr());
  

  int                *direction = (int *) user_data;
  double              dx  , dy  , dz  ; // current  cell
  double              dx_min;

  p4est_quadrant_t   *quad   = info->quad;
  p4est_locidx_t      quadid = info->quadid;
  p4est_topidx_t      treeid = info->treeid;

  /* 2 states: one for each side of the face */
  qdata_t            *data[2] = { NULL, NULL };

  /* the face Id in range 0..P4EST_FACES-1 */
  int                 qf[2] = { -1, -1 };

  double              scale = 0.0;
  quadrant_neighbor_t<qdata_t> n;

  /* geometrical terms */
  int                 dir_start = (direction == NULL ? 0 : *direction);
  int                 dir_end = (direction == NULL ? P4EST_DIM : *direction + 1);

  double              dxL, dxR;
  qdata_variables_t   dataL, dataR, dataC,Fx,Fy;
  
  double dt = solver->m_dt;
  //double nu = solver->m_cfg->config_read_double ("ramp.nu", 0.07);
  
  UNUSED (scale);
  UNUSED (dx_min);

  /* get current cell data */
  data[CELL_CURRENT] = userdata_mgr->quadrant_get_qdata (quad);
  dataC = data[CELL_CURRENT]->w;
  
  /* current cell length / location */

  //quadrant_get_dx_dy_dz(conn, NULL, quad, treeid, &dx, &dy, &dz);
  dx = dy = dz = quadrant_length (info->quad);
  dxL = dx;
  dxR = dx; 
  
  /* loop over all quadrant's faces */
  for( int dir = dir_start; dir < dir_end; ++dir) {
  
    for (int face = 0; face < 2; ++face) {

      /* get the current face in the given direction */
      qf[CELL_CURRENT] = QuadrantNeighborUtilsBase::direction_faces[dir][face];      
      
      /* 
       * get the Qdata of the neighbors on this face 
       * It is important to realize here that, if face touches the outside
       * boundary, there will be a call to our boundary_fn callback
       * which will fill the wm, wp states
       */
      QuadrantNeighborUtilsBurgers::quadrant_get_face_neighbor (solver,
								quadid,
								treeid,
								qf[CELL_CURRENT],
								&n);
      qf[CELL_NEIGHBOR] = n.face;

      /* 
       *
       */
      if (n.is_hanging) {

	/* half neighbors interface loop */
	for (int i = 0; i < P4EST_HALF; ++i) {
	  	  
	  /* perform update */
	  data[CELL_NEIGHBOR] = n.is.hanging.data[i];

	  if (face==0) {

	    // left
	    dxL = dx*0.5;

	    if (i==0) {
	      dataL = data[CELL_NEIGHBOR]->w;
	    } else {
	      dataL += data[CELL_NEIGHBOR]->w;
	    }

	  } else {

	    // right
	    dxR = dx*0.5;

	    if (i==0) {
	      dataR = data[CELL_NEIGHBOR]->w;
	    } else {
	      dataR += data[CELL_NEIGHBOR]->w;
	    }
	    
	  }
	  
	} /* end for i=0..P4EST_HELF */

      } else {

	data[CELL_NEIGHBOR] = n.is.full.data;
	
	if (face==0) {
	  
	  /* full neighbor (same size or larger) */
	  if (n.level < info->quad->level)
	    dxL = dx*2;
	  else
	    dxL = dx;
	  
	  dataL = data[CELL_NEIGHBOR]->w;

	} else {

	  /* full neighbor (same size or larger) */
	  if (n.level < info->quad->level)
	    dxR = dx*2;
	  else
	    dxR = dx;
	  
	  dataR = data[CELL_NEIGHBOR]->w;

	}
	
      } // end if (n.is_hanging)
      
      /* 
       * if cell is on the outside boundary, we need to free the extra "ghost" 
       * quadrant
       */
      if (n.tree_boundary) {
	sc_mempool_free (info->p4est->user_data_pool, n.is.full.data);
      }

    } // end for face

    if (dir == 0) {
    
      Fx  = convective_flux_x(dataL,dataR,dataC,dxL, dx, dxR);

    } else if (dir==1) {
      
      Fy  = convective_flux_y(dataL,dataR,dataC,dxL, dx, dxR);

    }

    
  } // end for dir

  // now update
  {

    data[CELL_CURRENT]->wnext.U = data[CELL_CURRENT]->w.U - (dt/dx) * (Fx.U+Fy.U);
    data[CELL_CURRENT]->wnext.V = data[CELL_CURRENT]->w.V - (dt/dx) * (Fx.V+Fy.V);

  }
  
} // iterator_burgers_update

// =======================================================
// =======================================================

// =======================================================
// =======================================================

// =======================================================
// =======================================================
IteratorsBurgers::IteratorsBurgers() : IteratorsBase() {

  this->fill_iterators_list();
  
} // IteratorsBurgers::IteratorsBurgers

// =======================================================
// =======================================================
IteratorsBurgers::~IteratorsBurgers()
{
  
} // IteratorsBurgers::~IteratorsBurgers

// =======================================================
// =======================================================
void IteratorsBurgers::fill_iterators_list()
{

  iteratorsList[IT_ID::START_STEP]          = iterator_start_step;
  iteratorsList[IT_ID::END_STEP]            = iterator_end_step;
  iteratorsList[IT_ID::MARK_ADAPT]          = iterator_mark_adapt;
  iteratorsList[IT_ID::CFL_TIME_STEP]       = iterator_time_step;
  iteratorsList[IT_ID::UPDATE]              = iterator_burgers_update;
  
} // IteratorsBurgers::fill_iterators_list

} // namespace burgers

} // namespace canop
