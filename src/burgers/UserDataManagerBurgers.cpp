#include "UserDataManagerBurgers.h"
#include "canoP_base.h"
#include "eos.h"
#include "ScalarLimiterFactory.h"

namespace canop {

namespace burgers {

using burgers_getter_t = double (UserDataManagerBurgers ::*) (p4est_quadrant_t * q);

using scalar_limiter_t = ScalarLimiterFactory::scalar_limiter_t;

// ====================================================================
// ====================================================================
UserDataManagerBurgers::UserDataManagerBurgers(bool useP4estMemory) :
  UserDataManager<UserDataTypesBurgers>()
{

  UNUSED(useP4estMemory);
  
} // UserDataManagerBurgers::UserDataManagerBurgers

// ====================================================================
// ====================================================================
UserDataManagerBurgers::~UserDataManagerBurgers()
{

} // UserDataManagerBurgers::~UserDataManagerBurgers

// ====================================================================
// ====================================================================
void
UserDataManagerBurgers::qdata_set_variables (qdata_t * data,
					     qdatavar_t * w)
{

  memcpy (&(data->w), w, sizeof (qdatavar_t));
  memcpy (&(data->wnext), w, sizeof (qdatavar_t));
  
} // UserDataManagerBurgers::qdata_set_variables

// ====================================================================
// ====================================================================
void
UserDataManagerBurgers::quadrant_set_state_data (p4est_quadrant_t * dst,
						p4est_quadrant_t * src)
{

  qdata_t *data_dst = quadrant_get_qdata (dst);
  qdata_t *data_src = quadrant_get_qdata (src);

  memcpy (&(data_dst->w),     &(data_src->w),     sizeof (qdatavar_t));
  memcpy (&(data_dst->wnext), &(data_src->wnext), sizeof (qdatavar_t));

} // UserDataManagerBurgers::quadrant_set_state_variables

// ====================================================================
// ====================================================================
void
UserDataManagerBurgers::quadrant_compute_mean(p4est_quadrant_t *qmean,
					      p4est_quadrant_t *quadout[P4EST_CHILDREN])
{

  qdata_t     *child_data;
  qdatavar_t   mean; // initialized to zero
  
  for (int8_t i = 0; i < P4EST_CHILDREN; ++i) {

    child_data = quadrant_get_qdata (quadout[i]);

    mean.U += child_data->w.U / P4EST_CHILDREN;
    mean.V += child_data->w.V / P4EST_CHILDREN;
#ifdef P4_TO_P8
    mean.W += child_data->w.W / P4EST_CHILDREN;
#endif
  }

  // finally, copy all of that into qmean
  quadrant_set_variables (qmean, &mean);

} // quadrant_compute_mean

// ====================================================================
// ====================================================================
void
UserDataManagerBurgers::qdata_print_variables (int loglevel, qdata_t * data)
{
  CANOP_LOGF (loglevel, "U    = %-15.8lf %.8lf\n",
              data->w.U, data->wnext.U);
  CANOP_LOGF (loglevel, "V    = %-15.8lf %.8lf\n",
              data->w.V, data->wnext.V);
#ifdef P4_TO_P8
  CANOP_LOGF (loglevel, "W    = %-15.8lf %.8lf\n",
              data->w.W, data->wnext.W);
#endif

} // UserDataManagerBurgers::qdata_print_variables

// ====================================================================
// ====================================================================
void
UserDataManagerBurgers::quadrant_copy_w_to_wnext (p4est_quadrant_t * q)
{
  qdata_t *data = quadrant_get_qdata (q);
  data->wnext = data->w;
  
} // UserDataManagerBurgers::quadrant_copy_w_to_wnext

// ====================================================================
// ====================================================================
void
UserDataManagerBurgers::quadrant_copy_wnext_to_w (p4est_quadrant_t * q)
{
  qdata_t *data = quadrant_get_qdata (q);
  data->w = data->wnext;
  
} // UserDataManagerBurgers::quadrant_copy_wnext_to_w

// ====================================================================
// ====================================================================
double
UserDataManagerBurgers::qdata_get_U (p4est_quadrant_t * q)
{
  qdata_t *data = quadrant_get_qdata (q);

  return data->w.U;
  
} // UserDataManagerBurgers::qdata_get_U

// ====================================================================
// ====================================================================
double
UserDataManagerBurgers::qdata_get_V (p4est_quadrant_t * q)
{
  qdata_t *data = quadrant_get_qdata (q);

  return data->w.V;
  
} // UserDataManagerBurgers::qdata_get_V

// ====================================================================
// ====================================================================
double
UserDataManagerBurgers::qdata_get_W (p4est_quadrant_t * q)
{
#ifdef P4_TO_P8
  qdata_t *data = quadrant_get_qdata (q);
  return data->w.W;
#else
  UNUSED(q);
  return 0.0;
#endif
  
} // UserDataManagerBurgers::qdata_get_W

// ====================================================================
// ====================================================================
qdata_getter_t
UserDataManagerBurgers::qdata_getter_byname (const std::string &varName)
{

  qdata_getter_t base_getter = nullptr;
  burgers_getter_t getter = nullptr;
  
  if (!varName.compare("U")) {
    getter = &UserDataManagerBurgers::qdata_get_U;
  }

  if (!varName.compare("V")) {
    getter = &UserDataManagerBurgers::qdata_get_V;
  }
  
  if (!varName.compare("W")) {
    getter = &UserDataManagerBurgers::qdata_get_W;
  }
  
  if (getter != nullptr) {

    // convert to base class member function pointer
    base_getter = static_cast<qdata_getter_t>(getter);

  } else {

    // print warning
    CANOP_GLOBAL_INFOF ("Unrecognized getter: \"%s\"\n", varName.c_str());
    
  }
  
  return base_getter;
  
} // UserDataManagerBurgers::qdata_getter_byname


// ====================================================================
// ====================================================================
qdata_field_type
UserDataManagerBurgers::qdata_type_byname(const std::string &varName)
{

  if (!varName.compare("U") ||
      !varName.compare("V") ||
      !varName.compare("W"))
    return QDATA_FIELD_SCALAR;

  // print warning
  CANOP_GLOBAL_INFOF ("Unrecognized getter: \"%s\"\n", varName.c_str());
  return QDATA_FIELD_UNKNOWN;

} // UserDataManagerBurgers::qdata_type_byname

} // namespace burgers

} // namespace canop
