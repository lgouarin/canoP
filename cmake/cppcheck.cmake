# adapted from
# https://github.com/ainfosec/ci_helloworld

find_program(CPPCHECK cppcheck)

if(CPPCHECK_BIN STREQUAL "CPPCHECK_BIN-NOTFOUND")
  message(FATAL_ERROR "unable to locate cppcheck")
endif()

# analyze all source files
file(GLOB_RECURSE ALL_SOURCE_FILES *.cpp *.h)

list(APPEND CPPCHECK_ARGS
  --enable=warning,style,performance,portability,unusedFunction,information,missingInclude
  --std=c++11
  --template="[{severity}][{id}] {message} {callstack} \(On {file}:{line}\)"
  --language=c++
  --verbose
  --quiet
  ${ALL_SOURCE_FILES}
  )

add_custom_target(
  cppcheck
  COMMAND ${CPPCHECK} ${CPPCHECK_ARGS}
  COMMENT "running cppcheck"
  )
