#!/bin/sh

function get_library () {
    url=$1
    name=$(basename ${url})

    wget ${url}
    tar zxvf ${name}
}

# get absolute prefix path
install_prefix=${1:-/usr}
install_prefix=$(readlink -f ${install_prefix})

# create a directory for the libs
[ -d "code" ] || mkdir code

# get the additional libraries
cd code
get_lib http://www.netlib.org/lapack/lapack-3.5.0.tgz
get_lib http://www.lua.org/ftp/lua-5.1.5.tar.gz
get_lib http://zlib.net/zlib-1.2.8.tar.gz
get_lib http://www.hdfgroup.org/ftp/HDF5/current/src/hdf5-1.8.13.tar.gz
git clone https://github.com/cburstedde/p4est.git
git clone https://alexfikl@bitbucket.org/alexfikl/p4estest.git

# load igloo modules
module load intel-cc
module load intel-fc
module load intel-mpi
module load cmake

# export variables
export CC=icc
export CXX=icpc
export FC=ifort
export I_MPI_CC=icc
export I_MPI_CXX=icpc
export I_MPI_F90=ifort
export CFLAGS="-O3 -xSSE4.2 -ip"
export FCFLAGS="-O3 -xSSE4.2 -ip"

###############################
#       LAPACK / BLAS
###############################
cd lapack-3.5.0.tgz
cp INSTALL/make.inc.ifort make.inc
sed -e '/EXT_ETIME/ s/^#*/# /' -i make.inc
sed -e '/INT_ETIME/ s/^# *//' -i make.inc
sed -e 's/librefblas/libblas/' -i make.inc
make blaslib
make lapacklib
install -m755 -d "${install_prefix}/{bin,lib,include}"
install -m755 libblas.a "${install_prefix}/lib/libblas.a"
install -m755 liblapack.a "${install_prefix}/lib/liblapack.a"

##############################
#           ZLIB
##############################
cd ../zlib-1.2.8
./configure --prefix=${install_prefix}
make
make install

#############################
#           HDF5
#############################
cd ../hdf5-1.8.13
./configure --prefix=${install_prefix} \
            --enable-parallel \
            --enable-fortran \
            --with-zlib="${install_prefix}/include,${install_prefix}/lib" \
            CFLAGS="-O3 -xSSE"
make
make install

############################
#           LUA
############################
cd ../lua-5.1.5
make linux install INSTALL_TOP=${install_prefix} CC=icc

############################
#           P4EST
############################
cd ../p4est
./bootstrap
./configure --prefix=${install_prefix}
            --enable-mpi \
            --enable-mpiio \
            --enable-shared \
            LIBS="-L${install_prefix}/lib" \
            SC_LIBS="-L${install_prefix}/lib" \
            CFLAGS="-I${install_prefix}/include" \
            FFLAGS="-I${install_prefix}/include" \
            BLAS_LIBS="${install_prefix}/lib/libblas.a"
make
make install
