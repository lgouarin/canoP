#!/bin/sh

a=${1:-1}
b=${2:-0}

for i in $(seq ${a} ${b})
do
  echo "    $(printf "<xi:include href=\"directional%05d.xmf\" xpointer=\"xpointer(//Xdmf/Domain/Grid)\" />" $i)";
done

echo "    </Grid>"
echo "  </Domain>"
echo "</Xdmf>"
