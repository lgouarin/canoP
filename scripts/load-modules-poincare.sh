#!/bin/bash

# load the necessary modules
case $P4EST_ENV in
"intel")
	# necessary for p4est and libsc
	module load intel-env/13.0.1
	module load openmpi/1.6.3_intel13.0.1
	module load autotools/Feb2014

	# necessary for canoP
	module load hdf5/1.8.10_intel_openmpi
	module load cmake/2.8.10.2

	# mkl libs to link against
	export LIBS="-lmkl_intel_ilp64 -lmkl_core -lmkl_sequential"
	;;
*)
	# necessary for p4est and libsc
	module load gnu-env/4.7.2
	module load openmpi/1.6.3_gnu47
	module load lapack/3.5_gnu47
	module load autotools/Feb2014

	# necessary for canoP
	module load hdf5/1.8.10_gnu47_openmpi
	module load cmake/2.8.10.2

	# location of blas library
	export LIBS="-L/gpfslocal/pub/lapack/build_3.5_gnu47/lib"
	;;
esac

# set extra environment flags, for some reason configure doesn't pick them up
export CC=mpicc
export CXX=mpicxx
export FC=mpif90

