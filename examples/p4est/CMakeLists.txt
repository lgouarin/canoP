add_executable(parallel_io parallel_io.cpp)
target_include_directories(parallel_io PUBLIC
  ${HDF5_INCLUDE_DIRS})
target_link_libraries(parallel_io
    MPI::MPI_CXX
    ${HDF5_LIBRARIES}
)

add_executable(ghost3d ghost3d.cpp)
target_link_libraries(ghost3d
    p4est::p4est
    MPI::MPI_CXX
)

add_executable(balance balance.cpp)
target_link_libraries(balance
    p4est::p4est
    MPI::MPI_CXX
)

add_executable(face_iteration face_iteration.cpp)
target_link_libraries(face_iteration
    p4est::p4est
    MPI::MPI_CXX
)
