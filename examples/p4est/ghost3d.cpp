#include <p8est_extended.h>
#include <p8est_bits.h>
#include <p8est_iterate.h>
#include <p8est_connectivity.h>
#include <p8est_ghost.h>
#include <p8est_vtk.h>

#define UNUSED(x) ((void)x)

static int          counter;

static void
quadrant_pprint (p8est_quadrant_t * q, int is_ghost, int rank)
{
  if (q == NULL) {
    printf ("%s quad missing\n", is_ghost ? "ghost" : "");
    SC_ABORT ("wee\n");
    return;
  }

  p4est_qcoord_t      x = (q->x) >> (P8EST_QMAXLEVEL - q->level);
  p4est_qcoord_t      y = (q->y) >> (P8EST_QMAXLEVEL - q->level);

  printf ("[p8est %d] x %d y %d level %d", rank, x, y, q->level);
  if (is_ghost) {
    printf (" value ghost");
  }
  else {
    printf (" value %d", q->p.user_int);
  }
  printf ("\n");
}

static void
init_fn (p8est_t * p8est, p4est_topidx_t which_tree,
         p8est_quadrant_t * quadrant)
{
  UNUSED (p8est);
  UNUSED (which_tree);

  quadrant->p.user_int = counter;
  counter += 1;
}

static int
refine_fn (p8est_t * p8est, p4est_topidx_t which_tree,
           p8est_quadrant_t * quadrant)
{
  UNUSED (p8est);
  UNUSED (which_tree);

  double              xyz[3];
  double              h =
    (double) P8EST_QUADRANT_LEN (quadrant->level) / P8EST_ROOT_LEN;

  p8est_qcoord_to_vertex (p8est->connectivity, which_tree,
                          quadrant->x, quadrant->y, quadrant->z, xyz);

  // the center
  xyz[0] += h / 2;
  xyz[1] += h / 2;
  xyz[2] += h / 2;

  int                 in_disk = ((SC_SQR (xyz[0] - 0.5) +
                                  SC_SQR (xyz[1] - 0.5) +
                                  SC_SQR (xyz[2] - 0.5)) < 0.15 ? 1 : 0);

  return in_disk && (quadrant->level < 4);
}

static void
iter_face_fn (p8est_iter_face_info_t * info, void *user_data)
{
  UNUSED (user_data);

  int                 mpirank = info->p4est->mpirank;
  p8est_quadrant_t   *quad = NULL;
  p8est_iter_face_side_t *side = NULL;

  for (size_t i = 0; i < info->sides.elem_count; ++i) {
    side = p8est_iter_fside_array_index (&info->sides, i);

    if (!side->is_hanging) {
      quad = side->is.full.quad;

      quadrant_pprint (quad, side->is.full.is_ghost, mpirank);
    }
    else {
      for (int j = 0; j < P8EST_HALF; ++j) {
        quad = side->is.hanging.quad[j];
        quadrant_pprint (quad, side->is.hanging.is_ghost[j], mpirank);
      }
    }
  }
}

int
main (int argc, char **argv)
{
  MPI_Comm            mpicomm;
  int                 mpisize, mpirank, mpiret;
  p8est_t            *p8est;
  p8est_connectivity_t *connectivity;
  p8est_ghost_t      *ghost_layer;

  /* initialize MPI and p8est internals */
  mpiret = MPI_Init (&argc, &argv);
  SC_CHECK_MPI (mpiret);
  mpicomm = MPI_COMM_WORLD;
  mpiret = MPI_Comm_size (mpicomm, &mpisize);
  SC_CHECK_MPI (mpiret);
  mpiret = MPI_Comm_rank (mpicomm, &mpirank);
  SC_CHECK_MPI (mpiret);

  /* initialize the loggers */
  sc_init (mpicomm, 1, 1, NULL, SC_LP_ALWAYS);
  p4est_init (NULL, SC_LP_ALWAYS);

  /* initialize the forest */
  connectivity = p8est_connectivity_new_unitcube ();
  p8est = p8est_new_ext (mpicomm, connectivity, 4, 2, 0, 0, init_fn, NULL);

  // refine a few times
  p8est_refine (p8est, 1, refine_fn, init_fn);
  p8est_balance (p8est, P8EST_CONNECT_FULL, init_fn);
  p8est_vtk_write_file (p8est, NULL, "ghost3d");

  // iterate and find a missing ghost
  ghost_layer = p8est_ghost_new (p8est, P8EST_CONNECT_FACE);
  p8est_iterate (p8est, ghost_layer, NULL, NULL, iter_face_fn, NULL, NULL);

  /* destroy p8est and its connectivity structure */
  p8est_destroy (p8est);
  p8est_connectivity_destroy (connectivity);
  p8est_ghost_destroy (ghost_layer);

  /* clean up and exit */
  sc_finalize ();

  mpiret = MPI_Finalize ();
  SC_CHECK_MPI (mpiret);

  return 0;
}
