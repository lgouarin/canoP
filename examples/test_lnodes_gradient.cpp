
#ifdef USE_3D
#include <p4est_to_p8est.h>
#endif

#ifndef P4_TO_P8
#include <p4est_algorithms.h>
#include <p4est_bits.h>
#include <p4est_extended.h>
#include <p4est_ghost.h>
#include <p4est_lnodes.h>
#else
#include <p8est_algorithms.h>
#include <p8est_bits.h>
#include <p8est_extended.h>
#include <p8est_ghost.h>
#include <p8est_lnodes.h>
#endif

#include "IO_Writer.h"
#include "Lnode_Utils.h"

#define UNUSED(x) ((void)(x))

/** Per-quadrant data for this test. */
typedef struct user_data
{
  double              rho;            /**< the state variable */
  double              dx_rho;         /**< the x component of the gradient */
  double              dy_rho;         /**< the y component of the gradient */
#ifdef P4_TO_P8
  double              dz_rho;         /**< the z component of the gradient */
#endif
}
user_data_t;

#ifndef P4_TO_P8
static int          refine_level = 2;
#else
static int          refine_level = 2;
#endif

static int          max_level = 3;

#ifndef P4_TO_P8
static p4est_connectivity_t *
p4est_connectivity_new_lnodes_test (void)
{
  const p4est_topidx_t num_vertices = 6;
  const p4est_topidx_t num_trees = 2;
  const p4est_topidx_t num_ctt = 0;
  const double        vertices[6 * 3] = {
    0, 0, 0,
    1, 0, 0,
    0, 1, 0,
    1, 1, 0,
    0, 2, 0,
    1, 2, 0,
  };
  const p4est_topidx_t tree_to_vertex[2 * 4] = {
    2, 3, 4, 5, 0, 1, 2, 3,
  };
  const p4est_topidx_t tree_to_tree[2 * 4] = {
    0, 0, 1, 0, 1, 1, 1, 0,
  };
  const int8_t        tree_to_face[2 * 4] = {
    0, 1, 3, 3, 0, 1, 2, 2,
  };

  return p4est_connectivity_new_copy (num_vertices, num_trees, 0,
                                      vertices, tree_to_vertex,
                                      tree_to_tree, tree_to_face,
                                      NULL, &num_ctt, NULL, NULL);
}
#endif

static int
refine_fn (p4est_t * p4est, p4est_topidx_t which_tree,
           p4est_quadrant_t * quadrant)
{
  int                 cid;
  double  X[3] = { 0, 0, 0 };
  p4est_qcoord_t      half_length = P4EST_QUADRANT_LEN (quadrant->level) / 2;

  p4est_qcoord_to_vertex (p4est->connectivity, which_tree,
                          quadrant->x + half_length,
                          quadrant->y + half_length,
#ifdef P4_TO_P8
                          quadrant->z + half_length,
#endif
                          X);

  if (which_tree == 2 || which_tree == 3) {
    return 0;
  }

  if ( quadrant->level >= max_level)
    return 0;

  if (X[0]*X[0] + X[1]*X[1] < 0.5*0.5)
    return 1;

  cid = p4est_quadrant_child_id (quadrant);

  if (cid == P4EST_CHILDREN - 1 ||
      (quadrant->x >= P4EST_LAST_OFFSET (P4EST_MAXLEVEL - 2) &&
       quadrant->y >= P4EST_LAST_OFFSET (P4EST_MAXLEVEL - 2)
#ifdef P4_TO_P8
       && quadrant->z >= P4EST_LAST_OFFSET (P4EST_MAXLEVEL - 2)
#endif
       )) {
    return 1;
  }
  if ((int) quadrant->level >= (refine_level - (int) (which_tree % 3))) {
    return 0;
  }
  if (quadrant->level == 1 && cid == 2) {
    return 1;
  }
  if (quadrant->x == P4EST_QUADRANT_LEN (2) &&
      quadrant->y == P4EST_LAST_OFFSET (2)) {
    return 1;
  }
  if (quadrant->y >= P4EST_QUADRANT_LEN (2)) {
    return 0;
  }

  return 1;
}

#ifndef P4_TO_P8
static int
refine_fn_lnodes_test (p4est_t * p4est, p4est_topidx_t which_tree,
                       p4est_quadrant_t * quadrant)
{
  UNUSED(p4est);
  
  int                 cid;

  cid = p4est_quadrant_child_id (quadrant);

  if (!which_tree && cid == 1) {
    return 1;
  }
  if (which_tree == 1 && cid == 2) {
    return 1;
  }
  return 0;
}
#endif

/** Allocate storage for processor-relevant nodal degrees of freedom.
 *
 * \param [in] lnodes   This structure is queried for the node count.
 * \return              Allocated double array; must be freed with P4EST_FREE.
 */
static double      *
allocate_vector_double (p4est_lnodes_t * lnodes)
{
  return P4EST_ALLOC (double, lnodes->num_local_nodes);
}

static double get_quadrant_scale(p4est_t * p4est,
                                 p4est_topidx_t which_tree,
                                 p4est_quadrant_t * quad)
{

  UNUSED(p4est);
  UNUSED(which_tree);

  /* assume here cartesian geometry */
  double dx = 1.0*P4EST_QUADRANT_LEN (quad->level) / P4EST_ROOT_LEN;

  return dx;
} /* get_quadrant_scale */

static double get_quadrant_volume(p4est_t * p4est,
                                  p4est_topidx_t which_tree,
                                  p4est_quadrant_t * quad)
{
  UNUSED(p4est);
  UNUSED(which_tree);

  /* assume here cartesian geometry */
  double dx = 1.0*P4EST_QUADRANT_LEN (quad->level) / P4EST_ROOT_LEN;

#ifndef P4_TO_P8
  return dx*dx;
#else
  return dx*dx*dx;
#endif

} /* get_quadrant_volume */

static void
init_fn (p4est_t * p4est,
         p4est_topidx_t which_tree,
         p4est_quadrant_t * quad)
{
  UNUSED(p4est);
  UNUSED(which_tree);

  user_data_t        *data = (user_data_t *) quad->p.user_data;

  // get the coordinates of the center of the quad
  double  XYZ[3] = { 0, 0, 0 };
  double  x,y,z;
  UNUSED (z);
  p4est_qcoord_t      half_length = P4EST_QUADRANT_LEN (quad->level) / 2;

  // get the physical coordinates of the center of the quad
  // assume geometry is regular cartesian
  p4est_qcoord_to_vertex (p4est->connectivity, which_tree,
                          quad->x + half_length,
                          quad->y + half_length,
#ifdef P4_TO_P8
                          quad->z + half_length,
#endif
                          XYZ);

  x = XYZ[0];
  y = XYZ[1];
  z = XYZ[2];

  data->rho = x*x - y*y;
  data->dx_rho = -1.;
  data->dy_rho = -1.;
#ifdef P4_TO_P8
  data->dz_rho = -1.;
#endif
} /* init_fn */

/* data getters */
double
qdata_get_rho (p4est_quadrant_t *q)
{
  user_data_t        *data = (user_data_t *) q->p.user_data;
  return data->rho;
}

void
qdata_get_grad_rho (p4est_quadrant_t *q, size_t dim, double *v)
{
  user_data_t        *data = (user_data_t *) q->p.user_data;

  v[0] = data->dx_rho;
  v[1] = data->dy_rho;
  if(dim == 3) {
#ifdef P4_TO_P8
    v[2] = data->dz_rho;
#else
    v[2] = 0;
#endif
  }
}

/* data setter */
void
qdata_set_grad_rho (p4est_quadrant_t *q, double *val)
{
  user_data_t      *data = (user_data_t *) q->p.user_data;
  data->dx_rho = val[0];
  data->dy_rho = val[1];
#ifdef P4_TO_P8
  data->dz_rho = val[2];
#endif
}

static void
test_lnodes_gradient (p4est_t * p4est, p4est_geometry_t * geom, p4est_lnodes_t * lnodes)
{
  /* local nodes number */
  const int           nloc = lnodes->num_local_nodes;
  /* local nodes index */
  p4est_locidx_t      lni;
  /* lnodes attached data */
  double             *lnodes_data;

  lnodes_data = allocate_vector_double (lnodes);

  interp_quad_to_lnodes (p4est, lnodes, qdata_get_rho, get_quadrant_volume, lnodes_data);

  gradient_lnodes_to_quad (p4est, lnodes, get_quadrant_scale, lnodes_data, qdata_set_grad_rho);

  /* use canoP parallel Hdf5/Xdmf IO */
  {

    int                 write_mesh_info = 1;
    const char         filename[] = "test_lnodes_gradient";
    double             *lnodes_ids;

    lnodes_ids = allocate_vector_double(lnodes);
    for (lni = 0; lni < nloc; ++lni) {
      lnodes_ids[lni] = (double)lni;
    }

    IO_Writer *w = new IO_Writer(p4est, geom, filename, write_mesh_info);
    w->open(filename);

    w->write_header(0.0);

    w->write_quadrant_attribute("rho", qdata_get_rho,
                                H5T_NATIVE_DOUBLE);

    w->write_quadrant_attribute("grad_rho", 3, qdata_get_grad_rho,
                              H5T_NATIVE_DOUBLE);

    w->write_lnode_attribute("rho_n", lnodes, lnodes_data,
                             H5T_NATIVE_DOUBLE);

    w->write_lnode_attribute("lni_n", lnodes, lnodes_ids,
                             H5T_NATIVE_DOUBLE);

    w->write_footer();
    w->close();

    delete w;

    P4EST_FREE(lnodes_ids);

  } // end hdf5/xdmf writer

  P4EST_FREE(lnodes_data);

} /* test_interpolation_quad_to_lnodes */

int
main (int argc, char **argv)
{
  sc_MPI_Comm         mpicomm;
  int                 mpiret;
  int                 mpisize, mpirank;
  p4est_t            *p4est;
  p4est_connectivity_t *conn;
  p4est_ghost_t      *ghost_layer;
  int                 i;
  p4est_lnodes_t     *lnodes;

  const char         *usage;


  /* initialize MPI */
  mpiret = sc_MPI_Init (&argc, &argv);
  SC_CHECK_MPI (mpiret);
  mpicomm = sc_MPI_COMM_WORLD;
  mpiret = sc_MPI_Comm_size (mpicomm, &mpisize);
  SC_CHECK_MPI (mpiret);
  mpiret = sc_MPI_Comm_rank (mpicomm, &mpirank);
  SC_CHECK_MPI (mpiret);

  sc_init (mpicomm, 1, 1, NULL, SC_LP_DEFAULT);
  p4est_init (NULL, SC_LP_DEFAULT);

  /* process command line arguments */
  usage =
    "Arguments: <configuration_number> <level>\n"
    "   Configuration is an integer\n"
    "   Level controls the maximum depth of refinement\n";

  i = 0;
  if (argc>1)
    i = atoi(argv[1]);

  if (argc>2)
    refine_level = atoi (argv[2]);

  //refine_fn = refine_normal_fn;
  //coarsen_fn = NULL;

  /* create connectivity and forest structures */
  switch (i) {
#ifndef P4_TO_P8
  case 0:
    conn = p4est_connectivity_new_unitsquare ();
    break;
  case 1:
    conn = p4est_connectivity_new_moebius ();
    break;
  case 2:
    conn = p4est_connectivity_new_star ();
    break;
  case 3:
    conn = p4est_connectivity_new_periodic ();
    break;
  case 4:
    conn = p4est_connectivity_new_lnodes_test ();
    break;
  case 5:
    conn = p4est_connectivity_new_brick (1, 2, 0, 0);
    break;
  default:
    conn = p4est_connectivity_new_unitsquare ();
    break;
#else
  case 0:
    conn = p8est_connectivity_new_unitcube ();
    break;
  case 1:
    conn = p8est_connectivity_new_periodic ();
    break;
  case 2:
    conn = p8est_connectivity_new_rotwrap ();
    break;
  case 3:
    conn = p8est_connectivity_new_rotcubes ();
    break;
  case 4:
    conn = p8est_connectivity_new_shell ();
    break;
  case 5:
    conn = p8est_connectivity_new_brick (1, 2, 3, 0, 0, 0);
    break;
  default:
    conn = p8est_connectivity_new_unitcube ();
    break;
#endif
  }

  p4est = p4est_new_ext (mpicomm, conn, 15, 0, 0,
                         sizeof (user_data_t), init_fn, NULL);
  /*p4est = p4est_new_ext (mpicomm, conn, 15, 0, 0,
    0, NULL, NULL);*/

  /* refine recursive to make the number of elements interesting */
  p4est_refine (p4est, 1, refine_fn, init_fn);

  /* do a uniform partition */
  p4est_partition (p4est, 0, nullptr);

  /* balance and repartition the forest */
  p4est_balance (p4est, P4EST_CONNECT_FULL, init_fn);
  p4est_partition (p4est, 1, nullptr);

  /* Write the forest to disk for visualization, one file per processor. */
  //write_quad_data_vtk (p4est, "test_lnodes2_interpolation");


  /* create ghost layer before lnodes data structure */
  ghost_layer = p4est_ghost_new (p4est, P4EST_CONNECT_FULL);

  /* create lnode structure */
  lnodes = p4est_lnodes_new (p4est, ghost_layer, 1);


  /*
   * Now start the test.
   */
  test_lnodes_gradient (p4est, NULL, lnodes);


  /* destroy ghost (no needed after node creation */
  p4est_ghost_destroy (ghost_layer);

  /* Destroy lnodes */
  p4est_lnodes_destroy (lnodes);

  /* Destroy the p4est and the connectivity structure. */
  p4est_destroy (p4est);
  p4est_connectivity_destroy (conn);

  /* exit */
  sc_finalize ();

  mpiret = sc_MPI_Finalize ();
  SC_CHECK_MPI (mpiret);

  return 0;
}

/* EOF test_lnodes_gradient.cpp */
