/*
 * Usage: test_icosahedron <level>
 */

#include <p4est_bits.h>
#include <p4est_extended.h>
#include <p4est_vtk.h>
#include <p4est_iterate.h>

#include "quadrant_utils.h"
#include "connectivity.h"
#include "geometry.h"
#include "IO_Writer.h"

typedef struct
{
  double              area; /* cell area */
}
user_data_t;

typedef struct
{
  sc_MPI_Comm         mpicomm;
  int                 mpisize;
  int                 mpirank;
}
mpi_context_t;

static int          refine_level = 0;

static void
init_fn (p4est_t * p4est, 
	 p4est_topidx_t which_tree,
         p4est_quadrant_t * quadrant)
{
  UNUSED(p4est);
  UNUSED(which_tree);

  user_data_t        *data = (user_data_t *) quadrant->p.user_data;

  data->area = 0.0;
}

static int
refine_icosahedron_fn (p4est_t * p4est, p4est_topidx_t which_tree,
		       p4est_quadrant_t * quadrant)
{

  p4est_geometry_t *geom = (p4est_geometry_t *) p4est->user_pointer;

  /* logical coordinates */
  double              xyz[3] = { 0, 0, 0 };

  /* physical coordinates */
  double              XYZ[3] = { 0, 0, 0 };

  double h2 = 0.5 * P4EST_QUADRANT_LEN (quadrant->level) / P4EST_ROOT_LEN;
  const double        intsize = 1.0 / P4EST_ROOT_LEN;

  /* 
   * get coordinates at cell center
   */
  xyz[0] = intsize*quadrant->x + h2;
  xyz[1] = intsize*quadrant->y + h2;
#ifdef P4_TO_P8
  xyz[2] = intsize*quadrant->z + h2;
#endif
  
  // from logical coordinates to physical coordinates (cartesian)
  geom->X (geom, which_tree, xyz, XYZ);

  /* if (quadrant->level > 6)
    return 0;
  if (XYZ[2]>0 && quadrant->level>=3)
  return 0; */

  if (quadrant->level > 4)
    return 0;

  return 1;
}

static void
area_compute_callback(p4est_iter_volume_info_t * info, void *user_data)
{
  UNUSED(user_data);

  p4est_t                *p4est = info->p4est;
  p4est_quadrant_t       *quad = info->quad;
  p4est_connectivity_t   *conn =  p4est->connectivity;
  p4est_geometry_t       *geom = (p4est_geometry_t *) p4est->user_pointer;
  p4est_topidx_t          which_tree = info->treeid;

  double area = 0.0;
  
  // get the coordinates of the vertex0
  double  XYZ0[3] = { 0, 0, 0 };
  quadrant_vertex_coord(conn, geom, which_tree, quad, 0, XYZ0);
  double  XYZ1[3] = { 0, 0, 0 };
  quadrant_vertex_coord(conn, geom, which_tree, quad, 1, XYZ1);
  double  XYZ2[3] = { 0, 0, 0 };
  quadrant_vertex_coord(conn, geom, which_tree, quad, 2, XYZ2);
  double  XYZ3[3] = { 0, 0, 0 };
  quadrant_vertex_coord(conn, geom, which_tree, quad, 3, XYZ3);

  double a,b,c;
  double s;
  double dot;
  double tmp;
  /* triangle 013 */
  dot = XYZ0[0]*XYZ1[0] + XYZ0[1]*XYZ1[1] + XYZ0[2]*XYZ1[2];
  c = acos(dot);
  dot = XYZ1[0]*XYZ3[0] + XYZ1[1]*XYZ3[1] + XYZ1[2]*XYZ3[2];
  a = acos(dot);
  dot = XYZ3[0]*XYZ0[0] + XYZ3[1]*XYZ0[1] + XYZ3[2]*XYZ0[2];
  b = acos(dot);
  s = (a+b+c)*0.5;

  tmp = 4*atan( sqrt( tan(0.5*s)*tan(0.5*(s-a))*tan(0.5*(s-b))*tan(0.5*(s-c)) ) );
  area += tmp;

  /* triangle 023 */
  dot = XYZ0[0]*XYZ2[0] + XYZ0[1]*XYZ2[1] + XYZ0[2]*XYZ2[2];
  c = acos(dot);
  dot = XYZ2[0]*XYZ3[0] + XYZ2[1]*XYZ3[1] + XYZ2[2]*XYZ3[2];
  a = acos(dot);
  dot = XYZ3[0]*XYZ0[0] + XYZ3[1]*XYZ0[1] + XYZ3[2]*XYZ0[2];
  b = acos(dot);
  s = (a+b+c)*0.5;

  tmp = 4*atan( sqrt( tan(0.5*s)*tan(0.5*(s-a))*tan(0.5*(s-b))*tan(0.5*(s-c)) ) );
  area += tmp;

  user_data_t        *data = (user_data_t *) quad->p.user_data;
  data->area = area;

} /* end area_compute_callback */

static void
total_area_compute_callback(p4est_iter_volume_info_t * info, void *user_data) {

  p4est_quadrant_t       *quad = info->quad;

  // local cell data
  user_data_t        *data = (user_data_t *) quad->p.user_data;

  double *total_area = (double *) user_data;
  *total_area += data->area;

} /* end total_area_compute_callback */

static void
getdata_callback(p4est_iter_volume_info_t * info, void *user_data)
{
  /* we passed the array of values to fill as the user_data in the call
     to p4est_iterate */
  sc_array_t         *area_data = (sc_array_t *) user_data;
  double             *this_data;
  p4est_t            *p4est = info->p4est;
  p4est_quadrant_t   *q = info->quad;
  p4est_topidx_t      which_tree = info->treeid;
  p4est_locidx_t      local_id = info->quadid;  /* this is the index of q *within its tree's numbering*.  We want to convert it its index for all the quadrants on this process, which we do below */
  p4est_tree_t       *tree;
  user_data_t        *data = (user_data_t *) q->p.user_data;
  p4est_locidx_t      arrayoffset;

  tree = p4est_tree_array_index (p4est->trees, which_tree);
  local_id += tree->quadrants_offset;   /* now the id is relative to the MPI process */
  arrayoffset = local_id;

  this_data = (double*) sc_array_index (area_data, arrayoffset);
  this_data[0] = data->area;

} /* getdata_callback */

/*
 * perform a volume iteration, and compute cell area
 */
static void
area_compute(p4est_t *p4est) {

  /* iterate to compute quadrant/cell area */
  /* *INDENT-OFF* */
  p4est_iterate (p4est,                 /* the forest */
		 NULL,                  /* the ghost layer */
		 NULL,                  /* the synchronized ghost data */
		 area_compute_callback, /* volume callback */
		 NULL,                  /* face callback */
#ifdef P4_TO_P8
		 NULL,                  /* there is no callback for the
					   edges between quadrants */
#endif
		 NULL);                 /* there is no callback for the
					   corners between quadrants */
    /* *INDENT-ON* */


} /* area_compute */

/*
 * perform a volume iteration, and compute cell area
 */
static void
total_area_compute(p4est_t *p4est) {

  double total_area;

  /* iterate to compute quadrant/cell area */
  /* *INDENT-OFF* */
  p4est_iterate (p4est,                 /* the forest */
		 NULL,                  /* the ghost layer */
		 &total_area,           /* the user data */
		 total_area_compute_callback, /* volume callback */
		 NULL,                  /* face callback */
#ifdef P4_TO_P8
		 NULL,                  /* there is no callback for the
					   edges between quadrants */
#endif
		 NULL);                 /* there is no callback for the
					   corners between quadrants */
    /* *INDENT-ON* */

  P4EST_GLOBAL_INFOF("total area is : %f\n",total_area);

} /* total_area_compute */

/* data getter */
double              qdata_get_area (p4est_quadrant_t * q)
{
  user_data_t        *data = (user_data_t *) q->p.user_data;

  return data->area;
}

void
write_user_data(p4est_t *p4est, p4est_geometry_t * geom, const char *filename)
{

  sc_array_t         *area_data;    /* array of cell data to write */
  p4est_locidx_t      numquads;

  numquads = p4est->local_num_quadrants;

  /* create a vector with one value per local quadrant */
  area_data = sc_array_new_size (sizeof(double), numquads);

  /* Use the iterator to visit every cell and fill in the data array */
  p4est_iterate (p4est, 
		 NULL,   /* we don't need any ghost quadrants for this loop */
                 (void *) area_data,     /* pass in array pointer so that we can fill it */
                 getdata_callback,   /* callback function to get cell data */
                 NULL,
#ifdef P4_TO_P8
                 NULL,
#endif
                 NULL);

  /* the following currently only works when using p4est branch 'vtk' or 'develop' */
#if 0
  /* create VTK output context and set its parameters */
  p4est_vtk_context_t *context = p4est_vtk_context_new (p4est, filename);
  p4est_vtk_context_set_scale (context, 0.95);
  p4est_vtk_context_set_geom  (context, geom);

  p4est_vtk_write_header (context);
  context = p4est_vtk_write_cell_dataf (context, 1, 1,
					1, 
					0, 
					1,  /* 1 scalar */
					0,  /* 0 vector */
					"area", area_data, 
					context);
  SC_CHECK_ABORT (context != NULL,
                  P4EST_STRING "_vtk: Error writing cell data");

  const int retval = p4est_vtk_write_footer (context);
  SC_CHECK_ABORT (!retval, P4EST_STRING "_vtk: Error writing footer");
#endif /* write with vtk file format */

  /* use canoP parallel Hdf5/Xdmf IO */
  {
    IO_Writer          *w;
    int                 write_mesh_info = 1;

    // io_writer constructor
    w = new IO_Writer (p4est, geom, filename, write_mesh_info);

    // open the new file and write our stuff
    w->open (filename);

    w->write_header (0.0);

    w->write_quadrant_attribute ("area", qdata_get_area,
				 H5T_NATIVE_DOUBLE);

    // close the file
    w->write_footer();
    w->close();

    // destroy IO_Writer
    delete w;

  } // end hdf5/xdmf writer

  sc_array_destroy (area_data);

} /* write_user_data */

int
main (int argc, char **argv)
{
  int                 mpiret;
  int                 wrongusage;
  const char         *usage;
  mpi_context_t       mpi_context, *mpi = &mpi_context;
  p4est_t            *p4est;
  p4est_connectivity_t *connectivity;
  p4est_geometry_t   *geom;
  p4est_refine_t      refine_fn;
  p4est_coarsen_t     coarsen_fn;

  /* initialize MPI and p4est internals */
  mpiret = sc_MPI_Init (&argc, &argv);
  SC_CHECK_MPI (mpiret);
  mpi->mpicomm = sc_MPI_COMM_WORLD;
  mpiret = sc_MPI_Comm_size (mpi->mpicomm, &mpi->mpisize);
  SC_CHECK_MPI (mpiret);
  mpiret = sc_MPI_Comm_rank (mpi->mpicomm, &mpi->mpirank);
  SC_CHECK_MPI (mpiret);

  sc_init (mpi->mpicomm, 1, 1, NULL, SC_LP_DEFAULT);
  p4est_init (NULL, SC_LP_DEFAULT);

  /* process command line arguments */
  usage =
    "Arguments: <level>\n";
  wrongusage = 0;
  if (!wrongusage && argc < 2) {
    wrongusage = 1;
  }
  if (wrongusage) {
    P4EST_GLOBAL_LERROR (usage);
    sc_abort_collective ("Usage error");
  }

  /* assign variables based on configuration */
  refine_level = atoi (argv[1]);
  refine_fn = refine_icosahedron_fn;
  coarsen_fn = NULL;

  /* create connectivity and forest structures */
  geom = NULL;
  {
    double R = 1.0; /* sphere radius default value */
    
    if (argc >= 3)
      R = atof (argv[2]);
    
    connectivity = p4est_connectivity_new_icosahedron ();
    geom = p4est_geometry_new_icosahedron (connectivity, R);
  }

  p4est = p4est_new_ext (mpi->mpicomm, connectivity, 15, 0, 0,
                         sizeof (user_data_t), init_fn, geom);
  //p4est_vtk_write_file (p4est, geom, "icosahedron_new");

  /* refinement and coarsening */
  p4est_refine (p4est, 1, refine_fn, init_fn);
  if (coarsen_fn != NULL) {
    p4est_coarsen (p4est, 1, coarsen_fn, init_fn);
  }
  //p4est_vtk_write_file (p4est, geom, "icosahedron_refined");

  /* balance */
  p4est_balance (p4est, P4EST_CONNECT_FULL, init_fn);
  p4est_vtk_write_file (p4est, geom, "icosahedron_balanced");

  P4EST_GLOBAL_INFOF("Is p4est grid balanced ? : %d\n",p4est_is_balanced (p4est, P4EST_CONNECT_FULL));

  /* partition */
  p4est_partition (p4est, 0, NULL);
  p4est_vtk_write_file (p4est, geom, "icosahedron_partition");

  /* flag all quadrants that touch the outside boundary */
  area_compute(p4est);
  total_area_compute(p4est);

  /* save outside boundary data */
  write_user_data(p4est, geom, "icosahedron_area");
  
  /* destroy the p4est and its connectivity structure */
  p4est_destroy (p4est);
  if (geom != NULL) {
    p4est_geometry_destroy (geom);
  }
  p4est_connectivity_destroy (connectivity);

  /* clean up and exit */
  sc_finalize ();

  mpiret = sc_MPI_Finalize ();
  SC_CHECK_MPI (mpiret);

  return 0;
}
