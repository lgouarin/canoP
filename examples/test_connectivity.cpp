/*
 * Usage: test_connectivity <filename.inp>
 *
 * Try to read an abaqus file to create a connectivity.
 */

#ifdef USE_3D
#include <p4est_to_p8est.h>
#endif

#ifndef P4_TO_P8
#include <p4est_connectivity.h>
#include <p4est_bits.h>
#include <p4est_extended.h>
#include <p4est_vtk.h>
#include <p4est_iterate.h>
#else
#include <p8est_connectivity.h>
#include <p8est_bits.h>
#include <p8est_extended.h>
#include <p8est_vtk.h>
#include <p8est_iterate.h>
#endif


#include "quadrant_utils.h"
#include "connectivity.h"
#include "geometry.h"
#include "IO_Writer.h"

typedef struct
{
  double              area; /* cell area */
  double              x,y,z; /* cell center coordinates */
  uint64_t            lid;  /* local "morton" index inside MPI process - 
			       can be seen as z-order index but reset
			       in each MPI process */
} user_data_t;

typedef struct
{
  sc_MPI_Comm         mpicomm;
  int                 mpisize;
  int                 mpirank;
} mpi_context_t;

//static int          refine_level = 2;

static void
init_fn (p4est_t * p4est, 
	 p4est_topidx_t which_tree,
         p4est_quadrant_t * quadrant)
{
  UNUSED(p4est);
  UNUSED(which_tree);

  user_data_t        *data = (user_data_t *) quadrant->p.user_data;

  data->area = 0.0;

  {
    p4est_geometry_t  *geom = (p4est_geometry_t *) p4est->user_pointer;
    
    double XYZc[3];
    quadrant_center_vertex (p4est->connectivity, geom, which_tree, quadrant, XYZc);
    data->x = XYZc[0];
    data->y = XYZc[1];
    data->z = XYZc[2];
  }
  
  data->lid = 0;

} // init_fn

static int
refine_dummy_fn (p4est_t * p4est,
		 p4est_topidx_t which_tree,
		 p4est_quadrant_t * quadrant)
{

  UNUSED(p4est);
  UNUSED(which_tree);

  /* logical coordinates */
  //double              xyz[3] = { 0, 0, 0 };

  /* physical coordinates */
  //double              XYZ[3] = { 0, 0, 0 };

  //double h2 = 0.5 * P4EST_QUADRANT_LEN (quadrant->level) / P4EST_ROOT_LEN;
  //const double        intsize = 1.0 / P4EST_ROOT_LEN;
  //p4est_qcoord_t      half_length = P4EST_QUADRANT_LEN (quadrant->level) / 2;
  
  /* 
   * get coordinates at cell center
   */
/*   p4est_qcoord_to_vertex (connectivity, tree, */
/* 			  quadrant->x + half_length, */
/* 			  quadrant->y + half_length, */
/* #ifdef P4_TO_P8 */
/* 			  quadrant->z + half_length, */
/* #endif */
/* 			  XYZ); */
  
  if (quadrant->level > 3)
    return 0;

  return 1;
} // dummy_fn

static int UNUSED_FUNCTION
refine_shell_fn (p4est_t * p4est,
		 p4est_topidx_t which_tree,
		 p4est_quadrant_t * quadrant)
{

  UNUSED(p4est);
  UNUSED(which_tree);

  /* logical coordinates */
  //double              xyz[3] = { 0, 0, 0 };

  // physical coordinates
  double              XYZ[3] = { 0, 0, 0 };

  // get geometry handle (link between logical space and physical space
  p4est_geometry_t  *geom = (p4est_geometry_t *) p4est->user_pointer;
    
  // get coordinates at cell center
  quadrant_center_vertex (p4est->connectivity, geom, which_tree, quadrant, XYZ);

  // get radius
  double r = XYZ[0]*XYZ[0] + XYZ[1]*XYZ[1] + XYZ[2]*XYZ[2] ;

  // stop refining
  if (r<0.8 and quadrant->level > 3)
    return 0;

  if (r<0.9 and quadrant->level > 4)
    return 0;

  if (quadrant->level > 5)
    return 0;

  // refine
  return 1;
  
} // refine_shell_fn

static int UNUSED_FUNCTION
refine_disk2d_fn (p4est_t * p4est,
		  p4est_topidx_t which_tree,
		  p4est_quadrant_t * quadrant)
{

  UNUSED(p4est);
  UNUSED(which_tree);

  /* logical coordinates */
  //double              xyz[3] = { 0, 0, 0 };

  // physical coordinates
  double              XYZ[3] = { 0, 0, 0 };

  // get geometry handle (link between logical space and physical space
  p4est_geometry_t  *geom = (p4est_geometry_t *) p4est->user_pointer;
    
  // get coordinates at cell center
  quadrant_center_vertex (p4est->connectivity, geom, which_tree, quadrant, XYZ);

  // get radius
  double r = XYZ[0]*XYZ[0] + XYZ[1]*XYZ[1] + XYZ[2]*XYZ[2] ;

  // stop refining
  if (r<0.4 and quadrant->level > 3)
    return 0;

  if (r<0.5 and quadrant->level > 4)
    return 0;

  if (r>0.8 and quadrant->level > 3)
    return 0;
  
  if (quadrant->level > 5)
    return 0;

  // refine
  return 1;
  
} // refine_disk2d_fn

static void
compute_area_callback(p4est_iter_volume_info_t * info, void *user_data)
{
  UNUSED(user_data);
  
  p4est_t                *p4est = info->p4est;
  p4est_quadrant_t       *quad = info->quad;
  p4est_connectivity_t   *conn =  p4est->connectivity;
  p4est_geometry_t       *geom = NULL; //(p4est_geometry_t *) p4est->user_pointer;
  p4est_topidx_t          which_tree = info->treeid;

  double area = 0.0;
  
  // get the coordinates of the vertex0
  double  XYZ0[3] = { 0, 0, 0 };
  quadrant_vertex_coord(conn, geom, which_tree, quad, 0, XYZ0);
  double  XYZ1[3] = { 0, 0, 0 };
  quadrant_vertex_coord(conn, geom, which_tree, quad, 1, XYZ1);
  double  XYZ2[3] = { 0, 0, 0 };
  quadrant_vertex_coord(conn, geom, which_tree, quad, 2, XYZ2);
  double  XYZ3[3] = { 0, 0, 0 };
  quadrant_vertex_coord(conn, geom, which_tree, quad, 3, XYZ3);

  area = fabs(XYZ0[0]-XYZ1[0]) * fabs(XYZ0[1]-XYZ2[1]);
  
  user_data_t        *data = (user_data_t *) quad->p.user_data;
  data->area = area;

} // compute_area_callback

static void
compute_total_area_callback(p4est_iter_volume_info_t * info, void *user_data) {
  
  p4est_quadrant_t       *quad = info->quad;
  
  // local cell data
  user_data_t        *data = (user_data_t *) quad->p.user_data;

  double *total_area = (double *) user_data;
  *total_area += data->area;

} // end compute_total_area_callback

static void
compute_lid_callback(p4est_iter_volume_info_t * info, void *user_data)
{
  UNUSED(user_data);

  p4est_t                *p4est = info->p4est;
  p4est_quadrant_t       *quad = info->quad;
  sc_array_t             *trees = p4est->trees;
  p4est_tree_t           *tree;

  p4est_topidx_t          which_tree = info->treeid;

  // the local id, which should "be" the morton index (local to current MPI process)
  uint64_t lid = info->quadid;

  tree = p4est_tree_array_index (trees, which_tree);
  lid += tree->quadrants_offset;   /* now the id is relative to the MPI process */
  
  user_data_t        *data = (user_data_t *) quad->p.user_data;
  data->lid = lid;

} // compute_lid_callback

static void
getdata_callback(p4est_iter_volume_info_t * info, void *user_data)
{
  /* we passed the array of values to fill as the user_data in the call
     to p4est_iterate */
  sc_array_t         *area_data = (sc_array_t *) user_data;
  double             *this_data;
  p4est_t            *p4est = info->p4est;
  p4est_quadrant_t   *q = info->quad;
  p4est_topidx_t      which_tree = info->treeid;
  p4est_locidx_t      local_id = info->quadid;  /* this is the index of q *within its tree's numbering*.  We want to convert it its index for all the quadrants on this process, which we do below */
  p4est_tree_t       *tree;
  user_data_t        *data = (user_data_t *) q->p.user_data;
  p4est_locidx_t      arrayoffset;

  tree = p4est_tree_array_index (p4est->trees, which_tree);
  local_id += tree->quadrants_offset;   /* now the id is relative to the MPI process */
  arrayoffset = local_id;

  this_data = (double*) sc_array_index (area_data, arrayoffset);
  this_data[0] = data->area;

} /* getdata_callback */

/*
 * perform a volume iteration, and compute cell area
 */
static void
compute_area(p4est_t *p4est) {

  /* iterate to compute quadrant/cell area */
  p4est_iterate (p4est,                 /* the forest */
		 NULL,                  /* the ghost layer */
		 NULL,                  /* the synchronized ghost data */
		 compute_area_callback, /* volume callback */
		 NULL,                  /* face callback */
#ifdef P4_TO_P8
		 NULL,                  /* there is no callback for the
					   edges between quadrants */
#endif
		 NULL);                 /* there is no callback for the
					   corners between quadrants */
  
} // compute_area

/*
 * perform a volume iteration to initialize global id's.
 */
static void
compute_lid(p4est_t *p4est) {
  
  p4est_iterate (p4est,                 /* the forest */
		 NULL,                  /* the ghost layer */
		 NULL,                  /* the synchronized ghost data */
		 compute_lid_callback,  /* volume callback */
		 NULL,                  /* face callback */
#ifdef P4_TO_P8
		 NULL,                  /* there is no callback for the
					   edges between quadrants */
#endif
		 NULL);                 /* there is no callback for the
					   corners between quadrants */

} // compute_lid

/*
 * perform a volume iteration, and compute cell area
 */
static void
compute_total_area(p4est_t *p4est) {

  double total_area;

  /* iterate to compute quadrant/cell area */
  p4est_iterate (p4est,                 /* the forest */
		 NULL,                  /* the ghost layer */
		 &total_area,           /* the user data */
		 compute_total_area_callback, /* volume callback */
		 NULL,                  /* face callback */
#ifdef P4_TO_P8
		 NULL,                  /* there is no callback for the
					   edges between quadrants */
#endif
		 NULL);                 /* there is no callback for the
					   corners between quadrants */
  
  P4EST_GLOBAL_INFOF("total area is : %f\n",total_area);
  
} // compute_total_area

/* data getter */
double              qdata_get_area (p4est_quadrant_t * q)
{
  user_data_t        *data = (user_data_t *) q->p.user_data;

  return data->area;
}
double              qdata_get_x (p4est_quadrant_t * q)
{
  user_data_t        *data = (user_data_t *) q->p.user_data;

  return data->x;
}
double              qdata_get_y (p4est_quadrant_t * q)
{
  user_data_t        *data = (user_data_t *) q->p.user_data;

  return data->y;
}
double              qdata_get_z (p4est_quadrant_t * q)
{
  user_data_t        *data = (user_data_t *) q->p.user_data;

  return data->z;
}

double              qdata_get_lid (p4est_quadrant_t * q)
{
  user_data_t        *data = (user_data_t *) q->p.user_data;

  return (double) data->lid;
}

void
write_user_data(p4est_t *p4est,
		p4est_geometry_t * geom,
		const char *filename)
{

  sc_array_t         *area_data;    /* array of cell data to write */
  p4est_locidx_t      numquads;

  numquads = p4est->local_num_quadrants;

  /* create a vector with 4 values (area+x+y+z) per local quadrant */
  area_data = sc_array_new_size (sizeof(double), 4*numquads);

  /* Use the iterator to visit every cell and fill in the data array */
  p4est_iterate (p4est, 
		 NULL,   /* we don't need any ghost quadrants for this loop */
                 (void *) area_data,     /* pass in array pointer so that we can fill it */
                 getdata_callback,   /* callback function to get cell data */
                 NULL,
#ifdef P4_TO_P8
                 NULL,
#endif
                 NULL);

  /* the following currently only works when using p4est branch 'vtk' or 'develop' */
#if 0
  /* create VTK output context and set its parameters */
  p4est_vtk_context_t *context = p4est_vtk_context_new (p4est, filename);
  p4est_vtk_context_set_scale (context, 0.95);
  p4est_vtk_context_set_geom  (context, geom);

  p4est_vtk_write_header (context);
  context = p4est_vtk_write_cell_dataf (context, 1, 1,
					1, 
					0, 
					1,  /* 1 scalar */
					0,  /* 0 vector */
					"area", area_data, 
					context);
  SC_CHECK_ABORT (context != NULL,
                  P4EST_STRING "_vtk: Error writing cell data");

  const int retval = p4est_vtk_write_footer (context);
  SC_CHECK_ABORT (!retval, P4EST_STRING "_vtk: Error writing footer");
#endif /* write with vtk file format */

  /* use canoP parallel Hdf5/Xdmf IO */
  {
    IO_Writer          *w;
    int                 write_mesh_info = 1;

    // io_writer constructor
    w = new IO_Writer (p4est, geom, filename, write_mesh_info);

    // open the new file and write our stuff
    w->open (filename);

    w->write_header (0.0);

    w->write_quadrant_attribute ("area", qdata_get_area,
				 H5T_NATIVE_DOUBLE);
    w->write_quadrant_attribute ("x", qdata_get_x,
				 H5T_NATIVE_DOUBLE);
    w->write_quadrant_attribute ("y", qdata_get_y,
				 H5T_NATIVE_DOUBLE);
    w->write_quadrant_attribute ("z", qdata_get_z,
				 H5T_NATIVE_DOUBLE);
    w->write_quadrant_attribute ("lid", qdata_get_lid,
				 H5T_NATIVE_DOUBLE);

    // close the file
    w->write_footer();
    w->close();

    // destroy IO_Writer
    delete w;

  } // end hdf5/xdmf writer

  sc_array_destroy (area_data);

} /* write_user_data */

int
main (int argc, char **argv)
{
  int                 mpiret;
  int                 wrongusage;
  const char         *usage;
  mpi_context_t       mpi_context, *mpi = &mpi_context;
  p4est_t            *p4est;
  p4est_connectivity_t *connectivity;
  p4est_geometry_t   *geom = NULL;
  p4est_refine_t      refine_fn;
  p4est_coarsen_t     coarsen_fn;

  /* initialize MPI and p4est internals */
  mpiret = sc_MPI_Init (&argc, &argv);
  SC_CHECK_MPI (mpiret);
  mpi->mpicomm = sc_MPI_COMM_WORLD;
  mpiret = sc_MPI_Comm_size (mpi->mpicomm, &mpi->mpisize);
  SC_CHECK_MPI (mpiret);
  mpiret = sc_MPI_Comm_rank (mpi->mpicomm, &mpi->mpirank);
  SC_CHECK_MPI (mpiret);

  sc_init (mpi->mpicomm, 1, 1, NULL, SC_LP_DEFAULT);
  p4est_init (NULL, SC_LP_DEFAULT);

  /* process command line arguments */
  usage =
    "Arguments: <connectivity>\n"
    "   connectivity is a filename (inp file format)\n"
    "   you may find connectivity file in CanoP top-level subdir named connectivity\n"
    ;
  wrongusage = 0;
  if (!wrongusage && argc > 2) {
    wrongusage = 1;
  }
  if (wrongusage) {
    P4EST_GLOBAL_LERROR (usage);
    sc_abort_collective ("Usage error");
  }

  /* assign variables based on configuration */
  refine_fn = refine_dummy_fn;
  coarsen_fn = NULL;

  /* create connectivity and forest structures */

  if (argc>2) {

    connectivity = p4est_connectivity_read_inp (argv[1]);

  } else {
    
    //connectivity = p4est_connectivity_new_forward_facing_step_small ();
    //connectivity = p4est_connectivity_new_forward_facing_step ();
    //connectivity = p4est_connectivity_new_backward_facing_step ();
#ifdef USE_3D
    //connectivity = p8est_connectivity_new_sphere ();
    //geom = p8est_geometry_new_sphere (connectivity, 1., 0.191728, 0.039856);
    connectivity = p8est_connectivity_new_shell ();
    geom = p8est_geometry_new_shell (connectivity, 1., 0.7);
    refine_fn = refine_shell_fn;
#else
    connectivity = p4est_connectivity_new_disk2d ();
    geom = p4est_geometry_new_disk2d (connectivity, 0.5, 1.0);
    refine_fn = refine_disk2d_fn;
#endif
    
  }
  
  p4est = p4est_new_ext (mpi->mpicomm, connectivity, 16, 0, 0,
                         sizeof (user_data_t), init_fn, geom);

  /* refinement and coarsening */
  p4est_refine (p4est, 1, refine_fn, init_fn);
  if (coarsen_fn != NULL) {
    p4est_coarsen (p4est, 1, coarsen_fn, init_fn);
  }

  /* balance */
  p4est_balance (p4est, P4EST_CONNECT_FULL, init_fn);

  P4EST_GLOBAL_INFOF("Is p4est grid balanced ? : %d\n",p4est_is_balanced (p4est, P4EST_CONNECT_FULL));

  /* partition */
  p4est_partition (p4est, 0, NULL);

  /* flag all quadrants that touch the outside boundary */
  compute_area(p4est);
  compute_total_area(p4est);

  compute_lid(p4est);
  
  /* save outside boundary data */
  write_user_data(p4est, geom, "test_connectivity_out");
  
  /* destroy the p4est and its connectivity structure */
  p4est_destroy (p4est);
  if (geom != NULL) {
    p4est_geometry_destroy (geom);
  }
  p4est_connectivity_destroy (connectivity);

  /* clean up and exit */
  sc_finalize ();

  mpiret = sc_MPI_Finalize ();
  SC_CHECK_MPI (mpiret);

  return 0;
}
