/*
 * Just make sure, a given lua variable can reference an other 
 * from a different section.
 */

#include <cstdlib>
#include <iostream>
#include "ConfigReader.h"

int
main (int argc, char **argv)
{

  ConfigReader *cfg;

  /* init configuration reader */
  if (argc < 2) {
    fprintf(stderr, "Please provide an iput lua filename as argv[1]\n");
    return EXIT_FAILURE;
  } else {
    cfg = new ConfigReader(argv[1]);
  }

  /* try to read settings1.data1 */
  double data1 = cfg->config_read_double ("settings1.data1", 1.0);
  printf("data1 = %f\n",data1);

  /* try to read a string */
  std::string data_string = cfg->config_read_std_string("settings1.text", "not found");
  std::cout << "settings1.text = " << data_string << std::endl;
    
  /* try to read settings2.data2 */
  double data2 = cfg->config_read_double ("settings2.data2", -1.0);
  printf("data2 = %f\n",data2);

  /* destroy our data structures */
  delete cfg;

  return EXIT_SUCCESS;
}
