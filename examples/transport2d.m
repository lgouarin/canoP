close all;
clear all;

% size of the mesh
nx = 50;
ny = nx;
nt = 50;

% initial condition
f = @(xp, yp) ((xp - 0.5).^2 + (yp - 0.5).^2) < 0.2^2;
fflux = @(u, cc, cn) (u > 0) .* cc + (u <= 0) .* cn;

% space discretisation
x = linspace(0, 1, nx + 1);
y = linspace(0, 1, ny + 1);
dx = x(2) - x(1);
dy = y(2) - y(1);

% time. we supposed u = 1 and dt / dx < 0.5
dt = dx / 2;

% 2d mesh
[X, Y] = meshgrid(x, y);
Xc = (X(2:end, 2:end) + X(1:end - 1, 1:end - 1)) / 2;
Yc = (Y(2:end, 2:end) + Y(1:end - 1, 1:end - 1)) / 2;

% the cells in the mesh. there's a layer of ghost cells on each side
I = [2:nx + 1];
J = [2:ny + 1];

% initialize the variables
c = zeros(nx + 2, ny + 2);
c(I, J) = f(Xc, Yc);

% compute the velocity on each edge
ux = ones(nx, ny + 2);  % velocity per cell (including ghosts)
uy = ones(nx + 2, ny);
ux = (ux(:, 1:end - 1) + ux(:, 2:end)) / 2;
uy = (uy(1:end - 1, :) + uy(2:end, :)) / 2;

% time loop
for i = 1:nt
    % compute bottom, top, right and left fluxes
    FT = fflux(uy(2:end, :), c(I, J), c(I + 1, J));
    FB = fflux(-uy(1:end - 1, :), c(I, J), c(I - 1, J));
    FL = fflux(-ux(:, 1:end - 1), c(I, J), c(I, J - 1));
    FR = fflux(ux(:, 2:end), c(I, J), c(I, J + 1));

    c(I, J) = c(I, J) + dt / dx * ux(:, 2:end) .* (c(I, J) - FR) - ...
                        dt / dx * ux(:, 1:end - 1) .* (c(I, J) - FL) + ...
                        dt / dx * uy(2:end, :) .* (c(I, J) - FT) - ...
                        dt / dx * uy(1:end - 1, :) .* (c(I, J) - FB);

    h = figure(1);
    surf(Xc, Yc, c(I, J));
    shading interp;
    view(0, 90);
    title(sprintf('iteration %d', i));
    print(h, '-dpng', '-r150', sprintf('upwind%04d.png', i));
end
