
#ifndef _BENCHMARK_PROBLEMS_H_
#define _BENCHMARK_PROBLEMS_H_

#ifndef P4_TO_P8
#include <p4est.h>
#else
#include <p8est.h>
#endif

#include <functional>
#include <map>
#include <string>

enum bc_type_t
{
  BC_PERIODIC,
  BC_DIRICHLET_HOM,
  BC_DIRICHLET_ANA,
  BC_DIRICHLET_MON,
};

struct problem_t
{
  const char           *name;
  std::map<std::string, std::string>
                        info;

  p4est_connectivity_t *conn;
  p4est_geometry_t     *geom;
  bc_type_t             bc;

  std::function<double(void*, double[3])>
                        rho_ana;
  std::function<double(void*, double[3])>
                        phi_ana;

  void                 *user_data;
  std::function<void(void*)>
                        del_user_data;
};

int problem_dirichlet_zero(int *pargc, char ***pargv, problem_t *prob);
int problem_dirichlet_nonz(int *pargc, char ***pargv, problem_t *prob);
int problem_periodic(int *pargc, char ***pargv, problem_t *prob);
int problem_sphere(int *pargc, char ***pargv, problem_t *prob);

#endif //_BENCHMARK_PROBLEMS_H_

