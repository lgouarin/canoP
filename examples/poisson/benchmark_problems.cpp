
#ifdef USE_3D
#include <p4est_to_p8est.h>
#endif

#ifndef P4_TO_P8
#include <p4est_connectivity.h>
#include <p4est_geometry.h>
#else
#include <p8est_connectivity.h>
#include <p8est_geometry.h>
#endif

#include "utils.h"

#include <cmath>
#include <cstring>

#include "benchmark_problems.h"

static void
p4est_free (void *p) {
  P4EST_FREE(p);
}

static void
noop (void *p) {
  UNUSED(p);
}

static double
dirichlet_zero_poly_rho (void *user_data, double xyz[3]) {
  double L[3] = {1, 1, 1};
  double res = 0.;

  if (user_data != nullptr) {
    double *data = (double *) user_data;
    for (int i = 0; i < P4EST_DIM; ++i)
      L[i] = data[i];
  }

  for (int i = 0; i < P4EST_DIM; ++i) {
    double tmp = 1. / SC_SQR(L[i]);
    for (int j = 0; j < P4EST_DIM; ++j) {
      if (j != i) {
        double xn = xyz[j] / L[j];
        tmp *= xn * (1. - xn);
      }
    }
    res += tmp;
  }

  return 2. * res;
} // dirichlet_zero_poly_rho

static double
dirichlet_zero_poly_phi (void *user_data, double xyz[3]) {
  double L[3] = {1, 1, 1};
  double res = 1.;

  if (user_data != nullptr) {
    double *data = (double *) user_data;
    for (int i = 0; i < P4EST_DIM; ++i)
      L[i] = data[i];
  }

  for (int i = 0; i < P4EST_DIM; ++i) {
    double xn = xyz[i] / L[i];
    res *= xn * (1. - xn);
  }

  return res;
} // dirichlet_zero_poly_phi

static double
dirichlet_zero_sin_rho (void *user_data, double xyz[3]) {
  double L[3] = {1, 1, 1};
  double res1 = 1., res2 = 0.;

  if (user_data != nullptr) {
    double *data = (double *) user_data;
    for (int i = 0; i < P4EST_DIM; ++i)
      L[i] = data[i];
  }

  for (int i = 0; i < P4EST_DIM; ++i) {
    double xn = xyz[i] / L[i];
    res1 *= std::sin(M_PI * xn);
    res2 += 1. / SC_SQR(L[i]);
  }

  return SC_SQR(M_PI) * res1 * res2;
} // dirichlet_zero_sin_rho

static double
dirichlet_zero_sin_phi (void *user_data, double xyz[3]) {
  double L[3] = {1, 1, 1};
  double res = 1.;

  if (user_data != nullptr) {
    double *data = (double *) user_data;
    for (int i = 0; i < P4EST_DIM; ++i)
      L[i] = data[i];
  }

  for (int i = 0; i < P4EST_DIM; ++i) {
    double xn = xyz[i] / L[i];
    res *= std::sin(M_PI * xn);
  }

  return res;
} // dirichlet_zero_sin_phi

static double
dirichlet_nonz_quadratic_rho (void *user_data, double xyz[3]) {
  UNUSED(user_data);
  UNUSED(xyz);

  return -2. * P4EST_DIM;
} // dirichlet_nonz_quadratic_rho

static double
dirichlet_nonz_quadratic_phi (void *user_data, double xyz[3]) {
  UNUSED(user_data);

  double res = 0.;

  for (int i = 0; i < P4EST_DIM; ++i)
    res += SC_SQR(xyz[i]);

  return res;
} // dirichlet_nonz_quadratic_phi

static double
dirichlet_nonz_linear_rho (void *user_data, double xyz[3]) {
  UNUSED(user_data);
  UNUSED(xyz);

  return 0.;
} // dirichlet_nonz_linear_rho

static double
dirichlet_nonz_linear_phi (void *user_data, double xyz[3]) {
  UNUSED(user_data);

  double res = 0.;

  for (int i = 0; i < P4EST_DIM; ++i)
    res += xyz[i];

  return res;
} // dirichlet_nonz_linear_phi

static double
periodic_rho (void *user_data, double xyz[3]) {
  double L[3] = {1, 1, 1};
  double res1 = 1., res2 = 0.;

  if (user_data != nullptr) {
    double *data = (double *) user_data;
    for (int i = 0; i < P4EST_DIM; ++i)
      L[i] = data[i];
  }

  for (int i = 0; i < P4EST_DIM; ++i) {
    double xn = xyz[i] / L[i];
    res1 *= std::cos(M_PI * xn);
    res2 += 1. / SC_SQR(L[i]);
  }

  return 4. * SC_SQR(M_PI) * res1 * res2;
} // periodic_rho

static double
periodic_phi (void *user_data, double xyz[3]) {
  double L[3] = {1, 1, 1};
  double res = 1.;

  if (user_data != nullptr) {
    double *data = (double *) user_data;
    for (int i = 0; i < P4EST_DIM; ++i)
      L[i] = data[i];
  }

  for (int i = 0; i < P4EST_DIM; ++i) {
    double xn = xyz[i] / L[i];
    res *= std::cos(2. * M_PI * xn);
  }

  return res;
} // periodic_phi

static double
sphere_rho (void *user_data, double xyz[3]) {
  double center[3] = {0.5, 0.5, 0.5};
  double radius = 0.4;
  double rho_0 = 1.;
  double r = 0.;

  if (user_data != nullptr) {
    double *data = (double *) user_data;
    radius = data[0];
    for (int i = 0; i < P4EST_DIM; ++i)
      center[i] = data[i + 1];
    rho_0 = data[P4EST_DIM + 1];
  }

  for (int i = 0; i < P4EST_DIM; ++i)
    r += SC_SQR(xyz[i] - center[i]);
  r /= SC_SQR(radius);

  return (r <= 1.) ? rho_0 : 0.;
} // sphere_rho

static double
sphere_phi (void *user_data, double xyz[3]) {
  double center[3] = {0.5, 0.5, 0.5};
  double radius = 0.4;
  double rho_0 = 1.;
  double r = 0.;
  double phi0, phi;

  if (user_data != nullptr) {
    double *data = (double *) user_data;
    radius = data[0];
    for (int i = 0; i < P4EST_DIM; ++i)
      center[i] = data[i + 1];
    rho_0 = data[P4EST_DIM + 1];
  }

  for (int i = 0; i < P4EST_DIM; ++i)
    r += SC_SQR(xyz[i] - center[i]);
  r = std::sqrt(r);
  r /= radius;

#ifndef P4_TO_P8
  phi0 = 2. * std::log(radius) - 1.;
  phi = (r <= 1.)? SC_SQR(r) : (1. + 2. * std::log(r));
#else
  phi0 = -3.;
  phi = (r <= 1.)? SC_SQR(r) : (3. - 2. / r);
#endif

  return -rho_0 * (phi + phi0) * SC_SQR(radius) / (2. * P4EST_DIM);
} // sphere_phi


#ifndef P4_TO_P8
static p4est_connectivity_t *
connectivity_new_tetris (void)
{
  const p4est_topidx_t num_vertices = 10;
  const p4est_topidx_t num_trees = 4;
  const p4est_topidx_t num_corners = 2;
  const double        vertices[10 * 3] = {
    1, 0, 0,
    2, 0, 0,
    0, 1, 0,
    1, 1, 0,
    2, 1, 0,
    3, 1, 0,
    0, 2, 0,
    1, 2, 0,
    2, 2, 0,
    3, 2, 0,
  };
  const p4est_topidx_t tree_to_vertex[4 * 4] = {
    0, 1, 3, 4,
    2, 3, 6, 7,
    3, 4, 7, 8,
    4, 5, 8, 9,
  };
  const p4est_topidx_t tree_to_tree[4 * 4] = {
    0, 0, 0, 2,
    1, 2, 1, 1,
    1, 3, 0, 2,
    2, 3, 3, 3,
  };
  const int8_t         tree_to_face[4 * 4] = {
    0, 1, 2, 2,
    0, 0, 2, 3,
    1, 0, 3, 3,
    1, 1, 2, 3,
  };
  const p4est_topidx_t tree_to_corner[4 * 4] = {
    -1, -1,  0,  1,
    -1,  0, -1, -1,
     0,  1, -1, -1,
     1, -1, -1, -1,
  };
  const p4est_topidx_t ctt_offset[2 + 1] = {
    0, 3, 6,
  };
  const p4est_topidx_t corner_to_tree[6] = {
    0, 1, 2,
    0, 2, 3,
  };
  const int8_t         corner_to_corner[6] = {
    2, 1, 0,
    3, 1, 0,
  };

  return p4est_connectivity_new_copy (num_vertices, num_trees, num_corners,
                                      vertices, tree_to_vertex,
                                      tree_to_tree, tree_to_face,
                                      tree_to_corner, ctt_offset,
                                      corner_to_tree, corner_to_corner);
}
#else
static p4est_connectivity_t *
connectivity_new_tetris (void)
{
  const p4est_topidx_t num_vertices = 20;
  const p4est_topidx_t num_trees = 4;
  const p4est_topidx_t num_edges = 2;
  const p4est_topidx_t num_corners = 0;
  const double        vertices[20 * 3] = {
    1, 0, 0,
    2, 0, 0,
    0, 1, 0,
    1, 1, 0,
    2, 1, 0,
    3, 1, 0,
    0, 2, 0,
    1, 2, 0,
    2, 2, 0,
    3, 2, 0,
    1, 0, 1,
    2, 0, 1,
    0, 1, 1,
    1, 1, 1,
    2, 1, 1,
    3, 1, 1,
    0, 2, 1,
    1, 2, 1,
    2, 2, 1,
    3, 2, 1,
  };
  const p4est_topidx_t tree_to_vertex[4 * 8] = {
    0, 1, 3, 4, 10, 11, 13, 14,
    2, 3, 6, 7, 12, 13, 16, 17,
    3, 4, 7, 8, 13, 14, 17, 18,
    4, 5, 8, 9, 14, 15, 18, 19,
  };
  const p4est_topidx_t tree_to_tree[4 * 6] = {
    0, 0, 0, 2, 0, 0,
    1, 2, 1, 1, 1, 1,
    1, 3, 0, 2, 2, 2,
    2, 3, 3, 3, 3, 3,
  };
  const int8_t         tree_to_face[4 * 6] = {
    0, 1, 2, 2, 4, 5,
    0, 0, 2, 3, 4, 5,
    1, 0, 3, 3, 4, 5,
    1, 1, 2, 3, 4, 5,
  };
  const p4est_topidx_t tree_to_edge[4 * 12] = {
    -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,  0,  1,
    -1, -1, -1, -1, -1, -1, -1, -1, -1,  0, -1, -1,
    -1, -1, -1, -1, -1, -1, -1, -1,  0,  1, -1, -1,
    -1, -1, -1, -1, -1, -1, -1, -1,  1, -1, -1, -1,
  };
  const p4est_topidx_t ett_offset[2 + 1] = {
    0, 3, 6,
  };
  const p4est_topidx_t edge_to_tree[6] = {
    0, 1, 2,
    0, 2, 3,
  };
  const int8_t         edge_to_edge[6] = {
    10, 9, 8,
    11, 9, 8,
  };
  const p4est_topidx_t ctt_offset[1] = {
    0,
  };

  return p4est_connectivity_new_copy (num_vertices, num_trees, num_edges, num_corners,
                                      vertices, tree_to_vertex,
                                      tree_to_tree, tree_to_face,
                                      tree_to_edge, ett_offset,
                                      edge_to_tree, edge_to_edge,
                                      NULL, ctt_offset, NULL, NULL);
}
#endif


int
problem_dirichlet_zero(int *pargc, char ***pargv, problem_t *prob) {
  int argc = *pargc;
  char **argv = *pargv;

  int sts = 0;
  int ntrees[P4EST_DIM];

  if (!sts && argc < P4EST_DIM + 1)
    sts = 1;
  for (int i = 0; i < P4EST_DIM; ++i) {
    if (!sts && sscanf(argv[i + 1], "%d", &ntrees[i]) != 1)
      sts = 1;
  }

  if (strcmp(argv[0], "poly") == 0) {
    prob->rho_ana = dirichlet_zero_poly_rho;
    prob->phi_ana = dirichlet_zero_poly_phi;
  }
  else if (strcmp(argv[0], "sin") == 0) {
    prob->rho_ana = dirichlet_zero_sin_rho;
    prob->phi_ana = dirichlet_zero_sin_phi;
  }
  else
    sts = 1;

  if (sts)
    return sts;

  prob->name = "dirichlet_zero";
  prob->info.emplace("variant", argv[0]);
  prob->info.emplace("sizes", "[" + std::to_string(ntrees[0])
                           + ", " + std::to_string(ntrees[1])
#ifdef P4_TO_P8
                           + ", " + std::to_string(ntrees[2])
#endif
                           + "]");

  *pargc -= P4EST_DIM + 1;
  *pargv += P4EST_DIM + 1;

  prob->user_data = P4EST_ALLOC(double, P4EST_DIM);
  prob->del_user_data = p4est_free;

  double *data = (double *) prob->user_data;
  for (int i = 0; i < P4EST_DIM; ++i)
    data[i] = (double) ntrees[i];

#ifndef P4_TO_P8
  prob->conn = p4est_connectivity_new_brick (ntrees[0], ntrees[1], 0, 0);
#else
  prob->conn = p8est_connectivity_new_brick (ntrees[0], ntrees[1], ntrees[2], 0, 0, 0);
#endif
  prob->geom = nullptr;
  prob->bc = BC_DIRICHLET_HOM;

  return 0;
}

int
problem_dirichlet_nonz(int *pargc, char ***pargv, problem_t *prob) {
  int argc = *pargc;
  char **argv = *pargv;

  int sts = 0;

  if (!sts && argc < 2)
    sts = 1;

  if (strcmp(argv[0], "quadratic") == 0) {
    prob->rho_ana = dirichlet_nonz_quadratic_rho;
    prob->phi_ana = dirichlet_nonz_quadratic_phi;
  }
  else if (strcmp(argv[0], "linear") == 0) {
    prob->rho_ana = dirichlet_nonz_linear_rho;
    prob->phi_ana = dirichlet_nonz_linear_phi;
  }
  else
    sts = 1;

  if (sts)
    return sts;

  if (strcmp(argv[1], "unit") == 0) {
#ifndef P4_TO_P8
    prob->conn = p4est_connectivity_new_unitsquare ();
#else
    prob->conn = p8est_connectivity_new_unitcube ();
#endif
  }
  else if (strcmp(argv[1], "brick") == 0) {
#ifndef P4_TO_P8
    prob->conn = p4est_connectivity_new_brick (2, 3, 0, 0);
#else
    prob->conn = p8est_connectivity_new_brick (2, 3, 4, 0, 0, 0);
#endif
  }
  else if (strcmp(argv[1], "tetris") == 0) {
    prob->conn = connectivity_new_tetris ();
  }
  else
    sts = 1;

  *pargc -= 2;
  *pargv += 2;

  prob->name = "dirichlet_nonz";
  prob->info.emplace("variant", argv[0]);
  prob->info.emplace("connectivity", argv[1]);

  prob->user_data = nullptr;
  prob->del_user_data = noop;

  prob->geom = nullptr;
  prob->bc = BC_DIRICHLET_ANA;

  return 0;
}

int
problem_periodic(int *pargc, char ***pargv, problem_t *prob) {
  int argc = *pargc;
  char **argv = *pargv;

  int sts = 0;
  int ntrees[P4EST_DIM];

  if (!sts && argc < P4EST_DIM)
    sts = 1;
  for (int i = 0; i < P4EST_DIM; ++i) {
    if (!sts && sscanf(argv[i], "%d", &ntrees[i]) != 1)
      sts = 1;
  }

  if (sts)
    return sts;

  *pargc -= P4EST_DIM;
  *pargv += P4EST_DIM;

  prob->name = "periodic";
  prob->info.emplace("sizes", "[" + std::to_string(ntrees[0])
                           + ", " + std::to_string(ntrees[1])
#ifdef P4_TO_P8
                           + ", " + std::to_string(ntrees[2])
#endif
                           + "]");

  prob->rho_ana = periodic_rho;
  prob->phi_ana = periodic_phi;

  prob->user_data = P4EST_ALLOC(double, P4EST_DIM);
  prob->del_user_data = p4est_free;

  double *data = (double *) prob->user_data;
  for (int i = 0; i < P4EST_DIM; ++i)
    data[i] = (double) ntrees[i];

#ifndef P4_TO_P8
  prob->conn = p4est_connectivity_new_brick (ntrees[0], ntrees[1], 1, 1);
#else
  prob->conn = p8est_connectivity_new_brick (ntrees[0], ntrees[1], ntrees[2], 1, 1, 1);
#endif
  prob->geom = nullptr;
  prob->bc = BC_PERIODIC;

  return 0;
}

int
problem_sphere(int *pargc, char ***pargv, problem_t *prob) {
  int argc = *pargc;
  char **argv = *pargv;

  int sts = 0;
  int ntrees;
  double radius;
  double center[P4EST_DIM];
  double rho0;

  if (!sts && argc < P4EST_DIM + 3)
    sts = 1;
  if (!sts && sscanf(argv[0], "%d", &ntrees) != 1)
    sts = 1;
  if (!sts && sscanf(argv[1], "%lg", &radius) != 1)
    sts = 1;
  for (int i = 0; i < P4EST_DIM; ++i) {
    if (!sts && sscanf(argv[i + 2], "%lg", &center[i]) != 1)
      sts = 1;
  }
  if (!sts && sscanf(argv[P4EST_DIM + 2], "%lg", &rho0) != 1)
    sts = 1;

  if (sts)
    return sts;

  *pargc -= P4EST_DIM + 3;
  *pargv += P4EST_DIM + 3;

  prob->name = "sphere";
  prob->info.emplace("size", std::to_string(ntrees));
  prob->info.emplace("radius", std::to_string(radius));
  prob->info.emplace("center", "[" + std::to_string(center[0])
                            + ", " + std::to_string(center[1])
#ifdef P4_TO_P8
                            + ", " + std::to_string(center[2])
#endif
                            + "]");
  prob->info.emplace("rho0", std::to_string(rho0));

  prob->rho_ana = sphere_rho;
  prob->phi_ana = sphere_phi;

  prob->user_data = P4EST_ALLOC(double, P4EST_DIM + 2);
  prob->del_user_data = p4est_free;

  double *data = (double *) prob->user_data;
  data[0] = radius;
  for (int i = 0; i < P4EST_DIM; ++i)
    data[i + 1] = center[i];
  data[P4EST_DIM + 1] = rho0;

#ifndef P4_TO_P8
  prob->conn = p4est_connectivity_new_brick (ntrees, ntrees, 0, 0);
#else
  prob->conn = p8est_connectivity_new_brick (ntrees, ntrees, ntrees, 0, 0, 0);
#endif
  prob->geom = nullptr;
  prob->bc = BC_DIRICHLET_MON;

  return 0;
}

