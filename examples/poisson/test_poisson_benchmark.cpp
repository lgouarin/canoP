
#ifdef USE_3D
#include <p4est_to_p8est.h>
#endif

#ifndef P4_TO_P8
#include <p4est_bits.h>
#include <p4est_extended.h>
#include <p4est_iterate.h>
#include <p4est_ghost.h>
#include <p4est_lnodes.h>
#include <p4est_vtk.h>
#else
#include <p8est_bits.h>
#include <p8est_extended.h>
#include <p8est_iterate.h>
#include <p8est_ghost.h>
#include <p8est_lnodes.h>
#include <p8est_vtk.h>
#endif

#include "quadrant_utils.h"
#include "connectivity.h"
#include "geometry.h"
#include "IndicatorFactory.h"
#include "IO_Writer.h"
#include "Lnode_Utils.h"

#include <cmath>
#include <cstdlib>
#include <cstring>
#include <fstream>
#include <map>
#include <sstream>
#include <string>

#include "benchmark_problems.h"

#include "PoissonFESolver.h"

static int level_min = 5;
static int level_max = 5;
static double eps_refine = 0.01;

struct user_data_t
{
  double              rho; // density   - cell averaged
  double              phi; // potential - cell centered
  double              phi_ana; // analytical potential - cell centered
  double              grad_phi[P4EST_DIM]; // the gradient of the potential
};

struct mpi_context_t
{
  sc_MPI_Comm         mpicomm;
  int                 mpisize;
  int                 mpirank;
};

struct forest_data_t
{
  p4est_ghost_t        *ghost;
  p4est_mesh_t         *mesh;

  problem_t            *prob;
};

static void
init_fn (p4est_t * p4est,
         p4est_topidx_t which_tree,
         p4est_quadrant_t * quadrant)
{
  user_data_t        *data = (user_data_t *) quadrant->p.user_data;
  forest_data_t      *fdata = (forest_data_t *) p4est->user_pointer;
  problem_t          *prob = fdata->prob;

  // get the coordinates of the center of the quad
  double  XYZ[3] = { 0, 0, 0 };
  p4est_qcoord_t      half_length = P4EST_QUADRANT_LEN (quadrant->level) / 2;

  // get the physical coordinates of the center of the quad
  // assume geometry is regular cartesian
  p4est_qcoord_to_vertex (p4est->connectivity, which_tree,
                          quadrant->x + half_length,
                          quadrant->y + half_length,
#ifdef P4_TO_P8
                          quadrant->z + half_length,
#endif
                          XYZ);

  data->rho = prob->rho_ana (prob->user_data, XYZ);
  data->phi_ana = prob->phi_ana (prob->user_data, XYZ);
} // init_fn

static int
refine_fn (p4est_t * p4est,
		   p4est_topidx_t which_tree,
		   p4est_quadrant_t * quadrant)
{
  double xyz_c[3] = {0, 0, 0}, xyz_f[3] = {0, 0, 0};
  p4est_qcoord_t half_length = P4EST_QUADRANT_LEN (quadrant->level) / 2;
  p4est_qcoord_t dx, dy;
#ifdef P4_TO_P8
  p4est_qcoord_t dz;
#endif
  double rho_c, rho_f, grad;

  forest_data_t *fdata = (forest_data_t *) p4est->user_pointer;
  problem_t     *prob = fdata->prob;

  if (quadrant->level < level_min)
    return 1;
  if (quadrant->level >= level_max)
    return 0;

  // Compute center coordinates
  p4est_qcoord_to_vertex (p4est->connectivity, which_tree,
                          quadrant->x + half_length,
                          quadrant->y + half_length,
#ifdef P4_TO_P8
                          quadrant->z + half_length,
#endif
                          xyz_c);
  rho_c = prob->rho_ana(prob->user_data, xyz_c);

  // Estimate analytical gradient
  grad = 0;
  for (int idir = 0; idir < P4EST_DIM; ++idir) {
    for (int isgn = 0; isgn < 2; ++isgn) {
      dx = (idir == 0) ? (2 * isgn * half_length) : half_length;
      dy = (idir == 1) ? (2 * isgn * half_length) : half_length;
#ifdef P4_TO_P8
      dz = (idir == 2) ? (2 * isgn * half_length) : half_length;
#endif
      p4est_qcoord_to_vertex (p4est->connectivity, which_tree,
                              quadrant->x + dx,
                              quadrant->y + dy,
#ifdef P4_TO_P8
                              quadrant->z + dz,
#endif
                              xyz_f);
      rho_f = prob->rho_ana(prob->user_data, xyz_f);

      grad = std::fmax(grad, indicator_scalar_gradient (rho_c, rho_f));
    }
  }

  if (grad > eps_refine)
    return 1;

  return 0;

} // refine_fn


static double
get_quadrant_scale(p4est_t * p4est,
                   p4est_topidx_t which_tree,
                   p4est_quadrant_t * quad)
{

  UNUSED(p4est);
  UNUSED(which_tree);

  /* assume here cartesian geometry */
  double dx = 1.0*P4EST_QUADRANT_LEN (quad->level) / P4EST_ROOT_LEN;

  return dx;
} /* get_quadrant_scale */

static double
get_quadrant_volume(p4est_t * p4est,
                    p4est_topidx_t which_tree,
                    p4est_quadrant_t * quad)
{

  UNUSED(p4est);
  UNUSED(which_tree);

  /* assume here cartesian geometry */
  double dx = 1.0*P4EST_QUADRANT_LEN (quad->level) / P4EST_ROOT_LEN;

#ifndef P4_TO_P8
  return dx*dx;
#else
  return dx*dx*dx;
#endif
} /* get_quadrant_volume */


/* data getters */
double              qdata_get_rho (p4est_quadrant_t * q)
{
  user_data_t        *data = (user_data_t *) q->p.user_data;

  return data->rho;
}

double
qdata_get_phi (p4est_quadrant_t *q)
{
  user_data_t        *data = (user_data_t *) q->p.user_data;
  return data->phi;
}

double
qdata_get_phi_ana (p4est_quadrant_t *q)
{
  user_data_t        *data = (user_data_t *) q->p.user_data;
  return data->phi_ana;
}

void
qdata_get_grad_phi (p4est_quadrant_t *q, size_t dim, double *v)
{
  user_data_t        *data = (user_data_t *) q->p.user_data;

  std::memcpy(v, data->grad_phi, P4EST_DIM * sizeof(double));
#ifndef P4_TO_P8
  if(dim == 3)
    v[2] = 0;
#endif
}

/* data setters */
void
qdata_set_phi (p4est_quadrant_t *q, double val)
{
  user_data_t      *data = (user_data_t *) q->p.user_data;
  data->phi = val;
}

void
qdata_set_grad_phi (p4est_quadrant_t *q, double *val)
{
  user_data_t      *data = (user_data_t *) q->p.user_data;
  std::memcpy(data->grad_phi, val, P4EST_DIM * sizeof(double));
}

struct error_data_t {
  std::function<double(p4est_quadrant_t *)> quad_getter1;
  std::function<double(p4est_quadrant_t *)> quad_getter2;
  std::function<double(p4est_t *, p4est_topidx_t, p4est_quadrant_t *)> quad_volume;

  double L1;
  double L2;

  double L1c;
  double L2c;
};

void
errors_callback (p4est_iter_volume_info_t *info, void *user_data)
{
  error_data_t     *err = (error_data_t *) user_data;
  p4est_quadrant_t *quad = info->quad;

  double delta, vol;
  double delta_L1, delta_L2;
  double temp_L1, temp_L2;

  delta = std::abs(err->quad_getter1(quad) - err->quad_getter2(quad));
  vol = err->quad_volume(info->p4est, info->treeid, quad);

  delta_L1 = delta * vol;
  delta_L2 = SC_SQR(delta) * vol;

  // Kahan compensated sum
  delta_L1 -= err->L1c;
  delta_L2 -= err->L2c;

  temp_L1 = err->L1 + delta_L1;
  temp_L2 = err->L2 + delta_L2;

  err->L1c = (temp_L1 - err->L1) - delta_L1;
  err->L2c = (temp_L2 - err->L2) - delta_L2;

  err->L1 = temp_L1;
  err->L2 = temp_L2;
} // errors_callback

void
compute_errors (p4est_t *p4est,
                std::function<double(p4est_quadrant_t *)> quad_getter1,
                std::function<double(p4est_quadrant_t *)> quad_getter2,
                std::function<double(p4est_t *, p4est_topidx_t, p4est_quadrant_t *)> quad_volume,
                double *error_L1,
                double *error_L2)
{
  error_data_t edata = {
    quad_getter1, quad_getter2, quad_volume,
    0., 0., 0., 0.
  };

  int mpiret;
  double errs_loc[2], errs[2];

  p4est_iterate (p4est, nullptr, (void *) &edata,
      errors_callback, // volume
      nullptr,         // face
#ifdef P4_TO_P8
      nullptr,         // edge
#endif
      nullptr          // corner
      );

  errs_loc[0] = edata.L1;
  errs_loc[1] = edata.L2;

  mpiret = MPI_Allreduce(errs_loc, errs, 2, MPI_DOUBLE, MPI_SUM, p4est->mpicomm);
  SC_CHECK_MPI(mpiret);

  if (error_L1 != nullptr)
    *error_L1 = errs[0];
  if (error_L2 != nullptr)
    *error_L2 = std::sqrt(errs[1]);
} // compute_errors


void
write_user_data(p4est_t *p4est, p4est_geometry_t * geom, p4est_lnodes_t *lnodes,
    const char *filename, double *rho_lnodes, double *phi_lnodes)
{
  IO_Writer          *w;
  int                 write_mesh_info = 1;

  // io_writer constructor
  w = new IO_Writer (p4est, geom, filename, write_mesh_info);

  // open the new file and write our stuff
  w->open (filename);

  w->write_header (0.0);

  w->write_quadrant_attribute ("rho", qdata_get_rho,
                               H5T_NATIVE_DOUBLE);

  w->write_quadrant_attribute ("phi", qdata_get_phi,
                               H5T_NATIVE_DOUBLE);

  w->write_quadrant_attribute ("phi_ana", qdata_get_phi_ana,
                               H5T_NATIVE_DOUBLE);

  w->write_quadrant_attribute ("grad_phi", 3, qdata_get_grad_phi,
                               H5T_NATIVE_DOUBLE);

  w->write_lnode_attribute ("rho_n", lnodes, rho_lnodes,
                            H5T_NATIVE_DOUBLE);

  w->write_lnode_attribute ("phi_n", lnodes, phi_lnodes,
                            H5T_NATIVE_DOUBLE);

  // close the file
  w->write_footer();
  w->close();

  // destroy IO_Writer
  delete w;

} /* write_user_data */

static void
write_benchmark_data(p4est_t *p4est, const char *fname,
    problem_t *prob, int level_min, int level_max, double eps_refine,
    int imax, double tol, int converged, poisson_info_t *pinfo,
    double err_L1, double err_L2, double err_L2_interp, double err_L2_nodes)
{
  if (p4est->mpirank != 0)
    return;

  std::ofstream f(fname, std::ios::app);

  if (!f.is_open())
    SC_ABORT("Unable to open statistics file");

  f << std::showpoint;
  f << "---" << std::endl;
  f << "problem: " << prob->name << std::endl;
  for (auto it = prob->info.begin(); it != prob->info.end(); ++it)
    f << it->first << ": " << it->second << std::endl;
  f << "levelmin: " << level_min << std::endl;
  f << "levelmax: " << level_max << std::endl;
  f << "eps_refine: " << eps_refine << std::endl;
  f << "imax: " << imax << std::endl;
  f << "tol: " << tol << std::endl;
  f << "converged: " << (converged? "true" : "false") << std::endl;
  f << "iterations: " << pinfo->iterations << std::endl;
  f.precision(10);
  f << std::scientific;
  f << "residual: " << pinfo->residual << std::endl;
  f << "errorL1: " << err_L1 << std::endl;
  f << "errorL2: " << err_L2 << std::endl;
  f << "interp_errorL2: " << err_L2_interp << std::endl;
  f << "nodes_errorL2: " << err_L2_nodes << std::endl;

} /* write_benchmark_data */

template <typename T>
static T *
lnodes_allocate_vector (p4est_lnodes_t * lnodes)
{
  return P4EST_ALLOC (T, lnodes->num_local_nodes);
} /* lnodes_allocate_vector */

int
main (int argc, char **argv)
{
  int                 mpiret;
  int                 wrongusage;
  const char         *usage;
  const char         *pbm_name;
  int                 log_verbosity;
  mpi_context_t       mpi_context, *mpi = &mpi_context;
  p4est_t            *p4est;
  p4est_connectivity_t *connectivity;
  p4est_mesh_t       *mesh;
  p4est_geometry_t   *geom;
  forest_data_t       fdata;
  problem_t           prob;

  int8_t             *bc;
  double             *rho_nodes, *phi_fe;
  p4est_ghost_t      *ghost;
  p4est_lnodes_t     *lnodes;
  double              rho_avg;
  double              err_L1, err_L2, err_L2_interp, err_L2_nodes;

  int                 imax = 10000;
  double              tol = 1e-6;
  int                 converged;
  poisson_info_t      pinfo;

  std::string         filename;

  std::map<std::string, std::function<int(int*, char***, problem_t*)> >
                      problems = {
    {"dirichlet_zero", problem_dirichlet_zero},
    {"dirichlet_nonz", problem_dirichlet_nonz},
    {"periodic",       problem_periodic},
    {"sphere",         problem_sphere}
  };

  /* initialize MPI and p4est internals */
  mpiret = sc_MPI_Init (&argc, &argv);
  SC_CHECK_MPI (mpiret);
  mpi->mpicomm = sc_MPI_COMM_WORLD;
  mpiret = sc_MPI_Comm_size (mpi->mpicomm, &mpi->mpisize);
  SC_CHECK_MPI (mpiret);
  mpiret = sc_MPI_Comm_rank (mpi->mpicomm, &mpi->mpirank);
  SC_CHECK_MPI (mpiret);

  log_verbosity = SC_LP_ALWAYS;
  sc_init (mpi->mpicomm, 1, 1, nullptr, log_verbosity);
  p4est_init (nullptr, log_verbosity);

  /* process command line arguments */
  usage = "Arguments: <levelmin> <levelmax> <problem> [<problem args>...] [<eps_refine> [<imax> [<tol>]]]\n";
  wrongusage = 0;
  if (!wrongusage && (argc < 4))
    wrongusage = 1;
  if (!wrongusage && sscanf(argv[1], "%d", &level_min) != 1)
    wrongusage = 1;
  if (!wrongusage && sscanf(argv[2], "%d", &level_max) != 1)
    wrongusage = 1;
  if (!wrongusage) {
    pbm_name = argv[3];
    argc -= 4;
    argv += 4;
    auto iprob = problems.find(pbm_name);
    if (iprob == problems.end())
      wrongusage = 1;
    if (!wrongusage && iprob->second(&argc, &argv, &prob))
      wrongusage = 1;
  }
  // XXX: argc and argv have been updated, they point to the last unprocessed arguments
  if (!wrongusage && argc > 0 && sscanf(argv[0], "%lg", &eps_refine) != 1)
    wrongusage = 1;
  if (!wrongusage && argc > 1 && sscanf(argv[1], "%d", &imax) != 1)
    wrongusage = 1;
  if (!wrongusage && argc > 2 && sscanf(argv[2], "%lg", &tol) != 1)
    wrongusage = 1;
  if (!wrongusage && argc > 3)
    wrongusage = 1;

  if (wrongusage) {
    P4EST_GLOBAL_LERROR (usage);
    sc_abort_collective ("Usage error");
  }

  fdata.ghost = nullptr;
  fdata.mesh = nullptr;
  fdata.prob = &prob;

  geom = prob.geom;
  connectivity = prob.conn;

  /* create forest */
  p4est = p4est_new_ext (mpi->mpicomm, connectivity, 15, 0, 0,
                         sizeof (user_data_t), init_fn, &fdata);

  /* refinement */
  p4est_refine (p4est, 1, refine_fn, init_fn);

  /* balance */
  p4est_balance (p4est, P4EST_CONNECT_FULL, init_fn);

  /* partition */
  p4est_partition (p4est, 0, nullptr);

  /* Poisson solver */

  /* Create the ghost layer to learn about parallel neighbors. */
  ghost = p4est_ghost_new (p4est, P4EST_CONNECT_FULL);
  fdata.ghost = ghost;

  /* Create the mesh structure to learn about global boundary faces. */
  mesh = p4est_mesh_new_ext (p4est, ghost, 1, 0, P4EST_CONNECT_FACE);
  fdata.mesh = mesh;

  /* Create a node numbering for continuous linear finite elements. */
  lnodes = p4est_lnodes_new (p4est, ghost, 1);

  /* Flag boundaries if needed */
  if (prob.bc == BC_PERIODIC) {
    bc = nullptr;
  }
  else {
    bc = lnodes_allocate_vector<int8_t> (lnodes);
    lnodes_flag_boundaries (p4est, mesh, lnodes, bc);
  }

  /* Interpolate rho_nodes from quadrants */
  rho_nodes = lnodes_allocate_vector<double> (lnodes);
  interp_quad_to_lnodes (p4est, lnodes, qdata_get_rho, get_quadrant_volume, rho_nodes);

  /* Subtract mean value if needed */
  if (prob.bc == BC_PERIODIC) {
    rho_avg = compute_average (p4est, qdata_get_rho, get_quadrant_volume);

    for (p4est_locidx_t i = 0; i < lnodes->num_local_nodes; ++i)
      rho_nodes[i] -= rho_avg;
  }

  /* Compute interpolation error */
  {
    using namespace std::placeholders;
    auto func = std::bind (prob.rho_ana, prob.user_data, _1);

    double *rho_ana_nodes = lnodes_allocate_vector<double> (lnodes);
    lnodes_eval (p4est, geom, lnodes, func, rho_ana_nodes);

    for (p4est_locidx_t i = 0; i < lnodes->num_local_nodes; ++i)
      rho_ana_nodes[i] -= rho_nodes[i];

    err_L2_interp = nodes_L2_norm (p4est, lnodes, rho_ana_nodes);

    P4EST_FREE (rho_ana_nodes);
  }

  /* Set boundary conditions */
  phi_fe = lnodes_allocate_vector<double> (lnodes);
  memset (phi_fe, 0, lnodes->num_local_nodes * sizeof(double));
  if (prob.bc == BC_DIRICHLET_ANA || prob.bc == BC_DIRICHLET_MON) {
    using namespace std::placeholders;
    std::function<double(double[3])> bc_func;

    if (prob.bc == BC_DIRICHLET_ANA) {
      bc_func = std::bind (prob.phi_ana, prob.user_data, _1);
    }
    else if (prob.bc == BC_DIRICHLET_MON) {
      double total_mass, barycenter[3];

      /* Compute a monopole expansion for the boundary conditions */
      compute_barycenter (p4est, geom, qdata_get_rho, get_quadrant_volume,
                          &total_mass, barycenter);
      P4EST_GLOBAL_LDEBUGF ("monopole expansion: mass = %g, center = (%f, %f, %f)\n",
          total_mass, barycenter[0], barycenter[1], barycenter[2]);
      /* Pass the total mass and barycenter to the monopole function */
      bc_func = std::bind (monopole_expansion, total_mass, barycenter, _1);
    }

    lnodes_set_boundaries (p4est, geom, lnodes, bc, bc_func, phi_fe);
  }

  /* Solve Poisson equation */
  converged = solve_poisson_lnodes (p4est, lnodes, bc, rho_nodes, phi_fe, imax, tol, &pinfo);

  /* Compute node-based error */
  {
    using namespace std::placeholders;
    auto func = std::bind (prob.phi_ana, prob.user_data, _1);

    double *phi_ana_nodes = lnodes_allocate_vector<double> (lnodes);
    lnodes_eval (p4est, geom, lnodes, func, phi_ana_nodes);

    for (p4est_locidx_t i = 0; i < lnodes->num_local_nodes; ++i)
      phi_ana_nodes[i] -= phi_fe[i];

    err_L2_nodes = nodes_L2_norm (p4est, lnodes, phi_ana_nodes);

    P4EST_FREE (phi_ana_nodes);
  }

  /* Interpolate phi and grad_phi back to quadrants */
  interp_lnodes_to_quad (p4est, lnodes, phi_fe, qdata_set_phi);
  gradient_lnodes_to_quad (p4est, lnodes, get_quadrant_scale, phi_fe, qdata_set_grad_phi);

  /* End of Poisson solver */

  /* Compute L1 and L2 errors */
  compute_errors (p4est, qdata_get_phi, qdata_get_phi_ana, get_quadrant_volume, &err_L1, &err_L2);
  P4EST_GLOBAL_PRODUCTIONF ("L1 error = %g, L2 error = %g\n", err_L1, err_L2);

  /* save output data */
  {
    std::stringstream fbuf;
    fbuf << "poisson_benchmark_" << prob.name << "_" << P4EST_DIM << "d";
    filename = fbuf.str();
  }
  write_user_data(p4est, geom, lnodes, filename.c_str(), rho_nodes, phi_fe);

  write_benchmark_data(p4est, "poisson_benchmarks.yml",
      &prob, level_min, level_max, eps_refine,
      imax, tol, converged, &pinfo, err_L1, err_L2, err_L2_interp, err_L2_nodes);

  /* destroy problem user data */
  prob.del_user_data(prob.user_data);

  /* Clean up allocated arrays */
  P4EST_FREE (phi_fe);
  P4EST_FREE (rho_nodes);
  if (bc != nullptr)
    P4EST_FREE (bc);

  /* destroy lnodes data */
  p4est_lnodes_destroy (lnodes);

  /* destroy the mesh structure */
  p4est_mesh_destroy (mesh);

  /* destroy the ghost structure */
  p4est_ghost_destroy (ghost);

  /* destroy the p4est and its connectivity structure */
  p4est_destroy (p4est);
  if (geom != nullptr) {
    p4est_geometry_destroy (geom);
  }
  p4est_connectivity_destroy (connectivity);

  /* clean up and exit */
  sc_finalize ();

  mpiret = sc_MPI_Finalize ();
  SC_CHECK_MPI (mpiret);

  return 0;
}
